/*****************************************************************************\
***																			***
***		Alchemie und Wirkung von Tr�nken									***
***																			***
\*****************************************************************************/

#include "atlantis.h"



int findkraut(char *s)
{
	return findstr (kraeuter, s, MAX_KRAEUTER);	
};

int findtrank(char *s)
{
	return findstr(traenke,s,MAX_TRAENKE);
};

// Initialisieren der Trankzutaten
void SetTraenke()
{
	int trank;

//	-+*+-

	trank = TR_WUNDPUDER;
	traenke_LVL[trank] = 1;
	zutaten[trank][KR_SUMPFKRAUT] = 1;
	zutaten[trank][KR_WIRZBLATT] = 2;

//	-+*+-

	trank = TR_JUNGFERNBLUT;
	traenke_LVL[trank] = 2;
	zutaten[trank][KR_FLACHTUPF] = 2;
	zutaten[trank][KR_TROCKENWURZ] = 3;
	zutaten[trank][KR_GROTTENOLM] = 1;		// und ein Bauer

//	-+*+-

	trank = TR_GEHIRNTOT;
	traenke_LVL[trank] = 3;
	zutaten[trank][KR_SCHNUPFNIES] = 2;
	zutaten[trank][KR_WIRZBLATT] = 1;
	zutaten[trank][KR_GROTTENOLM] = 1;

//	-+*+-

	trank = TR_WISSENSSALBE;
	traenke_LVL[trank] = 3;
	zutaten[trank][KR_TROCKENWURZ] = 2;
	zutaten[trank][KR_SCHNUPFNIES] = 3;
	zutaten[trank][KR_WIRZBLATT] = 1;

//	-+*+-

	trank = TR_NACHTFIEBER;
	traenke_LVL[trank] = 4;
	zutaten[trank][KR_BLUETENGRUMPF] = 3;
	zutaten[trank][KR_GROTTENOLM] = 4;
	zutaten[trank][KR_SCHNUPFNIES] = 1;
};



/*
 *	BENUTZE [Anzahl] <Trankname>
 */

void benutzen()
{
	region *r;
	unit *u;
	strlist *S;

	int trank;

	printf("- benutzen von Traenken\n");
	
	for(r = regions; r; r=r->next)
	{
		for(u = r->units; u; u=u->next)
		{
			for(S = u->orders; S; S = S->next)
			{
				if (igetkeyword (S->s) == K_BENUTZEN)
				{
					char *s;
					int menge;	// Menge die benutzt werden soll

					// Menge holen, wenn 0 dann unklare Menge und s enth�lt Trankname
					s = getstr();
					menge = atoi(s);
					if (menge) s = getstr(); else menge = 1;

					// diverse Tranktests
					trank = findtrank(s);
					if (trank<0)
					{
						sprintf(buf,"Trank '%s' gibt es nicht.",s);
						addalchemie(u,buf);
						return;
					}

					if (u->traenke[trank] == 0)
					{
						sprintf(buf,"Den Trank '%s' besitzen wir nicht.",s);
						addalchemie(u,buf);
						return;
					}

					if (u->traenke[trank] < menge)
					{
						sprintf(buf,"Wir haben statt %d nur %d %s.",menge,u->traenke[trank],traenke[trank]);
						addalchemie(u,buf);
						menge = u->traenke[trank];
					}

					switch (trank)
					{
						case TR_WUNDPUDER:
							sprintf(buf,"%s benutzt Wundpuder",unitid(u));
							addalchemie(u,buf);
							u->lebenspunkte -= menge * 10;
							break;
						case TR_NACHTFIEBER:
							sprintf(buf,"%s - Nachtfieber muss an feindliche Einheit mit GIB uebergeben werden",unitid(u));
							addalchemie(u,buf);
							break;
						case TR_JUNGFERNBLUT:
							sprintf(buf,"%s benutzt %d Jungfernblut",unitid(u),menge);
							addalchemie(u,buf);
							u->traenke[trank] -= menge;
							u->jungfernblut = menge * 100;		// nicht additiv, nur auffrischen
							break;
						case TR_WISSENSSALBE:
							sprintf(buf,"%s benutzt %d Wissenssalbe",unitid(u),menge);
							addalchemie(u,buf);
							u->traenke[trank] -= menge;
							u->wissenssalbe = menge * 300;		// nicht additiv, nur auffrischen
							break;
						case TR_GEHIRNTOT:
							sprintf(buf,"%s - Gehirntot muss an feindliche Einheiten mit GIB uebergeben werden.",unitid(u));
							addalchemie(u,buf);
							break;
						default:
							printf("\n!! nicht bearbeiteter Trank !! - %s\n",traenke[trank]);
							break;
					}
				}
			}
		}
	}

	return;
}

void trankaktionen()
{
	// hier wirken jetzt diverse Tr�nke, wie Gehirntot, Nachtfieber etc.
	int gehirntot;
	unit *u;
	region *r;
	int i,j;

	for(r=regions; r; r=r->next)
	{
		for(u=r->units; u; u=u->next)
		{
			if (u->gehirntot)	// Verdummung, wenn gelernt wird, gibts das quasi doppelt
			{
				gehirntot = 0;
				for(j=0; j<u->number; j++) gehirntot += 15 + rand() % 30;
				gehirntot = min(gehirntot,u->gehirntot);
				if (u->gehirntot)
				{
					i = 0;	
					for(j=0; j<MAXSKILLS; j++) if (u->skills[i] < u->skills[j]) i = j;
					gehirntot = min(u->skills[i],gehirntot);	// damit nix negatives rauskommt
					u->gehirntot -= gehirntot;
					u->skills[i] -= gehirntot;
					// keine Meldungen, stilles Verdummen
				}
			}
		}
	}
}

int alchemisten(faction * f)	// z�hlt alle Alchemisten der Partei
{
	int n;
	region *r;
	unit *u;

	n = 0;

	for (r = regions; r; r = r->next)
		for (u = r->units; u; u = u->next)
			if (u->skills[SK_ALCHEMIE] && u->faction == f)
				n += u->number;

	return n;
}