#include "atlantis.h"


void writeHeader(char *t)
{
	// �ffnen der �u�eren Tabelle
	fprintf(F, "<html>\n"
		"<head>\n"
		"<link id=\"cssElement\" rel=\"stylesheet\" type=\"text/css\" href=\"../stylesheet.css\">\n"
		"<meta http-equiv=\"expires\" content=\"0\">\n"
		"</head>\n"
		"\n"
		//"<body style=\"background-image:url(../gfx/backdrop2.jpg); background-attachment:fixed; background-repeat:no-repeat; margin: 0px;\">\n"
		"<body>\n"
		"\n"
		"<br><br>&nbsp;<br><br>\n"
		"\n"
		"<table width=\"550\" align=\"center\">\n"
		"  <tr>\n"
		"    <td>\n"
		"      <h2>%s</h2>\n"
		"      <br><br>\n"
		, t);
}

void writeFooter()
{
	// schlie�en der �u�eren Tabelle
	fprintf(F, "</tr></td></table></body></html>");
}

void writeHtml(char *t)
{
	fprintf(F, "%s\n", t);
}

void alleParteien()
{
	faction *f;
	int ra[MAXRACES], p[MAXRACES], t[MAXRACES];
	int i;
	int counter;
	region *r;
	unit *u;
	int wealth;				// gesammtes Einkommen
	int xarm, arm, reich;
	//int silber;				// alles Silber
	char pbuf[200];

	puts("\t- Parteien");

	for(i = 0; i < MAXRACES; i++) { ra[i] = 0; p[i] = 0; t[i] = 0; }

	cfopen("html\\welt\\parteien.html", "w");
	writeHeader("&Uuml;bersicht &uuml;ber alle V&ouml;lker");

	fprintf(F, "<table class=\"tableListeTable\"><tr><td align=\"center\">");
	for(f = factions; f; f = f->next)
	{
		if (f->no == 0) continue;
		if (f->website)
		{
			if (strlen(f->website))
			{
				fprintf(F, "<a href=\"%s\" target=\"totalNeuesFensterWeilIrgendwas\">%s</a><br>", f->website, f->name);

			} else
			{
				fprintf(F, "%s<br>", f->name);
			} 
		}
		ra[f->race]++;
	}
	writeHtml("</table><br><br>");

	for(r = regions; r; r = r->next)
	{
		for(u = r->units; u; u = u->next)
		{
			if (u->faction->no == 0) continue;
			p[u->race] += u->number;
			if (u->race != u->raceTT) t[u->raceTT] += u->number;
		}
	}

	// ausgabe der Kr�uter
	counter = 0;
	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"150\">Rassen</th><th width=\"100\">Anzahl</th><th width=\"100\">Personen</th><th width=\"100\">Echsen</th></tr>");
	for(i = 0; i < MAXRACES-1; i++)
	{
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		if (ra[i] < 5 && i > 0) { ra[i] = 0; p[i] = 0; t[i] = 0; }	// wenn zuwenig Parteien, dann alles ausblenden
		if (t[i] < 50) t[i] = 0;
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td><td align=\"center\">%i</td><td align=\"center\">%i</td></tr>", i == 0 ? "Partei getarnt" : racenames[i][0], ra[i], p[i], t[i]);
		counter++;
		writeHtml(buf);
	}
	writeHtml("<tr class=\"tableListeFootLine\"><td>Echsenmensch als diese Rasse getarnt</div></td></tr></table><br><br>");

	// Fantasya Wealth Report
	writeHtml("<br><br>&nbsp;<br><br>");
	wealth = 0;
	for(f = factions; f; f = f->next) f->einkommen = 0;
	for(r = regions; r; r = r->next) for(u = r->units; u; u = u->next) u->faction->einkommen += u->einkommen;
	for(f = factions; f; f = f->next) wealth += f->einkommen;

	arm = xarm = reich = 0;
	for(f = factions; f; f = f->next)
	{
		int ds = wealth / listlen(factions);
		if (f->einkommen < ds / 2)
		{
			arm++;
			if (f->einkommen < ds / 4) xarm++;
		}
		if (f->einkommen > ds * 2) reich++;
	}

	writeHtml("<h2>Fantasya Wealth Report</h2><br>");
	sprintf(pbuf, "<p>Das gesammte Einkommen aller V&ouml;lker liegt bei %i Silber. Das ergibt ein durchschnittliches Einkommen", wealth);
	writeHtml(pbuf);
	sprintf(pbuf, " von %i Silber pro Volk. %i%% der V&ouml;lker sind arm (davon sind %i%% extrem arm), %i%% sind reich. Der Rest ist wohlhabend</p><br><br>"
				, wealth / listlen(factions)
				, (int)( (double)((double)arm / (double)listlen(factions)) * 100.0f )
				, (int)( (double)((double)xarm / (double)listlen(factions)) * 100.0f )
				, (int)( (double)((double)reich / (double)listlen(factions)) * 100.0f ));
	writeHtml(pbuf);

	// jetzt Einkommen dem Spieler sagen
	for(f = factions; f; f = f->next)
	{
		f->gotit = 0;
		for(r = regions; r; r = r->next)
		{
			for(u = r->units; u; u = u->next)
			{
				if ((u->faction == f) && (!f->gotit))
				{
					sprintf(pbuf, "Das Einkommen Deines Volkes liegt bei %i Silber.", f->einkommen);
					addmessage(u, pbuf);
					f->gotit = 1;
				}
			} // alle Einheiten
		} // alle Regionen
	} // alle Parteien


	writeFooter();

	fclose(F);
}

void alleGegenstaende()
{
	unit *u;
	region *r;
	int items[MAXITEMS];
	int k[MAX_KRAEUTER];
	int t[MAX_TRAENKE];
	int i;
	int parteien = listlen(factions);
	int counter;

	for(i = 0; i < MAXITEMS; i++)		items[i] = 0;
	for(i = 0; i < MAX_KRAEUTER; i++)	k[i] = 0;
	for(i = 0; i < MAX_TRAENKE; i++)	t[i] = 0;

	puts("\t- Items");

	cfopen("html\\welt\\items.html", "w");
	writeHeader("Gegenst&auml;nde");

	// Z�hlung
	for(r = regions; r; r = r->next)
	{
		for(u = r->units; u; u = u->next)
		{
			if (u->faction->no == 0) continue;
			for(i = 0; i < MAXITEMS; i++)		if (u->items[i])	items[i] += u->items[i];
			for(i = 0; i < MAX_KRAEUTER; i++)	if (u->kraeuter[i]) k[i] += u->kraeuter[i];
			for(i = 0; i < MAX_TRAENKE; i++)	if (u->traenke[i])	t[i] += u->traenke[i];
		} // einheiten
	} // regionen

	// ausgabe der Items
	counter = 0;
	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"150\">Gegenstand</th><th width=\"100\">Anzahl</th><th width=\"100\">pro Partei</th></tr>");
	for(i = 0; i < MAXITEMS; i++)
	{
		if (i == I_EINHORN || i >= I_1) continue;	// keine magischen Items
		if (i == I_FLUGDRACHEN) continue;			// keine "Gottdinge" zeigen
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td><td align=\"center\">%i</td></tr>", strings[itemnames[0][i]][0], items[i], items[i] / parteien);
		counter++;
		writeHtml(buf);
	}

	writeHtml("</table><br><br>");

	// ausgabe der Kr�uter
	counter = 0;
	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"150\">Kr&auml;uter</th><th width=\"100\">Anzahl</th><th width=\"100\">pro Partei</th></tr>");
	for(i = 0; i < MAX_KRAEUTER; i++)
	{
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td><td align=\"center\">%i</td></tr>", kraeuter[i], k[i], k[i] / parteien);
		counter++;
		writeHtml(buf);
	}

	writeHtml("</table><br><br>");

	// ausgabe der Tr�nke
	counter = 0;
	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"150\">Tr&auml;nke</th><th width=\"100\">Anzahl</th><th width=\"100\">pro Partei</th></tr>");
	for(i = 0; i < MAX_TRAENKE; i++)
	{
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td><td align=\"center\">%i</td></tr>", traenke[i], t[i], t[i] / parteien);
		counter++;
		writeHtml(buf);
	}

	writeHtml("</table><br><br>");

	writeFooter();

	fclose(F);
}

void alleGebaeude()
{
	int g[MAX_BURGTYPE];
	int t[MAXBUILDINGS];
	int s[MAXSHIPS];
	int i;
	int counter;
	region *r;
	building *b;
	ship *sh;

	puts("\t- Buildings");
	for(i = 0; i < MAX_BURGTYPE; i++)	g[i] = 0;
	for(i = 0; i < MAXBUILDINGS; i++)	t[i] = 0;
	for(i = 0; i < MAXSHIPS; i++)		s[i] = 0;

	// Z�hlung
	for(r = regions; r; r = r->next)
	{
		for(b = r->buildings; b; b = b->next)
		{
			if (r->z < 1) continue;	// nur Oberwelt
			if (b->typ) g[b->typ]++; else t[buildingeffsize(b)]++;
		}
		for(sh = r->ships; sh; sh = sh->next)	s[sh->type]++;
	} // regionen

	//Ausgabe
	cfopen("html\\welt\\buildings.html", "w");

	writeHeader("Burgen, Geb&auml;ude und Schiffe");

	// Burgen
	counter = 0;
	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"200\">Burgen</th><th width=\"150\">Anzahl</th></tr>");
	for(i = 0; i < MAXBUILDINGS; i++)
	{
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", buildingnames[i], t[i]);
		counter++;
		writeHtml(buf);
	}
	writeHtml("</table><br><br>");

	// Gebaeude
	counter = 0;
	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"200\">Geb&auml;ude</th><th width=\"150\">Anzahl</th></tr>");
	for(i = 1; i < MAX_BURGTYPE; i++)
	{
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", BuildingNames[i], g[i]);
		counter++;
		writeHtml(buf);
	}
	writeHtml("</table><br><br>");

	// Schiffe
	counter = 0;
	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"200\">Schiffe</th><th width=\"150\">Anzahl</th></tr>");
	for(i = 0; i < MAXSHIPS; i++)
	{
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", shiptypes[0][i], s[i]);
		counter++;
		writeHtml(buf);
	}
	writeHtml("</table><br><br>");

	writeFooter();

	fclose(F);
}

void alleRegionen()
{
	int rt[MAXTERRAINS_NEW];
	int k[MAX_KRAEUTER];
	int baum, pferd, eisen, stein, bauern;
	region *r;
	int counter;
	int i;

	puts("\t- Regions");
	baum = pferd = eisen = stein = bauern = 0;
	for(i = 0; i < MAXTERRAINS; i++)	rt[i] = 1;
	for(i = 0; i < MAX_KRAEUTER; i++)	k[i] = 0;

	for(r = regions; r; r = r->next)
	{
		if (r->z <= 0) continue;
		if (r->terrain != T_PLAIN)
		{
			rt[r->terrain]++;
		} else 
		{ 
			if (r->terrain == T_PLAIN && r->trees < 600) rt[r->terrain]++; else rt[r->terrain+1]++;
		}
		baum += r->trees;
		pferd += r->horses;
		eisen += r->eisen;
		stein += r->steine;
		bauern += r->peasants;
		for(i = 0; i < MAX_KRAEUTER; i++) k[i] += r->kraut[i];
	}

	cfopen("html\\welt\\regionen.html", "w");

	writeHeader("allgemeine Regions&uuml;bersicht");

	// Regionen
	counter = 0;
	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"200\">Regionstyp</th><th width=\"150\">Anzahl</th></tr>");
	for(i = 0; i < MAXTERRAINS; i++)
	{
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", strings[terrainnames[i]][0], rt[i]);
		counter++;
		writeHtml(buf);
	}
	writeHtml("</table><br><br>");

	// Regionen
	counter = 0;
	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"150\">Kr&auml;uter</th><th width=\"100\">Anzahl</th><th width=\"100\">pro Region</th></tr>");
	for(i = 0; i < MAX_KRAEUTER; i++)
	{
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td><td align=\"center\">%i</td></tr>", kraeuter[i], k[i], k[i] / rt[i+1]);
		counter++;
		writeHtml(buf);
	}
	writeHtml("</table><br><br>");

	// sonstiges
	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"200\">sonstiges</th><th width=\"150\">Anzahl</th></tr>");

	sprintf(buf,"<tr class=\"tableListeHighLine\"><td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", "Bauern", bauern);
	writeHtml(buf);

	sprintf(buf,"<tr class=\"tableListeDarkLine\"><td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", "Pferde", pferd);
	writeHtml(buf);

	sprintf(buf,"<tr class=\"tableListeHighLine\"><td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", "B&auml;ume", baum);
	writeHtml(buf);

	sprintf(buf,"<tr class=\"tableListeDarkLine\"><td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", "Eisen", eisen);
	writeHtml(buf);

	sprintf(buf,"<tr class=\"tableListeHighLine\"><td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", "Steine", stein);
	writeHtml(buf);

	writeHtml("</table><br><br>");

	writeFooter();

	fclose(F);
}

void alleEinheiten()
{
	unit *u;
	region *r;
	int t[MAXSKILLS], h[MAXSKILLS], e[MAXSKILLS];	// Lerntage total / bester Skill / Einheiten total
	int sw[2][MAXRACES], sp[2][MAXRACES], bo[2][MAXRACES], ab[2][MAXRACES], kat[2][MAXRACES];
	int i;
	int counter;
	int partei = listlen(factions);

	puts("\t- Einheiten");
	for(i = 0; i < MAXSKILLS; i++)
	{
		t[i] = 0;
		h[i] = 0;
		e[i] = 0;
	}

	for(i = 0; i < MAXRACES; i++)
	{
		// mit        &    ohne				Pferd
		sw[0][i] = 0;	sw[1][i] = 0;		// Schwertk�mpfer
		sp[0][i] = 0;	sp[1][i] = 0;		// Speerk�mpfer
		bo[0][i] = 0;	bo[1][i] = 0;		// Bogen
		ab[0][i] = 0;	ab[1][i] = 0;		// Armbrust
		kat[0][i]= 0;	kat[1][i]= 0;		// Katapulte
	}

	// Zusammenfassen
	for(r = regions; r; r = r->next)
	{
		if (r->z < 0) continue;
		for(u = r->units; u; u = u->next)
		{
			for(i = 0; i < MAXSKILLS; i++)
			{
				if (u->skills[i])
				{
					t[i] += u->skills[i];
					if (h[i] < effskill(u, i)) h[i] = effskill(u, i);
					e[i]++;
					if ((i == SK_SWORD || i == SK_SPEAR || i == SK_CROSSBOW || i == SK_LONGBOW || i == SK_CATAPULT) && u->skills[i])
					{
						// erstmal unberitten
						switch (i)
						{
							case SK_SWORD:		sw[1][u->race] += min(u->number, u->items[I_SWORD]);
												break;
							case SK_SPEAR:		sp[1][u->race] += min(u->number, u->items[I_SPEAR]);
												break;
							case SK_CROSSBOW:	ab[1][u->race] += min(u->number, u->items[I_CROSSBOW]);
												break;
							case SK_LONGBOW:	bo[1][u->race] += min(u->number, u->items[I_LONGBOW]);
												break;
							case SK_CATAPULT:	kat[1][u->race] += min(u->number, u->items[I_CATAPULT]);
												break;
						}
					} // Krieger
				}
			} // Skills
		} // Einheiten
	} // Regionen

	// Ausgabe

	cfopen("html\\welt\\units.html", "w");
	writeHeader("Einheiten und Talente");

	// Talente
	counter = 0;
	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"150\">Talent</th><th width=\"100\">Lerntage total</th><th width=\"100\">beste Einheit</th><th width=\"100\">Einheiten gesammt</th></tr>");
	for(i = 0; i < MAXSKILLS; i++)
	{
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td><td align=\"center\">%i</td>"
					"<td align=\"center\">%i</td></tr>"
					, skillnames[i]
					, t[i]
					, h[i]
					, e[i] < 10 ? (e[i]+varianz(30,10)) : e[i]);
		counter++;
		writeHtml(buf);
	}
	writeHtml("</table><br><br>");

	// Krieger
	counter = 0;
	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"95\">Rasse</th><th width=\"70\">Schwert</th><th width=\"70\">Speer</th><th width=\"70\">Bogen</th><th width=\"70\">Armbrust</th><th width=\"70\">Katapult</th></tr>");
	for(i = 1; i < MAXRACES-1; i++)
	{
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td><td align=\"center\">%i</td>"
					"<td align=\"center\">%i</td><td align=\"center\">%i</td><td align=\"center\">%i</td></tr>"
					, racenames[i][0]
					, sw[1][i] < 100 ? (sw[1][i]+varianz(100,30)) : sw[1][i]
					, sp[1][i] < 100 ? (sp[1][i]+varianz(100,30)) : sp[1][i]
					, bo[1][i] < 100 ? (bo[1][i]+varianz(100,30)) : bo[1][i]
					, ab[1][i] < 100 ? (ab[1][i]+varianz(100,30)) : ab[1][i]
					, kat[1][i]< 100 ? (kat[1][i]+varianz(100,30)) : kat[1][i]);
		counter++;
		writeHtml(buf);
	}
	writeHtml("<tr class=\"tableListeFootLine\"><td>Angaben in Personen</div></td></tr></table><br><br>");

	writeFooter();

	fclose(F);
}

void SpielStatistik()
{
	unit *u;
	region *r;
	int uanz = 0;

	puts("\t- Spiel");

	for(r = regions; r; r = r->next)
	{
		for(u = r->units; u; u = u->next) uanz++;
	}

	cfopen("html\\welt\\spiel.html", "w");

	writeHeader("Spielstatistik");

	// ----



	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"150\">ah ja ....</th><th width=\"250\">&nbsp;</th></tr>");

	sprintf(buf,"<tr class=\"tableListeHighLine\"><td align=\"center\">%s</td><td align=\"center\">%s</td></tr>", "Host-Version", fanta_version);
	writeHtml(buf);
	sprintf(buf,"<tr class=\"tableListeDarkLine\"><td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", "Spieler", listlen(factions) - 1);
	writeHtml(buf);
	sprintf(buf,"<tr class=\"tableListeHighLine\"><td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", "Welten", (listlen(welten) / 2));
	writeHtml(buf);
	sprintf(buf,"<tr class=\"tableListeDarkLine\"><td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", "Regionen total", listlen(regions));
	writeHtml(buf);
	sprintf(buf,"<tr class=\"tableListeHighLine\"><td align=\"center\">%s</td><td align=\"center\">%i</td></tr>", "Einheiten total", uanz);
	writeHtml(buf);

	writeHtml("</td></tr></table><br><br>");


	// ----

	writeFooter();
	fclose(F);
}

void Unterwelt()
{
	region *r;
	unit *u;
	int t[MAXTYPES];
	int reg[MAXTERRAINS_NEW];
	int i;
	int anz = 1;		// Anzahl der Regionen
	int gold = 0;
	int counter = 0;

	for(i = 0; i < MAXTERRAINS_NEW; i++)	reg[i] = 0;
	for(i = 0; i < MAXTYPES; i++)			t[i] = 0;

	for(r = regions; r; r = r->next)
	{
		if (r->z >= 0) continue;
		reg[r->terrain]++;
		gold += r->gold;
		if (r->terrain != T_LAVASTROM) anz++;
		for(u = r->units; u; u = u->next)
		{
			t[u->type]++;
		}
	}

	puts("\t- Unterwelt");

	cfopen("html\\welt\\unterwelt.html", "w");

	writeHeader("Unterwelt");

	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"150\">Monster</th><th width=\"100\">Anzahl</th><th width=\"100\">pro Region</th></tr>");

	for(i = 1; i < MAXTYPES; i++)
	{
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td><td align=\"center\">%4.2f</td>"
					, strings[typenames[0][i]][0]
					, t[i]
					, (double) t[i] / (double) anz);
		counter++;
		writeHtml(buf);
	}
	writeHtml("<tr><td colspan=\"6\"><div align=\"right\"><small>Angaben in Einheiten</small></div></td></tr></table><br><br>");


	writeHtml("<table class=\"tableListeTable\"><tr class=\"tableListeHeadLine\">");
	writeHtml("<th width=\"150\">Regionen</th><th width=\"100\">Anzahl</th></tr>");

	for(i = T_LAVASTROM; i < MAXTERRAINS_NEW - 1; i++)	// Eis ausblenden & bei Lavastrom anfangen, Rest gibt es nicht in der Unterwelt
	{
		if (counter % 2) writeHtml("<tr class=\"tableListeDarkLine\">"); else writeHtml("<tr class=\"tableListeHighLine\">");
		sprintf(buf,"<td align=\"center\">%s</td><td align=\"center\">%i</td>"
					, strings[terrainnames[i]][0]
					, reg[i]);
		counter++;
		writeHtml(buf);
	}
	writeHtml("</table><br><br>");
	
	writeFooter();
	fclose(F);
}

void statistik()
{
	// alles ohne Monster
	puts("- erzeuge Statistik");
	alleParteien();
	alleGegenstaende();
	alleGebaeude();
	alleRegionen();
	alleEinheiten();
	SpielStatistik();
	Unterwelt();
}