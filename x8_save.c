#include "atlantis.h"

faction *f;	// wegen crpstrlist ausgelagert

/*
 * speichert den aktuellen Spielstand im neuen Format, das ganze funzt wie folgt:
 *
 * Das ganze ist wie der CR in einzelne Bl�cke aufgeteilt, ein Block beginnt mit
 * "BLOCK" (was sonst?) gefolgt vom Typ des Blockes (Partei,Regionen,etc)
 * e.g.: "BLOCK" "PARTEI"
 *
 * anschlie�en folgen Block interne Dinge, die werden dort aufgez�hlt und erkl�rt
 * wichtig dabei ist, das leere Items etc. _nicht_ mit aufgef�hrt werden!
 * (also ganz like CR)
 */

void WriteBlock(char *s)	// erzeugt einen neuen Block-Header
{
	ws("BLOCK");
	wspace();
	ws(s);
	wnl();
}

void WriteTag(char *s)		// beginnt einen neuen Tag
{
	wspace(); wspace();		// Tag's werden etwas einger�ckt
	ws(s);
	wspace();
}

void WriteSubBlock(char *s)	// erzeugt einen neuen Block-Header
{
	wspace(); wspace();		// Einr�ckung zu besseren �bersicht
	WriteBlock(s);
}

void WriteSubTag(char *s)		// beginnt einen neuen Tag
{
	wspace(); wspace();		// Einr�ckung zur besseren �bersicht
	WriteTag(s);
}

void WriteSub2Block(char *s)	// erzeugt einen neuen Block-Header
{
	wspace(); wspace();		// Einr�ckung zu besseren �bersicht
	WriteSubBlock(s);
}

void WriteSub2Tag(char *s)		// beginnt einen neuen Tag
{
	wspace(); wspace();		// Einr�ckung zur besseren �bersicht
	WriteSubTag(s);
}

void savegame()
{
	char pbuf[20];	// enth�lt die ID im Base36-Format, das Format ist "Partei <ID>", "Einheit <ID>", "Burg <ID>", "Schiff <ID>"
	int i;
	int n = 0;		// Counter f�r Messages
	int gotit = 0;

	faction *hf;
	faction *f;
	rfaction *rf;
	strlist *S;
	unit *u;
	region *r;
	building *b;
	ship *sh;
	welt *w;
	hoehle *h;

	hf = NULL;

	sprintf (buf, "data/%d.dat" , turn);
	if (!cfopen (buf, "wt"))
	{
		printf("Kann DAT-Spielstand nicht �ffnen!\n");
		return;
	}

	printf ("Schreibe %d. DAT-Spielstand ...\n",turn);

	ws ("DAT-Spielstand"); wspace(); wi(1);		// Version 1
	wnl();

	WriteBlock("SPIEL");
	WriteTag("Turn");		wi(turn);					wnl();
	WriteTag("Parteien");	wi(listlen(factions));		wnl();
	WriteTag("Regionen");	wi(listlen(regions));		wnl();
	WriteTag("Fantasya");	ws(fanta_version);			wnl();

	for(w = welten; w; w=w->next)
	{
		WriteSubBlock("WELT");
		WriteSubTag("Welt");	wi(w->ebene);			wnl();
		WriteSubTag("Min");		wi(w->min);				wnl();
		WriteSubTag("Max");		wi(w->max);				wnl();
		WriteSubTag("Mail");	ws(w->mail);			wnl();
		WriteSubTag("Save");	wi(w->sicher);			wnl();
	}

	for(h = hoehlen; h; h=h->next)
	{
		WriteSubBlock("HOEHLE");
		WriteSubTag("Ebene");	wi(h->ebene);			wnl();
		WriteSubTag("Name");	ws(h->name);			wnl();
		WriteSubTag("Nummer");	wi(h->nummer);			wnl();
		WriteSubTag("XKoord");	wi(h->x);				wnl();
		WriteSubTag("YKoord");	wi(h->y);				wnl();
		WriteSubTag("Offen");	wi(h->offen);			wnl();
		WriteSubTag("Beschr");	ws(h->beschr);			wnl();
	}

	for(f = factions; f ; f=f->next)
	{
		WriteBlock("PARTEI");

		sprintf(pbuf,"Partei %s",tobase36(f->no));
		WriteTag("ID");				ws(pbuf);				wnl();

		WriteTag("Nummer");			wi(f->no);				wnl();
		WriteTag("Name");			ws(f->name);			wnl();
		WriteTag("Addresse");		ws(f->addr);			wnl();
		WriteTag("Passwort");		ws(f->passw);			wnl();
		WriteTag("LastOrders");		wi(f->lastorders);		wnl();
		WriteTag("Newbie");			wi(f->newbie);			wnl();
		WriteTag("Value");			wi(value(f));			wnl();
		// WriteTag("Language");		wi(f->language);		wnl();
		WriteTag("Rasse");			wi(f->race);			wnl();
		WriteTag("xorigin");		wi(f->xorigin);			wnl();
		WriteTag("yorigin");		wi(f->yorigin);			wnl();
		WriteTag("Optionen");		wi(f->options);			wnl();
		WriteTag("Alter");			wi(f->alter);			wnl();
		WriteTag("Website");		ws(f->website);			wnl();
		WriteTag("Cheater");		wi(f->cheatenabled);	wnl();

		gotit = 0;
		for (i = 0; i != MAXSPELLS; i++)
        {
			if (f->showdata[i])
			{
				if (!gotit)
				{
					WriteSubBlock("SHOWDATA");
					gotit = 1;
				}
				WriteSubTag(strings[spellnames[i]][0]);
				wi (f->showdata[i]);
				wnl();
			}
        }

		gotit = 0;
		for (rf = f->allies; rf; rf = rf->next)
        {
			if (!gotit)
			{
				WriteSubBlock("ALLIANZEN");
				gotit = 1;
			}
			WriteSubTag("Partei");
			wi (rf->faction->no);
			wnl();
        }

		/*
		 * Messages vom Typ her nie mischen, gibt sonst Probleme beim einlesen
		 */

		// Mistakes
		for(S = f->mistakes; S ; S = S->next)
		{
			WriteSubBlock("MESSAGE");
			WriteSubTag("Typ");
			ws("Fehler");
			wnl();					// welcher Typ
			WriteSubTag("Msg");
			ws(S->s);				// diese Message
			wnl();
			WriteSubTag("Unit");
			if (S->uno) wi(S->uno); else wi(0);
			wnl();
			WriteSubTag("KoordX");	wi(S->regx); wnl();
			WriteSubTag("KoordY");	wi(S->regy); wnl();
			WriteSubTag("KoordZ");	wi(S->regz); wnl();
		}

		// Warnings
		for(S = f->warnings; S ; S = S->next)
		{
			WriteSubBlock("MESSAGE");
			WriteSubTag("Typ");
			ws("Warnungen"); wnl();		// welcher Type
			WriteSubTag("Msg");
			ws(S->s);					// diese Message
			wnl();
			WriteSubTag("Unit");
			if (S->uno) wi(S->uno); else wi(0);
			wnl();
			WriteSubTag("KoordX");	wi(S->regx); wnl();
			WriteSubTag("KoordY");	wi(S->regy); wnl();
			WriteSubTag("KoordZ");	wi(S->regz); wnl();
		}
		// Messages
		for(S = f->messages; S ; S = S->next)
		{
			WriteSubBlock("MESSAGE");
			WriteSubTag("Typ");
			ws("Messages"); wnl();		// welcher Type
			WriteSubTag("Msg");
			ws(S->s);					// diese Message
			wnl();
			WriteSubTag("Unit");
			if (S->uno) wi(S->uno); else wi(0);
			wnl();
			WriteSubTag("KoordX");	wi(S->regx); wnl();
			WriteSubTag("KoordY");	wi(S->regy); wnl();
			WriteSubTag("KoordZ");	wi(S->regz); wnl();
		}
		// Battles
		for(S = f->battles; S ; S = S->next)
		{
			WriteSubBlock("MESSAGE");
			WriteSubTag("Typ");
			ws("Kaempfe"); wnl();		// welcher Type
			WriteSubTag("Msg");
			ws(S->s);					// diese Message
			wnl();
			WriteSubTag("Unit");
			if (S->uno) wi(S->uno); else wi(0);
			wnl();
			WriteSubTag("KoordX");	wi(S->regx); wnl();
			WriteSubTag("KoordY");	wi(S->regy); wnl();
			WriteSubTag("KoordZ");	wi(S->regz); wnl();
		}
		// Events
		for(S = f->events; S ; S = S->next)
		{
			WriteSubBlock("MESSAGE");
			WriteSubTag("Typ");
			ws("Ereignisse"); wnl();	// welcher Type
			WriteSubTag("Msg");
			ws(S->s);					// diese Message
			wnl();
			WriteSubTag("Unit");
			if (S->uno) wi(S->uno); else wi(0);
			wnl();
			WriteSubTag("KoordX");	wi(S->regx); wnl();
			WriteSubTag("KoordY");	wi(S->regy); wnl();
			WriteSubTag("KoordZ");	wi(S->regz); wnl();
		}
		// Income
		for(S = f->income; S ; S = S->next)
		{
			WriteSubBlock("MESSAGE");
			WriteSubTag("Typ");
			ws("Einkommen"); wnl();		// welcher Type
			WriteSubTag("Msg");
			ws(S->s);					// diese Message
			wnl();
			WriteSubTag("Unit");
			if (S->uno) wi(S->uno); else wi(0);
			wnl();
			WriteSubTag("KoordX");	wi(S->regx); wnl();
			WriteSubTag("KoordY");	wi(S->regy); wnl();
			WriteSubTag("KoordZ");	wi(S->regz); wnl();
		}
		// Commerce
		for(S = f->commerce; S ; S = S->next)
		{
			WriteSubBlock("MESSAGE");
			WriteSubTag("Typ");
			ws("Handel"); wnl();		// welcher Type
			WriteSubTag("Msg");
			ws(S->s);					// diese Message
			wnl();
			WriteSubTag("Unit");
			if (S->uno) wi(S->uno); else wi(0);
			wnl();
			WriteSubTag("KoordX");	wi(S->regx); wnl();
			WriteSubTag("KoordY");	wi(S->regy); wnl();
			WriteSubTag("KoordZ");	wi(S->regz); wnl();
		}
		// Produktion
		for(S = f->production; S ; S = S->next)
		{
			WriteSubBlock("MESSAGE");
			WriteSubTag("Typ");
			ws("Produktion"); wnl();	// welcher Type
			WriteSubTag("Msg");
			ws(S->s);					// diese Message
			wnl();
			WriteSubTag("Unit");
			if (S->uno) wi(S->uno); else wi(0);
			wnl();
			WriteSubTag("KoordX");	wi(S->regx); wnl();
			WriteSubTag("KoordY");	wi(S->regy); wnl();
			WriteSubTag("KoordZ");	wi(S->regz); wnl();
		}
		// Movement
		for(S = f->movement; S ; S = S->next)
		{
			WriteSubBlock("MESSAGE");
			WriteSubTag("Typ");
			ws("Bewegungen"); wnl();	// welcher Type
			WriteSubTag("Msg");
			ws(S->s);					// diese Message
			wnl();
			WriteSubTag("Unit");
			if (S->uno) wi(S->uno); else wi(0);
			wnl();
			WriteSubTag("KoordX");	wi(S->regx); wnl();
			WriteSubTag("KoordY");	wi(S->regy); wnl();
			WriteSubTag("KoordZ");	wi(S->regz); wnl();
		}
		// Debug
		for(S = f->debug; S ; S = S->next)
		{
			WriteSubBlock("MESSAGE");
			WriteSubTag("Typ");
			ws("Debug"); wnl();			// welcher Type
			WriteSubTag("Msg");
			ws(S->s);					// diese Message
			wnl();
			WriteSubTag("Unit");
			if (S->uno) wi(S->uno); else wi(0);
			wnl();
			WriteSubTag("KoordX");	wi(S->regx); wnl();
			WriteSubTag("KoordY");	wi(S->regy); wnl();
			WriteSubTag("KoordZ");	wi(S->regz); wnl();
		}
		// Alchemie und Kr�uterkunde
		for(S = f->alchemie; S ; S = S->next)
		{
			WriteSubBlock("MESSAGE");
			WriteSubTag("Typ");
			ws("Alchemie"); wnl();		// welcher Type
			WriteSubTag("Msg");
			ws(S->s);					// diese Message
			wnl();
			WriteSubTag("Unit");
			if (S->uno) wi(S->uno); else wi(0);
			wnl();
			WriteSubTag("KoordX");	wi(S->regx); wnl();
			WriteSubTag("KoordY");	wi(S->regy); wnl();
			WriteSubTag("KoordZ");	wi(S->regz); wnl();
		}
		// Echsennews
		for(S = f->echsennews; S ; S = S->next)
		{
			WriteSubBlock("MESSAGE");
			WriteSubTag("Typ");
			ws("EchsenNews"); wnl();	// welcher Type
			WriteSubTag("Msg");
			ws(S->s);					// diese Message
			wnl();
			WriteSubTag("Unit");
			if (S->uno) wi(S->uno); else wi(0);
			wnl();
			WriteSubTag("KoordX");	wi(S->regx); wnl();
			WriteSubTag("KoordY");	wi(S->regy); wnl();
			WriteSubTag("KoordZ");	wi(S->regz); wnl();
		}

	}	// alle Parteien

	/*
	 * hier beginnen jetzt die Regionen
	 */

	for(r = regions; r ; r=r->next)
	{
		int gotit;

		WriteBlock("REGION");	// ne neue Region
		if (r->name)		{ WriteTag("Name");			ws(r->name);			wnl(); }	// Ozeane haben keine Namen
		WriteTag("XKoord");		wi(r->x);				wnl();
		WriteTag("YKoord");		wi(r->y);				wnl();
		WriteTag("ZKoord");		wi(r->z);				wnl();								// Welt
		if (r->display)		{ WriteTag("Beschreibung");	ws(r->display);			wnl(); }

		WriteTag("Terrain");	wi(r->terrain);			wnl();								// Nummer reicht, ist eh blo� einmal
		WriteTag("Alter");		wi(r->alter);			wnl();
		if (r->trees)		{ WriteTag("Baeume");		wi (r->trees);			wnl(); }
		if (r->horses)		{ WriteTag("Pferde");		wi (r->horses);			wnl(); }
		if (r->peasants)	{ WriteTag("Bauern");		wi (r->peasants);		wnl(); }
		if (r->money)		{ WriteTag("Silber");		wi (r->money);			wnl(); }
		if (r->movement)	{ WriteTag("Movement");		wi(r->movement);		wnl(); }
		if (r->eisen)		{ WriteTag("Eisen");		wi(r->eisen);			wnl(); }
		if (r->steine)		{ WriteTag("Steine");		wi(r->steine);			wnl(); }
		if (r->gold)		{ WriteTag("Gold");			wi(r->gold);			wnl(); }
		if (r->hoehle)		{ WriteTag("Hoehle");		wi(r->hoehle);			wnl(); }
		if (r->sturm)		{ WriteTag("Sturm");		wi(r->sturm);			wnl(); }
		if (r->eis)			{ WriteTag("Eis");			wi(r->eis);				wnl(); }
		WriteTag("mainKraut");	wi(r->mainKraut);		wnl();

		gotit = 0;
		for(i = 0; i < MAXDIRECTIONS; i++)
		{
			if (r->road[i])
			{
				if (!gotit) { gotit = 1; WriteSubBlock("STRASSE"); }
				WriteSubTag(directions[i]);
				wi(r->road[i]);
				wnl();
			}
		}

		if (r->terrain)
		{
			WriteSubBlock("LUXURIES");
			WriteSubTag("Produktion"); ws (strings[itemnames[0][FIRSTLUXURY + r->produced_good]][0]);	wnl();
			for (i = 0; i != MAXLUXURIES; i++)
			{
				if (r->demand[i])
				{
					WriteSubTag(strings[itemnames[0][FIRSTLUXURY + i]][0]);
					wi (r->demand[i]);														// Nachfrage der Bauern
					wnl();
				}
			}
		}

		gotit = 0;
		if (r->terrain)
		{
			for(i = 0; i<MAX_KRAEUTER; i++)
			{
				if (!gotit)
				{
					WriteSubBlock("KRAEUTER");
					gotit = 1;
				}
				if (r->kraut[i])
				{
					WriteSubTag(kraeuter[i]);
					wi(r->kraut[i]);
					wnl();
				}
			}
		}

		gotit = 0;
		for (S = r->comments ; S ; S = S->next)
		{
			if (!gotit)
			{
				gotit = 1;
				WriteSubBlock("KOMMENTARE");
			}
			WriteSubTag("Msg");
			ws(S->s);
			wnl();
		}

		gotit = 0;
		for (S = r->debug ; S ; S = S->next)
		{
			if (!gotit)
			{
				gotit = 1;
				WriteSubBlock("DEBUG");
			}
			WriteSubTag("Msg");
			ws(S->s);
			wnl();
		}

		// Burgen
		for (b = r->buildings; b; b = b->next)
        {
			// ein Wegweiser der verrottet ist, wird nicht mehr gesperichert
			if ((b->typ == BT_WEGWEISER) && (b->wegweiser == 0)) continue;

			WriteSubBlock("BURG");

			sprintf(pbuf,"Burg %s",tobase36(b->no));
			WriteSubTag("ID");				ws(pbuf);			wnl();

			WriteSubTag("Nummer");			wi(b->no);			wnl();
			WriteSubTag("Name");			ws(b->name);		wnl();
			if (b->display)			{ WriteSubTag("Beschreibung");	ws(b->display);	wnl(); }
			WriteSubTag("Size");			wi(b->size);		wnl();
			WriteSubTag("Typ");				wi(b->typ);			wnl();
			WriteSubTag("Funktion");		wi(b->funktion);	wnl();
			WriteSubTag("Wegweiser");		wi(b->wegweiser);	wnl();
        }

		// Schiffe
		for (sh = r->ships; sh; sh = sh->next)
        {
			WriteSubBlock("SCHIFF");

			sprintf(pbuf,"Schiff %s",tobase36(sh->no));
			WriteSubTag("ID");					ws(pbuf);			wnl();

			WriteSubTag("Nummer");			wi(sh->no);			wnl();
			WriteSubTag("Name");			ws(sh->name);		wnl();
			if (sh->display)		{ WriteSubTag("Beschreibung");	ws(sh->display);	wnl(); }
			WriteSubTag("Typ");				wi(sh->type);		wnl();
			WriteSubTag("Groesse");			wi(sh->left);		wnl();
        }

		// Einheiten
		for(u = r->units; u ; u=u->next)
		{
			WriteSubBlock("EINHEIT");

			sprintf(pbuf,"Einheit %s",tobase36(u->no));
			WriteSubTag("ID");					ws(pbuf);			wnl();

			WriteSubTag("Nummer");			wi(u->no);			wnl();
			WriteSubTag("Name");			ws(u->name);		wnl();
			if (u->display)			{ WriteSubTag("Beschreibung");	ws(u->display);		wnl(); }
			WriteSubTag("Personen");		wi(u->number);		wnl();
			if (u->type)			{ WriteSubTag("Typ");			wi(u->type);		wnl(); } // Mensch, Illusion, Monster etc.
			if (u->effect)			{ WriteSubTag("Effekt");		wi(u->effect);		wnl(); }
			if (u->enchanted)		{ WriteSubTag("Enchanted");		wi(u->enchanted);	wnl(); }
			WriteSubTag("Partei");			wi(u->faction->no);	wnl();	// geh�rt zu dieser Partei
			if (u->building)
			{
				WriteSubTag("Burg");		wi(u->building->no); wnl();
			}
			if (u->ship)
			{
				WriteSubTag("Schiff");		wi(u->ship->no);	wnl();
			}
			if (u->owner)			{ WriteSubTag("Owner");			wi(u->owner);			wnl(); } // was ist das ?
			if (u->status)			{ WriteSubTag("Status");		wi(u->status);			wnl(); }
			if (u->guard)			{ WriteSubTag("Guard");			wi(u->guard);			wnl(); }
			if (u->lastorder)		{ WriteSubTag("LastOrder");		ws(u->lastorder);		wnl(); }
			if (u->thisorder2)		{ WriteSubTag("ThisOrder2");	ws(u->thisorder2);		wnl(); }
			if (u->thisorder)		{ WriteSubTag("ThisOrder");		ws(u->thisorder);		wnl(); }
			if (u->prefix)			{ WriteSubTag("Praefix");		ws(u->prefix);			wnl(); }
			if (u->tarnung)			{ WriteSubTag("Tarnung");		wi(u->tarnung);			wnl(); }
			if (u->wissenssalbe)	{ WriteSubTag("Wissenssalbe");	wi(u->wissenssalbe);	wnl(); }
			if (u->jungfernblut)	{ WriteSubTag("Jungfernblut");	wi(u->jungfernblut);	wnl(); }
			if (u->gehirntot)		{ WriteSubTag("Gehirntot");		wi(u->gehirntot);		wnl(); }
			if (u->lebenspunkte)	{ WriteSubTag("Lebenspunkte");	wi(u->lebenspunkte);	wnl(); }
			if (u->einkommen)		{ WriteSubTag("Einkommen");		wi(u->einkommen);		wnl(); }
			WriteSubTag("Kampfzauber");		wi(u->combatspell);	wnl();
			WriteSubTag("Rasse");			wi(u->race);		wnl();
			WriteSubTag("Tarntypus");		wi(u->raceTT);		wnl();
			WriteSubTag("Parteitarnung");	wi(u->tarn_no);		wnl();	// immer speichern

			gotit = 0;
			for(S = u->comments; S ; S=S->next)
			{
				if (!gotit)
				{
					WriteSub2Block("VORLAGE");						// da meist f�r Vorlage verwendet
					gotit = 1;
				}
				WriteSub2Tag("Msg"); ws(S->s); wnl();				// permanente Kommentare ohne eigenen Block
			}

			gotit = 0;
			for (i = 0; i != MAXSKILLS; i++)
            {
				if (u->skills[i])
				{
					if (!gotit)
					{
						gotit = 1;
						WriteSub2Block("SKILLS");
					}
					WriteSub2Tag(skillnames[i]);
					wi(u->skills[i]);
					wnl();
				}
            } // SKILLS

			gotit = 0;
			for (i = 0; i != MAXITEMS; i++)
            {
				if (u->items[i])
				{
					if (!gotit)
					{
						gotit = 1;
						WriteSub2Block("ITEMS");
					}
					WriteSub2Tag(strings[itemnames[0][i]][0]);
					wi(u->items[i]);
					wnl();
				}
            } // Items

			gotit = 0;
			for (i = 0; i != MAXSPELLS; i++)
            {
				if (u->spells[i])
				{
					if (!gotit)
					{
						gotit = 1;
						WriteSub2Block("SPELLS");
					}
					WriteSub2Tag(strings[spellnames[i]][0]);		// da jeder Spruch nur einmal "vorhanden" ist, reicht es in
					wnl();											// zu speichern
				}
            } // Zauberspr�che

			gotit = 0;
			for(i = 0; i<MAX_KRAEUTER; i++)
			{
				if (u->kraeuter[i])
				{
					if (!gotit)
					{
						gotit = 1;
						WriteSub2Block("KRAEUTERITEMS");
					}
					WriteSub2Tag(kraeuter[i]);
					wi(u->kraeuter[i]);
					wnl();
				}
			} // Kr�uter

			gotit = 0;
			for(i=0; i<MAX_TRAENKE; i++)
			{
				if(u->traenke[i])
				{
					if (!gotit)
					{
						gotit = 1;
						WriteSub2Block("TRAENKE");
					}
					WriteSub2Tag(traenke[i]);
					wi(u->traenke[i]);
					wnl();
				}
			} // Tr�nke
		}
	}	// alle Regionen

	fprintf(F,"\"DAT-Ende\"");
	if (fclose(F)) printf("konnte Spielstand nicht schliessen!\n");
}

enum {
	B_FALSE,
	B_SPIEL,
	B_PARTEI,
	B_MESSAGE,
	B_REGION,
	B_LUXURIES,
	B_DEBUG,
	B_EINHEIT,
	B_SKILLS,
	B_ITEMS,
	B_BURG,
	B_SCHIFF,
	B_SPELLS,
	B_KOMMENTARE,
	B_SHOWDATA,
	B_ALLIANZEN,
	B_VORLAGE,
	B_KRAEUTER,
	B_KRAEUTERITEMS,
	B_WELT,
	B_TRAENKE,
	B_HOEHLEN,
	B_STRASSE,
	MAX_BLOCK };

char *blocknames[MAX_BLOCK] =
{
	"FALSE",
	"SPIEL",
	"PARTEI",
	"MESSAGE",
	"REGION",
	"LUXURIES",
	"DEBUG",
	"EINHEIT",
	"SKILLS",
	"ITEMS",
	"BURG",
	"SCHIFF",
	"SPELLS",
	"KOMMENTARE",
	"SHOWDATA",
	"ALLIANZEN",
	"VORLAGE",
	"KRAEUTER",				// w�chst in Region
	"KRAEUTERITEMS",		// besitzt die Einheit
	"WELT",
	"TRAENKE",
	"HOEHLE",
	"STRASSE",
};

enum {
	MT_FEHLER,
	MT_WARNUNGEN,
	MT_MESSAGES,
	MT_KAEMPFE,
	MT_EREIGNISSE,
	MT_EINKOMMEN,
	MT_HANDEL,
	MT_PRODUKTION,
	MT_BEWEGUNGEN,
	MT_DEBUG,
	MT_ALCHEMIE,
	MT_ECHSENNEWS,
	MAX_MESSAGESTYPE };

char *messagenames[MAX_MESSAGESTYPE] =
{
	"Fehler",
	"Warnungen",
	"Messages",
	"Kaempfe",
	"Ereignisse",
	"Einkommen",
	"Handel",
	"Produktion",
	"Bewegungen",
	"Debug",
	"Alchemie",
	"EchsenNews",
};

void crs(char **to)
{
	char pbuf[MAXSTRING];					// sollte reichen

	rs(pbuf);
	*to = cmalloc(strlen(pbuf)+1);
	strcpy(*to,pbuf);
}

void loadgame()
{
	int block=0,oldblock=0;					// d.h. noch kein Block geladen
	int i = 0;
	int regionlength=0;
	int factionlength=0;
	int gotit = 0;
	int gamestream = 0;						// solange '0' wird eingelesen

	faction *tf = NULL;						// aktuelle Strukturen, like (t)his(f)action aso
	unit *tu = NULL;
	region *tr = NULL;
	ship *ts = NULL;
	building *tb = NULL;
	rfaction *ta = NULL;					// ThisAllianz
	strlist *tm = NULL;						// ThisMessage
	welt *tw = NULL;						// ThisWelt
	hoehle *th = NULL;						// ThisHoehle
	unit *lu = NULL;						// LastUnit ... letzte Unit die erzeugt wurde ... lu->kette = tu !!

	rfaction *rf;
	region *r;
	unit *u;

	faction *ff = NULL;						// like FirstFactions
	region * fr = NULL;						// like FirstRegions

	void * hp = NULL;						// Hilfspointer

	char pbuf[MAXSTRING];					// sollte reichen
	char idbuf[30];							// Speicherung der ID und somit zur Fehlersuche

	sprintf(pbuf,"data/%d.dat",turn);
	if (!cfopen(pbuf,"r"))
	{
		printf("Kann DAT-Spielstand #%d nicht oeffnen!\n",turn);
		exit(EXIT_FAILURE);
	}

	highest_unit_no = 0;					// dito

	while(!feof(F))
	{
		if (!gamestream) rs(pbuf);			// einlesen
		oldblock = block;					// alten Block merken
		if (!strcmp(pbuf,"BLOCK"))			// und auf neuen Block testen;
		{
			block = 0;						// es wurde ja ein neuer Block eingelesen
			rs(pbuf);						// Blocktyp einlesen
			for(i = 0; i<MAX_BLOCK ; i++)
			{
				if (!strcmp(pbuf,blocknames[i]))
				{
					block = i; oldblock = 0;
				}
			}

			switch(block)
			{
				case B_PARTEI:
					hp = cmalloc(sizeof(faction));
					memset (hp, 0, sizeof (faction));
					if (ff)
					{
						tf->next = hp;
					} else
					{
						ff = hp;
					}
					tf = hp;
					hp = NULL;
					factionlength--;
					indicator_count_down(factionlength);
					break;
				case B_ALLIANZEN: /* wird jetzt erst unten erledigt
					hp = cmalloc(sizeof(rfaction));
					memset (hp, 0, sizeof (rfaction));
					if (tf->allies)
					{
						ta->next = hp;
					} else
					{
						tf->allies = hp;
					}
					ta = hp;
					hp = NULL; */
					break;
				case B_MESSAGE:
					// erst kontrollieren ob hp frei ist
					if (hp)
					{
						printf("Messageblock ohne Inhalt!\nPartei '%s'\n",tf->name);
						printf("%s", tm->s);
					}
					hp = cmalloc(sizeof(strlist) + MAXSTRING);
					memset (hp, 0, sizeof (strlist) + MAXSTRING);
					// Einsortierung erfolgt sp�ter, da es unterschiedliche Typen gibt
					oldblock = 0;	// da jede Message einen eigenen Block besitzt
					break;
				case B_REGION:
					maxRegions++;	// eines hochz�hlen
					hp = cmalloc(sizeof(region));
					memset (hp, 0, sizeof (region));
					if (fr)
					{
						tr->next = hp;
					} else
					{
						fr = hp;
					}
					tr = hp;
					hp = NULL;
					if (regionlength < 0)
					{
						printf("\n  lade Regionen: ");
						regionlength = 0 - regionlength;
						indicator_reset(regionlength);
						// ole und wech die alten Daten
						factions = ff;
						regions = fr;
					}
					regionlength--;
					indicator_count_down(regionlength);
					oldblock = 0;	// da nicht jede Region noch SubBl�cke besitzt
					break;
				case B_BURG:
					hp = cmalloc(sizeof(building));
					memset (hp, 0, sizeof (building));
					if (tr->buildings)
					{
						tb->next = hp;
					} else
					{
						tr->buildings = hp;
					}
					tb = hp;
					oldblock = 0;
					break;
				case B_SCHIFF:
					hp = cmalloc(sizeof(ship));
					memset (hp, 0, sizeof (ship));
					if (tr->ships)
					{
						ts->next = hp;
					} else
					{
						tr->ships = hp;
					}
					ts = hp;
					break;
				case B_EINHEIT:
					hp = cmalloc(sizeof(unit));
					memset (hp, 0, sizeof (unit));
					if (tr->units)
					{
						tu->next = hp;
					} else
					{
						tr->units = hp;
					}
					if (units) tu->kette = hp; else units = hp;
					tu = hp;
					tu->r = tr;
					break;
				case B_WELT:
					hp = cmalloc(sizeof(welt));
					if (welten)
					{
						tw->next = hp;
					} else
					{
						welten = hp;
					}
					tw = hp;
					break;
				case B_HOEHLEN:
					hp = cmalloc(sizeof(hoehle));
					if (hoehlen)
					{
						th->next = hp;
					} else
					{
						hoehlen = hp;
					}
					th = hp;
					break;
				case B_VORLAGE:
				case B_SPIEL:		// Bl�cke ohne neuen Speicher
				case B_LUXURIES:
				case B_SKILLS:
				case B_ITEMS:
				case B_DEBUG:
				case B_KOMMENTARE:
				case B_KRAEUTER:
				case B_KRAEUTERITEMS:
				case B_SHOWDATA:
				case B_SPELLS:
				case B_TRAENKE:
				case B_STRASSE:
					break;
				default:
					printf("unbekannter Block '%s'\nBlock nicht gefunden\n",pbuf);
					break;
			}

			if (!block)
			{
				printf("\nunbekannter Block '%s'\nblock == 0\n",pbuf);
			}
		} // neuen Block erkennen

		if (!strcmp(pbuf,"DAT-Ende"))
		{
			gamestream = 1;
			block = 0;
			oldblock = 1;
			break;
		}

		if ((block == oldblock) && block && !gamestream)
		{
			gotit = 0;
			switch(block)
			{
				case B_SPIEL:
					if (!strcmp(pbuf,"Turn"))		
					{ 
						gotit = ri(); 
						if (gotit != turn)
						{
							printf("Gelesener Turn und gespeicherter Turn sind unterschiedlich! %d <-> %d\n",turn,gotit);
							exit(EXIT_FAILURE);
						}
						gotit = 1;
					}
					if (!strcmp(pbuf,"Parteien"))	
					{ 
						printf("  lade Parteien: ");
						factionlength = ri(); 
						indicator_reset(factionlength);
						gotit = 1; 
					}
					if (!strcmp(pbuf,"Regionen"))	{ regionlength = 0 - ri();			gotit = 1; }
					if (!strcmp(pbuf,"Fantasya"))
					{
						rs(pbuf);
						if (strcmp(pbuf,fanta_version)) printf("\n- ein aelterer Fantasya-Spielstand -> %s\n",pbuf);
						gotit = 1;
					}
					break;
				case B_WELT:
					if (!strcmp(pbuf,"Welt"))		{ tw->ebene = ri();					gotit = 1; }
					if (!strcmp(pbuf,"Min"))		{ tw->min = ri();					gotit = 1; }
					if (!strcmp(pbuf,"Max"))		{ tw->max = ri();					gotit = 1; }
					if (!strcmp(pbuf,"Mail"))		{ crs(&tw->mail);					gotit = 1; }
					if (!strcmp(pbuf,"Save"))		{ tw->sicher = ri();				gotit = 1; }
					break;
				case B_HOEHLEN:
					if (!strcmp(pbuf,"Name"))		{ crs(&th->name);					gotit = 1; }
					if (!strcmp(pbuf,"Nummer"))		{ th->nummer = ri();				gotit = 1; }
					if (!strcmp(pbuf,"XKoord"))		{ th->x = ri();						gotit = 1; }
					if (!strcmp(pbuf,"YKoord"))		{ th->y = ri();						gotit = 1; }
					if (!strcmp(pbuf,"Ebene"))		{ th->ebene = ri();					gotit = 1; }
					if (!strcmp(pbuf,"Offen"))		{ th->offen = ri();					gotit = 1; }
					if (!strcmp(pbuf,"Beschr"))		{ crs(&th->beschr);					gotit = 1; }
					break;
				case B_PARTEI:
					if (!strcmp(pbuf,"ID"))			{ rs(idbuf);						gotit = 1; }
					if (!strcmp(pbuf,"Nummer"))		{ tf->no = ri();					gotit = 1; }
					if (!strcmp(pbuf,"Name"))		{ crs(&tf->name);					gotit = 1; }
					if (!strcmp(pbuf,"Addresse"))	{ crs(&tf->addr);					gotit = 1; }
					if (!strcmp(pbuf,"Passwort"))	{ crs(&tf->passw);					gotit = 1; }
					if (!strcmp(pbuf,"LastOrders"))	{ tf->lastorders = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Newbie"))		{ tf->newbie = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Value"))		{ tf->old_value = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Rasse"))		{ tf->race = ri();					gotit = 1; }
					if (!strcmp(pbuf,"xorigin"))	{ tf->xorigin = ri();				gotit = 1; }
					if (!strcmp(pbuf,"yorigin"))	{ tf->yorigin = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Optionen"))	{ tf->options = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Alter"))		{ tf->alter = ri();					gotit = 1; }
					if (!strcmp(pbuf,"Website"))	{ crs(&tf->website);				gotit = 1; }
					if (!strcmp(pbuf,"Cheater"))	{ tf->cheatenabled = ri();			gotit = 1; }
					break;
				case B_SHOWDATA:
					for(i = 0; i<MAXSPELLS; i++)
					{
						if (!strcmp(pbuf,strings[spellnames[i]][0]))	{ tf->showdata[i] = ri(); gotit = 1; }
					}
					break;
				case B_ALLIANZEN:
					if (!strcmp(pbuf,"Partei"))		
					{ 
						hp = cmalloc(sizeof(rfaction));
						memset (hp, 0, sizeof (rfaction));
						if (tf->allies)
						{
							ta->next = hp;
						} else
						{
							tf->allies = hp;
						}
						ta = hp;
						hp = NULL;
						ta->factionno = ri(); gotit = 1; 
					}
					break;
				case B_MESSAGE:
					if (!strcmp(pbuf,"Typ"))
					{
						rs(pbuf);	// Message Typ einlesen
						for(i=0 ; i<MAX_MESSAGESTYPE; i++)
						{
							if (!strcmp(pbuf,messagenames[i]))
							{
								// Messagetyp gefunden und jetzt verbinden
								switch (i)
								{
									case MT_FEHLER:		if (tf->mistakes)		{ tm->next = hp; } else { tf->mistakes = hp; }
														gotit = 1;
														break;
									case MT_WARNUNGEN:	if (tf->warnings)		{ tm->next = hp; } else { tf->warnings = hp; }
														gotit = 1;
														break;
									case MT_MESSAGES:	if (tf->messages)		{ tm->next = hp; } else { tf->messages = hp; }
														gotit = 1;
														break;
									case MT_KAEMPFE:	if (tf->battles)		{ tm->next = hp; } else { tf->battles = hp; }
														gotit = 1;
														break;
									case MT_EREIGNISSE:	if (tf->events)			{ tm->next = hp; } else { tf->events = hp; }
														gotit = 1;
														break;
									case MT_EINKOMMEN:	if (tf->income)			{ tm->next = hp; } else { tf->income = hp; }
														gotit = 1;
														break;
									case MT_HANDEL:		if (tf->commerce)		{ tm->next = hp; } else { tf->commerce = hp; }
														gotit = 1;
														break;
									case MT_PRODUKTION:	if (tf->production)		{ tm->next = hp; } else { tf->production = hp; }
														gotit = 1;
														break;
									case MT_BEWEGUNGEN:	if (tf->movement)		{ tm->next = hp; } else { tf->movement = hp; }
														gotit = 1;
														break;
									case MT_DEBUG:		if (tf->debug)			{ tm->next = hp; } else { tf->debug = hp; }
														gotit = 1;
														break;
									case MT_ALCHEMIE:	if (tf->alchemie)		{ tm->next = hp; } else { tf->alchemie = hp; }
														gotit = 1;
														break;
									case MT_ECHSENNEWS:	if (tf->echsennews)		{ tm->next = hp; } else { tf->echsennews = hp; }
														gotit = 1;
														break;
								}
							} // MsgName
						} // for-schleife
						if (!gotit)
						{
							printf("Messagetyp nicht gefunden!\nPartei '%s'  - Typ '%s'\n",tf->name,pbuf);
							exit(EXIT_FAILURE);
						}
						tm = hp;
						hp = NULL;
						//tm->guardien = 0;
					} // Typ
					if (!strcmp(pbuf,"Msg"))	// *****************************
					{ 
						rs(pbuf);				// Message einlesen
						if (!tm) { printf("kein freier MsgBlock!\n"); exit(EXIT_FAILURE); }
						strncpy(tm->s,pbuf, MAXSTRING);
						gotit = 1;
					}
					if (!strcmp(pbuf,"Unit"))			{ tm->uno = ri();				gotit = 1; }
					if (!strcmp(pbuf,"KoordX"))			{ tm->regx = ri();				gotit = 1; }
					if (!strcmp(pbuf,"KoordY"))			{ tm->regy = ri();				gotit = 1; }
					if (!strcmp(pbuf,"KoordZ"))			{ tm->regz = ri();				gotit = 1; }
					//if (tm->guardien)
					//{
					//	puts("\nGuardien-Error");
					//	printf("Unit: %s", tobase36(tm->uno));
					//	printf("Msg : %s", tm->s);
					//	// *Dendybar, der Punktierte, Oberster Meister (h4q), Die Kronelben F
					//}
					break;
				case B_REGION:
					if (!strcmp(pbuf,"XKoord"))			{ tr->x = ri();					gotit = 1; }
					if (!strcmp(pbuf,"YKoord"))			{ tr->y = ri();					gotit = 1; }
					if (!strcmp(pbuf,"ZKoord"))			{ tr->z = ri();					gotit = 1; }
					if (!strcmp(pbuf,"Name"))			{ crs(&tr->name);				gotit = 1; }
					if (!strcmp(pbuf,"Beschreibung"))	{ crs(&tr->display);			gotit = 1; }
					if (!strcmp(pbuf,"Terrain"))		{ tr->terrain = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Baeume"))			{ tr->trees = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Pferde"))			{ tr->horses = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Bauern"))			{ tr->peasants = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Silber"))			{ tr->money = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Strasse"))
					{
						int roadSize = ri();
						for(i = 0; i < MAXDIRECTIONS; i++) tr->road[i] = roadSize;
						gotit = 1; 
					}
					if (!strcmp(pbuf,"Alter"))			{ tr->alter = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Movement"))		{ tr->movement = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Eisen"))			{ tr->eisen = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Steine"))			{ tr->steine = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Gold"))			{ tr->gold = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Hoehle"))			{ tr->hoehle = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Sturm"))			{ tr->sturm = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Eis"))			{ tr->eis = ri();				gotit = 1; }
					if (!strcmp(pbuf,"mainKraut"))		{ tr->mainKraut = ri();			gotit = 1; }
					break;
				case B_LUXURIES:
					for(i = 0; i<MAXLUXURIES; i++)
					{
						if (!strcmp(pbuf,strings[itemnames[0][FIRSTLUXURY + i]][0])) { tr->demand[i] = ri(); gotit = 1; }
					}
					if (!strcmp(pbuf,"Produktion"))
					{
						rs(pbuf);
						for(i = 0; i<MAXLUXURIES; i++)
						{
							if (!strcmp(pbuf,strings[itemnames[0][FIRSTLUXURY + i]][0])) { tr->produced_good = i; gotit = 1; }
						}
					}
					break;
				case B_KOMMENTARE:
					if (!strcmp(pbuf,"Msg"))			
					{ 
						hp = cmalloc(sizeof(strlist)+MAXSTRING);
						memset (hp, 0, sizeof (strlist)+MAXSTRING);
						if (tr->comments)
						{
							tm->next = hp;
						} else
						{
							tr->comments = hp;
						}
						tm = hp;
						rs(pbuf); strcpy(tm->s,pbuf); gotit = 1; 
					}
					break;
				case B_DEBUG:
					if (!strcmp(pbuf,"Msg"))			
					{ 
						hp = cmalloc(sizeof(strlist)+MAXSTRING);
						memset (hp, 0, sizeof (strlist)+MAXSTRING);
						if (tr->debug)
						{
							tm->next = hp;
						} else
						{
							tr->debug = hp;
						}
						tm = hp;
						rs(pbuf); strcpy(tm->s,pbuf); gotit = 1; 
					}
					break;
				case B_BURG:
					if (!strcmp(pbuf,"ID"))				{ rs(idbuf);					gotit = 1; }
					if (!strcmp(pbuf,"Nummer"))			{ tb->no = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Name"))			{ crs(&tb->name);				gotit = 1; }
					if (!strcmp(pbuf,"Beschreibung"))	{ crs(&tb->display);			gotit = 1; }
					if (!strcmp(pbuf,"Size"))			{ tb->size = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Typ"))			{ tb->typ = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Funktion"))		{ tb->funktion = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Wegweiser"))		{ tb->wegweiser = ri();			gotit = 1; }
					break;
				case B_SCHIFF:
					if (!strcmp(pbuf,"ID"))				{ rs(idbuf);					gotit = 1; }
					if (!strcmp(pbuf,"Nummer"))			{ ts->no = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Name"))			{ crs(&ts->name);				gotit = 1; }
					if (!strcmp(pbuf,"Beschreibung"))	{ crs(&ts->display);			gotit = 1; }
					if (!strcmp(pbuf,"Typ"))			{ ts->type = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Groesse"))		{ ts->left = ri();				gotit = 1; }
					break;
				case B_EINHEIT:
					if (!strcmp(pbuf,"ID"))				{ rs(idbuf);					gotit = 1; }
					if (!strcmp(pbuf,"Nummer"))			
					{ 
						tu->no = ri();				
						gotit = 1; 
						if (tu->no > highest_unit_no) highest_unit_no = tu->no;
					}
					if (!strcmp(pbuf,"Name"))			{ crs(&tu->name);				gotit = 1; }
					if (!strcmp(pbuf,"Beschreibung"))	{ crs(&tu->display);			gotit = 1; }
					if (!strcmp(pbuf,"Personen"))		{ tu->number = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Typ"))			{ tu->type = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Effekt"))			{ tu->effect = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Enchanted"))		{ tu->enchanted = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Partei"))			
					{ 
						tu->faction = findfaction(ri()); 
						tu->tarn_no = tu->faction->no;		// wird seperat Nachgeladen
						gotit = 1; 
					}
					if (!strcmp(pbuf,"Burg"))			{ tu->building = findbuilding(ri()); gotit = 1; }
					if (!strcmp(pbuf,"Schiff"))			{ tu->ship = findship(ri());	gotit = 1; }
					if (!strcmp(pbuf,"Owner"))			{ tu->owner = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Status"))			{ tu->status = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Guard"))			{ tu->guard = ri();				gotit = 1; }
					if (!strcmp(pbuf,"LastOrder"))		{ crs(&tu->lastorder);			gotit = 1; }
					if (!strcmp(pbuf,"ThisOrder"))		{ crs(&tu->thisorder);			gotit = 1; }
					if (!strcmp(pbuf,"ThisOrder2"))		{ crs(&tu->thisorder2);			gotit = 1; }
					if (!strcmp(pbuf,"Kampfzauber"))	{ tu->combatspell = ri();		gotit = 1; }
					if (!strcmp(pbuf,"Rasse"))			{ tu->race = ri();				gotit = 1; }
					if (!strcmp(pbuf,"Tarntypus"))		{ tu->raceTT = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Parteitarnung"))	{ tu->tarn_no = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Tarnung"))		{ tu->tarnung = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Praefix"))		{ crs(&tu->prefix);				gotit = 1; }
					if (!strcmp(pbuf,"Lebenspunkte"))	{ tu->lebenspunkte = ri();		gotit = 1; }
					if (!strcmp(pbuf,"Jungfernblut"))	{ tu->jungfernblut = ri();		gotit = 1; }
					if (!strcmp(pbuf,"Wissenssalbe"))	{ tu->wissenssalbe = ri();		gotit = 1; }
					if (!strcmp(pbuf,"Gehirntot"))		{ tu->gehirntot = ri();			gotit = 1; }
					if (!strcmp(pbuf,"Einkommen"))		{ tu->einkommen = ri();			gotit = 1; }
					break;
				case B_VORLAGE:
					if (!strcmp(pbuf,"Msg"))
					{
						hp = cmalloc(sizeof(strlist)+MAXSTRING);
						memset (hp, 0, sizeof (strlist)+MAXSTRING);
						if (tu->comments)
						{
							tm->next = hp;
						} else
						{
							tu->comments = hp;
						}
						tm = hp;
						rs(pbuf);				// Message einlesen
						strcpy(tm->s,pbuf);
						gotit = 1;
					}
					break;
				case B_SKILLS:
					for(i=0 ; i<MAXSKILLS; i++)
					{
						if (!strcmp(pbuf,skillnames[i])) { tu->skills[i] = ri(); gotit = 1; }
					}
					break;
				case B_ITEMS:
					for(i=0 ; i<MAXITEMS; i++)
					{
						if (!stricmp(pbuf,strings[itemnames[0][i]][0])) { tu->items[i] = ri(); gotit = 1; }
					}
					break;
				case B_SPELLS:
					for(i=0 ; i<MAXSPELLS; i++)
					{
						if (!strcmp(pbuf,strings[spellnames[i]][0])) { tu->spells[i] = 1; gotit = 1; }
					}
					break;
				case B_KRAEUTER:
					for(i=0; i<MAX_KRAEUTER; i++)
					{
						if (!strcmp(pbuf,kraeuter[i])) { tr->kraut[i]=ri();		gotit = 1; }
					}
					break;
				case B_KRAEUTERITEMS:
					for(i=0; i<MAX_KRAEUTER; i++)
					{
						if (!strcmp(pbuf,kraeuter[i])) { tu->kraeuter[i] = ri();	gotit = 1; }
					}
					break;
				case B_TRAENKE:
					for(i=0; i<MAX_TRAENKE; i++)
					{
						if (!strcmp(pbuf,traenke[i]))	{ tu->traenke[i] = ri();	gotit = 1; }
					}
					break;
				case B_STRASSE:
					for(i = 0; i < MAXDIRECTIONS; i++)
					{
						if (!strcmp(pbuf, directions[i])) { tr->road[i] = ri();		gotit = 1; }
					}
					break;
			} // switch(block)
			if (!gotit)
			{
				printf("\nunbekanntes Tag '%s' fuer Block '%s'\nID: %s\n",pbuf,blocknames[block],idbuf);
				rs(pbuf);
				printf("naechster String -> '%s'\n",pbuf);
				exit(EXIT_FAILURE);
			}
		}
	} // !feof(F)

	puts("\n- Daten der Parteien durchgehen...");
	puts("- auf Einheit 't', 'te', 'tem' und 'temp' kontrollieren");

	for (r = regions; r; r = r->next)
    {
		/* Initialize faction seendata values */
		for (u = r->units; u; u = u->next) 
		{
			for (i = 0; i != MAXSPELLS; i++) if (u->spells[i]) u->faction->seendata[i] = 1;
			u->faction->number += u->number;
			u->faction->money += u->items[I_SILVER];
			u->faction->alive = 1;
			if ((u->no == 29)			||	// t
				(u->no == 1058)			||	// te
				(u->no == 38110)		||	// tem
				(u->no == 1371985)		||	// temp

				(u->no == 11)			||	// b
				(u->no == 406)			||	// ba
				(u->no == 14646)		||	// bau
				(u->no == 527270)		||	// baue

				(u->no == 14)			||	// e
				(u->no == 522)			||	// ei
				(u->no == 18815)		||	// ein
				(u->no == 677357))			// einh
			{
				printf("\tEinheit '%s' gefunden, ", tobase36(u->no));
				//newunitnummer(u);
				printf("nach '%s' konvertiert\n", tobase36(u->no));
				printf("\tInfo an %s <%s>!\n\n",u->faction->name,u->faction->addr);
			} // UnitNummer konvertieren
		} // Einheiten
	} // Regionen

	// Allianzen verbinden
	for (f = factions; f; f = f->next)
	{
		for (rf = f->allies; rf; rf = rf->next) 
		{
			rf->faction = findfaction (rf->factionno);
		}
		f->newbie = 0;
	}

	// die ganzen Nachrichten wurden mit der Unit Nummer gespeichert, aber f�r die Ausgabe im CR / NR wird
	// die Unit selber ben�tigt, also suchen wir jetzt die Unit global und merken sie entsprechend
/*	for(f = factions; f; f = f->next)
	{
		strlist *S;

		for(S = f->mistakes; S; S = S->next) S->u = findunitglobal(S->uno);
		for(S = f->warnings; S; S = S->next) S->u = findunitglobal(S->uno);
		for(S = f->messages; S; S = S->next) S->u = findunitglobal(S->uno);
		for(S = f->battles; S; S = S->next) S->u = findunitglobal(S->uno);
		for(S = f->events; S; S = S->next) S->u = findunitglobal(S->uno);
		for(S = f->income; S; S = S->next) S->u = findunitglobal(S->uno);
		for(S = f->commerce; S; S = S->next) S->u = findunitglobal(S->uno);
		for(S = f->production; S; S = S->next) S->u = findunitglobal(S->uno);
		for(S = f->movement; S; S = S->next) S->u = findunitglobal(S->uno);
		for(S = f->debug; S; S = S->next) S->u = findunitglobal(S->uno);
		for(S = f->alchemie; S; S = S->next) S->u = findunitglobal(S->uno);
		for(S = f->echsennews; S; S = S->next) S->u = findunitglobal(S->uno);
	}*/
	
	ConnectUnits();
	doppelteEinheiten();
	findcubus();	// neue Verkettung der Regionen ... schnelleres findregion();
	connectregions ();
}

// ************************************************************************************************************
// ************************************************************************************************************
// ************************************************************************************************************

//	ss() - SaveString()
//	si() - SaveInteger()
//	sb() - SaveBlock()

/*
 *	speichert einen String ... alles wird als String gespeichert auch Zahlen ... um das Format k�mmert sich ein extra Funktion
 *  l: Ebene der Speicherung
 *  t: ist der Tag ... like "BLOCK" oso
 *  d: das sind die Daten
 */
void ss(int l, char *t, char *d)
{
	int i;
	if (l) { for(i = 0; i < l; i++) { wspace(); wspace(); } }
	ws(t);
	wspace();
	ws(d);
	wnl();
}

/*
 *	speichert eine Zahl
 *  l: Ebene der Speicherung
 *  t: ist der Tag ... like "BLOCK" oso
 *  v: Wert der zu speichern ist
 */
void si(int l, char *t, int v)
{
	char pbuf[100];
	sprintf(pbuf, "%d", v);
	ss(l, t, pbuf);
}

/*
 *	speichert einen Block
 *  l: Ebene der Speicherung
 *  n: das ist der name des Blockes
 */
void sb(int l, char *n)
{
	if (l) l--;	// ein Block ist immer etwas weiter links
	ss(l, "BLOCK", n);
}


int krpstrlist (char *title, int n , strlist * S)
{
	while (S)
    {
		sb (2, "MESSAGE");
		if (strcmp(title,"FEHLER") == 0)		ss(2, "Typ", "Fehler");
		if (strcmp(title,"WARNUNGEN") == 0)		ss(2, "Typ", "Warnungen");
		if (strcmp(title,"MELDUNGEN") == 0)		ss(2, "Typ", "Meldungen");
		if (strcmp(title,"KAEMPFE") == 0)		ss(2, "Typ", "Kaempfe");
		if (strcmp(title,"EREIGNISSE") == 0)	ss(2, "Typ", "Ereignisse");
		if (strcmp(title,"EINKOMMEN") == 0)		ss(2, "Typ", "Einkommen");
		if (strcmp(title,"HANDEL") == 0)		ss(2, "Typ", "Handel");
		if (strcmp(title,"PRODUKTION") == 0)	ss(2, "Typ", "Produktion");
		if (strcmp(title,"BEWEGUNGEN") == 0)	ss(2, "Typ", "Bewegungen");
		if (strcmp(title,"ALCHEMIE") == 0)		ss(2, "Typ", "Alchemie");
		if (strcmp(title,"ECHSENNEWS") == 0)	ss(2, "Typ", "Echsennews");
		ss(2, "Msg", S->s);
		si(2, "Count", n);
		if (S->uno)
		{
			// nur ausgeben wenn Einheit noch Personen hat, sonst wird es in Magellan nicht angezeigt
			ss(2, "Unit", tobase36(S->uno));
			si(2, "RegX", S->regx);
			si(2, "RegY", S->regy);
			si(2, "RegZ", S->regz);
		}
		S = S->next;
		n++;
    }
	return n;
}
/*
 *	hier wird jetzt der neue KR erzeugt ... quasi der f�r Kolumbus ... dazu werden aber noch alte Funktionen
 *  und Variablen benutzt ... F - f�r das File wo hingespeichert werden soll ... und die Funktionen ws(), wspace(),
 *  wi() und wnl()
 *
 *  wichtigstes Ziel im KR ist die v�llige Zahlenfreiheit ... d.h. Regionen werden als die Region gespeichert wie sie
 *  lesbar sind ... also als "Ozean" und nicht als "0" ... gleiches gilt f�r alle anderen Zahlenkombinationen ... viele
 *  Sachen werden auch in den Config-Files f�r Kolumbus eingestellt ... also so wenig wie m�glich feste Daten
 */
void Kolumbus()
{
	faction *f, *hf;
	rfaction *rf;
	unit *u, *bu;
	region *r;
	ship *sh;
	building *b;
	char pbuf[110];
	int i, n;
	char cr[5000][3];
	int visible;
	int head = 0;
	int cr_count;
	strlist *tempstr;

	puts("- erzeuge Kolumbus Reports");

	for(f = factions; f; f = f->next)
	{
		if (f->no == 0) continue;			// keine Monster KR's

		sprintf(pbuf, "reports\\%s.kr", tobase36(f->no));
		F = fopen(pbuf, "w");
		if (F)
		{
			printf("\t- (%s) %s\n", tobase36(f->no), f->name);

			// einige Sachen auf 0 setzen
			for(hf = factions; hf; hf = hf->next) hf->gotit = 0;
			for(i = 0; i<5000; i++)
			{ 
				cr[i][0] = 0;
				cr[i][1] = 0;
				cr[i][2] = 0;
			}

			fprintf(F, "KOLUMBUS 1\n");						// erste Zeile enth�lt immer KOLUMBUS und dann die Versionsnummer
			//ss(0, "Game", "Fantasya");						// wenn nicht angegeben, dann automatisch "Fantasya"
			si(0, "Runde", turn);

			// Daten �ber eigene Partei
			sb(1, "Partei");
			ss(1, "Name", f->name);
			ss(1, "Nummer", tobase36(f->no));
			ss(1, "eMail", f->addr);
			ss(1, "Rasse", racenames[f->race][0]);			// 1. Schritt f�r dynamische Daten
			si(1, "Kosten", RecruitCost[f->race]);			// Rekrutierungskosten
			ss(1, "Passwort", f->passw);

			n = 1;
			n = krpstrlist ("FEHLER", 0 , f->mistakes);
			n = krpstrlist ("WARNUNGEN", n , f->warnings);
			n = krpstrlist ("MELDUNGEN", n , f->messages);
			n = krpstrlist ("KAEMPFE", n , f->battles);
			n = krpstrlist ("EREIGNISSE", n , f->events);
			n = krpstrlist ("EINKOMMEN", n , f->income);
			n = krpstrlist ("HANDEL", n , f->commerce);
			n = krpstrlist ("PRODUKTION", n , f->production);
			n = krpstrlist ("BEWEGUNGEN", n , f->movement);
			n = krpstrlist ("ALCHEMIE", n , f->alchemie);
			n = krpstrlist ("ECHSENNEWS", n , f->echsennews);

			for (r = regions; r; r = r->next)
		    {
				r->gotit = 0;		// Region noch nicht "gezeichnet"
				for (u = r->units; u; u = u->next) if (u->faction == f) break;
				if (!u) continue;
				for (u = r->units; u; u = u->next)
				{
					visible = cansee (f, r, u);
					if (visible == 2)
					{
						hf = findfaction(u->tarn_no);	// Ausgabe dieser Partei !!
						if (hf->no != f->no)
						{
							if (hf->gotit == 0)
							{
								hf->gotit = 1;
								sb(2, "Partei");
								ss(2, "Nummer", tobase36(hf->no));
								ss(2, "Name", hf->name);
								ss(2, "eMail", hf->addr);
								ss(2, "Rasse", racenames[hf->race][0]);	// ein RIESIGER Bug ... hier werden Echsenmenschen enttarnt
							}
						}
					}
				}
			} // alle Regionen

			// Allianzen
			for (rf = f->allies; rf; rf = rf->next)
		    {
				sb(2, "Alianzen");
				ss(2, "Volk", tobase36(rf->faction->no));
				ss(2, "Name", rf->faction->name);
				ss(2, "Alles", "Ja");						// hier muss sp�ter noch getrennt werden
			};

			for (r = regions; r; r = r->next)
			{
				for (u = r->units; u; u = u->next) if (u->faction == f) break;		// eigene Einheiten suchen
				if (!u) continue;	// Region ist komplett leer
		
				r->gotit = 1;		// Region wurde bereits "gezeichnet"
		
				sb(2, "REGION");
				si(2, "RegX", r->x - f->xorigin);
				si(2, "RegY", r->y - f->yorigin);
				si(2, "RegZ", r->z);
				ss(2, "Terrain", strings[terrainnames[mainterrain (r)]][0]);
				if (r->terrain != T_OCEAN) ss(2, "Name", r->name);
				si(2, "Bauern", r->peasants);
				si(2, "Pferde", r->horses);
				si(2, "Baeume", r->trees);
				si(2, "Silber", r->money);
				if (r->display) ss(2, "Beschreibung", r->display);
				if (production[r->terrain]) si(2, "Lohn", buildingeffsize (largestbuilding (r)) + 10);
				si(2, "Unterhaltung", r->money / ENTERTAINFRACTION);
				si(2, "Rekruten", r->peasants / RECRUITFRACTION);

				if (r->terrain != T_OCEAN)
				{
					sb(3, "Preise");
					si(3, "Balsam",		(itemprice[0] * r->demand[0] / 100 * (0 == r->produced_good ? -1 : 1)));
					si(3, "Gewuerz",	(itemprice[1] * r->demand[1] / 100 * (1 == r->produced_good ? -1 : 1)));
					si(3, "Juwel",		(itemprice[2] * r->demand[2] / 100 * (2 == r->produced_good ? -1 : 1)));
					si(3, "Myrrhe",		(itemprice[3] * r->demand[3] / 100 * (3 == r->produced_good ? -1 : 1)));
					si(3, "Oel",		(itemprice[4] * r->demand[4] / 100 * (4 == r->produced_good ? -1 : 1)));
					si(3, "Seide",		(itemprice[5] * r->demand[5] / 100 * (5 == r->produced_good ? -1 : 1)));
					si(3, "Weihrauch",	(itemprice[6] * r->demand[6] / 100 * (6 == r->produced_good ? -1 : 1)));
				};

				// sehr unklar diese Stelle ... glaube das knallt in Kolumbus!
				sb(3, "Grenzen");
				for(i = 0; i<6; i++)
				{
					ss(3, "Typ", "Strasse");
					ss(3, "Richtung", directions[i]);
					si(3, "Wert", 100 * r->road[i] / roadreq[r->terrain]);
				}

				if (r->hoehle)
				{
					hoehle *h = findhoehle(r->hoehle);

					sb(3, "Gebaeude");				// Hoehlen brauchen keine Nummern, zum Gl�ck
					ss(3, "Name", h->name);
					ss(3, "Typ", "Hoehle");
					if (h->beschr) ss(3, "Beschreibung", h->beschr);
				}

				for (b = r->buildings; b; b = b->next)
				{
					sb(3, "Gebaeude");
					ss(3, "Name", b->name);
					ss(3, "Nummer", tobase36(b->no));
					if (b->display) ss(3, "Beschreibung", b->display);
					ss(3, "Typ", b->typ ? BuildingNames[b->typ] : buildingnames[buildingeffsize(b)]);
					si(3, "Groesse", b->size);
					u = buildingowner (r, b);
					if (u) ss(3, "Besitzer", tobase36(u->no));
					if (u && cansee (f, r, u) == 2) ss(3, "Volk", tobase36(u->tarn_no));
					b->voll = b->size;
				}

				for (sh = r->ships; sh; sh = sh->next)
				{
					sb(3, "Schiff");
					ss(3, "Name", sh->name);
					if (sh->display) ss(3, "Beschreibung", sh->display);
					ss(3, "Nummer", tobase36(sh->no));
					ss(3, "Typ", shiptypes[0][sh->type]);
					u = shipowner (r, sh);
					if (u) ss(3, "Kapitaen", tobase36(u->no));
					if (u && cansee (f , r , u) == 2) ss(3, "Volk", tobase36(u->tarn_no));
					si(3, "Groesse", shipcost[sh->type] - sh->left);
				}


				for (u = r->units; u; u = u->next)
				{
					if (visible = cansee (f, r, u))
					{
						bu = (u->building) ? NULL : u;
						sb(3, "Einheit");
						ss(3, "Nummer", tobase36(u->no));
						ss(3, "Name", u->name);
						ss(3, "Beschreibung", u->display);
		
						if (u->building)
						{
							unit *hu;
					
							// fremde Einheit in eigener Burg, wenn nicht werden in der Burg nur je 1 Person angezeigt
							bu = NULL;
							if (u->building)
							{
								for(hu=r->units; hu; hu=hu->next)
								{
									if ((u->building) && (u->building == hu->building) && (hu->faction == f)) bu = hu;
								}
							} else
							{
								bu = u;
							}
							// wenn die Burg voll ist, also im Kampf keine Einheiten mehr sch�tzt, kann sie auch
							// keine Einheiten verstecken
							if (u->building)
							{
								u->building->voll -= u->number;
								if (u->building->voll <= 0) bu = u;
							}
						} // Geb�ude

						if (visible == 2)
						{
							if (f->no == u->faction->no)
							{
								ss(3, "Partei", tobase36(u->faction->no));
								if (u->tarn_no != f->no)
								{
									ss(3, "Verkleidet", tobase36(u->tarn_no));
								}
							} else
							{
								if (u->tarn_no)
								{
									ss(3, "Partei", tobase36(u->tarn_no));
								} else
								{
									ss(3, "Partei", "-1"); // Partei getarnt, weil TARNE PARTEI Befehl
								}
							}
						} else
						{
							ss(3, "Partei", "-1");		// Partei getarnt, weil nicht sichtbar durch Tarnung
						}

						if ((u->tarn_no != u->faction->no) && (u->faction == f)) ss(3, "Verkleidung", tobase36(u->tarn_no));
						si(3, "Personen", (!bu) ? 1 : u->number);
						if (u->prefix) ss(3, "Prefix", u->prefix);

						switch (u->type)
						{
							// Aussehen
							case U_ILLUSION:
							case U_GUARDS:		ss(3, "Typ", "Mensch");
												break;
							case U_MAN:			ss(3, "Typ", u->faction->no ? racenames[u->raceTT][0] : "Menschen");	break;
							case U_UNDEAD:		ss(3, "Typ", "Untote");			break;
							case U_FIREDRAGON:	ss(3, "Typ", "Feuerdrachen");	break;
							case U_DRAGON:		ss(3, "Typ", "Drachen");		break;
							case U_WYRM:		ss(3, "Typ", "Wyrme");			break;
							default:			ss(3, "Typ", "ein Mosnter");	break;
						}

						if (u->guard)		ss(3, "Wache", "Ja");
						if (u->building)	ss(3, "Gebaeude", tobase36(u->building->no));
						if (u->ship)		ss(3, "Schiff", tobase36(u->ship->no));
				
						if (u->status == ST_AVOID)
						{
							ss(3, "Kampfstatus", "kaempf nicht");
						} else 
						{
							if (u->status == ST_BEHIND)
							{
								ss(3, "Kampfstatus", "kaempft hinten");
							} else
							{
								ss(3, "Kampfstatus", "kaempft vorne");
							}
						} // Kampfstatus

						if (u->faction == f)
						{
							if (u->tarnung && effskill(u,SK_STEALTH)) si(3, "Tarnung",effskill(u,SK_STEALTH)); // immer beste Tarnung
							if (u->faction->race == R_DEMON) ss(3, "wahrerTyp", racenames[u->faction->race][0]);
							ss(3, "Trefferpunkte", getLebenszustand(u));

							sb(4, "Befehle");
							if (u->lastorder) ss(4, "Monat", u->lastorder);
							if ((u->thisorder) && (!u->lastorder)) ss(4, "Monat", u->thisorder);
							if (u->thisorder2) ss(4, "Lieferungen", u->thisorder2);
							if (u->comments)
							{
								tempstr = u->comments;
								while (tempstr)
								{
									ss(4, "Kommentare", tempstr->s);
									tempstr = tempstr->next;
								};
							};
					
							head = 0;
							for (i = 0; i != MAXSKILLS; i++)
							{
								if (u->skills[i] != 0)
								{
									if (!head) { sb(4, "Talente"); head = 1; }
									si(4, skillnames[i], u->skills[i]);
								};
							}

							head = 0;
							for (i = 0; i != MAXSPELLS; i++)
							{
								if (u->spells[i] != 0)
								{
									if (head == 0)
									{
										sb(4, "Zaubersprueche");
										head = 1;
									};
									ss(4, "Spruch", strings[spellnames[i]] [f->language]);
								};
							}
						} // f->faction == f

						if (visible == 2)
						{
							// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
							// diese Zeile erzeugt immer einen GEGENSTAENDE-Block !!!!
							// dieser ist ggf. leer
							sb(4,"Gegenstaende");
							// ist aber im Moment einfacher, da brauch ich mir jetzt nicht die Pladde machen
							// wie ich das mit den Kr�utern und Tr�nken einbaue
							// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					
							for (i = 0; i != LASTLUXURY; i++)
							{
								if ( u->items[i] != 0 )
								{
									if (i != I_SILVER)
									{
										int menge = u->items[i];
										if ((u->building) && (!bu) && (u->faction != f)) menge = 1;
										si(4, strings[itemnames[0][i]][0], menge);
									} else
									{
										if (f != u->faction)
										{
											if (u->items[i] < 500 * u->number) ss(4, "Silberbeutel", "1"); else ss(4, "Schatzkiste", "1");
										} else
										{
											si(4, "Silber",u->items[i]);
										}
									}
								};
							}
						
							// Kr�uter
							head = 0;
							if (u->faction != f)
							{
								for(i=0; i<MAX_KRAEUTER; i++) if (!head && u->kraeuter[i]) { head=1; ss(4, "Kraeuterbeutel", "1"); }
							} else
							{
								for(i=0; i<MAX_KRAEUTER; i++)
								{
									if (u->kraeuter[i]) si(4, kraeuter[i], u->kraeuter[i]);
								}
							}
						
							// Traenke & Tinkturen
							head = 0;
							if (u->faction != f)
							{
								for(i=0; i<MAX_TRAENKE; i++) if (!head && u->traenke[i]) { head=1; ss(4, "Tinkturen", "1"); }
							} else
							{
								for(i=0; i<MAX_TRAENKE; i++)
								{
									if (u->traenke[i]) si(4, traenke[i], u->traenke[i]);
								}
							}
						} // Items, Kr�uter, Tr�nke & Tinkturen
					} // ((visible = cansee (f, r, u)))
				} // alle Einheiten der Region
			} // alle Regionen f�r Einheiten


			for(r=regions; r; r=r->next)
			{
				for(sh = r->ships; sh; sh=sh->next)
				{
					int i;
					unit *u = shipowner(r,sh);

					if (u)
					{
						if (u->faction == f)
						{
							for(i=0; i<MAXSPEED; i++)
							{
								if (sh->rv[i]) if (!sh->rv[i]->gotit)	// wenn sie nicht bisher gezeichnet wurde
								{
									region *hr = sh->rv[i];
									hr->gotit = 1;				// Region wird/wurde gezeichnet
									sb(2, "Region");
									si(2, "RegX", r->x - f->xorigin);
									si(2, "RegY", r->y - f->yorigin);
									si(2, "RegZ", r->z);
									ss(2, "Terrain", strings[terrainnames[mainterrain (r)]][0]);
									if (r->terrain != T_OCEAN) ss(2, "Name", r->name);
								}
							}
						} // Schiffe die zur Partei geh�ren
					} // Einheiten
				} // Schiffe in den Regionen
			} // alle Regionen

			// jetzt noch alle angrenzenden Region "zeichen"
			cr_count = 0;
		    for (r = regions; r; r = r->next)
			{
				if (r->gotit == 1)
				{
					for (i = 0; i != MAXDIRECTIONS; i++)
					{
						int rx,ry;		// zu suchende Region
						region* hr;		// zu suchende Region
				
						rx = r->x + delta_x[i];		// beginnend mit NW, dann im Uhrzeigersinn
						ry = r->y + delta_y[i];
						hr = findregion(rx, ry, r->z);
				
						if (hr != NULL) // dann ist es keine Chaos Region
						{
							if (hr->gotit == 0)	// wurde noch nicht "gezeichnet"
							{
								// Region muss noch gezeichnet werden
								sb(2, "Region");
								si(2, "RegX", hr->x - f->xorigin);
								si(2, "RegY", hr->y - f->yorigin);
								si(2, "RegZ", hr->z);
								ss(2, "Terrain", strings[terrainnames[mainterrain (hr)]][0]);
								if (hr->terrain != T_OCEAN) ss(2, "Name", hr->name);
								hr->gotit = 2;	// Region wurde gezeichnet, damit aber an angrenzenden Regionen
										// keine weiteren angrenzenden Regionen gezeichnet werden, wird
										// diese Region mit 2 gekennzeichnet !!
							} else
							{
								// Region wurde bereits gezeichnet
							}
						} else
						{
							/*
							 * diese Region ist Chaos! Deshalb erstmal zwischen speichern, da eine Chaos-Region
							 * noch keine eigene Struktur hat, Programm w�rde also abst�rzen
							 */
							cr[cr_count][0] = rx;
							cr[cr_count][1] = ry;
							cr[cr_count][2] = r->z;
							cr_count++;
							if (cr_count == 5000) 
							{
								printf("\n -+*+-   zuviele Chaos-Regionen   -+*+-\n\n");
								exit(EXIT_FAILURE);
							}
						} // hf != NULL
					}	// MAXDIRECTIONS
				}	// r->gotit = 1
			} // alle angrenzenden Region

			// nur noch Chaos-Regionen zeichnen
			for(i = 0;i < cr_count; i++)
			{
				sb(2, "Region");
				si(2, "RegX", cr[i][0] - f->xorigin);
				si(2, "RegY", cr[i][1] - f->yorigin);
				si(2, "RegZ", cr[i][2]);
				ss(2, "Terrain", "Chaos");
			}

		} // g�ltiger KR
	} // alle V�lker
}



void saveUnits()
{
	unit *u;
	region *r;
	FILE *f = fopen("units.dat", "w");
	for(r = regions; r; r = r->next) for(u = r->units; u; u = u->next) fprintf(f, "%s\n", tobase36(u->no));
	fclose(f);
}
