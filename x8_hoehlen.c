#include "atlantis.h"

#define maxHoehlennamen 8
#define maxHoehlenbesch 4

hoehle *hoehlen;

char *hoehlennamen[maxHoehlennamen] =
{
	"Dunkelloch",
	"Hoellentor",
	"Schwarzloch",
	"Hoellenloch",
	"Schwarztor",
	"Dunkeltor",
	"Hoellen Schlund",
	"Gruft des Grauens",
};

char *hoehlenbeschreibung[maxHoehlenbesch] =
{
	"Diese Hoehle ist irgendwann eingestuerzt, aber es scheint loses Geroell zu sein.",
	"Ein gro�es Tor wurde vor diese Hoehle gebaut, das Schloss ist scheinbar verrostet.",
	"Unheimliche Klaenge kommen aus dem inneren der Hoehle.",
	"Endlose Schwaerze durchzieht diese Hoehle.",
};



hoehle *findhoehle(int n)	// H�hle anhand der Nummer suchen
{
	hoehle *h;
	
	for(h = hoehlen; h; h = h->next) if (h->nummer == n) return h;
	return NULL;
}

void MakeHoehle(region *r)
{
	hoehle *hp, *h;

	if (r->hoehle)
	{
		// H�hle existiert schon ... wird aber jetzt "geschlossen"
		h = findhoehle(r->hoehle);
		h->offen = 0;
		return;
	}

	// zuerst erschaffen wir die H�hle
	hp = cmalloc(sizeof(hoehle));
	hp->ebene = r->z;
	hp->x = r->x;
	hp->y = r->y;

	hp->nummer = 0;
	do
    hp->nummer++;
	while (findbuilding (hp->nummer));

	sprintf(buf, "%s", hoehlennamen[hp->nummer % maxHoehlennamen]);
	mnstrcpy (&hp->name, buf, NAMESIZE);

	sprintf(buf, "%s", hoehlenbeschreibung[hp->nummer % maxHoehlenbesch]);
	mnstrcpy (&hp->beschr, buf, NAMESIZE);

	if (hoehlen)
	{
		h = hoehlen;
		while(h->next) h = h->next;
		h->next = hp;						// anh�ngen
	} else
	{
		hoehlen = hp;
	}
	
	// dann wird die H�hle in den Regionen bekannt gemacht
	r->hoehle = hp->nummer;						// Oberwelt wird ja �bergeben
	r = findregion(r->x, r->y, -(r->z));		// Unterwelt erst noch suchen
	r->hoehle = hp->nummer;
}

void BetreteHoehle(region *r, unit *u)
{
	hoehle *h;
	region *ziel;

	h = findhoehle(r->hoehle);
	if (!h->offen)
	{
		sprintf(buf, "%s bricht beim betreten der Hoehle %s das Siegel, das vor den Legionen des Chaos schuetzt.", unitid(u), h->name);
	} else
	{
		if (r->z > 0)
		{
			sprintf(buf, "%s betritt die Hoehle %s zur Unterwelt.", unitid(u), h->name);
		} else
		{
			sprintf(buf, "%s verlaesst durch die Hoehle %s die Unterwelt.", unitid(u), h->name);
		}
	}
	h->offen = 1;
	addmovement(u, buf);

	ziel = findregion(r->x, r->y, -(r->z));		// Gegenregion suchen
	translist (&r->units, &ziel->units, u);		// translist(Quelle, Ziel, Einheit)

	u->guard = 0;								// kein BEWACHE mehr
}

// hier werden jetzt die Vulkane der Unterwelt aktiv bzw. erl�schen, dabei werden alle Geb�ude etc. zerst�rt
void Vulkane()
{
	region *r;
	//unit *u;
	building *b;

	for(r = regions; r; r = r->next)
	{
		if (((r->terrain == T_VULKAN) || (r->terrain == T_AKTIVER_VULKAN)) && (rand() % 256 < 16))
		{
			if (r->terrain == T_VULKAN)
			{
				r->terrain = T_AKTIVER_VULKAN;
				r->gold = 0;				// Goldmine versiegt

				// jetzt werden alle Geb�ude besch�digt
				for(b = r->buildings; b; b = b->next)
				{
					if (b->typ != BT_RUINE)
					{
						int n = b->size / 2;
						b->size -= max(n,1);
						if (b->size <= 1)
						{
							b->typ = BT_RUINE;
							b->size = 1;
						}
					}
				}
			} else
			{
				r->terrain = T_VULKAN;
				r->gold = varianz(30,10);	// eine neue Goldmine
			}
		}
	}
}