/* German Atlantis PB(E)M host Copyright (C) 1995-1999  Alexander Schroeder

 based on:
 
  Atlantis v1.0  13 September 1993 Copyright 1993 by Russell Wallace
  
   This program may be freely used, modified and distributed.  It may
   not be sold or used commercially without prior written permission
from the author.  */

#include "atlantis.h"
#include	<io.h>
#include	<direct.h>
#include <stdio.h>


#define xisdigit(c)     ((c) == '-' || ((c) >= '0' && (c) <= '9')) /* Changed by Bjoern Becker 27.07.2000 */
#define COMMENT_CHAR		';'
#define SUMMARY_BASENAME	"parteien"
#define ADDRESS_BASENAME	"adressen"

//#define DEBUG_SAVED_GAME
//#define DEBUG_MEMORY_USAGE

/* Konstanten, die man #definieren kann:

 DEBUG_SAVED_GAME: Die Daten-Datei wird beim Laden auf
 Doppeleintraege ueberprueft und alle Namen werden auf dem
 Bildschirm ausgegeben.
 
  DEBUG_MEMORY_USAGE: Der ungefaehre Speicherverbraucht wird mit der
Zusammenfassung zusammen geschrieben.  */

int data_version;

/* ------------------------------------------------------------- */

int
cfopen (char *filename, char *mode)
{
	F = fopen (filename, mode);

	if (F == 0)
    {
		printf ("Ich kann die Datei '%s' nicht im %s-Modus oeffnen.\n", filename, mode);
		system("pause");
		return 0;
    }
	return 1;
}

int
skipline (void)
{
	int c=0;
	
	while (c != EOF && c != '\n')
		c = fgetc (F);
	return c;
}

void
getbuf (void)
{
	int i=0, j=0, c=0, old_c=0, quote=0;
	
	/* buf vorsichtshalber auf 0, i: position in buf, j: position in
	quote, quote: 1 wenn innerhalb eines Textes in "Gaensefuesschen",
	c: aktueller, gelesener char */
	buf[0] = 0;                   
	for (;;)
    {
		/* old_c wird verwendet, weil in buf anstelle von tabs, spaces
		und newlines immer ein SPACE_REPLACEMENT steht (wird am ende
		der Schlaufe gesetzt).  */
		old_c = c;
		c = fgetc (F);
		
		/* Nach einem Semikolon folgen Kommentare bis zum Zeilenende.  */
		if (c == COMMENT_CHAR && !quote)
        {
			buf[i] = 0;
			c = skipline ();
        }
		
		/* Falls die Zeile zu lange ist, wird der Text abgebrochen, und
		die Zeile zuende gelesen.  Ein ge-quoteter Text war
		wahrscheinlich zu lang (Gaensefuesschen vergessen oder
		zuviel).  Es wird wieder auf normalen Zeilenmodus
		umgeschalten (quote=0).  Nun wird in c entweder '\n' oder EOF
		stehen.  Bei Funktionen, welche ihren eigenen buf[]
		definieren, muss selber auf zu grosse strings geprueft werden
		-- hier werden strings bis zur Maximallaenge eingelesen.  */
		if (i == sizeof buf - 1)
        {
			buf[i] = 0;
			c = skipline ();
			quote = 0;
        }
		
		// folgende Zeile d�rfte f�r eine Menge Fehler verantwortlich sein
		// seit Runde 1o3 bzw. o3.1o.o4 werden Zeilen immer automatisch
		// mit einem Quote beendet ... d.h. das die Zeilen nicht mehr so einfach
		// umgebrochen werden k�nnen ... in PProzesser.exe wird der Slash
		// als Zeilenumbruch breits umgesetzt ... also wie bisher mit
		// einfach Zeile umbrechen ist nicht mehr
		if (c == '\n') quote = 0;

		/* Bricht die Datei mitten in der Zeile ab, wird die Zeile
		ignoriert.  Dieser Test muss nach allen skipline () Aufrufen
		stehen, da diese auch mit EOF enden koennen.  */
		if (c == EOF)
        {
			buf[0] = EOF;
			break;
        }
		
		/* Befehle werden durch Zeilenende beendet.  Quotes durch eine
		Leerzeile (dh. zwei Zeilenenden hintereinander).  Die Zeile
		wird - mit oder ohne quote - durch 0 oder ';' abgebrochen
		(normalerweise werden so Kommentare ignoriert).  */
		if ((c == '\n' && !quote) ||
			(c == '\n' && quote && old_c == '\n'))
        {
			buf[i] = 0;
			if (i > 0 && buf[i - 1] == SPACE_REPLACEMENT)
				buf[i - 1] = 0;
			break;
        }
		
		/* quotes werden mit `"' begonnen oder beenden */
		if (c == '"')
        {
			quote = !quote;
			if (quote)
				j = 0;
			continue;
        }
		
		/* whitespace - alle spaces und tabs nach spaces und tabs oder
		am Anfang der Zeile oder am Anfang von strings werden
		ignoriert.  */
		if ((isspace (c) && isspace (old_c)) ||
			(j == 0 && isspace (c)))
			continue;
		
		j++;          /* Textlaenge seit Zeilen oder string beginn.  */
		
					  /* In quotes werden alle spaces und tabs durch SPACE_REPLACEMENT
					  ersetzt, ansonsten wird c eingesetzt.  Ausserhalb von quotes
					  werden tabs durch spaces ersetzt, ansonsten wird auch c
		eingesetzt.  getstr etc operieren nur mit spaces. */
		if (isspace (c))
			buf[i++] = quote ? SPACE_REPLACEMENT : ' ';
		else
			buf[i++] = c;
    }
}

/* ------------------------------------------------------------- */

void
readunit (faction * f)
{
	int i;
	unit *u;
	strlist *S, **SP;
	
	if (!f) return;
	
	i = getib36 ();
	u = findunitglobal (i);
	
	if (u && u->faction == f)
    {
		/* SP zeigt nun auf den Pointer zur ersten StrListe.  Auf die
		erste StrListe wird mit u->orders gezeigt.  Deswegen also
		&u->orders, denn SP zeigt nicht auf die selbe stelle wie
		u->orders, sondern auf die stelle VON u->orders!  */
		SP = &u->orders;
		
		for (;;)
        {
			getbuf ();
			
			/* Erst wenn wir sicher sind, dass kein Befehl eingegeben
			wurde, checken wir, ob nun eine neue Einheit oder ein
			neuer Spieler drankommt.  */
			if (igetkeyword (buf) < 0)
			{
				switch (igetparam (buf))
				{
					case P_UNIT:
					case P_FACTION:
					case P_ERESSEA:
					case P_FANTASYA:
					case P_NEXT:
					case P_NEXT2:
					case P_REGION:
									return;
				}
			}
			
			if (buf[0] == EOF) return;
			
			if (buf[0])
            {
				/* Nun wird eine StrListe S mit dem Inhalt buf2
				angelegt.  S->next zeigt nirgends hin.  */
				S = makestrlist (buf);
				
				/* Nun werden zwei Operationen ausgefuehrt (addlist2 ist ein
				#definiertes macro!):
				
                *SP = S -- Der Pointer, auf den SP zeigte, zeigt nun
                *auf S! Also entweder u->orders oder S(alt)->next
                *zeigen nun auf das neue S!
				 
				SP = &S->next -- SP zeigt nun auf den Pointer
				S->next.  Nicht auf dieselbe Stelle wie S->next,
				sondern auf die Stelle VON S->next! */
				
				addlist2 (SP, S);
				
				/* Und das letzte S->next darf natuerlich nirgends mehr
				hinzeigen, es wird auf null gesetzt.  Warum das nicht
				bei addlist2 getan wird, ist mir schleierhaft.  So
				wie es jetzt implementiert ist, kann man es (hier auf
				alle Faelle) hinter addlist2 schreiben, oder auch nur
				am Ende einmal aufrufen - das waere aber bei allen
				returns, und am ende des for (;;) blockes.  Grmpf.
				Dann lieber hier, immer, und dafuer sauberer... */
				
				*SP = 0;
            }
        }
    } else
    {
		if (u)
		{
			sprintf (buf, "Die Einheit %s gehoert uns nicht.", tobase36(i) );
			printf("Einheit %s ... lt. Spielstand %s ... intern", unitid(u), factionid(u->faction));
			printf(" %s\n", factionid(f));
			addstrlist (&f->mistakes, buf);
		} else
		{
			// was ganz komisches passiert hier ... und zwar wird irgendwie das N�CHSTER nie erkannt
			// danach kann ja noch was kommen ... meist Werbung von den Freemailern ... zum Beispiel
			// "Ein Grund...." ... das "Ein" wird als Einheit interpretiert ... und "Grund" als Nummer
			// dadurch entsteht eine Endloschleif ... deshalb wird jetzt hier die n�chste Zeile eingelesen
			printf("leere Einheit - '%s'\n", buf);
			getbuf();
		}
    }
}

/* ------------------------------------------------------------- */

faction *
readfaction (faction *oldfaction)
{
	int i;
	faction *f;
	region *r;
	unit *u;
	
	i = getib36 ();
	if (i == 0)
	{
		// wenn jetzt statt "tarne partei 0" nur ein "partei 0" steht , wird auf die
		// Monsterpartei gewechselt
		printf("\nwechsel von Partei %s auf Partei 0 verhindert\n", factionid(oldfaction));
		return oldfaction;
	}
	f = findfaction (i);
	
	if (f)
    {
	/* Kontrolliere, ob das Passwort richtig eingegeben wurde.  Es
		muss in "Gaensefuesschen" stehen!!  */
		if (f->passw && strcmp (f->passw, getstr ()))
        {
			addstrlist (&f->mistakes, "Das Passwort wurde falsch eingegeben");
			printf("\tfalsches Passwort f�r \"%s\"\n", factionid(f));
			return 0;
        }
		
		/* Loesche alle alten Befehle, falls schon welche eingeben
		worden sind.  */
		/* Kommentar von Bjoern Becker : Ausserdem die am 20.07.2000 eingef�hrten Kommentare l�schen */
		
		for (r = regions; r; r = r->next)
			for (u = r->units; u; u = u->next)
				if (u->faction == f)
				{
					freelist (u->orders);
					u->orders = 0;
					/* Added by Bjoern Becker , 20.07.2000 */
					
					freelist (u->comments);
					u->comments = 0;
					
					/* end of changes */
				}
				
				/* Die Partei hat sich zumindest gemeldet, so dass sie noch
				nicht als untaetig gilt.  */
				f->lastorders = turn + 1; // turn wird erst anschlie�end erh�ht
    }
	else
		printf (" -- Befehle fuer die ungueltige Partei Nr. %s.\r\n"
		"    Befehle:     ", tobase36(i) );
	
	return f;
}

/* ------------------------------------------------------------- */

int
readorders (void)
{
	faction *f;
	struct stat fs;
	
	/* Der Name der Befehlsdatei muss in buf drinnen stehen */
	
	if (!cfopen (buf, "r")) return 0;
	stat (buf, &fs);
	
	puts ("- lese Befehlsdatei...");
	
	getbuf ();
	
	/* Auffinden der ersten Partei, und danach abarbeiten bis zur
	letzten Partei.  Gleichzeitig wird ein Indicator angezeigt.  Wie
	immer kann der Indicator problemlos entfernt werden.  */
	printf ("    Befehle: ");
	indicator_reset (fs.st_size);
	
	f = 0;
	while (buf[0] != EOF)
		switch (igetparam (buf))
	{
	/* In readfaction wird nur eine Zeile gelesen: Diejenige mit
	dem Passwort.  Die befehle der units werden geloescht, und
		die Partei wird als aktiv vermerkt. */
	case P_FACTION:
	case P_FANTASYA:
	case P_ERESSEA:
        f = readfaction (f);	// es muss die alte Partei �bergeben werden
        getbuf ();
        break;
		
        /* Falls in readunit abgebrochen wird, steht dort entweder
		eine neue Partei, eine neue Einheit oder das Fileende.  Das
		switch wird erneut durchlaufen, und die entsprechende
		Funktion aufgerufen.  Man darf buf auf alle Faelle nicht
		ueberschreiben!  Bei allen anderen Eintraegen hier, muss
		buf erneut gefuellt werden, da die betreffende Information
		in nur einer Zeile steht, und nun die naechste gelesen
		werden muss.  */
	case P_UNIT:
        if (f) readunit (f); else getbuf ();
        indicator_count_up (ftell (F));
		break;
		
	case P_NEXT:
	case P_NEXT2:
        f = NULL;
	default:
        getbuf ();
	}
	indicator_done ();
	putchar ('\n');
	
	fclose (F);
	return 1;
}

/* ------------------------------------------------------------- */

int nextc;

void
rc (void)
{
	nextc = fgetc (F); // printf("%c",nextc);
}

/* Read a string from file F into array s.  No memory allocation!  */
void
rs (char *s)
{
	while (nextc != '"')
    {
		if (nextc == EOF)
        {
			puts ("Der Datenfile bricht vorzeitig ab.(rs\\1)");
			exit (1);
        }
		
		rc ();
    }
	
	rc ();
	
	while (nextc != '"')
    {
		if (nextc == EOF)
        {
			puts ("Der Datenfile bricht vorzeitig ab.(rs\\2)");
			exit (1);
        }
		
		*s++ = nextc;
		rc ();
    }
	
	rc ();
	*s = 0;
}

/* Read a string from file F to pointer s.  Allocate memory for s.  */
void
mrs (char **s)
{
	rs (buf);
	if (buf[0])
		mstrcpy (s, buf);
}

/* Read a, int from file F and return it.  */
int
ri (void)
{
	int i;
	char buf[20];
	
	i = 0;
	
	while (!xisdigit (nextc))
    {
		if (nextc == EOF)
        {
			puts ("Der Datenfile bricht vorzeitig ab. (ri)");
			exit (1);
        }
		
		rc ();
    }
	
	while (xisdigit (nextc))
    {
		buf[i++] = nextc;
		rc ();
    }
	
	buf[i] = 0;
	return atoi (buf);
}

/* Liest eine strlist von Diskette (zB. die Meldungen fuer eine
Partei) und speichert sie am Ende der strliste SP ab.  */
void
rstrlist (strlist ** SP)
{
	int n;
	strlist *S;
	
	n = ri ();
	
	while (--n >= 0)
    {
		rs (buf);
		S = makestrlist (buf);
		addlist2 (SP, S);
    }
	
	*SP = 0;
}

/* Liest eine strlist und vergisst sie sofort wieder (lean mode).  */
void
skipstrlist ()
{
	int n;
	n = ri ();
	while (--n >= 0)
		rs (buf);
}

void
number_space_free ()
{
	int i, m;
	
	puts ("Freie Parteien:");
	m=0;
	for (i=0; i!=5; i++)
    {
		do
		m++;
		while (findfaction (m));
		printf (" %s", tobase36(m) );
    }
	
	puts ("\nFreie Burgen:");
	m=0;
	for (i=0; i!=5; i++)
    {
		do
		m++;
		while (findbuilding (m));
		printf (" %s", tobase36(m) );
    }
	
	puts ("\nFreie Schiffe:");
	m=0;
	for (i=0; i!=5; i++)
    {
		do
		m++;
		while (findship (m));
		printf (" %s", tobase36(m) );
    }
	
	puts ("\nFreie Einheiten:");
	m=0;
	for (i=0; i!=5; i++)
    {
		do
		m++;
		while (findunitglobal (m));
		printf (" %s", tobase36(m) );
    }
	putchar ('\n');
}

void
readgame (int lean)
{
	int i, n, p, error=0, region_will_be_deleted=0;
	faction *f, **fp;
	rfaction *rf, **rfp;
	region *r, *r2, **rp;
	building *b, **bp;
	ship *sh, **shp;
	unit *u, **up;
	int dummy;
	int tempi = 0;	// Tempor�re INT
	
	return;			// wir laden auch nicht mehr

	dummy=0;
	
	sprintf (buf, "data/%d", turn);
	if (!cfopen (buf, "r")) exit (1);
	rc ();
	
	/* Globale Variablen.  */
	
	data_version = ri ();
	if (data_version == V621) data_version = V63; /* Added by Bjoern Becker , 27.07.2000 */
	turn = ri ();
	printf ("- Version: %d.%d, Runde %d.\n", data_version / 10 , data_version % 10 , turn);
	highest_unit_no = 0;
	
	/* Read factions.  */
	
	n = ri ();
	fp = &factions;
	
	/* fflush (stdout); */
	printf ("    Parteien: ");
#ifndef DEBUG_SAVED_GAME
	indicator_reset (n);
#endif      
	
	while (--n >= 0)
    {
#ifndef DEBUG_SAVED_GAME
		indicator_count_down (n);
#endif      
#ifdef DEBUG_SAVED_GAME
		indicator_tick ();
#endif      
		
		f = cmalloc (sizeof (faction));
		memset (f, 0, sizeof (faction));
		
		f->no = ri ();
		mrs (&f->name);
		mrs (&f->addr);
		mrs (&f->passw);
		f->lastorders = ri ();
		if (data_version > V24)
		{
			f->newbie = ri ();
			f->newbie = 0;		// Newbies l�schen
			f->old_value = ri ();
		}
		if (data_version > V50)
			f->language = ri ();
		
		/* Added by Bjoern Becker , 28.07.2000 */	
		
		if (data_version > V63)
		{
			f->race = ri ();
			f->xorigin = ri ();
			f->yorigin = ri ();
			// if (data_version > V68)
			// {
			//	f->alter = ri();
			// }
		}
		else
		{
			f->race = 1;
			f->xorigin = rand() % 20;
			f->xorigin = rand() % 20;
			f->alter = 1;
		};
		
		/* End of Changes */
		
		f->options = ri ();
		
		for (i = 0; i != MAXSPELLS; i++)
			if (data_version >= V60 || i <= SP_12)
				/* Ab 6.0 gibt es SP_13 bis SP_17 neu. */
				f->showdata[i] = ri ();
			
			p = ri ();
			rfp = &f->allies;
			
			while (--p >= 0)
			{
				rf = cmalloc (sizeof (rfaction));
				rf->factionno = ri ();
				addlist2 (rfp, rf);
			}
			
			*rfp = 0;
			
			if (lean)
			{
				skipstrlist (/* &f->mistakes */);
				skipstrlist (/* &f->warnings */);
				skipstrlist (/* &f->messages */);
				skipstrlist (/* &f->battles */);
				skipstrlist (/* &f->events */);
				skipstrlist (/* &f->income */);
				skipstrlist (/* &f->commerce */);
				skipstrlist (/* &f->production */);
				skipstrlist (/* &f->movement */);
				skipstrlist (/* &f->debug */);
			}
			else
			{
				rstrlist (&f->mistakes);
				rstrlist (&f->warnings);
				rstrlist (&f->messages);
				rstrlist (&f->battles);
				rstrlist (&f->events);
				rstrlist (&f->income);
				rstrlist (&f->commerce);
				rstrlist (&f->production);
				rstrlist (&f->movement);
				rstrlist (&f->debug);
			}
#ifdef DEBUG_SAVED_GAME
			if (findfaction (f->no))
			{
				printf ("\nPartei Nr. %s kommt doppelt vor.\n-", tobase36(f->no) );
				error=1;
			}
#endif      
			addlist2 (fp, f);
    }
	putchar ('\n');
	
	*fp = 0;
	
	/* Regionen */
	n = ri ();
	assert (n);
	rp = &regions;
	
	printf ("    Regionen: ");
#ifndef DEBUG_SAVED_GAME
	indicator_reset (n);
#endif      
	
	while (--n >= 0)
    {
		
#ifndef DEBUG_SAVED_GAME
		indicator_count_down (n);
#endif      
		
		r = cmalloc (sizeof (region));
		memset (r, 0, sizeof (region));
		
		r->x = ri ();
		r->y = ri ();
		if (data_version < V60) r->z = 0; else r->z = ri ();
		mrs (&r->name);
		mrs (&r->display);
		r->terrain = ri ();
		r->trees = ri ();
		r->horses = ri ();
		r->peasants = ri ();
		r->money = ri ();
		// hier einfach auf die Richtung NO gesetzt ... ist ein FEHLER!!!!
		// bin nur zu faul ... mein bl�der Fisch mal wieder ... sollte aber
		// hier egal sein, da das die alte Funktion zum einlesen ist
		r->road[0] = ri ();
		if (data_version > V68)
		{
		/*r->alter = ri();
		r->movement = ri();
		r->kraut = ri();
			r->kraut2 = ri();*/
		} else
		{
		}
		
		assert (r->x == sphericalx (r->x,r->z));
		assert (r->y == sphericaly (r->y,r->z));
		assert (r->terrain >= 0);
		assert (r->trees >= 0);
		assert (r->horses >= 0);
		assert (r->peasants >= 0);
		assert (r->money >= 0);
		assert (r->road >= 0);
		
		/* Test ob die Region doppelt vorkommt.  Falls ja: Einheiten,
		Burgen und Schiffe dieser Region mit der bestehenden Region
		zusammenlegen.  Dies wurde eingebaut, um den Bug mit dem
		unnoetigen Aufrufen von makeblock aus show_map heraus
		auszubuegeln.  */
		region_will_be_deleted = 0;
		r2 = findregion (r->x, r->y, r->z);
		if (r2)
		{
			printf ("%s (%d) kommt doppelt vor.\n", regionid(r ,0 ,0), r->z);
			printf ("Sie wird mit %s zusammengelegt.\n", regionid(r2 , 0 , 0));
			/* Das uebertragen von Dingen von einer Region in die andere
			waerend dem Lesen wuerde einige Aenderungen am
			bestehenden code erfordern.  Deswegen dieses unsaubere
			Loesung mit dem region_will_be_deleted.  Dafuer lassen
			sich auch alle Aenderungen leicht lokalisieren.  */
			region_will_be_deleted = 1;
		}
		else
		{
		/* Die Region kommt nicht in die Liste aller Regionen, damit
		sie noch in dieser Funktion wieder entfernt werden kann.
		Nach dem Lesen aller nachfolgender Burgen, Schiffe und
		Einheiten werden nachdem zB. alle Burgen der Region r
		gelesen wurden diese von r zu r2 transferiert, so dass
		ganz am Ende die Region r ohne Zugemuese dasteht und
			geloescht werden kann.  */
			addlist2 (rp, r);
		}
		
		for (i = 0; i != MAXLUXURIES; i++)
		{
			r->demand[i] = ri ();
			if (data_version < V40)
			{
				if (r->demand[i])
					r->demand[i] *= 100;
				else
				{
					r->demand[i] = 100;
					r->produced_good = i;
				}
			}
		}
		if (data_version >= V40)
			r->produced_good = ri ();
		
		rstrlist (&r->comments);
		rstrlist (&r->debug);
		
		/* Burgen */
		p = ri ();
		bp = &r->buildings;
		
		while (--p >= 0)
        {
			b = cmalloc (sizeof (building));
			memset (b, 0, sizeof (building));
			
			b->no = ri ();
			mrs (&b->name);
			mrs (&b->display);
			b->size = ri ();
			
#ifdef DEBUG_SAVED_GAME
			if (findbuilding (b->no))
			{
				printf ("\nBurg Nr. %s kommt doppelt vor.\n-", tobase36(b->no) );
				error=1;
			}
#endif      
			addlist2 (bp, b);
        }
		
		*bp = 0;
		
		/* Burgen von r in die urspruengliche Region r2 bewegen.  */ 
		if (region_will_be_deleted)
			for (;r->buildings;)
				translist (&r->buildings, &r2->buildings, r->buildings);
			
			/* Schiffe */
			p = ri ();
			shp = &r->ships;
			
			while (--p >= 0)
			{
				sh = cmalloc (sizeof (ship));
				memset (sh, 0, sizeof (ship));
				
				sh->no = ri ();
				mrs (&sh->name);
				mrs (&sh->display);
				sh->type = ri ();
				sh->left = ri ();
				
#ifdef DEBUG_SAVED_GAME
				if (findship (sh->no))
				{
					printf ("\nSchiff Nr. %s kommt doppelt vor.\n-", tobase36(sh->no) );
					error=1;
				}
#endif      
				addlist2 (shp, sh);
			}
			
			*shp = 0;
			
			/* Schiffe von r in die urspruengliche Region r2 bewegen.  */ 
			if (region_will_be_deleted)
				for (;r->ships;)
					translist (&r->ships, &r2->ships, r->ships);
				
				/* Einheiten */
				p = ri ();
				up = &r->units; /* up zeigt immer auf die letzte Einheit.  */
				
				while (--p >= 0)
				{
#ifdef DEBUG_SAVED_GAME
					indicator_tick ();
#endif 
					u = cmalloc (sizeof (unit));
					memset (u, 0, sizeof (unit));
					
					u->no = ri ();
					if (u->no > highest_unit_no) highest_unit_no = u->no;
					mrs (&u->name);
					mrs (&u->display);
					u->number = ri ();
					u->type = ri ();
					u->items[I_SILVER] = ri ();
					u->effect = ri ();
					if (data_version < V24 && u->type != U_ILLUSION) u->effect = 0;
					if (data_version >= V24) u->enchanted = ri ();
					u->faction = findfaction (ri ());
					u->building = findbuilding (ri ());
					u->ship = findship (ri ());
					u->owner = ri ();
					u->status = ri ();
					u->guard = ri ();
					
					/* Default Befehle gibt es nur fuer Menschen!  rs (buf)
					bedeuted, dass dass der gelesene String einfach
					weggeworfen wird.  Dort sollte sowieso nichts stehen (bei
					Monstern).  */
					if (u->type == U_MAN)
						mrs (&u->lastorder);
					else
						rs (buf);
					if (data_version >= V32)
					{
						if (u->type == U_MAN)
							mrs (&u->thisorder2);
						else
							rs (buf);
					}
					
					u->combatspell = ri ();
					
					/*
					* Fantasya
					*/
					
					if (data_version > V63)
					{
						u->race = ri ();		// eigentlich noch GA 6.6
						u->raceTT = ri ();		// aber hier
					}
					else u->race = 1;
					if (data_version > V66)
					{
						mrs (&u->prefix);
						u->tarn_no = ri();
						u->tarnung = ri();
					} else
					{
						// Prefix wird nicht ge�ndert
						u->tarn_no = u->faction->no;	// als eigene Partei ausgeben
						u->tarnung = 1;				// sofern Einheit das Talent hat
					}
					
					
					// Beschreibungen ??
					if (data_version > V62) rstrlist ( &u->comments );
					
					/* end of changes */
					
#ifdef DEBUG_SAVED_GAME
					if (findunitglobal (u->no))
					{
						printf ("\nEinheit Nr. %s (%s) kommt doppelt vor.\n-", u->name, tobase36(u->no) );
						error=1;
					}
#endif      
					
					assert (u->number >= 0);
					assert (u->items[I_SILVER] >= 0);
					assert (u->type >= 0);
					assert (u->effect >= 0);
					
					// Talente
					if (data_version <= V68)
					{
						for(i=0; i != MAXSKILLS; i++) u->skills[i] = ri();
					}
					
					// Items & Co.
					for (i = 0; i != MAXITEMS; i++)
					{
						if (data_version <= V65)
						{
							if (i == I_SILVER)
							{
							}
							else
							{
								u->items[i] = ri ();
							};
						}
						else
						{
							u->items[i] = ri ();
						};
					};
					
					for (i = 0; i != MAXSPELLS; i++)
						if (data_version >= V60 || i <= SP_12)
							/* Ab 6.0 gibt es SP_13 bis SP_17 neu. */
							u->spells[i] = ri ();
						
						if (data_version < V20 && u->owner && r->units)
						{
							/* Eigentuemer am Anfang, falls sie nicht am Anfang sind */
							
							u->next = r->units;
							r->units = u;
						}
						else
							addlist2 (up, u);
        }
		
		*up = 0;
		
		/* Einheiten von r in die urspruengliche Region r2 bewegen.  */ 
		if (region_will_be_deleted)
			for (;r->units;)
				translist (&r->units, &r2->units, r->units);
			
			/* Speicher fuer die unbenoetigte Region freigeben.  */
			if (region_will_be_deleted)
			{
				assert (r->units == 0);
				assert (r->buildings == 0);
				assert (r->ships == 0);
				printf("%s wurde wieder entfernt.\n", regionid(r ,0 ,0));
				free (r);
			}
			
    }
	putchar ('\n');
	
	*rp = 0;
	
	/* Link rfaction structures */
	
	puts ("- Daten der Parteien durchgehen...");
	
	for (f = factions; f; f = f->next)
		for (rf = f->allies; rf; rf = rf->next)
			rf->faction = findfaction (rf->factionno);
		
		for (r = regions; r; r = r->next)
		{
			/* Initialize faction seendata values */
			
			for (u = r->units; u; u = u->next)
				for (i = 0; i != MAXSPELLS; i++)
					if (u->spells[i])
						u->faction->seendata[i] = 1;
					
					/* Check for alive factions */
					
					for (u = r->units; u; u = u->next)
						u->faction->alive = 1;
					
		}
		
		connectregions ();
		fclose (F);
		
#ifdef DEBUG_SAVED_GAME
		number_space_free ();
#endif      
		assert (!error);
}

/* ------------------------------------------------------------- */

long value (faction *f)
{
	long n;
	
	n = (f->number * RecruitCost[f->race] + f->money);
	if (!n) n = f->old_value;
	return n;
}

int
growth (faction *f)
{
	int n;
	
	n = 100 * (value (f) - f->old_value);
	if (f->old_value)
		return (n / f->old_value);
	else if (value (f))
		return (n / value (f));
	else
		return n;
}
/* ------------------------------------------------------------- */

void
wc (int c)
{
	fputc (c, F);
}

void
wsn (char *s)
{
	while (*s) wc (*s++);
}

void
wnl (void)
{
	wc ('\n');
}

void
wspace (void)
{
	wc (' ');
}

void
ws (char *s)
{
	char db[1000];
	_snprintf(db, 950, "%s", s);
	wc ('"');
	if (s) wsn (s);
	wc ('"');
}

void
wi (int n)
{
	sprintf (buf, "%d", n);
	wsn (buf);
}

void
wstrlist (strlist * S)
{
	wi (listlen (S));
	wnl ();
	
	while (S)
    {
		ws (S->s);
		wnl ();
		S = S->next;
    }
}


/* ------------------------------------------------------------- */

void
output_addresses (void)
{
	faction *f;
	
	/* adressen liste */
	
	for (f = factions; f; f = f->next)
    {
		rparagraph (factionid (f), 0, 0);
		if (f->addr)
			rparagraph (f->addr, 4, 0);
		fputs ("\n", F);
    }
}

void output_playersini (void)
{
	faction *f;
	char outbuffer[100];
	
	/* adressen liste */
	fputs ("[Players]\n" , F);
	for (f = factions; f; f = f->next)
    {
		if (f->no != 0)
		{
			sprintf (outbuffer, "%s", tobase36(f->no) );
			fputs (outbuffer , F);
			fputs ( "=" , F);
			fputs ( f->addr , F);
			fputs ( "\n" , F);
		};
    };
	/* passwort liste */
	fputs ("[Passwords]\n" , F);
	for (f = factions; f; f = f->next)
    {
		if (f->no != 0)
		{
			sprintf (outbuffer, "%s", tobase36(f->no) );
			fputs (outbuffer , F);
			fputs ( "=" , F);
			fputs ( f->passw , F);
			fputs ( "\n" , F);
		};
    }
	fprintf(F,"\n");
}

void output_newsletter (void)
{
	faction *f;
	
	/* adressen liste */
	for (f = factions; f; f = f->next)
    {
		if (f->no != 0)
		{
			fputs ( f->addr , F);
			fputs ( "\n" , F);
		};
    };
}

void
showaddresses ()
{
	F = stdout;
	output_addresses ();
}

void output_Verteilerliste()
{
	faction *f = factions;

	fprintf(F, "\\TITLE Fanta Mailverteiler\n");
	fprintf(F, "\\REPLYTO fragen@fantasya-pbem.de\n");
	fprintf(F, "\\SIGNATURE 1\n");
	fprintf(F, "\n");

	for(f = factions; f; f = f->next)
	{
		fprintf(F, "%s\n", f->addr);
	}
}

void writeaddresses ()
{
	sprintf (buf, "reports/adressen");
	if (!cfopen (buf, "w")) return;
	printf ("Schreibe Liste der Adressen (%s)...\n", buf);
	output_addresses ();
	fclose (F);
	
	// Added by Bjoern Becker , 15.08.2000
	
	sprintf (buf , "players.ini");
	if (!cfopen (buf, "w")) return;
	printf ("Schreibe players.ini ...\n");
	output_playersini ();
	//NewPlayersIni();
	fclose (F);
	
	sprintf (buf,  "newsletter.dat");
	if (!cfopen (buf, "w")) return;
	printf ("Schreibe newsletter.dat ...\n");
	output_newsletter ();
	fclose (F);
	
	sprintf (buf,  "fantasya.pml");
	if (!cfopen (buf, "w")) return;
	printf ("Schreibe Verteilerliste.dat ...\n");
	output_Verteilerliste();
	fclose (F);
	
	// End of changes
}

long int
mem_str (char *s)
{
	return s ? strlen (s) : 0;
}

long int
mem_strlist (strlist *S)
{
	long int m=0;
	while (S)
    {
		m += mem_str (S->s);
		S = S->next;
    }
	return m;
}

#define DEBUG_MEMORY_USAGE	/* ick will alles wissen */

void // hier ist irgendwo ein Fehler drinnen
writesummary (void)			// Changed by Bjoern Becker , 16.08.2000
{
	int inhabitedregions=0;
	int civilregions=0;
	int buildregions=0;
	int peasants=0;
	int peasantmoney=0;
	int nunits=0;
	int playerpop=0;
	int playermoney=0;
	int armed_men=0;
	int i, nmrs[ORDERGAP], newbies=0;
	int race[MAXRACES];		// Rassen z�hlen
	faction *f;
	faction *hf;
	region *r, *r2;
	unit *u;
#ifdef DEBUG_MEMORY_USAGE
	long int mem_factions=0;
	long int mem_regions=0;
	long int mem_buildings=0;
	long int mem_ships=0;
	long int mem_units=0;
	long int mem_orders=0;
	runit *ru;
	building *b;
	ship *sh;
	rfaction *rf;
#endif
	
	sprintf (buf, "reports/Abschluss ZAT.dat");
	if (!cfopen (buf, "w")) return;
	printf ("Schreibe Zusammenfassung (%s)... ", buf);

	for (f = factions; f; f = f->next)
    {
		indicator_tick ();
		f->nregions = 0;
		f->nunits = 0;
		f->number = 0;
		f->money = 0;
#ifdef DEBUG_MEMORY_USAGE
		mem_factions += sizeof (faction);
		mem_factions += mem_str (f->name);
		mem_factions += mem_str (f->addr);
		mem_factions += mem_str (f->passw);
		for (rf = f->allies; rf; rf = rf->next) mem_factions += sizeof (rfaction);
		mem_factions += mem_strlist (f->mistakes);
		mem_factions += mem_strlist (f->warnings);
		mem_factions += mem_strlist (f->messages);
		mem_factions += mem_strlist (f->battles);
		mem_factions += mem_strlist (f->events);
		mem_factions += mem_strlist (f->income);
		mem_factions += mem_strlist (f->commerce);
		mem_factions += mem_strlist (f->production);
		mem_factions += mem_strlist (f->movement);
		mem_factions += mem_strlist (f->debug);
#endif
    }
	
	// Regionen durchgehen
	for (r = regions; r; r = r->next)
    {
		// Kleiner Test um zu kontrollieren, dass keine falschen Regionen entstanden sind.
		r2 = findregion (r->x, r->y, r->z);
		if (r2 != r) printf ("\n%s (%d,%d,%d) kommt doppelt vor.", regionid(r, 0 ,0), r->x, r->y, r->z);
		
		// Bauern und Einheiten zaehlen.
		if (r->peasants || r->units)
		{
			indicator_tick ();
			inhabitedregions++;
			peasants += r->peasants;
			peasantmoney += r->money;
			
#ifdef DEBUG_MEMORY_USAGE
			mem_regions += sizeof (region);
			mem_regions += mem_str (r->name);
			mem_regions += mem_str (r->display);
			mem_regions += mem_strlist (r->comments);
			mem_regions += mem_strlist (r->debug);
			for (sh = r->ships; sh; sh = sh->next)
			{
				mem_ships += sizeof (ship);
				mem_ships += mem_str (sh->name);
				mem_ships += mem_str (sh->display);
			}
			for (b = r->buildings; b; b = b->next)
			{
				mem_buildings += sizeof (building);
				mem_buildings += mem_str (b->name);
				mem_buildings += mem_str (b->display);
			}
#endif
			
			// nregions darf nur einmal pro Partei per Region incrementiert werden.
			for (f = factions; f; f = f->next) f->dh = 0;
			for (u = r->units; u; u = u->next)
			{
				nunits++;
				playerpop += u->number;
				playermoney += u->items[I_SILVER];
				armed_men += armedmen (u);
				
				u->faction->nunits++;
				u->faction->number += u->number;
				u->faction->money += u->items[I_SILVER];
				
				if (!u->faction->dh) u->faction->nregions++;
				u->faction->dh = 1;
				
#ifdef DEBUG_MEMORY_USAGE
				mem_units += sizeof (unit);
				mem_units += mem_str (u->name);
				mem_units += mem_str (u->display);
				for (ru = u->contacts; ru; ru = ru->next) mem_units += sizeof (runit);
				mem_orders += mem_strlist (u->orders);
#endif
			}
		}
    }
	for(i=0;i<MAXRACES;i++) race[i]=0;
	for(hf=factions;hf;hf=hf->next) race[hf->race]++;

	fprintf (F, "Zusammenfassung fuer Fantasya %s\n\n", gamedate (findfaction (0)));
	
	fprintf (F, "Regionen:\t\t%d\n", listlen (regions));
	fprintf (F, "Bewohnte Regionen:\t%d\n\n", inhabitedregions);
	
	fprintf (F, "Parteien:\t\t%d\n", listlen (factions));
	fprintf (F, "Einheiten:\t\t%d\n\n", nunits);
	
	fprintf (F, "Spielerpopulation:\t%d\n", playerpop);
	fprintf (F, " davon bewaffnet:\t%d\n", armed_men);
	fprintf (F, "Bauernpopulation:\t%d\n", peasants);
	fprintf (F, "Population gesamt:\t%d\n\n", playerpop + peasants);
	
	fprintf (F, "Reichtum Spieler:\t$%d\n", playermoney);
	fprintf (F, "Reichtum Bauern:\t$%d\n", peasantmoney);
	fprintf (F, "Reichtum gesamt:\t$%d\n\n", playermoney + peasantmoney);

	for(i=1; i<MAXRACES-1; i++)
	{
		fprintf(F," %15s:     %3d V�lker\n",racenames[i][1],race[i]);
	}
	fprintf(F,"\n");
	
#ifdef DEBUG_MEMORY_USAGE
	fputs ("Belegter Speicher\n\n", F);
	fprintf (F, "fuer Parteien:\t\t%ld\n", mem_factions);
	fprintf (F, "fuer Regionen:\t\t%ld\n", mem_regions);
	fprintf (F, "fuer Burgen:\t\t%ld\n", mem_buildings);
	fprintf (F, "fuer Schiffe:\t\t%ld\n", mem_ships);
	fprintf (F, "fuer Einheiten:\t\t%ld\n", mem_units);
	fprintf (F, "fuer Befehle:\t\t%ld\n\n", mem_orders);
#endif

	// NMRs und Newbies zaehlen.
	for (i = 0; i != ORDERGAP; i++) nmrs[i] = 0;
	newbies = 0;
	for (f = factions; f; f = f->next)
    {
		if (f->newbie)
			newbies++;
		else
		{
			int nt = turn - f->lastorders;
			//nmrs[nt]++;
		}
    }
	for (i = 0; i != ORDERGAP; i++) fprintf (F, "%d %s\t\t\t%d\n", i, i != 1 ? "NMRs:" : "NMR: ", nmrs[i]);
	fprintf (F, "Newbies:\t\t%d\n", newbies);
	
	if (factions) fprintf (F, "\nNr.\tReg.\tEinh.\tPers.\tSilber\tWert\tZuwachs\tNMRs\n\n");
	for (f = factions; f; f = f->next) fprintf (F, "%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", tobase36(f->no), f->nregions, f->nunits, f->number, f->money, value (f), growth (f), f->newbie ? -1 : (turn - f->lastorders));

	fclose (F);
	putchar ('\n');

	inhabitedregions = 0;
	peasants = 0;
	peasantmoney = 0;
	nunits = 0;
	playerpop = 0;
	playermoney = 0;
	armed_men = 0;
	newbies = 0;
	
	sprintf (buf, "reports/Statistik.dat");
	if (!cfopen (buf, "w")) return;
	printf ("Schreibe Zusammenfassung (%s)... ", buf);
	
	for (f = factions; f; f = f->next)
    {
		indicator_tick ();
		f->nregions = 0;
		f->nunits = 0;
		f->number = 0;
		f->money = 0;
		//f->einkommen = 0;
    }
	
	// Regionen durchgehen
	for (r = regions; r; r = r->next)
    {
    	// Kleiner Test um zu kontrollieren, dass keine falschen Regionen entstanden sind.
		r2 = findregion (r->x, r->y, r->z);
		if (r2 != r) printf ("\n%s (%d,%d,%d) kommt doppelt vor.", regionid(r, 0 ,0), r->x, r->y, r->z);
		
		// Bauern und Einheiten zaehlen.
		if (r->units) civilregions++;
		if (r->buildings) buildregions++;
		
		if (r->peasants || r->units)
		{
			indicator_tick ();
			inhabitedregions++;
			peasants += r->peasants;
			peasantmoney += r->money;
			
			// nregions darf nur einmal pro Partei per Region incrementiert werden.
			for (f = factions; f; f = f->next) f->dh = 0;
			for (u = r->units; u; u = u->next)
			{
				if (u->faction->no != 0)
				{
					nunits++;
					playerpop += u->number;
					playermoney += u->items[I_SILVER];
					armed_men += armedmen (u);
				};
				
				u->faction->nunits++;
				u->faction->number += u->number;
				u->faction->money += u->items[I_SILVER];
				
				if (!u->faction->dh) u->faction->nregions++;
				u->faction->dh = 1;
			}
		}
    }
	
	fprintf (F, "Zusammenfassung fuer Fantasya %s\n\n", gamedate (findfaction (0)));
	
	fprintf (F, "Bewohnte Regionen:\t%d\n", inhabitedregions);
	fprintf (F, "davon zivilisiert:\t%d\n", civilregions);
	fprintf (F, " und davon bebaut:\t%d\n\n",buildregions);
	
	fprintf (F, "         Parteien:\t%d\n", listlen (factions)-1);
	fprintf (F, "        Einheiten:\t%d\n\n", nunits);
	
	fprintf (F, "Spielerpopulation:\t%d\n", playerpop);
	fprintf (F, "  davon bewaffnet:\t%d\n", armed_men);
	fprintf (F, " Bauernpopulation:\t%d\n", peasants);
	fprintf (F, "Population gesamt:\t%d\n\n", playerpop + peasants);
	
	for(i=1; i<MAXRACES-1; i++)
	{
		fprintf(F,"  %15s:     %3d V�lker\n",racenames[i][1],race[i]);
	}
	fprintf(F,"\n");
	
	fprintf (F, " Reichtum Spieler:\t$%d\n", playermoney);
	fprintf (F, "  Reichtum Bauern:\t$%d\n", peasantmoney);
	fprintf (F, "  Reichtum gesamt:\t$%d\n\n", playermoney + peasantmoney);
	
	// NMRs und Newbies zaehlen.
	for (i = 0; i != ORDERGAP; i++) nmrs[i] = 0;
	newbies = 0;
	for (f = factions; f; f = f->next)
    {
		if (f->no != 0)
		{
			if (f->newbie)
				newbies++;
			else
			{
				int nt = turn - f->lastorders;
				if (nt <0)			printf("\t->nt kleiner als 0");
				if (nt > ORDERGAP)	printf("\t->nt groesser als ORDERGAP");
				//nmrs[nt]++;
			}
		};
    }
	for (i = 0; i != ORDERGAP; i++) fprintf (F, "%d %s\t\t\t%d\n", i, i != 1 ? "NMRs:" : "NMR: ", nmrs[i]);
	fprintf (F, "Newbies:\t\t%d\n", newbies);
	
	fclose (F);
	putchar ('\n');
}

/* ------------------------------------------------------------- */

void initgame (int lean)
{
	int max_turn = -1;		//	changed by Torsten Steigner, 31.07.00
	int						dp;
	long					handle;
	struct _finddata_t		data_t;
	
	dp=_chdir("data\\");
	
	if (dp == 0)
	{
		handle=_findfirst("*.*", &data_t);
		while (_findnext(handle, &data_t)==0)
			if (atoi (data_t.name) > max_turn && data_t.name[0] >= '0'
				&& data_t.name[0] <= '9')
				max_turn=atoi(data_t.name);
			_findclose(handle);
			_chdir("..");
	}
	else
		_mkdir("data\\");
	
		/* Falls turn schon gesetzt wurde (turn != -1), wird versucht, den
		file mit der Nr. turn zu lesen, ansonsten wird file Nr. max_turn
		verwendet, also der Letzte. Bei Fehlern waehrend dem Lesen des
		Spieles in readgame () wird mit exit (1) beendet. Vgl. auch
		Funktionen rs () und ri (), deswegen brechen wir hier bei einem
	Fehler auch mit exit (1) ab. */
	
	/* Test, ob es ueberhaupt die verlangete Nr. turn geben kann */
	if (turn != -1 && turn > max_turn)
    {
		printf ("Es gibt nur Datenfiles bis Runde %d.\n", max_turn);
		exit (1);
    }
	
	/* Wird keine Datei gefunden, wird ein neues Spiel erzeugt, und keine
	Datei gelesen. */
	if (max_turn == -1)
    {
		turn = 0;
		
		puts ("Keine Spieldaten gefunden, erzeuge neues Spiel...\n");
		_mkdir("data");
		_mkdir("reports");
		_mkdir("karten");
		
		createmonsters ();
		// makeblock (0, 0, 1);
		createworld();
		
		/* Die neu erschaffenen Strukturen werden abgespeichert. */
		writesummary ();
		savegame();
		return;
    }
	
	/* Das file Nr. turn wird gelesen, aber es gibt hoehere Nummern, also
	warnen wir. */
	if (turn != -1 && max_turn > turn)
		printf ("Lese Datenfile %d, obwohl %d der Letzte ist.\n",
		turn, max_turn);
	
	/* Nun wird das Datenfile Nr. turn bzw. max_turn gelesen. */
	if (turn == -1)
		turn = max_turn;
	
	printf ("Lese Datenfile %d ...\n", turn);
	// readgame (lean);
	loadgame();
	
	/* Initialisieren des random number generators. */
	srand (time(NULL));
}
