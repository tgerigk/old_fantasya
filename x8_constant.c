/*
 * diverse Dinge
 */

#include "atlantis.h"

char *kraeuter[MAX_KRAEUTER] =
{
	"Sumpfkraut",		// Sumpf
	"Bluetengrumpf",	// Ebene
	"Wirzblatt",		// Wald
	"Flachtupf",		// Hochland
	"Trockenwurz",		// W�ste
	"Grotenolm",		// Berge
	"Schnupfnies",		// Gletscher
};

char *traenke[MAX_TRAENKE] =
{
	"Wundpuder",
	"Jungfernblut",
	"Gehirntot",
	"Wissenssalbe",
	"Nachtfieber",
};

int zutaten[MAX_TRAENKE][MAX_KRAEUTER] =
{
	{ 0,0,0,0,0,0,0, },		// Wundpuder
	{ 0,0,0,0,0,0,0, },		// Jungfernblut
	{ 0,0,0,0,0,0,0, },		// Gehirntot
	{ 0,0,0,0,0,0,0, },		// Wissensalbe
	{ 0,0,0,0,0,0,0, },		// Nachtfieber
};

int traenke_LVL[MAX_TRAENKE] =
{
	0,
	0,
	0,
	0,
	0,
};

welt *welten = NULL;		// 1. Welt einer langen Kette
hoehle *hoehlen = NULL;		// alle H�hlen zur Unterwelt (und zur�ck)
maxRegions = 0;

char *x8_Options[MAX_OPTIONS] =
{
	"Text",
	"Computer",
	"Statistik",
	"Zugvorlage",
	"Zeitung",
	"e-Mails",
	"ZIP",
	"Verschiedenes",
	"Einkommen",
	"Handel",
	"Produktion",
	"Bewegungen",
	"Kolumbus",
	"Gulrak",
	"CRTrennung",
};

char * BuildingNames[MAX_BURGTYPE] =
{
	"Burg",
	"Holzfaellerhuette",
	"Mine",
	"Steinbruch",
	"Saegewerk",
	"Bergwerk",
	"Schmiede",
	"Werkstatt",
	"Kueche",
	"Steg",
	"Hafen",
	"Seehafen",
	"Schiffswerft",
	"Monument",
	"Ruine",			// kann nicht gebaut werden
	"Wegweiser",		// zerf�llt irgendwann
	"Steingrube",
	"Kirche",
};

// hier ist das Wachstum der B�ume festgelegt und zwar pro Monat, im Winter w�chst also nix
int forestgrowth[12] = { 0, 0, 0, 1, 1, 2, 2, 2, 1, 1, 1, 0, };

// Wachstum bzw hier Vermehrung der Pferde, Januar und Februar gibts nix Poppen
int horsegrowth[12] = { 0, 0, 1, 1, 2, 3, 4, 5, 4, 3, 2, 1, };

// Sturmwahrscheinlichkeit in (mal 1o) Prozent, dazu kommt noch Wert der Region selber
// also sturmwahr[] * 10 * region->sturm
int sturmwahr[12] = { 0, 0, 1, 2, 1, 0, 0, 1, 2, 3, 2, 1, };

int lebenspunkte[MAXRACES][3] =
{
	{ 50, 2, 2, },		// Monster
	{ 20, 6, 2, },		// Mensch
	{ 25, 8, 2, },		// Zwerg
	{ 22, 7, 2, },		// Elf
	{ 23, 11, 4, },		// Ork
	{ 30, 15, 6, },		// Troll
	{ 17, 8, 4, },		// Halbling
	{ 25, 8, 2, },		// Aquaner
	{ 40, 20, 8, },		// Echse
	{ 30, 10, 4, },		// Zentauren
};
