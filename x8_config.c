#include "atlantis.h"

/*****************************************************************************************************\
	hier werden alle Konfigurations Dateien erzeugt
\*****************************************************************************************************/

void KolumbusOutput()
{
	FILE *f;
	int i;
	int t = turn;
	
	/*
	 * also ... es gibt mehrere Talente ... also gibt es eine Section Talente, die die Anzahl enth�lt ... alle
	 * weiteren Daten �ber die Talente sind via Talente_## zu erreichen ... das gleiche gilt f�r Burgen, Items
	 * und so weiter ... alles Sections werden in einzahl angegeben ... also Talente -> Talente_## ... und nicht
	 * Talente -> Talent_##
	 */

	// erstmal die config.ini �ffnen ... sie enth�lt alle Daten f�r Kolumbus ... daher auch die
	// Teilung der Talente etc. ... dadurch ist es machbar, das alles in eine Datei kommt
	f = fopen("kolumbus\\config.ini", "w");

	// eine Liste aller Talente
	if (f)
	{
		puts("- Talente");
		fprintf(f, "[Talent]\n");
		fprintf(f, "Anzahl=%d\n", MAXSKILLS);
		for(i = 0; i < MAXSKILLS; i++) fprintf(f, "[Talent_%d]\nname=%s\n", i, skillnames[i]);
	} else puts("- Fehler beim erzeugen der 'talente.ini'");

	// alle Items, Kr�uter und Tr�nke
	if (f)
	{
		int it = MAXITEMS + MAX_KRAEUTER + MAX_TRAENKE;
		puts("- Items");
		fprintf(f, "[Item]\n");
		fprintf(f, "Anzahl=%d\n", MAXITEMS + MAX_KRAEUTER + MAX_TRAENKE + 4); // f�r 4 siehe Ende der Funktion !!
		//for(i = 0; i < MAXITEMS; i++) fprintf(f, "Item_%d=%s\n", i, strings[itemnames[0][i]][0]);
		for(i = 0; i < MAXITEMS; i++)
		{
			fprintf(f, "[Item_%d]\n", i);
			fprintf(f, "name=%s\n", strings[itemnames[0][i]][0]);
			if (i < LASTLUXURY) if (i != I_SILVER) fprintf(f, "gewicht=%d\n", (int) itemweight[i]); else fputs("gewicht=0.01\n", f);
			if (i < LASTPRODUCT && itemskill[i] >= 0) 
			{
				fprintf(f, "talent=%s\n", skillnames[itemskill[i]]);
				fprintf(f, "skill=%d\n", itemquality[i]);
				if (rawmaterial[i]) fprintf(f, "material=%s\n", strings[itemnames[0][rawmaterial[i]]][0]);
				if (rawquantity[i]) fprintf(f, "menge=%d\n", rawquantity[i]);
			}
		}
		for(i = 0; i < MAX_KRAEUTER; i++)
		{
			fprintf(f, "[Item_%d]\n", i + MAXITEMS);	// Gegenst�nde m�ssen dazugez�hlt werden
			fprintf(f, "name=%s\n", kraeuter[i]);
			fprintf(f, "skill=2\n");
			fprintf(f, "talent=%s\n", skillnames[SK_KRAEUTERKUNDE]);
		}
		for(i = 0; i < MAX_TRAENKE; i++)
		{
			int j;
			fprintf(f, "[Item_%d]\n", i + MAXITEMS + MAX_KRAEUTER);	// Gegenst�nde & Kr�uter m�ssen dazugez�hlt werden
			fprintf(f, "name=%s\n", traenke[i]);
			fprintf(f, "skill=%d\n", traenke_LVL[i]);
			fprintf(f, "talent=%s\n", skillnames[SK_ALCHEMIE]);
			for(j = 0; j < MAX_KRAEUTER; j++) if (zutaten[i][j]) fprintf(f, "%s=%d\n", kraeuter[j], zutaten[i][j]);
		}
		// jetzt m�ssen noch Schatzkiste etc. dazu gepackt werden
		fprintf(f, "[Item_%d]\n", it + 0);  fprintf(f, "name=Schatzkiste\n");
		fprintf(f, "[Item_%d]\n", it + 1);  fprintf(f, "name=Silberbeutel\n");
		fprintf(f, "[Item_%d]\n", it + 2);  fprintf(f, "name=Kraeuterbeutel\n");
		fprintf(f, "[Item_%d]\n", it + 3);  fprintf(f, "name=Traenke & Tinkturen\n");
	} else puts("- Fehler beim erzeugen der 'items.ini'");

	/*
	 * Geb�ude und Burgen unterscheiden sich dahin, das Burgen einfach weiter gebaut werden und dadurch zu einem
	 * anderem Typ werden, Geb�ude bleiben bei ihren Typ erhalten und Gebaeude ben�tigen verschieden Ressourcen,
	 * Burgen ben�tigen nur Steine
	 */

	// alle Burgen
	if (f)
	{
		puts("- Burgen");
		fprintf(f, "[Burg]\n");
		fprintf(f, "Anzahl=%d\n", MAXBUILDINGS);
		for(i = 0; i < MAXBUILDINGS; i++)
		{
			fprintf(f, "[Burg_%d]\n", i);
			fprintf(f, "name=%s\n", buildingnames[i]);
			fprintf(f, "skill=%d\n", i < 1 ? 1 : i);
			fprintf(f, "size=%d\n", buildingcapacity[i]);
			fprintf(f, "lohn=%d\n", i);
		}
	} else puts("- Fehler beim erzeugen der 'burg.ini'");

	// Geb�ude
	if (f)
	{
		puts("- Gebaeude");
		fprintf(f, "[Gebaeude]\n");
		fprintf(f, "Anzahl=%d\n", MAX_BURGTYPE);
		for(i = 0; i < MAX_BURGTYPE; i++)
		{
			fprintf(f, "[Gebaeude_%d]\n", i);
			fprintf(f, "name=%s\n", BuildingNames[i]);
			if (produktionsmatrix[i][PM_HOLZ])			fprintf(f, "holz=%d\n", produktionsmatrix[i][PM_HOLZ]);
			if (produktionsmatrix[i][PM_EISEN])			fprintf(f, "eisen=%d\n", produktionsmatrix[i][PM_EISEN]);
			if (produktionsmatrix[i][PM_STEIN])			fprintf(f, "stein=%d\n", produktionsmatrix[i][PM_STEIN]);
			if (produktionsmatrix[i][PM_GOLD])			fprintf(f, "gold=%d\n", produktionsmatrix[i][PM_GOLD]);
			if (produktionsmatrix[i][PM_KOSTEN])		fprintf(f, "baukosten=%d\n", produktionsmatrix[i][PM_KOSTEN]);
			if (produktionsmatrix[i][PM_TALENT])		fprintf(f, "skill=%d\n", produktionsmatrix[i][PM_TALENT]);
			if (produktionsmatrix[i][PM_UNTERHALT])		fprintf(f, "kosten=%d\n", produktionsmatrix[i][PM_UNTERHALT]);	// f�r Gebaeude
			if (produktionsmatrix[i][PM_UNTERHALT2])	fprintf(f, "unterhalt=%d\n", produktionsmatrix[i][PM_UNTERHALT2]);// f�r Einheiten
			if (produktionsmatrix[i][PM_SIZE])			fprintf(f, "groesse=%d\n", produktionsmatrix[i][PM_SIZE]);
			if (produktionsmatrix[i][PM_BURG])			fprintf(f, "burg=%s\n", buildingnames[produktionsmatrix[i][PM_BURG]]);
			if (produktionsmatrix[i][PM_BUILDING])		fprintf(f, "gebaeude=%s\n", BuildingNames[produktionsmatrix[i][PM_BUILDING]]);
		}
	} else puts("- Fehler beim erzeugen der 'gebaeude.ini'");

	// Rassen
	if (f)
	{
		puts("- Rassen");
		fprintf(f, "[Rasse]\n");
		fprintf(f, "Anzahl=%d\n", MAXRACES);
		for(i = 0; i < MAXRACES; i++)
		{
			int j;
			fprintf(f, "[Rasse_%d]\n", i);
			fprintf(f, "name=%s\n", racenames[i][0]);
			fprintf(f, "kosten=%d\n", RecruitCost[i]);
			fprintf(f, "trefferpunkte=%d\n", lebenspunkte[i][0]);
			fprintf(f, "magier=%d\n", MaxMagicans[i]);
			fprintf(f, "alchemisten=%d\n", MaxAlchemie[i]);
			fprintf(f, "gewicht=%d\n", personweight[i]);
			fprintf(f, "kapazit�t=%d\n", personcapacity[i]);
			for(j = 0; j < MAXSKILLS; j++) if (skilloffset[i][j]) fprintf(f, "%s=%d\n", skillnames[j], skilloffset[i][j]);
		}
	} else puts("- Fehler beim erzeugen der 'rassen.ini'");

	// Schiffe
	if (f)
	{
		puts("- Schiffe");
		fprintf(f, "[Schiff]\n");
		fprintf(f, "Anzahl=%d\n", MAXSHIPS);
		for(i = 0; i < MAXSHIPS; i++)
		{
			fprintf(f, "[Schiff_%d]\n", i);
			fprintf(f, "name=%s\n", shiptypes[0][i]);
			fprintf(f, "kapazit�t=%d\n", shipcapacity[i]);
			fprintf(f, "holz=%d\n", shipcost[i]);
			fprintf(f, "geschwindigkeit=%d\n", shipspeed[i]);
			fprintf(f, "kapitaen=%d\n", i ? i : 1);				// mind. Segeln Kapit�n
			fprintf(f, "sailor=%d\n", sailors[i]);				// mind. Segeln alle
			fprintf(f, "skill=%d\n", i ? i : 1);				// mind. Schiffbau
		}
	} else puts("- Fehler beim erzeugen von 'schiffe.ini'");

	// Terrains
	if (f)
	{
		puts("- Terrains");
		fprintf(f, "[Terrain]\n");
		fprintf(f, "Anzahl=%d\n", MAXTERRAINS_NEW);
		for(i = 0; i < MAXTERRAINS_NEW; i++)
		{
			fprintf(f, "[Terrain_%d]\n", i);
			fprintf(f, "name=%s\n", strings[terrainnames[i]][0]);
			if (production[i])	fprintf(f, "platz=%d\n", production[i]);
			if (roadreq[i])		fprintf(f, "strasse=%d\n", roadreq[i]);				// in Fantasya in alle Richtungen
		}
	} else puts("- Fehler beim erzeugen von 'terrains.ini'");

	// Luxusg�ter
	if (f)
	{
		puts("- Luxusgueter");
		fprintf(f, "[Luxus]\n");
		fprintf(f, "Anzahl=%d\n", MAXLUXURIES);
		for(i = 0; i < MAXLUXURIES; i++) fprintf(f, "Luxus_%d=%s\n", i, strings[itemnames[0][FIRSTLUXURY + i]][0]);
	} else puts("- Fehler beim erzeugen von 'luxus.ini'");

	// Zauberspr�che
	if (f)
	{
		puts("- Zaubersprueche");
		fprintf(f, "[Spruch]\n");
		fprintf(f, "Anzahl=%d\n", MAXSPELLS);
		for(i = 0; i < MAXSPELLS; i++)
		{
			fprintf(f, "[Spruch_%d]\n", i);
			fprintf(f, "name=%s\n", strings[spellnames[i]][0]);
			if (spellitem[i] >= 0) fprintf(f, "item=%s\n", strings[itemnames[0][spellitem[i]]][0]);
			if (spelllevel[i]) fprintf(f, "level=%d\n", spelllevel[i]);
			if (iscombatspell[i]) fprintf(f, "kampfzauber=1\n");
			fprintf(f, "text=%s\n", strings[spelldata[i]][0]);
		}
	} else puts("- Fehler beim erzeugen von 'zauber.ini'");

	fclose(f);	// erzeugt ggf. einen Absturz ... aber egal
}


/*****************************************************************************************************\

\*****************************************************************************************************/

void VorlageRassen(FILE *out)
{
	// hier m�ssen alle Rassen nach den Echsen ausgeblendet werden
	int RACEOUT = 1;
	int i,j;

	printf("Rassen, ");
	fprintf(out, "[Races]\n");

	// Rassennamen
	fprintf(out,"\"Mehrzahl\"");
	for(i=1; i<MAXRACES - RACEOUT; i++) fprintf(out,"\t,\"%s\"",racenames[i][1]);
	fprintf(out,"\n");
	fprintf(out,"\"Einzahl\"");
	for(i=1; i<MAXRACES - RACEOUT; i++) fprintf(out,",\t\"%s\"",racenames[i][0]);
	fprintf(out,"\n");

	// Unterhalt
	fprintf(out,"\"Unterhalt\"");
	for(i=1; i<MAXRACES - RACEOUT; i++) fprintf(out,"\t,10");
	fprintf(out,"\n");

	// Gewicht
	fprintf(out,"\"Gewicht\"");
	for(i=1; i<MAXRACES - RACEOUT; i++) fprintf(out,"\t,10");
	fprintf(out,"\n");

	// Kapazit�t
	fprintf(out,"\"Kapazit�t\"");
	for(i=1; i<MAXRACES - RACEOUT; i++) fprintf(out,"\t,10");
	fprintf(out,"\n");

	// Rekrutierungskosten
	fprintf(out,"\"Rekrutierungskosten\"");
	for(i=1; i<MAXRACES - RACEOUT; i++) fprintf(out,"\t,%d",RecruitCost[i]);
	fprintf(out,"\n");

	// Skills
	for(i=0; i<MAXSKILLS; i++)
	{
		fprintf(out,"\"%s\"",skillnames[i]);
		for(j=1; j<MAXRACES-1; j++)
		{
			fprintf(out,"\t,%d",skilloffset[j][i]);
		}
		fprintf(out,"\n");
	}

	fprintf(out, "\n\n");
}

void VorlageTerrains(FILE *out)
{
	int i;

	printf("Terrains, ");
	fprintf(out, "[Terrains]\n");

	for(i = 0; i <MAXTERRAINS_NEW; i++)
	{
		fprintf(out, "\"%s\",\t\"%c\",\t\"%c\",\t%i,\t%i,\t%i,\t%i\n"
				, strings[terrainnames[i]][0]
				, terrainsymbols[i]
				, tolower(terrainsymbols[i])
				, production[i] * 10
				, (i == T_OCEAN || i == T_LAVASTROM) ? 0 : 1
				, (i == T_MOUNTAIN || i == T_GLACIER) ? 1 : 0
				, 0);
	}
	fprintf(out, "\n");
}

void VorlageShips(FILE *out)
{
	int i;
	printf("Schiffe, ");

	fprintf(out, "[Ships]\n");

	for(i = 0; i < MAXSHIPS; i++)
	{
		fprintf(out, "\"%s\",\t%i\n", shiptypes[0][i], shipcapacity[i]);
	}
	fprintf(out, "\n");
}

void VorlageCastles(FILE *out)
{
	int i;
	printf("Burgen, ");

	fprintf(out, "[Castles]\n");

	for(i = 0; i < MAXBUILDINGS; i++)
	{
		fprintf(out, "\"%s\",\t%i,\t%i\n", buildingnames[i], buildingcapacity[i], (i == 0) ? 1 : i);
	}
	fprintf(out, "\n");
}

void VorlageThings(FILE *out)
{
	int i;
	printf("Gegenstaende ");

	fprintf(out, "[Things]\n");

	for(i = 0; i < LASTLUXURY; i++)
	{
		fprintf(out, "\"%s\",\t%f\n", strings[itemnames[0][i]][0], itemweight[i]);
	}
	for(i = 0; i < MAX_KRAEUTER; i++)
	{
		fprintf(out, "\"%s\",\t%i\n", kraeuter[i], 0);
	}
	for(i = 0; i < MAX_TRAENKE; i++)
	{
		fprintf(out, "\"%s\",\t%i\n", traenke[i], 0);
	}
	fprintf(out, "\n");
}

void VorlageBuildings(FILE *out)
{
	int i;
	printf("und Gebaeude");

	fprintf(out, "[Buildings]\n");

	for(i = 1; i < MAX_BURGTYPE; i++)
	{
		fprintf(out, "\"%s\",\t\"Baukosten\",\t", BuildingNames[i]);
		fprintf(out, "%i,\t\"Stein\",\t%i,\t\"Eisen\",\t%i,\t\"Holz\",\t%i,\t\"Gold\",\t%i,\t\"Silber\",\t\"Unterhalt\",\t%i,\t\"Silber\"\n"
					, produktionsmatrix[i][PM_STEIN]
					, produktionsmatrix[i][PM_EISEN]
					, produktionsmatrix[i][PM_HOLZ]
					, produktionsmatrix[i][PM_GOLD]
					, produktionsmatrix[i][PM_KOSTEN]
					, produktionsmatrix[i][PM_UNTERHALT]);
	}
	fprintf(out, "\n");
}

void VorlageOutput()
{
	FILE *out;

	printf("Vorlage nach \"XML Anleitung/Magellan/vorlage/fantasya.cfg\"\n");
	out = fopen("XML Anleitung/Magellan/vorlage/fantasya.cfg","w");

	fprintf(out, ";Config f�r Vorlage ... Fantasya %s\n\n", fanta_version);

	VorlageTerrains(out);
	VorlageRassen(out);
	VorlageShips(out);
	VorlageCastles(out);
	VorlageThings(out);
	VorlageBuildings(out);

	fclose(out);

	printf("\n");
}

/*****************************************************************************************************\

\*****************************************************************************************************/

void ConfigOutput()
{
	// printf("-- Kolumbus --\n");
	// KolumbusOutput();

	printf("\n\n-- Vorlage --\n");
	VorlageOutput();

	printf("\n\n");
}