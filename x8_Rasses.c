/*
 * Rassendefinition und Funktionen f�r Fantasya
 */

#include "atlantis.h"



unsigned char RecruitCost[MAXRACES];
unsigned char MaxMagicans[MAXRACES];
unsigned char MaxAlchemie[MAXRACES];

int personweight[MAXRACES];				// Gewichte der einzelnen Rassen
int personcapacity[MAXRACES];			// Transportkraft der Rassen (+10 f�r eigenes Gewicht)



void SetSkillOffSet(void)
{
	int rasse;

	/*
	 * Da hier jede Rasse einzeln gesetzt wird ist es sehr viel �bersichtlicher als
	 * in 'constant.c', war allerdings eine Heidenarbeit
	 */

	puts("- setzte Rassenmodifikationen");

	rasse = R_HUMAN;

	RecruitCost[rasse] = 75;
	racehp[rasse] = 20;
	MaxMagicans[rasse] = 3;
	MaxAlchemie[rasse] = 3;
	personweight[rasse] = 10;
	personcapacity[rasse] = 17;

	skilloffset[rasse][SK_CROSSBOW]			= 0;
    skilloffset[rasse][SK_LONGBOW]			= 0;
    skilloffset[rasse][SK_CATAPULT]			= 0;
    skilloffset[rasse][SK_SWORD]			= 0;
    skilloffset[rasse][SK_SPEAR]			= 0;
    skilloffset[rasse][SK_RIDING]			= 0;
    skilloffset[rasse][SK_TACTICS]			= 0;
    skilloffset[rasse][SK_MINING]			= 0;
    skilloffset[rasse][SK_BUILDING]			= 0;
    skilloffset[rasse][SK_TRADE]			= 0;
    skilloffset[rasse][SK_LUMBERJACK]		= 0;
    skilloffset[rasse][SK_MAGIC]			= 0;
    skilloffset[rasse][SK_HORSE_TRAINING]	= 0;
    skilloffset[rasse][SK_ARMORER]			= 0;
    skilloffset[rasse][SK_SHIPBUILDING]		= 0;
    skilloffset[rasse][SK_SAILING]			= 0;
    skilloffset[rasse][SK_QUARRYING]		= 0;
    skilloffset[rasse][SK_ROAD_BUILDING]	= 0;
    skilloffset[rasse][SK_STEALTH]			= 0;
    skilloffset[rasse][SK_ENTERTAINMENT]	= 0;
    skilloffset[rasse][SK_WEAPONSMITH]		= 0;
    skilloffset[rasse][SK_CARTMAKER]		= 0;
    skilloffset[rasse][SK_OBSERVATION]		= 0;
    skilloffset[rasse][SK_TAXING]			= 0;
    skilloffset[rasse][SK_FLETCHER]			= 0;
	skilloffset[rasse][SK_SPIONAGE]			= 0;
	skilloffset[rasse][SK_ALCHEMIE]			= 0;
	skilloffset[rasse][SK_KRAEUTERKUNDE]	= 0;
	skilloffset[rasse][SK_AUSDAUER]			= 0;
	skilloffset[rasse][SK_RELIGION]			= 0;

//	-+*+-

	rasse = R_DWARF;

	RecruitCost[rasse] = 110;
	racehp[rasse] = 22;
	MaxMagicans[rasse] = 3;
	MaxAlchemie[rasse] = 3;
	personweight[rasse] = 10;
	personcapacity[rasse] = 17;

	skilloffset[rasse][SK_CROSSBOW]			= 0;
    skilloffset[rasse][SK_LONGBOW]			= -1;
    skilloffset[rasse][SK_CATAPULT]			= 2;
    skilloffset[rasse][SK_SWORD]			= 1;
    skilloffset[rasse][SK_SPEAR]			= 0;
    skilloffset[rasse][SK_RIDING]			= -2;
    skilloffset[rasse][SK_TACTICS]			= 0;
    skilloffset[rasse][SK_MINING]			= 2;
    skilloffset[rasse][SK_BUILDING]			= 2;
    skilloffset[rasse][SK_TRADE]			= 1;
    skilloffset[rasse][SK_LUMBERJACK]		= -1;
    skilloffset[rasse][SK_MAGIC]			= -2;
    skilloffset[rasse][SK_HORSE_TRAINING]	= -2;
    skilloffset[rasse][SK_ARMORER]			= 2;
    skilloffset[rasse][SK_SHIPBUILDING]		= -1;
    skilloffset[rasse][SK_SAILING]			= -2;
    skilloffset[rasse][SK_QUARRYING]		= 2;
    skilloffset[rasse][SK_ROAD_BUILDING]	= 2;
    skilloffset[rasse][SK_STEALTH]			= -1;
    skilloffset[rasse][SK_ENTERTAINMENT]	= -1;
    skilloffset[rasse][SK_WEAPONSMITH]		= 2;
    skilloffset[rasse][SK_CARTMAKER]		= 0;
    skilloffset[rasse][SK_OBSERVATION]		= 0;
    skilloffset[rasse][SK_TAXING]			= 1;
    skilloffset[rasse][SK_FLETCHER]			= 0;
	skilloffset[rasse][SK_SPIONAGE]			= 0;
	skilloffset[rasse][SK_ALCHEMIE]			= -2;
	skilloffset[rasse][SK_KRAEUTERKUNDE]	= 0;
	skilloffset[rasse][SK_AUSDAUER]			= 0;
	skilloffset[rasse][SK_RELIGION]			= 1;

//	-+*+-

	rasse = R_ELF;

	RecruitCost[rasse] = 130;
	racehp[rasse] = 18;
	MaxMagicans[rasse] = 4;
	MaxAlchemie[rasse] = 3;
	personweight[rasse] = 10;
	personcapacity[rasse] = 17;

	skilloffset[rasse][SK_CROSSBOW]			= 0;
    skilloffset[rasse][SK_LONGBOW]			= 2;
    skilloffset[rasse][SK_CATAPULT]			= -2;
    skilloffset[rasse][SK_SWORD]			= 0;
    skilloffset[rasse][SK_SPEAR]			= 0;
    skilloffset[rasse][SK_RIDING]			= 0;
    skilloffset[rasse][SK_TACTICS]			= 0;
    skilloffset[rasse][SK_MINING]			= -2;
    skilloffset[rasse][SK_BUILDING]			= -1;
    skilloffset[rasse][SK_TRADE]			= 0;
    skilloffset[rasse][SK_LUMBERJACK]		= 0;
    skilloffset[rasse][SK_MAGIC]			= 1;
    skilloffset[rasse][SK_HORSE_TRAINING]	= 1;
    skilloffset[rasse][SK_ARMORER]			= -1;
    skilloffset[rasse][SK_SHIPBUILDING]		= -1;
    skilloffset[rasse][SK_SAILING]			= -1;
    skilloffset[rasse][SK_QUARRYING]		= -1;
    skilloffset[rasse][SK_ROAD_BUILDING]	= -1;
    skilloffset[rasse][SK_STEALTH]			= 1;
    skilloffset[rasse][SK_ENTERTAINMENT]	= 0;
    skilloffset[rasse][SK_WEAPONSMITH]		= 0;
    skilloffset[rasse][SK_CARTMAKER]		= 0;
    skilloffset[rasse][SK_OBSERVATION]		= 1;
    skilloffset[rasse][SK_TAXING]			= 0;
    skilloffset[rasse][SK_FLETCHER]			= 2;
	skilloffset[rasse][SK_SPIONAGE]			= 0;
	skilloffset[rasse][SK_ALCHEMIE]			= 1;
	skilloffset[rasse][SK_KRAEUTERKUNDE]	= 1;
	skilloffset[rasse][SK_AUSDAUER]			= 0;
	skilloffset[rasse][SK_RELIGION]			= 1;

//	-+*+-

	rasse = R_ORK;

	RecruitCost[rasse] = 70;
	racehp[rasse] = 24;
	MaxMagicans[rasse] = 3;
	MaxAlchemie[rasse] = 3;
	personweight[rasse] = 10;
	personcapacity[rasse] = 17;

	skilloffset[rasse][SK_CROSSBOW]			= 0;
    skilloffset[rasse][SK_LONGBOW]			= 0;
    skilloffset[rasse][SK_CATAPULT]			= 0;
    skilloffset[rasse][SK_SWORD]			= 0;
    skilloffset[rasse][SK_SPEAR]			= 0;
    skilloffset[rasse][SK_RIDING]			= 0;
    skilloffset[rasse][SK_TACTICS]			= 1;
    skilloffset[rasse][SK_MINING]			= 1;
    skilloffset[rasse][SK_BUILDING]			= 1;
    skilloffset[rasse][SK_TRADE]			= -3;
    skilloffset[rasse][SK_LUMBERJACK]		= 1;
    skilloffset[rasse][SK_MAGIC]			= -1;
    skilloffset[rasse][SK_HORSE_TRAINING]	= -1;
    skilloffset[rasse][SK_ARMORER]			= 1;
    skilloffset[rasse][SK_SHIPBUILDING]		= -1;
    skilloffset[rasse][SK_SAILING]			= -1;
    skilloffset[rasse][SK_QUARRYING]		= 1;
    skilloffset[rasse][SK_ROAD_BUILDING]	= 0;
    skilloffset[rasse][SK_STEALTH]			= 0;
    skilloffset[rasse][SK_ENTERTAINMENT]	= -2;
    skilloffset[rasse][SK_WEAPONSMITH]		= 2;
    skilloffset[rasse][SK_CARTMAKER]		= -1;
    skilloffset[rasse][SK_OBSERVATION]		= 0;
    skilloffset[rasse][SK_TAXING]			= 1;
    skilloffset[rasse][SK_FLETCHER]			= 0;
	skilloffset[rasse][SK_SPIONAGE]			= 0;
	skilloffset[rasse][SK_ALCHEMIE]			= 1;
	skilloffset[rasse][SK_KRAEUTERKUNDE]	= -1;
	skilloffset[rasse][SK_AUSDAUER]			= 0;
	skilloffset[rasse][SK_RELIGION]			= -1;

//	-+*+-

	rasse = R_TROLL;

	RecruitCost[rasse] = 90;
	racehp[rasse] = 28;
	MaxMagicans[rasse] = 3;
	MaxAlchemie[rasse] = 3;
	personweight[rasse] = 20;
	personcapacity[rasse] = 34;

	skilloffset[rasse][SK_CROSSBOW]			= 0;
    skilloffset[rasse][SK_LONGBOW]			= -2;
    skilloffset[rasse][SK_CATAPULT]			= 2;
    skilloffset[rasse][SK_SWORD]			= 1;
    skilloffset[rasse][SK_SPEAR]			= 0;
    skilloffset[rasse][SK_RIDING]			= -2;
    skilloffset[rasse][SK_TACTICS]			= -1;
    skilloffset[rasse][SK_MINING]			= 2;
    skilloffset[rasse][SK_BUILDING]			= 2;
    skilloffset[rasse][SK_TRADE]			= 0;
    skilloffset[rasse][SK_LUMBERJACK]		= 0;
    skilloffset[rasse][SK_MAGIC]			= 0;
    skilloffset[rasse][SK_HORSE_TRAINING]	= -1;
    skilloffset[rasse][SK_ARMORER]			= 2;
    skilloffset[rasse][SK_SHIPBUILDING]		= -1;
    skilloffset[rasse][SK_SAILING]			= -1;
    skilloffset[rasse][SK_QUARRYING]		= 2;
    skilloffset[rasse][SK_ROAD_BUILDING]	= 2;
    skilloffset[rasse][SK_STEALTH]			= -3;
    skilloffset[rasse][SK_ENTERTAINMENT]	= -1;
    skilloffset[rasse][SK_WEAPONSMITH]		= 0;
    skilloffset[rasse][SK_CARTMAKER]		= 0;
    skilloffset[rasse][SK_OBSERVATION]		= -1;
    skilloffset[rasse][SK_TAXING]			= 1;
    skilloffset[rasse][SK_FLETCHER]			= 0;
	skilloffset[rasse][SK_SPIONAGE]			= 0;
	skilloffset[rasse][SK_ALCHEMIE]			= -1;
	skilloffset[rasse][SK_KRAEUTERKUNDE]	= -1;
	skilloffset[rasse][SK_AUSDAUER]			= 0;
	skilloffset[rasse][SK_RELIGION]			= -1;

//	-+*+-

	rasse = R_HALFLING;

	RecruitCost[rasse] = 60;
	racehp[rasse] = 17;
	MaxMagicans[rasse] = 3;
	MaxAlchemie[rasse] = 5;
	personweight[rasse] = 8;
	personcapacity[rasse] = 16;

	skilloffset[rasse][SK_CROSSBOW]			= 1;
    skilloffset[rasse][SK_LONGBOW]			= -1;
    skilloffset[rasse][SK_CATAPULT]			= -1;
    skilloffset[rasse][SK_SWORD]			= -1;
    skilloffset[rasse][SK_SPEAR]			= -1;
    skilloffset[rasse][SK_RIDING]			= -1;
    skilloffset[rasse][SK_TACTICS]			= 0;
    skilloffset[rasse][SK_MINING]			= 1;
    skilloffset[rasse][SK_BUILDING]			= 1;
    skilloffset[rasse][SK_TRADE]			= 2;
    skilloffset[rasse][SK_LUMBERJACK]		= 0;
    skilloffset[rasse][SK_MAGIC]			= 0;
    skilloffset[rasse][SK_HORSE_TRAINING]	= -1;
    skilloffset[rasse][SK_ARMORER]			= 0;
    skilloffset[rasse][SK_SHIPBUILDING]		= -1;
    skilloffset[rasse][SK_SAILING]			= -2;
    skilloffset[rasse][SK_QUARRYING]		= 0;
    skilloffset[rasse][SK_ROAD_BUILDING]	= 1;
    skilloffset[rasse][SK_STEALTH]			= 1;
    skilloffset[rasse][SK_ENTERTAINMENT]	= 1;
    skilloffset[rasse][SK_WEAPONSMITH]		= 0;
    skilloffset[rasse][SK_CARTMAKER]		= 2;
    skilloffset[rasse][SK_OBSERVATION]		= 1;
    skilloffset[rasse][SK_TAXING]			= -1;
    skilloffset[rasse][SK_FLETCHER]			= 0;
	skilloffset[rasse][SK_SPIONAGE]			= 0;
	skilloffset[rasse][SK_ALCHEMIE]			= 1;
	skilloffset[rasse][SK_KRAEUTERKUNDE]	= 2;
	skilloffset[rasse][SK_AUSDAUER]			= 0;
	skilloffset[rasse][SK_RELIGION]			= -2;

//	-+*+-

	rasse = R_MARINER;

	RecruitCost[rasse] = 80;
	racehp[rasse] = 22;
	MaxMagicans[rasse] = 3;
	MaxAlchemie[rasse] = 3;
	personweight[rasse] = 10;
	personcapacity[rasse] = 17;

	skilloffset[rasse][SK_CROSSBOW]			= -1;
    skilloffset[rasse][SK_LONGBOW]			= 0;
    skilloffset[rasse][SK_CATAPULT]			= -2;
    skilloffset[rasse][SK_SWORD]			= -1;
    skilloffset[rasse][SK_SPEAR]			= 1;
    skilloffset[rasse][SK_RIDING]			= -1;
    skilloffset[rasse][SK_TACTICS]			= 0;
    skilloffset[rasse][SK_MINING]			= -1;
    skilloffset[rasse][SK_BUILDING]			= 0;
    skilloffset[rasse][SK_TRADE]			= 2;
    skilloffset[rasse][SK_LUMBERJACK]		= 1;
    skilloffset[rasse][SK_MAGIC]			= 0;
    skilloffset[rasse][SK_HORSE_TRAINING]	= -1;
    skilloffset[rasse][SK_ARMORER]			= -2;
    skilloffset[rasse][SK_SHIPBUILDING]		= 2;
    skilloffset[rasse][SK_SAILING]			= 2;
    skilloffset[rasse][SK_QUARRYING]		= 0;
    skilloffset[rasse][SK_ROAD_BUILDING]	= -2;
    skilloffset[rasse][SK_STEALTH]			= 0;
    skilloffset[rasse][SK_ENTERTAINMENT]	= 0;
    skilloffset[rasse][SK_WEAPONSMITH]		= 0;
    skilloffset[rasse][SK_CARTMAKER]		= -1;
    skilloffset[rasse][SK_OBSERVATION]		= 0;
    skilloffset[rasse][SK_TAXING]			= 0;
    skilloffset[rasse][SK_FLETCHER]			= 0;
	skilloffset[rasse][SK_SPIONAGE]			= 0;
	skilloffset[rasse][SK_ALCHEMIE]			= -1;
	skilloffset[rasse][SK_KRAEUTERKUNDE]	= 1;
	skilloffset[rasse][SK_AUSDAUER]			= 0;
	skilloffset[rasse][SK_RELIGION]			= -1;

//	-+*+-

	rasse = R_DEMON;

	RecruitCost[rasse] = 150;
	racehp[rasse] = 40;
	MaxMagicans[rasse] = 3;
	MaxAlchemie[rasse] = 3;
	personweight[rasse] = 10;
	personcapacity[rasse] = 17;

	skilloffset[rasse][SK_CROSSBOW]			= 1;
    skilloffset[rasse][SK_LONGBOW]			= 2;
    skilloffset[rasse][SK_CATAPULT]			= 2;
    skilloffset[rasse][SK_SWORD]			= 2;
    skilloffset[rasse][SK_SPEAR]			= 1;
    skilloffset[rasse][SK_RIDING]			= -1;
    skilloffset[rasse][SK_TACTICS]			= 1;
    skilloffset[rasse][SK_MINING]			= 1;
    skilloffset[rasse][SK_BUILDING]			= -2;
    skilloffset[rasse][SK_TRADE]			= -3;
    skilloffset[rasse][SK_LUMBERJACK]		= 0;
    skilloffset[rasse][SK_MAGIC]			= -1;
    skilloffset[rasse][SK_HORSE_TRAINING]	= -3;
    skilloffset[rasse][SK_ARMORER]			= 2;
    skilloffset[rasse][SK_SHIPBUILDING]		= -2;
    skilloffset[rasse][SK_SAILING]			= -3;
    skilloffset[rasse][SK_QUARRYING]		= 0;
    skilloffset[rasse][SK_ROAD_BUILDING]	= 0;
    skilloffset[rasse][SK_STEALTH]			= 2;
    skilloffset[rasse][SK_ENTERTAINMENT]	= -3;
    skilloffset[rasse][SK_WEAPONSMITH]		= 2;
    skilloffset[rasse][SK_CARTMAKER]		= 0;
    skilloffset[rasse][SK_OBSERVATION]		= -2;
    skilloffset[rasse][SK_TAXING]			= 2;
    skilloffset[rasse][SK_FLETCHER]			= 1;
	skilloffset[rasse][SK_SPIONAGE]			= 0;
	skilloffset[rasse][SK_ALCHEMIE]			= 2;
	skilloffset[rasse][SK_KRAEUTERKUNDE]	= -2;
	skilloffset[rasse][SK_AUSDAUER]			= 0;
	skilloffset[rasse][SK_RELIGION]			= 1;

//	-+*+-

	rasse = R_ZENTAUREN;

	RecruitCost[rasse] = 100;
	racehp[rasse] = 30;
	MaxMagicans[rasse] = 3;
	MaxAlchemie[rasse] = 3;
	personweight[rasse] = 10;
	personcapacity[rasse] = 17;

	skilloffset[rasse][SK_CROSSBOW]			= -2;
    skilloffset[rasse][SK_LONGBOW]			= 2;
    skilloffset[rasse][SK_CATAPULT]			= 2;
    skilloffset[rasse][SK_SWORD]			= 2;
    skilloffset[rasse][SK_SPEAR]			= -2;
    skilloffset[rasse][SK_RIDING]			= -1;
    skilloffset[rasse][SK_TACTICS]			= 2;
    skilloffset[rasse][SK_MINING]			= 0;
    skilloffset[rasse][SK_BUILDING]			= 0;
    skilloffset[rasse][SK_TRADE]			= -1;
    skilloffset[rasse][SK_LUMBERJACK]		= 0;
    skilloffset[rasse][SK_MAGIC]			= 2;
    skilloffset[rasse][SK_HORSE_TRAINING]	= -1;
    skilloffset[rasse][SK_ARMORER]			= 0;
    skilloffset[rasse][SK_SHIPBUILDING]		= -1;
    skilloffset[rasse][SK_SAILING]			= 0;
    skilloffset[rasse][SK_QUARRYING]		= 0;
    skilloffset[rasse][SK_ROAD_BUILDING]	= -1;
    skilloffset[rasse][SK_STEALTH]			= 2;
    skilloffset[rasse][SK_ENTERTAINMENT]	= -2;
    skilloffset[rasse][SK_WEAPONSMITH]		= 0;
    skilloffset[rasse][SK_CARTMAKER]		= 0;
    skilloffset[rasse][SK_OBSERVATION]		= 0;
    skilloffset[rasse][SK_TAXING]			= 2;
    skilloffset[rasse][SK_FLETCHER]			= 0;
	skilloffset[rasse][SK_SPIONAGE]			= -2;
	skilloffset[rasse][SK_ALCHEMIE]			= -1;
	skilloffset[rasse][SK_KRAEUTERKUNDE]	= -1;
	skilloffset[rasse][SK_AUSDAUER]			= 0;
	skilloffset[rasse][SK_RELIGION]			= 2;
};

int SelectARace()
{
	int mraces = MAXRACES;
	int i;

#ifdef FANTA_LIMITED_EDITION
	mraces--;	// eigene Rassen verstecken
#endif

	for(i=1; i < mraces; i++) printf("%d - %s\n",i,racenames[i][0]); printf("? "); scanf("%d",&i);
	i = min(i,mraces-1);
	i = max(1,i); printf("Rasse: %s\n",racenames[i][0]);
	return i;
};

/*
 * Verwaltung s�mtlicher Tarnungen
 */

void tarnen(unit * u, strlist * S)
{
	int p1,p2;		// welche Parameter
	char *s;		// f�r Rasse
	faction *f;		// f�r Partei
	int race=0;		// diese Rasse
	int i;

	p1 = getparam();	// sollte jetzt "Einheit" "Partei" oder "Rasse" sein

	switch (p1)
	{
		case P_RASSE:
			s = getstr();
			for(i=1;i<MAXRACES;i++)
			{
				if (stricmp(racenames[i][1],s) == 0) race = i;
				if (stricmp(racenames[i][0],s) == 0) race = i;
			}
			if (race == 0)
			{
				mistake2(u,S,"Rasse ist unbekannt");
				return;
			}
			if (u->race != R_DEMON)
			{
				sprintf(buf,"Nur Echsenmenschen k�nnen sich als andere Rasse tarnen.");
				addevent(u,buf);
				return;
			}
			u->raceTT = race;
			sprintf(buf,"%s tarnt sich als '%s'",u->name,racenames[u->raceTT][0]);
			addevent(u ,buf);
			break;
		case P_UNIT:
			p2 = getparam();	// sollte "NICHT" sein
			if (p2 == P_NOT)
			{
				u->tarnung = 0;
				sprintf(buf,"%s zeigt sich wieder",u->name);
				addevent(u ,buf);
			} else
			{
				u->tarnung = 1;
				sprintf(buf,"%s versucht sich zu verstecken",u->name);
				addevent(u ,buf);
			}
			break;
		case P_FACTION:
			f = getfaction();
			if (f == NULL) 
			{
				mistake2(u,S,"Partei wurde nicht gefunden");
				return;
			}
			u->tarn_no = f->no;
			if (u->tarn_no != 0) sprintf(buf,"%s versucht sich als Mitglied von Volk '%s' auszugeben",u->name,f->name); else sprintf(buf,"%s zeigt seine Parteizugeh�rigkeit nicht mehr",u->name);
			addevent(u ,buf);
			break;
		default:
			mistake2(u,S,"v�llig unklar");
			return;
	}
}

void tarnung(void)
{
	unit *u;
	region *r;
	strlist *S;

	printf("- Tarnen von Einheit, Partei und Rassen\n");
	for(r = regions; r ; r=r->next)
	{
		for(u = r->units; u ; u=u->next)
		{
			for (S = u->orders; S; S = S->next)
			{
				switch (igetkeyword (S->s))
				{
					case K_TARNUNG:
						tarnen (u, S);
						break;
				} // switch
			} // for Orders
		} // for Units
	} // for Regions
}

void prefix()
{
	unit *u;
	region *r;
	strlist *S;
	char *s;

	printf("- Pr�fix\n");
	for(r = regions ; r ; r=r->next)
	{
		for(u = r->units ; u ; u=u->next)
		{
			for(S=u->orders ; S ; S=S->next)
			{
				if (igetkeyword(S->s) == K_PRAEFIX)
				{
					s = getstr();
					mnstrcpy (&u->prefix, s, NAMESIZE);
				} // K_PRAEFIX
			} // u->orders
		} // r->units
	} // regions
}

/*
 * Kontrolle auf die Parteitarnung
 */
void checkparteitarnung()
{
	unit *u;
	region *r;
	faction *f;

	for(r=regions ; r; r=r->next)
	{
		for(u=r->units ; u; u=u->next)
		{
			f = findfaction(u->tarn_no);
			if ((f == NULL) && (u->tarn_no != 0)) // wenn u->tarn_no EQ 0, dann parteitarnung
			{
				u->tarn_no = 0;
				mistakeu(u,"Versteck jetzt seine Parteizugeh�rigkeit");
			} // Change
		} // Einheiten
	} // regionen
}



void spionieren()
{
	region *r;
	unit *u;
	char pbuf[MAXSTRING];
	int i;
	int gotit;
	int wahrnehmungziel;	// egal welche Einheit, nur bewache, muss zur Partei des Zieles geh�ren
	int talentcounter = 0;
	
	printf("- Spionage");
	for(r=regions; r; r=r->next)
	{
		for(u=r->units; u; u=u->next)
		{
			if (igetkeyword(u->thisorder) == K_SPIONIEREN)
			{
				unit *ziel;
				
				ziel = getunit(r,u);
				if (!ziel)
				{
					mistakeu(u,"Einheit nicht gefunden");
					continue;
				}

				if (u->faction == ziel->faction)
				{
					mistakeu(u,"Das ist unsere Einheit");
					continue;
				}

				if ((ziel->building) && (u->building!=ziel->building))
				{
					mistakeu(u,"Wir sind nicht im selben Gebaeude");
					continue;
				}
				
				// schauen ob Wahrnehmung der Zeilpartei gr��er als Spionage ist
				wahrnehmungziel = highskill(ziel->faction,r,SK_OBSERVATION);
				if (wahrnehmungziel > effskill(u,SK_OBSERVATION))
				{
					sprintf(buf,"%s sollte von %s ausspioniert werden!",unitid(ziel),unitid(u));
					addevent(ziel,buf);
					sprintf(buf,"%s wurde bei der Spionage ertappt",unitid(u));
					addevent(u,buf);
					continue;
				}
				if (wahrnehmungziel == effskill(u,SK_OBSERVATION))
				{
					sprintf(buf,"%s f�hlt sich beobachtet",unitid(ziel));
					addevent(ziel,buf);
					if (rand()%128 < 64)	// jetzt eine 5o% Chanche das es nicht klappt
					{
						sprintf(buf,"%s wurde in %s bei der Spionage ertappt",unitid(u),regionid(r,u->faction->xorigin,u->faction->yorigin));
						addevent(u,buf);
						continue;
					}
				}

				gotit = 0;
				switch(rand() % 6)
				{
					case 0:	sprintf(buf,"%s gehoert zu Partei %s (%s)",unitid(ziel),ziel->faction->name,tobase36(ziel->faction->no));
							addevent(u,buf);
							break;
					case 1: if (ziel->status == ST_FIGHT) sprintf(buf,"%s kaempft vorne",unitid(ziel));
							if (ziel->status == ST_BEHIND) sprintf(buf,"%s kaempft hinten",unitid(ziel));
							if (ziel->status == ST_AVOID) sprintf(buf,"%s kaempft nicht",unitid(ziel));
							addevent(u,buf);
							break;
					case 2: sprintf(buf,"%s besitzt %d Silber",unitid(ziel),ziel->items[I_SILVER]);
							addevent(u,buf);
							break;
					case 3: sprintf(buf,"%s hat im Kraeuterbeutel ",unitid(ziel));
							for(i=0; i<MAX_KRAEUTER; i++)
							{
								if (ziel->kraeuter[i])
								{
									sprintf(pbuf,"%d %s",ziel->kraeuter[i],kraeuter[i]);
									if (gotit)
									{
										strcat(buf,", ");
									}
									strcat(buf,pbuf);
									gotit = 1;
								}
							}
							addevent(u,buf);
							break;
					//case 5:
					case 4: sprintf(buf,"%s hat an Traenken & Tinkturen ",unitid(ziel));
							for(i=0; i<MAX_TRAENKE; i++)
							{
								if (ziel->traenke[i])
								{
									sprintf(pbuf,"%d %s",ziel->traenke[i],traenke[i]);
									if (gotit)
									{
										strcat(buf,", ");
									}
									strcat(buf,pbuf);
									gotit = 1;
								}
							}
							addevent(u,buf);
							break;
					default: i = 0;

							i = (rand() % MAXSKILLS);		// hier wird jetzt ein Talent ausgew�hlt
							gotit = -1;						// noch kein Talent
							while (gotit == -1 && talentcounter != 2)
							{
								if (ziel->skills[i]) gotit = i;
								i++;
								if (i == MAXSKILLS) { talentcounter++; i = 0; }
							}
							if (gotit != -1)
							{
								sprintf(buf,"%s kann %s %d",unitid(ziel),skillnames[gotit],effskill(ziel,gotit));
							} else
							{
								sprintf(buf,"%s kann noch gar nichts",unitid(ziel));
							}
							addevent(u,buf);
							break;
				}
			}
		}
	}
	printf(" ... fertig\n");
}



char *getLebenszustand(unit *u)
{
	// es wird der Gesundheitszustand nicht von den Werten aus der Tabelle berechnet, sondern zu
	// den Werten mit Ausdauer, ansonsten ist die Einheit immer gleich tot und vorher im super Zustand
	// { "schwer verwundet", "verwundet", "angeschlagen", "leicht angeschlagen", "gut" };
	static char str[20];
	int a	= effskill(u, SK_AUSDAUER);
	int a1	= max((a-1), 0);			// sonst kommt in der n�chsten Formel ein negativer Wert raus
	int lp	= (lebenspunkte[u->race][0] + (lebenspunkte[u->race][0] * ((a*5 + a1*2) / 100))) * u->number;

	int	s	= (int) (((double) u->lebenspunkte / (double) lp) * (double) 4);		// um auf einen Wert zwischen 0 und 4 zu kommen

	// printf("LP: %d - %d\n", lp, s);

	switch (s)
	{
		// hier muss jetzt umgedreht werden, da u->lebenspunkte nach oben gez�hlt wird
		case 0:	sprintf(str, "gut");					break;
		case 1: sprintf(str, "leicht angeschlagen");	break;
		case 2: sprintf(str, "angeschlagen");			break;
		case 3: sprintf(str, "verwundet");				break;
		case 4:	sprintf(str, "schwer verwundet");		break;
		default: sprintf(str, "[Fehler] bitte entsprechend melden");	break;
	}

	return str;
}

int maxLebenszustand(unit *u)
{
	// es wird der Gesundheitszustand nicht von den Werten aus der Tabelle berechnet, sondern zu
	// den Werten mit Ausdauer, ansonsten ist die Einheit immer gleich tot und vorher im super Zustand
	int a	= effskill(u, SK_AUSDAUER);
	int a1	= max((a-1), 0);			// sonst kommt in der n�chsten Formel ein negativer Wert raus
	return	(lebenspunkte[u->race][0] + (lebenspunkte[u->race][0] * ((a*5 + a1*2) / 100))) * u->number;
}
