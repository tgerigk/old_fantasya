/* German Atlantis PB(E)M host Copyright (C) 1995-1998   Alexander Schroeder

 based on:
 
  Atlantis v1.0  13 September 1993 Copyright 1993 by Russell Wallace
  
   This program may be freely used, modified and distributed. It may not be
   sold or used commercially without prior written permission from the
   author.
   
*/

#include "atlantis.h"
#include <math.h>

int
canlearn (unit *u, int i)
{
/* Eine Einheit kann einen Spruch lernen, wenn ihr halbes Magietalent (aufgerundet) gleich gross
	oder groesser wie die Stufe des Spruches ist.  */
	return ((effskill (u, SK_MAGIC) + 1) / 2 >= spelllevel[i]);
}

void
givespell (int i, unit *u, unit *u2, strlist *S)
{
/* Nocheinmal, da man weder an Bauern noch an Niemand einen Spruch geben
	kann. */
	
	if (!u2)
    {
		if (getunitpeasants)
        {
			mistake2 (u, S, "Die Bauern koennen nicht zaubern");
			return;
        }
		else
        {
			mistake2 (u, S, "Die Einheit wurde nicht gefunden");
			return;
        }
    };
	
	if (!u->spells[i])
    {
		mistake2 (u, S, "Die Einheit beherrscht diesen Spruch gar nicht");
		return;
    }
	
	if (!canlearn (u2, i))
    {
		mistake2 (u, S, "Die Einheit kann nicht gut genug zaubern, um diesen Spruch zu lernen");
		return;
    }
	
	u2->spells[i] = 1;
	
	addevent (u , translate (ST_GIVE_SPELL, u->faction->language, unitid (u), unitid2 (u2),
		strings[spellnames[i]][u->faction->language]));
	if (u->faction != u2->faction)
		addevent (u2 , translate (ST_GIVE_SPELL, u2->faction->language, unitid (u), unitid2 (u2),
		strings[spellnames[i]][u->faction->language]));
	
	if (!u2->faction->seendata[i])
    {
		u2->faction->seendata[i] = 1;
		u2->faction->showdata[i] = 1;
    }
}

void
giveallspells (unit  *u, unit *u2, strlist *S)
{
	int i;
	
	sprintf (buf, "%s leiht ", unitid (u));
	scat (unitid (u2));
	scat (" das Zauberbuch aus.");
	addevent (u , buf);
	if (u->faction != u2->faction)
		addevent (u2 , buf);
	
	/* Dieser code steht aehnlich auch in givemen. */
	for (i = 0; i != MAXSPELLS; i++)
		if (u->spells[i] && !u2->spells[i] && canlearn (u2, i))
			givespell (i, u, u2, S);
}

void
giveitem (int n, int i, unit *u, region *r, unit *u2, strlist *S)
{
	if (n > u->items[i])
		n = u->items[i];
	
	if (n == 0)
    {
		mistake2 (u, S, "Die Einheit hat diesen Gegenstand nicht");
		return;
    }
	
	u->items[i] -= n;
	
	if (!u2)
    {
		if (i == I_HORSE)
			r->horses += n;
		
		if (getunitpeasants)
			addcommerce (u , translate (ST_GIVE_PEASANTS_ITEMS, u->faction->language, unitid (u), n, 
			strings[itemnames[n != 1][i]][u->faction->language]));
		else
			addcommerce (u , translate (ST_GIVE_NOBODY_ITEMS, u->faction->language, unitid (u), n, 
			strings[itemnames[n != 1][i]][u->faction->language]));
		return;
    }
	
	u2->items[i] += n;
	
	if (u->faction != u2->faction)
    {
		addcommerce (u , translate (ST_GIVE_UNIT_ITEMS, u->faction->language, unitid (u), n, 
			strings[itemnames[n != 1][i]][u->faction->language], unitid2 (u2)));
		addcommerce (u2 , translate (ST_GIVE_UNIT_ITEMS, u2->faction->language, unitid (u), n, 
			strings[itemnames[n != 1][i]][u2->faction->language], unitid2 (u2)));
    }
	else
		addevent (u , translate (ST_GIVE_UNIT_ITEMS, u->faction->language, unitid (u), n, 
		strings[itemnames[n != 1][i]][u->faction->language], unitid2 (u2)));
}

void
givemen (int n, unit * u, region * r, unit * u2, strlist * S)
{
	int i, j, k;
	int lp;	// Lebenspunkte pro Person
	
	if (n > u->number) n = u->number;
	
	lp = 0;
	if (u->number) lp = u->lebenspunkte / u->number;	// Lebenspunkte pro Person berechnen

	if (n == 0)
    {
		mistake2 (u, S, strings[ST_NO_MEN_TO_GIVE][u->faction->language]);
		return;
    }
	
	/* Falls Magier transferiert werden: */
	if (u2 && (u->skills[SK_MAGIC] || u2->skills[SK_MAGIC]))
    {
		/* Zaehle Magier in der Zielpartei: */
		k = magicians (u2->faction);
		
		/* Falls die Zieleinheit keine Magier sind, werden sie nun welche. */
		if (!u2->skills[SK_MAGIC]) k += u2->number;
		
		/* Falls sie in eine neue Partei kommen, zaehlen sie selber auch mit zum Kontigent an Magier der Zielpartei. */
		if (u2->faction != u->faction) k += n;
		
		/* Falls dies nun zuviele sind gibt es einen Fehler */
		if (k > MAXMAGICIANS)
        {
			mistake2 (u, S, translate (ST_MAX_MAGICIANS, u->faction->language, MAXMAGICIANS));
			return;
        }
    }

	if (u2)
	{
		if (u->race != u2->race)
		{
			sprintf(buf,"%s hat eine andere Rasse als %s", unitid(u), unitid(u2));
			addevent(u,buf);
			addevent(u2,buf);
			return;
		}
	}
	
	k = u->number - n;
	
	/* Transfer Talente */
	for (i = 0; i != MAXSKILLS; i++)
    {
		j = distribute (u->number, k, u->skills[i]);
		if (u2) u2->skills[i] += u->skills[i] - j;
		u->skills[i] = j;
    }
	
	u->number = k;
	u->lebenspunkte = lp * k;	// Lebenspunkte neu berechnen

	if (u2)
    {
		u2->number += n;
		u2->lebenspunkte += lp * n;	// Lebenspunkte m�ssen hinzu
		
		/* Transfer der Sprueche ohne Meldung, aehnlich wie giveallspells.  */
		for (i = 0; i != MAXSPELLS; i++)
			if (u->spells[i] && !u2->spells[i] && canlearn (u2, i)) u2->spells[i] = 1;
			
			addevent (u , translate (ST_GIVE_MEN, u->faction->language,unitid (u), n, unitid2 (u2)));
			if (u->faction != u2->faction)
				addevent (u2 , translate (ST_GIVE_MEN, u2->faction->language, unitid (u), n, unitid2 (u2)));
    }
	else
    {
		if (getunitpeasants)
        {
			r->peasants += n;
			if (k)
				addevent (u , 
				translate (ST_GIVE_MEN_TO_PEASANTS, 
				u->faction->language, unitid (u)));
			else
				addevent (u , 
				translate (ST_GIVE_ALL_MEN_TO_PEASANTS, 
				u->faction->language, unitid (u)));
        }
		else
        {
			if (k)
				addevent (u , 
				translate (ST_GIVE_MEN_TO_NOTHING, 
				u->faction->language, unitid (u), n));
			else
				addevent (u , 
				translate (ST_GIVE_ALL_MEN_TO_NOTHING, 
				u->faction->language, unitid (u), n));
        }
    }
}

void
givesilver (int n, unit * u, region * r, unit * u2, strlist * S)
{
	if (n > u->items[I_SILVER])
		n = u->items[I_SILVER];
	
	if (n == 0)
    {
		mistake2 (u, S, strings[ST_NO_MONEY][u->faction->language]);
		return;
    }
	
	u->items[I_SILVER] -= n;
	
	if (u2)
    {
		u2->items[I_SILVER] += n;
		
		sprintf (buf, "%s zahlte $%d an ", unitid (u), n);
		scat (unitid (u2));
		scat (".");
		if (u->faction != u2->faction)
		{
			addcommerce (u , buf);
			addcommerce (u2 , buf);
		}
		else
			addevent (u , buf);
    }
	else
    {
		if (getunitpeasants)
        {
			r->money += n;
			
			sprintf (buf, "%s verteilt $%d unter den Bauern.", unitid (u), n);
        }
		else
			sprintf (buf, "%s wirft $%d weg.", unitid (u), n);
		addcommerce (u , buf);
    }
}

void
giveall (unit * u, region * r, unit * u2, strlist * S)
{
	int i;

	if (u->items[I_SILVER]) givesilver (u->items[I_SILVER], u, r, u2, S);
	for (i = 0; i != MAXITEMS; i++) if (u->items[i]) giveitem (u->items[i], i, u, r, u2, S);
}



int gaeste(int anz)
{
	// Formel stimmt nicht !!!
	return (int) ( log10( (double) anz / (double) 50.0 ) * (double) 20.0 );
}


void countPersonen()
{
	region* r;
	unit* u;
	faction *f;
	int anz=0;	// m�gliche Migranten
	int ist = 0;
	
	puts("- Personen und Gaeste zaehlen");
	for(f = factions; f; f = f->next) { f->anzahl = 0; f->gaeste = 0; }
	for(r=regions; r; r=r->next)
	{
		for(u=r->units; u; u=u->next)
		{
			if (!u->faction) continue;
			// hier keine Migranten z�hlen !!!!!!!!!!!!!!!!
			if (u->faction->race == u->race) u->faction->anzahl += u->number;
			if (u->faction->race != u->race) if (u->race != R_NORACE) u->faction->gaeste += u->number; else u->faction->anzahl += u->number;
		}
	}
}


void giveunit (unit * u, region * r, unit * u2, strlist * S)
{
	int anz;
	if (!u2)
    {
		giveall (u, r, u2, S);
		return;
    }
	
	// wenn die Einheit bereits �bergeben wurde
	if (u->faction == u2->faction) return;

	if (u->faction->race != u2->faction->race)
	{
		anz = gaeste(u->faction->anzahl);	// Anzahl der m�glichen Gaeste
		anz -= u->faction->gaeste;			// minus Anzahl der bereits vorhanden Gaeste

		if (anz<1)
		{
			sprintf(buf,"%s kann nicht �bergeben werden, zuviele Migranten.", unitid(u));
			addevent (u,buf);
			addevent(u2,buf);
			return;
		}
	}

	if (u->skills[SK_MAGIC]	&& magicians (u2->faction) + u->number > MAXMAGICIANS)
    {
		sprintf (buf, "Es kann maximal %d Magier pro Partei geben", MAXMAGICIANS);
		mistake2 (u, S, buf);
		return;
    }
	
	sprintf (buf, "%s wechselt zu %s", unitid (u), factionid (u2->faction));
	addevent (u , buf);
	addevent (u2 , buf);
	u->faction = u2->faction;
	u->tarn_no = u2->faction->no;		// als neue Partei ausgeben
}

/* ------------------------------------------------------------- */

/* Diese Funktion wird auch verwendet, um das fuer den Lebensunterhalt
noetige Geld (meist $10) einzusammeln.  Dies ist der side-effect.
Der return Wert wird in der naechsten Funktion collectsomemoney
gebraucht, um den Text der Message zu machen.  */
int
collectmoney (region * r, unit * collector, int n)
{
	int i, sum=0;
	unit *u;
	
	/* n gibt an, wieviel Silber noch eingesammelt werden soll.  */
	
	for (u = r->units; u && n >= 0; u = u->next)
		if (u->faction == collector->faction &&
			u != collector &&
			u->type == U_MAN &&    /* nur menschen geben geld her */
			can_contact (r, collector, u))
		{
			i = min (u->items[I_SILVER], n);
			/* u->money wird immer 0, ausser bei der letzten Einheit,
			welche nicht mehr alles hergeben muss.  */
			u->items[I_SILVER] -= i;
			sum += i;
			n -= i;
		}
		collector->items[I_SILVER] += sum;
		return sum;
}

void
collectsomemoney (region * r, unit * collector, int n)
{
	sprintf (buf, "%s sammelte $%d ein.", unitid (collector), collectmoney (r, collector, n));
	addevent (collector, buf);
}

void
collectallmoney (region * r, unit *collector)
{
	int sum=0;
	unit *u;
	
	for (u = r->units; u; u = u->next)
		if (u->faction == collector->faction &&	u != collector && u->type == U_MAN && can_contact (r, collector, u))
		{
			sum += u->items[I_SILVER];
			u->items[I_SILVER] = 0;
		}
		collector->items[I_SILVER] += sum;
		sprintf (buf, "%s sammelte $%d ein.", unitid (collector), sum);
		addevent (collector , buf);
}

/* ------------------------------------------------------------- */

void
giving (void)
{
	region *r;
	unit *u, *u2;
	strlist *S;
	char *s;
	int i, n;
	int kontakt;		// 1 wenn die Einheit Kontakt aufgenommen hat, sonst 0
	int wahrnehmung;	// h�chstes Talent der Zielpartei
	
	puts ("- geben...");
	
	/* GIB vor STIRB!  */
	for (r = regions; r; r = r->next)
		for (u = r->units; u; u = u->next)
			for (S = u->orders; S; S = S->next)
				switch (igetkeyword (S->s))
			{
	  case K_COLLECT:
		  
		  s = getstr ();
		  if (findparam (s) == P_ALL)
		  {
			  collectallmoney (r, u);
			  continue;
		  }
		  
		  n = atoip (s);
		  if (n)
		  {
			  collectsomemoney (r, u, n);
			  continue;
		  }
		  
		  mistake2 (u, S, "Die Silbermenge wurde nicht angegeben");
		  break;
		  
	  case K_DELIVER:
	  case K_GIVE:
		  
		  /* Drachen, Illusionen etc geben nichts her */
		  if (u->type != U_MAN)
		  {
			  mistake2 (u, S, translate (ST_MONSTERS_DONT_GIVE, u->faction->language, strings[typenames[1][u->type]][u->faction->language]));
			  continue;
		  }
		  
		  u2 = getunit (r, u);
		  if (u==u2)
		  {
			  mistake2 (u, S, "Das ist die selbe Einheit");
			  continue;
		  }
		  
		  /* Ziel auffinden */
		  if (!u2 && !getunit0 && !getunitpeasants)
		  {
			  mistake2 (u, S, "Die Einheit wurde nicht gefunden");
			  continue;
		  }
		  
		  kontakt = 1;	// Empf�ngereinheit hat erstmal Kontakt aufgenommen
		  if (u2 && !contacts (r, u2, u)) kontakt = 0;	// leider doch nicht
		  // sp�ter wird Kontakt nochmal kontrolliert, jetzt darf noch nicht abgebrochen werden
		  // da ja einige Tr�nke ohne KONTAKTIERE �bergeben werden m�ssen
		  
		  /* Drachen, Illusionen etc nehmen auch nichts an (va. keine neuen Leute!).  */
		  if (u2 && u2->type != U_MAN)
		  {
			  mistake2 (u, S, translate (ST_MONSTERS_DONT_TAKE, u->faction->language, strings[typenames[1][u2->type]][u->faction->language]));
			  continue;
		  }
		  
		  s = getstr ();
		  switch (findparam (s))
		  {
		  case P_CONTROL:
			  /* KONTROLLE in enter ()! */
			  continue;
			  
//		  case P_ALL:
//			  /* ALLES: Mit einer anderen Einheit mischen.  */
//			  if (kontakt) giveall (u, r, u2, S); else mistake2(u,S,"Einheit hat keinen Kontakt");
//			  continue;
			  
		  case P_SPELLBOOK:
			  /* ZAUBERBUCH: uebergibt alle Sprueche.  */
			  if (kontakt) giveallspells (u, u2, S); else mistake2(u,S,"Einheit hat keinen Kontakt");
			  continue;
			  
		  case P_UNIT:
			  /* EINHEIT: Einheiten als ganzes an eine andere Partei uebergeben.  */
			  if (kontakt) giveunit (u, r, u2, S); else mistake2(u,S,"Einheit hat keinen Kontakt");
			  continue;
		  }
		  
		  /* Falls nicht, dann steht in s vielleicht ein Zauberspruch */
		  i = findspell (s);
		  if (i >= 0)
		  {
			  if (kontakt) givespell (i, u, u2, S); else mistake2(u,S,"Einheit hat keinen Kontakt");
			  continue;
		  }
		  
		  /* Falls nicht, dann steht darin eine Anzahl */
		  n = atoip (s);
		  /* und nun folgt ein Gegenstand... */
		  s = getstr ();
		  
		  switch (findparam (s))
		  {
			  /* PERSONEN */
		  case P_MAN:
		  case P_PERSON:
			  if (kontakt) givemen (n, u, r, u2, S); else mistake2(u,S,"Einheit hat keinen Kontakt");
			  continue;
			  
			  /* SILBER */
		  case P_SILVER:
			  if (kontakt) givesilver (n, u, r, u2, S); else mistake2(u,S,"Einheit hat keinen Kontakt");
			  continue;
		  }
		  
		  i = findkraut(s);
		  if (i>=0)
		  {
			  if (kontakt) gib_kraeuter(n,u,r,u2,i,S); else mistake2(u,S,"Einheit hat keinen Kontakt");
			  continue;
		  }
		  
		  i = findtrank(s);
		  if (i>=0)		
		  {
			  if (kontakt)
			  {
				  gib_traenke(n,u,r,u2,i,S); 
				  continue;
			  }
			  // wenn kein Kontakt dann Kontrolle ob Trank so �bergeben werden kann, in 'i' ist der trank enthalten
			  wahrnehmung = highskill(u2->faction,r,SK_OBSERVATION);
			  if ((i == TR_NACHTFIEBER) || (i == TR_GEHIRNTOT))
			  {
				  if (wahrnehmung > effskill(u,SK_STEALTH))
				  {
					  sprintf(buf,"%s sollte von %s einen Trank untergeschummelt bekommen",unitid(u2),unitid(u));
					  addevent(u2,buf);
					  sprintf(buf,"%s hat mitbekommen, das wir ihm einen TRank unterschummeln wollten",unitid(u2));
					  addevent(u,buf);
					  continue;
				  }
				  if (wahrnehmung == effskill(u,SK_STEALTH))
				  {
					  sprintf(buf,"%s f�hlt sich beobachtet",unitid(u2));
					  addevent(u2,buf);
					  if (rand()%128 < 64)		// 5o% Fehlschlag
					  {
						  sprintf(buf,"%s hat eventuell gemerkt, das wir ihm einen Trank unterschummeln wollten",unitid(u2));
						  addevent(u,buf);
						  continue;
					  }
				  }
			  }
			  
			  switch (i)
			  {
			  case TR_NACHTFIEBER:
				  sprintf(buf,"%s hat %s %d Nachtfieber uebergeben", unitid(u), unitid(u2), n);
				  addalchemie(u, buf);
				  u2->lebenspunkte += n * 10;
				  break;
			  case TR_GEHIRNTOT:
				  u2->gehirntot = n * 300;	// 3oo Lerntage pro Gehirntot   -   nicht additiv, nur auffrischen
				  sprintf(buf,"%s hat %s %d Gehirntot uebergeben",unitid(u), unitid(u2), n);
				  addalchemie(u, buf);
				  break;
			  default:
				  // alle anderen Tr�nke ben�tigen einen Kontakt
				  mistake2(u,S,"Einheit hat keinen Kontakt f�r �bergabe des Trankes");
				  break;
			  }
			  continue;
		  }
		  
		  i = finditem (s);
		  if (i >= 0)
		  {
			  if (kontakt) giveitem (n, i, u, r, u2, S); else mistake2(u,S,"Einheit hat keinen Kontakt");
			  continue;
		  }
		  
		  /* Wenn es bis hierhin nichts war, dann hat die Einheit soetwas nicht.  */
		  mistake2 (u, S, "So etwas hat die Einheit nicht");
	  }
}
