/*
 * das neue Men� f�r Fantasya, quasi Worldeditor
 */



//#include <windows.h>
#include "atlantis.h"
#include <conio.h>



welt *world;		// aktuelle Welt
int regx, regy;		// aktuelle Koordinaten in der Welt
int debug = 0;

//HANDLE *console;	// zur Verwaltung diverser Feinheiten



extern char factionsymbols (region * r);
extern char armedsymbols (region *r);

faction *pmf;		// diese Partei wird von drawmap() ben�tigt, es soll nur diese Partei angezeigt werden



void clrscr()
{
	system("cls");
//	cls(stdout);
	return;
}

void drawmap (int mode)
{
	int x, y, z, minx, miny, maxx, maxy, minz, maxz;
	region *r;
	int n;
	welt *w;
	
	F = stdout;

	minz = INT_MAX;
	maxz = INT_MIN;
		
	w = world;
	z = world->ebene;

	// Karte ausgeben
    {
		
		minx = INT_MAX;
		maxx = INT_MIN;
		miny = INT_MAX;
		maxy = INT_MIN;
		
		for (r = regions; r; r = r->next)
			if (r->z == z)
			{
				minx = min (minx, r->x);
				maxx = max (maxx, r->x);
				miny = min (miny, r->y);
				maxy = max (maxy, r->y);
			}
			
			printf("\n\n");
			
			for (y = maxy; y >= miny; y--)
			{
				memset (buf, ' ', sizeof buf);
				buf[maxx - minx + 1] = 0;
				
				for (r = regions; r; r = r->next)
					if (r->y == y && r->z == z)
					{
						/* if (r->units && mode == M_FACTIONS)
						{
							buf[r->x - minx] = factionsymbols (r);
						} else
						{
							if (r->units && mode == M_UNARMED)
							{
								buf[r->x - minx] = armedsymbols (r);
							} else
							{	// default: terrain
								buf[r->x - minx] = terrainsymbols[mainterrain (r)];
							}
						} */
						buf[r->x - minx] = terrainsymbols[mainterrain (r)];
						switch(mode)
						{
							case M_PARTEI:		if ((r->units) && (pmf))
												{
													unit *u;
													for(u = r->units; u; u = u->next) if (pmf == u->faction) buf[r->x - minx] = '1';
												}
												break;

							case M_FACTIONS:	if (r->units) buf[r->x - minx] = factionsymbols (r);
												break;
							case M_UNARMED:		if (r->units) buf[r->x - minx] = armedsymbols (r);
												break;
							case M_HOEHLE:		{
													region *hr;
													hr = findregion(r->x, r->y, -(r->z)); // passende Unterweltebene
													if (hr) if ((hr->terrain != T_LAVASTROM) && (r->terrain != T_OCEAN))
													{
														buf[r->x - minx] = r->hoehle == 0 ? '0' : '1';
														break;
													}
												}
												break;
							case M_STURM:		if (r->terrain == T_OCEAN) buf[r->x-minx] = '0' + r->sturm;
												break;
							case M_EIS:			if (r->eis) if (r->terrain == T_OCEAN) buf[r->x-minx] = '0' + r->eis;
												break;
						}
					}
					for (n = y; n > miny; n--) fputc (' ', F);	// Added by Bjoern Becker , 16.08.2000
					for (x = 0; buf[x]; x++)
					{
						if (y == regy && x == -minx+regx)
						{
							fputc ('(', F);
						} else 
						{
							if (y == regy && x == -minx + 1+regx)
							{
								fputc (')', F);
							} else
							{
								fputc (' ', F);
							}
						}
						fputc (buf[x], F);
					}
					fputs ("\n", F);
			}
			fputs ("\n", F);
    }
}



void TerrainInfo(region *r)
{
	building *b;
	ship *sh;
	unit *u;

	printf("%s \"%s\" (%d,%d), %d Baeume, %d Bauern, %d$\n", strings[terrainnames[mainterrain (r)]][0], r->name, r->x, r->y, r->trees, r->peasants, r->money);

	for(b = r->buildings; b; b=b->next)
	{
		printf("%s \"%s\" (%s), Groesse %d\n", b->typ == BT_BURG ? buildingnames[buildingeffsize(b)] : BuildingNames[b->typ], b->name, tobase36(b->no), b->size);
	}

	for(sh = r->ships; sh; sh=sh->next)
	{
		printf("%s \"%s\" (%s)\n", shiptypes[0][sh->type], sh->name, tobase36(sh->no));
	}

	for(u = r->units; u; u = u->next)
	{
		printf("%s, %s, %s\n",unitid(u), racenames[u->race][0], factionid(u->faction));
	}

	if (debug)
	{
		strlist *s;
		puts("- Debuginfo");
		for(s = r->debug; s; s = s->next) printf("\t%s\n", s->s);
	}
}

void printmapmode(int mapmode)
{
	switch(mapmode)
	{
		case 0:		printf("Terrain");				break;
		case 1:		printf("Partei");				break;
		case 2:		printf("Krieger");				break;
		case 3:		printf("Hoehlen");				break;
		case 4:		printf("Sturm");				break;
		case 5:		printf("Eis");					break;
		case 6:		printf("Partei Map");			break;
		default:	printf("Mapmode unbekannt");	break;
	}
}

void newmenu()
{
	int ende = 0;			// neues Men� beenden?
	int c;
	int mapmode = 0, maptoggle = 0;
	int i;

	int hv = 0;				// Hilfsvariable
	region *hr;				// Hilfsregion bzw. aktuelle Region, sollte eigentlich als tr abgek�rzt werden
	building *hb;
	unit *hu;
//	faction *hf;

	world = welten;
	regx = regy = 0;		// auf Weltursprung

	while (!ende)
	{
		clrscr();
		printf("\n\t-+*+-   Fantasya %s   -+*+-\n\n", fanta_version);
		printf("Welt: %d - %s %s",world->ebene, world->mail, !world->sicher ? "": "[gesichert]");
		
		// Mapmode anzeigen
		printf("Mapmode: ");
		printmapmode(mapmode);
		printf(" (");
		printmapmode(maptoggle);		// alternative
		printf(")\n");
		
		hr = findregion(regx,regy,world->ebene);
		if (!hr)
		{
			puts("-> erzeuge einmal Regionen");
			makeblock (regx, regy, world->ebene);
			maxRegions = listlen(regions);
			findcubus();
			connectregions();
			hr = findregion(regx,regy,world->ebene);
/*			if (!hr)
			{
				// wenn jetzt wieder nix, dann ist Grenze der Welt erreicht
				regx = regy = 0;	// zur�ck zum Nullpunkt
				hr = findregion(regx,regy,world->ebene);
			}*/
		}

		drawmap(mapmode);
		TerrainInfo(hr);

		while( !_kbhit() ) /* warte auf eine Taste */;

		c = _getch(); // diese dann einlesen
		fflush(stdin);

		switch(c)
		{
			case 'Q': ende = 1;
					  break;
			case 'q': printf("Welche Welt ? (aktuelle %d) ", world->ebene);
					  scanf("%d",&c); fflush_stdin();
					  world = GetWorld(c);				// kackt ab, wenn Welt nicht existiert !!!
					  regx = regy = 0;
					  break;

			// Steuerung des Cursors, teilweise mit durchfallen um auf +/- 1o zu kommen
			case 'W': regy += 9;
			case 'w': regy++; break;
			case 'S': regy -= 9;
			case 's': regy--; break;
			case 'A': regx -= 9;
			case 'a': regx--; break;
			case 'D': regx += 9;
			case 'd': regx++; break;
			case 'c': // Zentrierung auf Ursprung
			case 'C': regx = regy = 0; break;

			case 'M': // toggeln der Map zwischen gew�hlten Modus und Regionsansicht
					{
					  int d = 0;
					  if (mapmode == M_TERRAIN)
					  {
						  d = mapmode;
						  mapmode = maptoggle;
						  maptoggle = d;
					  } else
					  {
						  d = maptoggle;
						  maptoggle = mapmode;
						  mapmode = d;
					  }
					  break;
					}
			case 'm': mapmode = mapmode == MAXMODES-1 ? 0 : mapmode + 1;
					  break;

			case 't': // Terraforming
			case 'T': hv = hr->terrain;
					  if (c == 't' ? (hv == MAXTERRAINS-1) : (hv == MAXTERRAINS_NEW-1))
					  {
						  hv = 0;
					  } else
					  {
						  if ((hr->terrain == T_PLAIN) && (hr->trees > 599))
						  {
							  hv += 2;
						  }	else 
						  {
							  hv++;
						  }
					  }
					  if (hv > MAXTERRAINS_NEW-1) hv = 0;
					  terraform(hr,hv);
					  if (hr->terrain > MAXTERRAINS_NEW-1) hr->terrain -= MAXTERRAINS;
					  break;

			case 'z': terraform(hr, rand() % MAXTERRAINS);
					  break;

			case '0': MakeHoehle(hr);												break;

			// schnelle Terraforming, umfasst nur Ozean, Ebene, Wald und Berge
			case '1': terraform(findregion(regx,regy,world->ebene), T_OCEAN);		break;
			case '2': terraform(findregion(regx,regy,world->ebene), T_PLAIN);		break;
			case '3': terraform(findregion(regx,regy,world->ebene), T_FOREST);		break;
			case '4': terraform(findregion(regx,regy,world->ebene), T_MOUNTAIN);	break;
			case '5': terraform(findregion(regx,regy,world->ebene), T_LAVASTROM);	break;

			case 'B': // Geb�ude anlegen
					  for(i=0; i<MAX_BURGTYPE; i++) printf("%d - %s\n", i, BuildingNames[i]); i = -1;
					  while ((i<0) || (i>MAX_BURGTYPE)) { printf("> "); scanf("%d",&i); fflush(stdin); }
					  if (i>0) // alles au�er Burgen
					  {
						  // Geb�ude anlegen
						  hb = cmalloc (sizeof (building));
						  memset (hb, 0, sizeof (building));
						  do hb->no++; while (findbuilding (hb->no));

						  if (!produktionsmatrix[i][PM_SIZE])
						  {
							  printf("Groesse ? "); scanf("%d",&hv);
						  } else 
						  {
							  hv = produktionsmatrix[i][PM_SIZE];
						  }
						  hb->size = hv;

						  hb->typ = i;
						  sprintf (buf, "%s %s", BuildingNames[i], tobase36(hb->no) );
						  mnstrcpy (&hb->name, buf, NAMESIZE);
						  addlist (&hr->buildings, hb);					  
						  break;
					  }
					  // hier geht es automatisch zum Burgenerstellen weiter !!!!
					  printf("\n");
			case 'b': // Burgen erstellen
					  for(i=0; i<MAXBUILDINGS; i++) printf("%d - %s\n", i, buildingnames[i]); i = -1;
					  while ((i<0) || (i>MAXBUILDINGS)) { printf("> "); scanf("%d",&i); fflush(stdin); }
					  // Burg anlegen
					  hb = cmalloc (sizeof (building));
					  memset (hb, 0, sizeof (building));
					  do hb->no++; while (findbuilding (hb->no));
					  hb->size = buildingcapacity[i];
					  if (!hb->size) hb->size++;
					  sprintf (buf, "Burg %s", tobase36(hb->no) );
					  mnstrcpy (&hb->name, buf, NAMESIZE);
					  addlist (&hr->buildings, hb);					  
					  break;

			case 'N': // neuen Spieler aussetzen
					  printf("e-Mail: ");
					  gets (buf);
					  if (buf[0]) 
					  {
						addaplayerat(hr);

						// jetzt noch einige Sch�nheitskorrekturen
						hr->peasants = max(hr->peasants, 5000);
						hr->money = max(hr->money, 50000);
					  }
					  break;

			case 'E': // neue Einheit
					  hu = createunit(hr);
					  printf("Volk: ");
					  gets(buf);
					  hu->faction = findfaction(todec(buf));
					  hu->number = 1;								// eine Person
					  hu->tarn_no = hu->faction->no;				// als eigene Partei ausgeben
					  hu->race = hu->faction->race;					// Rasse
					  hu->raceTT = hu->faction->race;				// ACHTUNG!! hier wird ggf. automatisch als Echsenmensch getarnt
					  break;
			case 'R': switch (rand() % MAXTYPES)
					  {
						  case U_FIREDRAGON:		hu = make_firedragon_unit (hr, findfaction(0), rand() % 5);			break;
						  case U_DRAGON:			hu = make_dragon_unit (hr, findfaction(0), rand() % 4);				break;
						  case U_WYRM:				hu = make_wyrm_unit (hr, findfaction(0), rand() % 2);				break;
						  default:					hu = make_undead_unit (hr, findfaction(0), rand() % 100);			break;
					  }
					  break;

			case 'P': // nur eine Partei via M_PARTEI anzeigen, hier ausw�hlen
					  printf("Volk: ");
					  gets(buf);
					  pmf = findfaction(todec(buf));
					  if (!pmf) puts("Partei existiert nicht!");
					  break;

			case 'g': printf("X> "); scanf("%d",&regx); fflush_stdin();
					  printf("Y> "); scanf("%d",&regy); fflush_stdin();
					  break;
		}
	}

	printf("\n\n");
	return;
}
