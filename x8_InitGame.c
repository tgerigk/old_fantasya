#include "atlantis.h"

int ozean;			// max. 3 Ozean d�rfen angrenzen
char linein[220];	// eine Zeile   *LOL*
unit *units;

char *BuildingNames[MAX_BURGTYPE];	// GA verwendet !! buildingnames !!

int CheckRegion(int x,int y,int z)
{
	region *r;
	unit *u;
	int i=0;		// Anzahl der anwesenden Personen

	r = findregion(x,y,z);

	// wenn Region nicht existiert ist sie leer (wird ja sp�ter erzeugt)
	if (!r)
	{
		ozean++;
		return 1;		// gilt aber als Ozean
	}

	for(u=r->units;u;u=u->next)
	{
		i += u->number;
		if (u->faction->newbie == 1)
		{
			printf("Newbies in N�he\n");
			return 0;	// Newbies nicht gegenseitig z�hlen, da nur 3, somit 3 Personen in Region herauskommen
		}
	}

	// wenn mehr als 10 Personen, dann nix aussetzen
	if (i>0) { ozean += 6; return 0; }

	// soweit alles klar
	if (r->terrain == T_OCEAN) ozean++;
	return 1;
}

int GetARace()
{
	/*
	 * Funktion erwartet in 'linein' die Rasse in der Mehrzahl
	 */

	int i;

	for(i=1;i<MAXRACES;i++)
	{
		if (stricmp(racenames[i][1],linein) == 0) return i;
	}
	printf("Rasse unbekannt '%s'\n",linein);
	exit(EXIT_FAILURE);
}

void AddPlayerListe()
{
	/*
	 * Funktion durchsucht die Regionen nach m�glichen Aussetzungspl�tzen f�r neue Spieler.
	 * Die Datei 'anmeldungen.dat' enth�lt die neuen Spieler.
	 * Zeile 1 ist die Anzahl der neuen Spieler, alle weiteren Zeilen haben die Reihenfolge
	 * e-Mail -> Rasse -> Tarntypus. Der Tarntypus ist nur f�r Echsenmenschen wichtig, wird aber
	 * aus Faulheit mit angegeben.
	 */
	FILE *dat;			// neue Spieler
	int anzahl;			// Anzahl der neuen Spieler
	region *r;			// diese Region (this region)
	int field;			// Laufkoordinate (-36 -> 54)
	int x,y;			// Koordinaten zum durchsuchen
	int welt = 0;		// erstmal nur die 0. Welt;

	dat = fopen("anmeldungen.dat","r");
	if (dat==NULL)
	{
		printf("kann 'anmeldungen.dat' nicht �ffnen!\n");
		return;
	}

	fscanf(dat,"%d",&anzahl);
	printf("setze %d neue Spieler\n",anzahl);
	for(field=-36;field<55;field++)
	{
		x = -37;
		for(y = field; y>-37; y--)
		{
			x++;
			r = findregion(x,y,welt);
			if ((mainterrain(r) == T_PLAIN) && (r->units == NULL) && (anzahl>0))		// eine Ebene, evt. hier aussetzen ??
			{
				int oke = 0;	// muss 6 ergeben wenn ausgesetzt werden kann

				ozean = 0;		// alte Ozeane l�schen
				printf("freie Ebene bei (%d,%d)\n",r->x,r->y);
				oke += CheckRegion(r->x,(r->y+1),welt);			// NO
				oke += CheckRegion((r->x+1),r->y,welt);			// O
				oke += CheckRegion((r->x+1),(r->y-1),welt);		// SO
				oke += CheckRegion(r->x,(r->y-1),welt);			// SW
				oke += CheckRegion((r->x-1),r->y,welt);			// W
				oke += CheckRegion((r->x-1),(r->y+1),welt);		// NW
				if (ozean>3)
				{
					printf("zuviele Ozeane!\n");
					oke = 0;							// Ozeane max. 3
				}
				if (oke == 6)
				{
					int i;

					printf("\n%d Ozeane\n",ozean);

					for(i=3; i ; i--)
					{
						// neuen Spieler aussetzen
						if (anzahl>0)
						{
							faction *f;
							unit *u;

							fscanf(dat,"%s",&linein);	// e-Mail
							sprintf(buf,"%s",linein);
							
							// erzeugen der Partei
							printf("\nerzeuge Partei .. ");

							f = cmalloc (sizeof (faction));
							memset (f, 0, sizeof (faction));

							mnstrcpy (&f->addr, buf, DISPLAYSIZE);

							generate_password (f);
							f->lastorders = turn;
							f->alive = 1;
							f->newbie = 1;
							f->alter = turn;

							fscanf(dat,"%s",&linein);	// Rasse
							f->race = GetARace();
							f->xorigin = r->x;
							f->yorigin = r->y;

							f->options = (int)pow (2, x8_TEXT) + (int)pow (2, x8_CR) + (int)pow (2, x8_STATISTIK)
										 + (int)pow (2, x8_ZUGVORLAGE) + (int)pow (2, x8_ZEITUNG) + (int)pow (2, x8_EMAILS)
										 + (int)pow (2, x8_ZIP);

							do
								f->no++;
							while (findfaction (f->no));

							sprintf (buf, "Volk %s", tobase36(f->no) );
							mnstrcpy (&f->name, buf, NAMESIZE);
							addlist (&factions, f);

							// und einer Einheit
							printf("erzeuge Einheit ...");

							u = createunit (r);
							u->number = 1;
							u->items[I_SILVER] = STARTMONEY+(STARTMONEY_BONUS*turn);
							u->skills[SK_OBSERVATION]=1700;
							u->items[I_WOOD]=30;
							u->items[I_STONE]=30;
							u->faction = f;
							u->tarn_no = f->no;

							fscanf(dat,"%s",&linein);	// Tarntypus
							u->race = f->race;
							u->raceTT = u->race == R_DEMON ? GetARace() : f->race;
							
							if (u->race == R_ORK)
							{
								u->skills[SK_SWORD] = 30;
								u->skills[SK_SPEAR] = 30;
							}

							printf(" oke\n");
							anzahl--;
						}
					}
				}
			}	// mainterrain == T_PLAIN
		} // Koordinaten
	} // Field-Lauf

	if (anzahl>0) 
	{
		printf("nicht alle Newbies ausgesetzt!\n");
		exit(EXIT_FAILURE);
	}
}

/*
 * Initialiesierung neuer Spielinhalte mit Zufall oder festgelegten Werten
 */
void InitNewGameRules()
{
	int i;
	static int monthnames[] =
	{
		ST_JANUARY,
		ST_FEBRUARY,
		ST_MARCH,
		ST_APRIL,
		ST_MAY,
		ST_JUNE,
		ST_JULY,
		ST_AUGUST,
		ST_SEPTEMBER,
		ST_OCTOBER,
		ST_NOVEMBER,
		ST_DECEMBER,
	};
	
	for(i=0; i < 12; i++) printf("%2d .. %s\n", i, strings[monthnames[i]][0]);
}

welt *GetWorld(int ebene)
{
	welt *w;
	for(w = welten; w; w=w->next)
	{
		if (w->ebene == ebene) return w;
	}

	return 0;
}

void Rundschreiben(void)
{
	faction *f;
	FILE * BAT;
	char subject[80];
	char pbuf[200];

	BAT = fopen("reports\\mailit.rx","w");
	fprintf(BAT,"/* Rundschreiben */\nADDRESS YAM\n");

	printf("Betreff ? "); gets(subject);
	for (f = factions; f; f = f->next)
	{
		if (netaddress (f->addr))
		{
			sprintf(pbuf, "blat.exe reports\\rundschreiben.dat -to %s -server 127.0.0.1 -u "
						  "admin -pw homepage7 -replyto mogel@x8bit.de "
						  "-from fantasya@x8bit.de -subject \"[Fantasya] %s\""
						  ,buf			// enth�lt die e-Mail
						  ,subject);
			printf("versende Rundeschreiben f�r %s (%s)\n", f->name, tobase36(f->no));
			system(pbuf);
		}
	}

	fclose(BAT);
}

long highunitnummer = 0;			// h�chste im Spiel vorhandene Unitnummer

// diese Funktion f�ngt bei 0 an Nummern zu vergeben, dabei wird die Funktion findunitglobal() verwendet
// wenn die Funktion eine freie Nummer gefunden hat, wird diese in highunit gespeichert, dann wird
// beim n�chsten Aufruf ab dieser Nummer angefangen ... highunitnummer wird beim Laden des Spieles
// festgelegt, wenn tun bzw. highunit gleich highunitnummer sind, wird nicht mehr gesucht, sondern
// einfach weiter gez�hlt

int equaltun(int tun)				// verhindert falsche Nummern
{
	if ((tun == 29)			||	// t
		(tun == 1058)		||	// te
		(tun == 38110)		||	// tem
		(tun == 1371985)	||	// temp

		(tun == 11)			||	// b
		(tun == 406)		||	// ba
		(tun == 14646)		||	// bau
		(tun == 527270)		||	// baue

		(tun == 14)			||	// e
		(tun == 522)		||	// ei
		(tun == 18815)		||	// ein
		(tun == 677357))tun++;	// einh
	return tun;
}

void newunitnummer(unit *u)
{
	static int highunit = 0;	// wir merken uns die letzte vergebene Nummer ... d�rfte auch die H�chste sein
	long tun = highunit;		// fangen an bei der letzten h�chsten Nummer zu z�hlen

	if (tun < highunitnummer)
	{
		do
		{
			tun = equaltun(tun);
			indicator_tick();
		}
		while (findunitglobal(tun));
	} else
	{
		do
		{
			tun = equaltun(tun);		// dito ?
			indicator_tick();
		} while (findunitglobal(tun));
	}
	u->no = tun;

	highunit = tun;				// merken f�r n�chsten Schritt
	if (tun > highunitnummer) highunitnummer = tun;
}

void SetUnterweltMonster()
{
	region *r;

	printf("- kontrolliere Unterwelt und setze ggf. Monster\n");
	for(r = regions; r; r = r->next)
	{
		if ((r->z < 0) && (r->terrain != T_LAVASTROM) && (r->terrain != T_OCEAN)) 
			while(!r->units)
			{
				if (!rand() % 4) seed_monsters_unterwelt(r);
			}
	}
}

unit *GlobalUnit;

void CheckUnit(unit *u)
{
	if (u) printf("\nCheckUnit - Silber: %d\n", u->items[I_SILVER]);
}



// Diese Funktion setzt die Wahrscheinlichkeit, das ein Sturm auf dem Ozean das
// Schiff abtreibt. Dazu werden als erstes die ganzen Ozeanregionen auf 1
// gesetzt. Dann werden alle Regioen die von 1 umgeben sind auf 2 gesetzt
// dann werden alle Regionen die von 2 umgeben sind auf 3 ... blabla
// diese Werte werden erst auf gotit zwischengespeichert und nach dem durchsuchen
// auf sturm geschrieben, gib t sonst eine Nebeneffekt

void SetSturmRegion()
{
	region *r;
	int aktreg = 0;
	int i;			// auf diesen Wert setzen

	puts("- setze Sturmregionen");

	for(r = regions; r; r = r->next) if (r->terrain == T_OCEAN) { r->sturm = 1; r->gotit = 1; }

	for(i = 2; i != 9; i++)
	{
		for(r = regions; r; r = r->next)
		{
			int d=0;		// Regionen z�hlen, die einen Wert niedriger haben
			int j;

			for(j = 0; j < MAXDIRECTIONS; j++) if ((r->connect[i]) && (r->connect[j])) if ((r->connect[j]->terrain == T_OCEAN) && (r->connect[j]->sturm == i-1)) d++;
			if (d == 6) r->gotit = i;		// Region ist vollst�ndig von i-1 umgeben
		}

		for(r = regions; r; r = r->next) r->sturm = r->gotit;
	}
}



// Diese Funktion setzt die Ozeanregionen, die im Winter vereisen. Dazu werden alle
// Ozeanregionen, die an Land grenzen auf 1 gesetzt. Dann werden alle Ozeanregionen
// die an 1 grenzen wieder gel�scht, wenn sie nicht voll von 1 oder Land umgeben sind
// Anschlie�end wird 1 oder 2 vergeben, 1 ist Eis im Dezember/Januar und 2 ist Januar/Februar

void SetEisRegion()
{
	region *r;
	int i;

	puts("- setze Eisregionen");

	for(r = regions; r; r = r->next)
	{
		r->gotit = 0;			// alte GOTITs l�schen
		r->eisbackup = r->eis;	// unten ist irgendwo ein Zufallsfaktor, damit sich aber die Eisregionen nicht
								// von einer Runde zur n�chsten �ndern, werden die alten Daten zischengespeichert
	}

	// alle auf 1 setzten, wenn connect[i] != T_OCEAN
	for(r = regions; r; r = r->next)
	{
		for(i = 0; i != MAXDIRECTIONS; i++)
		{
			if (r->connect[i])
			{
				if ((r->terrain == T_OCEAN) && (r->connect[i]->terrain != T_OCEAN))
				{
					if (!r->connect[i]->movement) r->eis = 1;	// nur wenn das angrenzende Land keine Insel ist setzen !!
				}
			}
		}
	}

	// alle 1 l�schen, wenn sie nicht von Eis oder Land umgeben sind
	for(r = regions; r; r = r->next)
	{
		for(i = 0; i != MAXDIRECTIONS; i++)
		{
			if (r->connect[i]) if ((r->connect[i]->terrain == T_OCEAN) && (r->connect[i]->eis == 0)) r->gotit = 1;
		}
	}
	for(r = regions; r; r = r->next)
	{
		if (r->gotit) { r->eis = 0; r->gotit = 0; }			// Eis l�schen
		if (r->eis) r->eis = rand() % 2 + 1;				// jetzt "Monate" verteilen
		if (r->eisbackup) r->eis = r->eisbackup;			// alten Daten wieder herstellen
	}
}

void EisenBug()
//void EisenScan()			// scannt alle Regionen auf g�ltige Eisenvorr�te
{
	region *r;
	int fails1 = 0;
	int fails2 = 0;
	int pos = 0;

	printf("- scanne Regionen nach falschen Eisenvorkommen");
	indicator_reset(listlen(regions));

	for(r = regions; r; r = r->next)
	{
		if (r->terrain == T_MOUNTAIN)
		{
			if (!IN(r->eisen,70,130))
			{
				fails1++;
				printf("%s -> %d Eisen ... korregiert auf 1oo\n", regionid(r, 0, 0), r->eisen);
				r->eisen = 100;
			}
		} else
		{
			if (r->eisen)
			{
				fails2++;
				printf("%s -> %d Eisen ... korregiert auf o\n", regionid(r, 0, 0), r->eisen);
				r->eisen = 0;
			}
		}
		indicator_count_up(++pos);
	}

	printf("%d Regionen mit zu wenig Eisen und %d Regionen mit falschem Eisen\n", fails1, fails2);
}

void ConnectUnitTest()
{
	region *r;
	unit *u;
	unit *ou;
	int gotit;
	int fehler = 0;

	// jetzt testen ob alle Einheiten da sind!
	printf(" ... Kette testen ...");
	for(r = regions; r; r = r->next)
	{
		for(u = r->units; u; u = u->next)
		{
			gotit = 0;
			for(ou = units; ou; ou = ou->kette) if (ou == u) { gotit = 1; break; }
			if (!gotit) fehler++;
		}
	}
	printf(" Fehler: %d", fehler);
	if (fehler) system("pause");
}

void ConnectUnits()		// alle Einheiten als eine Liste aufreihen, gleichzeitig auch noch die Region
{						// mit in den Struct speichern
	region * r;
	unit * u;
	unit * ou = NULL;	// letzte benutze Einheit
	int regs = 0;

	printf("- Einheiten auf eine Liste reihen, Region setzen, Einkommen l�schen ");
	indicator_reset(listlen(regions));
	units = NULL;

	for(r = regions; r; r = r->next)
	{
		for(u = r->units; u; u = u->next)
		{
			u->kette = NULL;
			u->r = r;					// Region speichern f�r effskill()
			if (!ou) ou = u; else ou->kette = u;
			if (!units) units = ou;
			ou = u;
		}
		indicator_count_up(++regs);
	}
	printf("\n");
}

int varianz(int m, int v)				// gibt eine Zufallszahl zwischen [(m-v)...(m)...(m+v)] aus
{
	int z = m;
	if (rand() % 2) z += rand() % v; else z -= rand() % v;
	return z;
}

void generatePHPListe();

void FineTuning()
{
	region *r, *hr;
	puts(" - teste auf doppelte Regionen");
	for(r = regions; r->next; r = r->next) r->gotit = 0;
	for(r = regions; r->next; r = r->next)
	{
		region *ref;
		for(ref = r; ref; ref = ref->next)
		{
			if (ref == r) continue;
			if ((r->x == ref->x) && (r->y == ref->y) && (r->z == ref->z))
			{
				printf(" -> %s", ref->name);
				r->gotit = 1;
			}
		}
	}
	puts(" - loesche doppelte Regionen");
	r = regions;
	hr = NULL;
	while(r)
	{
		if (r->gotit)
		{
			if (hr) hr->next = r->next;	// es bleibt ein Speicherleck!!
		} else
		{
			hr = r;
		}
		r = r->next;
	}
}


// neue Nummer f�r Einheit oder Partei
void nummer()
{
	region *r, *hr;
	unit *u, *hu;
	strlist *S;
	char *s;
	int n;
	rfaction *rf;
	faction *f;

	puts("- neue Nummer vergeben");

	for(r = regions; r; r = r->next)
	{
		for(u = r->units; u; u = u->next)
		{
			for (S = u->orders; S; S = S->next)
			{
				if (igetkeyword (S->s) == K_NUMMER)
				{
					switch(getparam())
					{
						case P_FACTION:	s = getstr();
										if (!s[0])
										{
											mistakeu(u, "Keine Nummer angegeben");
											return;
										}
										n = todec(s);
										if (findfaction(n))
										{
											mistakeu(u, "Parteinummer schon vergeben");
											return;
										}
										// jetzt durch alle Allianzen und Nummer �ndern
										for(f = factions; f; f = f->next)
										{
											for(rf = f->allies; rf; rf = rf->next) if (rf->factionno == u->faction->no) rf->factionno = n;
										}

										// nun noch die einzelnen Einheiten auf die neue Partei setzen auch die tarn_no, falls
										// sie sich von der aktuellen Nummer unterscheidet
										for(hr = regions; hr; hr = hr->next)
										{
											for(hu = hr->units; hu; hu = hu->next)
											{
												if (u->faction->no == hu->tarn_no) hu->tarn_no = n;
											}
										}

										// dann die eigentliche Parteinummer �ndern
										u->faction->no = n;
										sprintf(buf, "Parteinummer wurde auf '%s' geaendert", tobase36(u->faction->no));
										addmessage(u, buf);
										break;

						case P_UNIT:	s = getstr();
										if (!s[0])
										{
											mistakeu(u, "Keine Nummer angegeben");
											return;
										}
										n = todec(s);
										if (n != equaltun(n))
										{
											// eigentlich ist das eine illegale Nummer ... aber man kann den Spieler ja
											// nicht mit internen Problemen belasten
											mistakeu(u, "Einheitnummer schon vergeben");
											return;
										}
										if (findunitglobal(n))
										{
											mistakeu(u, "Einheitnummer schon vergeben");
											return;
										}
										u->no = n;
										sprintf(buf, "Einheitnummer wurde auf '%s' geaendert", tobase36(u->no));
										addmessage(u, buf);
										break;
					}
				}
			} // Befehle
		} // Einheiten
	} // Regionen
}

// neue Nummer f�r Einheit oder Partei
void website()
{
	region *r;
	unit *u;
	strlist *S;
	char *s;
	
	puts("- Website setzen");
	for(r = regions; r; r = r->next)
	{
		for(u = r->units; u; u = u->next)
		{
			for (S = u->orders; S; S = S->next)
			{
				if (igetkeyword (S->s) == K_WEBSITE)
				{
					s = getstr();
					mnstrcpy (&u->faction->website, s, DISPLAYSIZE);
				}
			} // Befehle
		} // Einheiten
	} // Regionen
}

void cleanWorld(welt *w)
{
	region *r, *hr;
	unit *u, *hu;
	building *b, *hb;
	ship *sh, *hsh;
	int e;
	int regs = 0;

	e = w->ebene;
	printf("\t ... loesche Welt %i ... ", e);
	indicator_reset(listlen(regions));
	regs = 0;
	for(r = regions; r; r = hr)
	{
		hr = r->next;	// da "r" ggf. gel�scht wird
		indicator_count_up(++regs);
		if (r->z != e) continue;
		for(u = r->units; u; u = hu)		{ hu = u->next; freelist (u->orders); removelist(&r->units, u);	}
		for(b = r->buildings; b; b = hb)	{ hb = b->next;	removelist(&r->buildings, b); }
		for(sh = r->ships; sh; sh = hsh)	{ hsh = sh->next; removelist(&r->ships, sh); }
		removelist(&regions, r);
	}

	removelist(&welten, GetWorld(e));

	printf("\n");
}

void killWorld()
{
	welt *w;
	int e;
	int regs = 0;
	
	printf ("Welt? ");
	gets (buf);
	if (buf[0] == 0) return;
	e = atoi (buf);
	
	w = GetWorld(e);
	if (!w)
	{
		printf("\nWelt existiert nicht!\n");
		return;
	}

	cleanWorld(GetWorld(e));		// Oberwelt
	cleanWorld(GetWorld(0 - e));	// Unterwelt

	printf("fertig\n");
}



void doppelteEinheiten()
{
	region *r;
	region *hr;
	unit *u;
	unit *u2;
	unit *hu;
	int i;
	FILE *f;
	int anz = 0;

	return; 
	printf("- doppelte Einheiten ", listlen2(units));
	indicator_reset(listlen2(units));

	for(u = units; u; u = u->kette)
	{
		u->gotit = 0;
		indicator_count_up(++anz);
		for(u2 = u; u2; u2 = u2->kette)
		{
			if (u2 == u) continue;	// gleiche Einheit, also �berspringen und weiter machen
			if (u2->no == u->no)
			{
				printf("\tEinheit %s (%s) ist doppelt", tobase36(u->no), factionid(u->faction));
				
				// Monsterpartei wird einfach gekillt
				if (u2->faction->no == 0)
				{ 
					puts(" ... gekillt");
					u2->gotit = 1;	// muss entfernt werden
					continue;
				}
				
				// Monsterpartei wird einfach gekillt
				if (u->faction->no == 0)
				{ 
					puts(" ... gekillt");
					u->gotit = 1;
					continue;
				}
				
				// gleiche Partei
				if (u->faction == u2->faction)
				{
					puts(" ... gleiche Partei ... lege zusammen");
					for(i = 0; i < MAXITEMS; i++) u->items[i] += u2->items[i];
					for(i = 0; i < MAX_KRAEUTER; i++) u->kraeuter[i] += u2->kraeuter[i];
					for(i = 0; i < MAX_TRAENKE; i++) u->traenke[i] += u2->traenke[i];
					for(i = 0; i < MAXSKILLS; i++) u->skills[i] += u2->skills[i];
					for(i = 0; i < MAXSPELLS; i++) u->spells[i] += u2->spells[i];
					u2->gotit = 1;
				}
				
				// nicht gleiche Partei
				if (u->faction != u2->faction)
				{
					puts(" ... manuelles Managment !!!");
					u->gotit = -1;
				}
			}
		}
	}

	printf("... killen ");
	// jetzt alle Einheiten ausgeben, die doppelt sind
	f = fopen("temp/doppelteEinheiten.txt", "w");
	hr = NULL;
	for(r = regions; r; r = r->next)
	{
		for(u = r->units; u; u = u->next)
		{
			if (u->gotit == -1)
			{
				fprintf(f, "Einheit %s (%s)\n", tobase36(u->no), factionid(u->faction));
			}
			if (hu) 
			{
				if (hr) removelist(&hr->units, hu);
				hu = NULL;
			}
			if (u->gotit == 1) // muss aus der Kette gel�scht werden
			{
				hu = u;
				hr = r;
			}
		}
	}
	fclose(f);

	printf("\n");
}



void DebugTest()
{
	unit *u;
	region *r;

	puts("- DebugTest()");
	for(r = regions; r; r = r ->next)
	{
		for(u = r->units; u; u = u->next)
		{
			if ((u->bevor == u->after) && (u->action))
			{
				printf("Fehler bei Einheit %s ", tobase36(u->no));
				printf("(%s)", factionid(u->faction));
			}
		}
	}
	puts("\n<Enter>");
	gets(buf);
}


void sortiere(region *r, unit *u, int num)
{
	unit *first;			// hinter diese Einheit soll/muss sortiert werden
	unit *last;				// hinter dieser Einheit war die Einheit mal
	char pbuf[200];

	u->gotit = 1;

	// erstmal die Einheit suchen, hinter die sortiert wird ... dazu brauchen wir die Nummer
	// der folgenden Einheit ... das sollte 'num' sein ... ist 'num' die erste
	// Einheit der Region, dann ist 'first' NULL
	for(first = r->units; first; first = first->next) if (first->next) if (first->next->no == num) break;

	// jetzt wird die Einheit gesucht, die vor 'u' ist ... 'last' existiert sofern
	// 'u' nicht die erste Einheit der Region ist
	for(last = r->units; last; last = last->next) if (last->next == u) break;

	// wenn jetzt die Einheit schon vor der anderen ist, dann abbrechen ... das passiert, wenn
	// first gleich u ist ... da first ja vor der Einheit steht, wo hin sortiert werden soll
	if (u == first)
	{
		sprintf(pbuf, "%s ist schon wo sie hin soll", unitid(u));
		addmessage(u, pbuf);
		return;
	}

	// wenn 'last' und 'first' gesetzt sind ... dann brauch nur getauscht werden
	// weil Einheit ist kompl. innerhalb der Kette
	if (last && first)
	{
		last->next = u->next;		// wenn das die letzte Einheit war, dann ist 'last' jetzt das Ende
		u->next = first->next;
		first->next = u;
		sprintf(pbuf, "%s wurde neu einsortiert", unitid(u));
		addmessage(u, pbuf);
		return;
	}

	// wenn 'first' NULL ist ... dann ist die Einheit ganz oben
	if (!first && last)
	{
		last->next = u->next;		// wenn das die letzte Einheit war, dann ist 'last' jetzt das Ende
		u->next = r->units;
		r->units = u;
		sprintf(pbuf, "%s wurde neu einsortiert", unitid(u));
		addmessage(u, pbuf);
		return;
	}

	if (!first && !last)
	{
		mistakeu(u, "Einheit ist alleine in der Region");
		return;
	}

	if ((first->no != num) && (!first->next)) { mistakeu(u, "Einheit existiert nicht"); return; }

	sprintf(pbuf, "%s ist schon wo sie hin soll", unitid(u));
	mistakeu(u, pbuf);
}

void sortieren()
{
	region *r;
	unit *u;
	strlist *S;

	puts("- sortieren");
	for(u = units; u; u = u->kette) u->gotit = 0;
	for(r = regions; r; r = r->next)
	{
		for(u = r->units; u; u = u->next)
		{
			for (S = u->orders; S; S = S->next)
			{
				if (igetkeyword (S->s) == K_SORTIEREN)
				{
					if (!u->gotit) sortiere(r, u, todec(getstr()));
				} // Befehl
			} // alle Befehle
		} // alle Einheiten
	}
	puts("done");
}

void checkWorlds()
{
	welt *w, *hw;
	region *r;
	unit *u;

	puts("- teste Nutzung der Welten");

	// Test auf Nutzung der Welten
	for(w = welten; w; w = w->next) w->gotit = 0;
	for(r = regions; r; r = r->next) for(u = r->units; u; u = u->next) if (u->faction->no) GetWorld(r->z)->gotit = 1; // Welt wird noch verwendet

	// Ober und Unterwelt abgleichen ... es werden immer beide genutzt
	for(w = welten; w; w = w->next) if (w->gotit) { GetWorld(0 - w->ebene)->gotit = 1; continue; }

	for(w = welten; w; w = hw)
	{
		hw = w->next;
		if (w->gotit) continue;			// Welt wird noch benutzt
		cleanWorld(w);					// Welt l�schen
	}
}
