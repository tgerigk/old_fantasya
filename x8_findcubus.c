/*********************************************************************************\
***																				***
***		findcubus.c																***
***																				***
\*********************************************************************************/

// alle Regionen sind jetzt via ->NEXT verkn�pft
// bei vielen Regionen, werde immer alle Regionen abgerasst um eine Region zu
// finden, da immer von vorne angefangen wird - O(n�)

// findcubus() verkn�pft jetzt alle Regionen �ber NextX, NextY, LastX und LastY
// d.h. es wird in der Struktur WELT die NULL-Region (0/0/EBENE) eingetragen
// von das aus wird auf der X-Koordinaten alle weiteren Regionen angehangen,
// die die gleiche Y-Koordinate haben (also NextX bzw. LastX)
// dann wird von denen die Y-Koordinate angehangen (NextY bzw. LastY)

// findregion_old() sucht nach der alten Methode �ber NEXT und ben�tigt
// ca. 81oo Schritte pro Welt
// findregion() sucht dann im Cubus und ben�tigt max. 1oo Schritte f�r
// alle Welten

#include "atlantis.h"
#define BLOCKSIZE 9

region *findincubus(int x, int y, int z);
int debugcubus = 0;

void testcubus(int x, int y, int z)
{
	region *r;

	printf("\n\n- Testfunktion fuer Cubus ...\n");
	debugcubus = 1;	// Suchmeldungen anschalten

	r = findincubus(x,y,z);

	printf("\n<abgeschlossen> ... ");
	scanf("%d", &debugcubus);

	debugcubus = 0;	// Suchmeldungen wieder abschalten
}

void findcubus()
{
	welt *w;
	region *r;
	int reganz = 0;

	printf("- FindCubus aufbauen ...");

	// Startebene eintragen
	for(w = welten; w; w=w->next)
	{
		w->reg = findregion_old(0,0,w->ebene);
		printf("\n\tWelt %3d (%20s) ... ", w->ebene, w->mail);
		reganz = 0;
		indicator_reset(maxRegions);

		for(r = regions; r; r = r->next)
		{
			if (r->z == w->ebene)
			{
				if (r->x >= 0 && r->y == 0) r->NextX = findregion_old(r->x+1, r->y, w->ebene);
				if (r->x <= 0 && r->y == 0) r->LastX = findregion_old(r->x-1, r->y, w->ebene);
				if (r->y >=0) r->NextY = findregion_old(r->x, r->y+1, w->ebene);
				if (r->y <=0) r->LastY = findregion_old(r->x, r->y-1, w->ebene);
			}
			reganz++;
			indicator_count_up(reganz);
		}
	}
	printf("\n");

	// testcubus(7,7,1);
}



region *findincubus(int x, int y, int z)
{
	region *r = NULL;
	welt *w = NULL;

	int endeX = 0;		// 1 .. Region gefunden / 2 .. abgebrochen bzw. nicht gefunden
	int endeY = 0;
	int step = 0;

	for(w = welten; w; w=w->next)
	{
		if (debugcubus)
		{
			printf("\tWelt %d ... %s - %d\n", w->ebene, regionid(w->reg,0,0), w->reg->z);
		}
		if (w->ebene == z) { r = w->reg; break; }
	}
	if (!r) return NULL;	// Ebene existiert noch nicht mal!

	// als erstes wird die X-Koordinate abgesucht
	while(endeX == 0)
	{
		if (r->x == x)
		{
			// X gefunden ... passt also
			endeX = 1;
			break;
		} else
		{
			// n�chste Region ausw�hlen
			if (r->x > x)
			{
				// nach links, da gesuchte Region links liegt (x < 0)
				r = r->LastX;
			} else
			{
				r = r->NextX;
			}
			if (!r) { endeX = endeY = 2; }	// Region nicht gefunden ... damit 2. Schleife nicht ausgef�hrt wird, hier auch gleich endeY setzen
		}
		step++;
		if (debugcubus) printf("\t%d - %s\n", step, regionid(r,0,0));
	}

	// dann folgt die Y-Koordinate
	while(endeY == 0)
	{
		if (r->y == y)
		{
			// Y gefunden
			endeY = 1;
			break;
		} else
		{
			if (r->y > y)
			{
				// runter
				r = r->LastY;
			} else
			{
				r = r->NextY;
			}
			if (!r) { endeY = 2; }
		}
		step++;
		if (debugcubus) printf("\t%d - %s\n", step, regionid(r,0,0));
	}

	if (debugcubus) { printf("\tEnde -> %s - %d Schritte\n", regionid(r,0,0), step); }

	if (r) return r;		// Region zur�ck geben
	// printf("...findregion_old()...");
	r = findregion_old(x,y,z);	// zur Sicherheit nochmal die alte Funktion abgrassen
	return r;
}

