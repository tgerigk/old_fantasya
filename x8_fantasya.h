/*
 * x8_Fantasya.h
 */



#ifndef x8_FANTASYA_H
#define x8_FANTASYA_H
#ifndef FANTA_LIMITED_EDITION
#define fanta_version "o.o1.8 / 2 (o2.o4.2oo6) "
#else
#define fanta_version "o.o1.4 / LE (27.o5.2oo4)"
#endif
#define IN(ist,min,max) ( ((ist) >= (min)) && ((ist) <= (max)) )



// Strukturen
typedef struct welt
{
	struct welt *next;
	int min;				// Welt ist immer Quadratisch bzw. Torus
	int max;
	int ebene;				// welche Ebene bzw. Welt
	char *mail;				// Anprechpartner
	int sicher;				// ob Welt ge�ndert werden darf oder nicht, also privates Spiel
	int gotit;				// die �bliche Funktion
	region *reg;			// die Region im Zentrum, also (0/0/EBENE)
} welt;

typedef struct hoehle
{
	struct hoehle *next;
	long nummer;			// H�hlen werden mit BETRETE <NUMMER> betreten
	char *name;				// BENENNE HOEHLE "blabla" ... es gibt pro Region nur eine H�hle!
	char *beschr;
	int ebene;				// Ebene, wird nur einmal gespeichert, da Unterwelt an "Null" gespiegelt wird und nur in eine Welt geh�rt
	int x;					// Koordinaten, in beiden Welten
	int y;
	int offen;				// (seit irgendwann egal) bei 1 ist die H�hle offen, d.h. die Monster k�nnen durch diese H�hle in die Oberwelt
} hoehle;

/*
 * Prototypen von x8-Funktionen
 */

// x8_Rasses.c   - ja, das sollte x8_Races.c hei�en
void SetSkillOffSet();
int SelectARace();
void tarnung(void);
void prefix(void);
void checkparteitarnung(void);
void spionieren(void);
char *getLebenszustand(unit *u);
int maxLebenszustand(unit *u);		// enth�lt aus die Ausdauerprozente



// x8_InitGame.c
void AddPlayerListe();
void InitNewGameRules();
void EisenBug();					// Korrektur desjenigen
welt *GetWorld(int ebene);
void Rundschreiben(void);
void newunitnummer(unit *u);
void SetUnterweltMonster();
void CheckUnit(unit *u);			// Debug Funktion
void SetSturmRegion();				// setzt die Sturmregionen
void SetEisRegion();				// setzt die Ozean die gefrieren im Winter
void ConnectUnits();
void ConnectUnitTest();
int varianz(int m, int v);			// Zufallszahl zwischen [(m-v)...m...(m+v)]
void FineTuning();
void nummer();
void killWorld();
void doppelteEinheiten();
void DebugTest();					// ausgabe der Debug Variablen der einzelnen Einheiten
void website();
void sortieren();
int equaltun();
void checkWorlds();					// testen ob Welten noch genutzt werden



// x8_save.c
void loadgame();
void savegame();
void Kolumbus();
void KolumbusOutput();
void saveUnits();



// x8_economic.c
void make_kraeuter(region *r, unit *u, int kraut, int menge);
void make_trank(region *r, unit *u, int kraut, int menge);
void gib_kraeuter(long n,unit * u,region * r,unit * u2,int kraut,strlist *S);
void gib_traenke(long menge,unit * u,region * r,unit * ziel,int trank,strlist *S);
void InselnBewegen();
void fflush_stdin();	// ....................................................... <--- fflush(stdin);



// x8_Alchemie.c
int findkraut(char *c);
int findtrank(char *s);
void SetTraenke();
void benutzen();
void trankaktionen();
int alchemisten(faction * f);



// x8_menu.c
void newmenu();



// x8_building.c
void SetProduktionsMatrix();
void make_building(region *r, unit *u);
void Gebaeude();
int imGebaeude(unit *u, region *r);
void predigen();
void DropIllegalBuildingAndShip();


// x8_hoehlen.c
hoehle *findhoehle(int n);						// sucht eine H�hle
void MakeHoehle(region *r);						// erstellt eine H�hle
void BetreteHoehle(region *r, unit *u);			// H�hle betreten ... also Einheit �ndert die Ebene



// x8_config.c
void ConfigOutput();



// x8_reports.c
void XMLReport(faction *f);



// extern bzw. in alten Dateien
int highskill(faction *f, region *r, int skill);			// atlantis.c
void seed_monsters_unterwelt(region *r);					// creation.c
void countPersonen();										// give.c
int findskill (char *s);									// skills.c



// Variablen
extern unsigned char RecruitCost[MAXRACES];
extern unsigned char MaxMagicans[MAXRACES];
extern welt *welten;
extern hoehle *hoehlen;
extern int forestgrowth[12];
extern int horsegrowth[12];
extern int sturmwahr[12];
extern int maxRegions;					// maximale Anzahl aller Regionen
extern long highunitnummer;				// h�chste Unitnummer im Spiel
extern int lebenspunkte[MAXRACES][3];	// Lebenspunkte, Hungerpunkte, Varianz



extern unit *GlobalUnit;		// DebugVariable



// Alchemie
extern char *hilfe_optionen[HILFE_MAX];
extern char *traenke[MAX_TRAENKE];
extern int traenke_LVL[MAX_TRAENKE];
extern char *kraeuter[MAX_KRAEUTER];
extern int zutaten[MAX_TRAENKE][MAX_KRAEUTER];

// FindCubus
void findcubus();
region *findincubus(int x, int y, int z);

// Statistik
void statistik();

// Cheating
void cheatit();

#endif