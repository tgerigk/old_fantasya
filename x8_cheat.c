#include "atlantis.h"



void cheat_person(unit *u, char *cmd)
{
	char pbuf[200];
	sprintf(pbuf, "Personen Cheat", cmd);
	addevent(u, pbuf);
	u->number = atoip (cmd);
}

void cheat_item(unit *u, char *cmd)
{
	char pbuf[200];
	int item = finditem(cmd);
	int count = atoip (getstr());
	if (item < 0)
	{
		mistakeu(u, "Item nicht gefunden");
		return;
	}
	if (count < 0)
	{
		mistakeu(u, "Anzahl kleiner als 0");
		return;
	}
	sprintf(pbuf, "Item Cheat: %s auf %i gesetzt", strings[itemnames[0][item]][0], count);
	addevent(u, pbuf);
	u->items[item] = count;
}

void cheat_skill(unit *u, char *cmd)
{
	char pbuf[200];
	int skill = findskill(cmd);
	int count = atoip (getstr());
	int i;

	if (skill < 0)
	{
		mistakeu(u, "Skill nicht gefunden");
		return;
	}
	if (count < 0)
	{
		mistakeu(u, "Anzahl kleiner als 0");
		return;
	}

	switch(skill)
	{
		case SK_MAGIC:	// neue Magier bekommen hiermit automatisch alle Zauberspr�che
						// zugewiesen ... werden aber n�chste Runde auf das max. m�gliche
						// gel�scht
						for(i = 0; i < MAXSPELLS; i++) u->spells[i] = 1;
						break;

		default:	break;
	}
	sprintf(pbuf, "Skill Cheat: %s auf %i Lerntage gesetzt", skillnames[skill], count);
	addevent(u, pbuf);
	u->skills[skill] = count;
}

void cheat_race(unit *u, char *cmd)
{
	char pbuf[200];
	int race; // = findstr(racenames[0], cmd, MAXRACES);
	for(race = 0; race < MAXRACES; race++)
	{
		if (!strnicmp(racenames[race][0], cmd, strlen(cmd)))
		{
			sprintf(pbuf, "Race Cheat: auf %s", racenames[race][0]);
			addevent(u, pbuf);
			u->race = race;
			u->raceTT = race;
			u->type = U_MAN;
			return;
		}
	}
	mistakeu(u, "Rasse nicht gefunden");
}

void setMonster(unit *u, int type)
{
	char pbuf[200];
	u->race = 0;
	u->raceTT = 0;
	u->type = type;
	sprintf(pbuf, "Beast Cheat");
	addevent(u, pbuf);
}

void cheat_beast(unit *u, char *cmd)
{
	char pbuf[200];
	if (!stricmp(cmd, "dragon")) { setMonster(u, U_DRAGON); return; }
	if (!stricmp(cmd, "firedragon")) { setMonster(u, U_FIREDRAGON); return; }
	if (!stricmp(cmd, "undead")) { setMonster(u, U_UNDEAD); return; }
	sprintf(pbuf, "Unbekanntes Monster '%s'", cmd);
	mistakeu(u, pbuf);
}

void docheat(unit *u, char *cmd)
{
	char pbuf[200];
	int param = findparam(cmd);
	switch(param)
	{
		case P_PERSON:	cheat_person(u, getstr());
						break;
		case P_ITEM:	cheat_item(u, getstr());
						break;
		case P_SKILL:	cheat_skill(u, getstr());
						break;
		case P_RACE:	cheat_race(u, getstr());
						break;
		case P_BEAST:	cheat_beast(u, getstr());
						break;
		default:		sprintf(pbuf, "Nicht mal richtig cheaten koennen! ;-) Unbekannter Parameter '%s'", cmd);
						mistakeu(u, pbuf);
	}
}

void cheatit()
{
	region *r;
	unit *u;
	strlist *S;
	faction *f;

	puts("- cheaten");
	for(u = units; u; u = u->kette) u->gotit = 0;
	for(r = regions; r; r = r->next)
	{
		for(u = r->units; u; u = u->next)
		{
			for (S = u->orders; S; S = S->next)
			{
				if (igetkeyword (S->s) == K_CHEAT)
				{
					if (u->faction->cheatenabled)
					{
						if (!u->gotit) docheat(u, getstr());
					} else
					{
						mistakeu(u, "Cheats sind f�r Deine Partei nicht aktiviert!");
					}
				} // Befehl
			} // alle Befehle
		} // alle Einheiten
	}

	// Cheats langsam ausschalten
	for(f = factions; f; f = f->next) if (f->cheatenabled) f->cheatenabled--;
}
