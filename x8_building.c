/*************************************************************************\
***																		***
***	enth�lt alle neuen Geb�ude und deren Wirkung,						***
***																		***
\*************************************************************************/

#include "atlantis.h"

/*
	PM_HOLZ,
	PM_EISEN,
	PM_STEIN,
	PM_KOSTEN,		// Baukosten pro Gr��enpunkt
	PM_TALENT,		// Talent ist immer Burgenbau
	PM_UNTERHALT,	// Unterhalt pro Runde, gilt ab Baubeginn !
	PM_UNTERHALT2,	// Zus�tzlicher Unterhalt f�r _alle_ Einheiten im Geb�ude
	PM_SIZE,		// Gr��e des Geb�udes, 0 -> unendlich
	PM_BURG,		// welcher Burgtyp wird vorrausgesetzt, hier nur als effsize
	PM_BUILDING,	// welches Geb�ude wird noch ben�tigt !! b->size EQ PM_SIZE !!
*/

void SetProduktionsMatrix()
{
	int b;		// Geb�udetyp

	puts("- setzen der ProduktionsMatrix f�r Geb�ude");



	b = BT_BURG;	// bleibt leer, weil altes Geb�ude

	produktionsmatrix[b][PM_HOLZ]		= 0;
	produktionsmatrix[b][PM_EISEN]		= 0;
	produktionsmatrix[b][PM_STEIN]		= 0;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 0;
	produktionsmatrix[b][PM_TALENT]		= 0;
	produktionsmatrix[b][PM_UNTERHALT]	= 0;
	produktionsmatrix[b][PM_UNTERHALT2]	= 0;
	produktionsmatrix[b][PM_SIZE]		= 0;
	produktionsmatrix[b][PM_BURG]		= 0;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_HOLZFAELLERHUETTE;

	produktionsmatrix[b][PM_HOLZ]		= 1;
	produktionsmatrix[b][PM_EISEN]		= 3;
	produktionsmatrix[b][PM_STEIN]		= 2;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 100;
	produktionsmatrix[b][PM_TALENT]		= 3;
	produktionsmatrix[b][PM_UNTERHALT]	= 50;
	produktionsmatrix[b][PM_UNTERHALT2]	= 0;
	produktionsmatrix[b][PM_SIZE]		= 0;
	produktionsmatrix[b][PM_BURG]		= 0;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_MINE;

	produktionsmatrix[b][PM_HOLZ]		= 3;
	produktionsmatrix[b][PM_EISEN]		= 2;
	produktionsmatrix[b][PM_STEIN]		= 1;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 150;
	produktionsmatrix[b][PM_TALENT]		= 3;
	produktionsmatrix[b][PM_UNTERHALT]	= 75;
	produktionsmatrix[b][PM_UNTERHALT2]	= 0;
	produktionsmatrix[b][PM_SIZE]		= 0;
	produktionsmatrix[b][PM_BURG]		= 0;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_STEINBRUCH;

	produktionsmatrix[b][PM_HOLZ]		= 1;
	produktionsmatrix[b][PM_EISEN]		= 2;
	produktionsmatrix[b][PM_STEIN]		= 3;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 100;
	produktionsmatrix[b][PM_TALENT]		= 3;
	produktionsmatrix[b][PM_UNTERHALT]	= 50;
	produktionsmatrix[b][PM_UNTERHALT2]	= 0;
	produktionsmatrix[b][PM_SIZE]		= 0;
	produktionsmatrix[b][PM_BURG]		= 0;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_SAEGEWERK;

	produktionsmatrix[b][PM_HOLZ]		= 6;
	produktionsmatrix[b][PM_EISEN]		= 6;
	produktionsmatrix[b][PM_STEIN]		= 5;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 250;
	produktionsmatrix[b][PM_TALENT]		= 5;
	produktionsmatrix[b][PM_UNTERHALT]	= 100;
	produktionsmatrix[b][PM_UNTERHALT2]	= 5;
	produktionsmatrix[b][PM_SIZE]		= 0;
	produktionsmatrix[b][PM_BURG]		= B_TOWER;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_BERGWERK;

	produktionsmatrix[b][PM_HOLZ]		= 5;
	produktionsmatrix[b][PM_EISEN]		= 6;
	produktionsmatrix[b][PM_STEIN]		= 7;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 300;
	produktionsmatrix[b][PM_TALENT]		= 5;
	produktionsmatrix[b][PM_UNTERHALT]	= 150;
	produktionsmatrix[b][PM_UNTERHALT2]	= 5;
	produktionsmatrix[b][PM_SIZE]		= 0;
	produktionsmatrix[b][PM_BURG]		= B_TOWER;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_SCHMIEDE;

	produktionsmatrix[b][PM_HOLZ]		= 5;
	produktionsmatrix[b][PM_EISEN]		= 5;
	produktionsmatrix[b][PM_STEIN]		= 5;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 250;
	produktionsmatrix[b][PM_TALENT]		= 4;
	produktionsmatrix[b][PM_UNTERHALT]	= 100;
	produktionsmatrix[b][PM_UNTERHALT2]	= 5;
	produktionsmatrix[b][PM_SIZE]		= 0;
	produktionsmatrix[b][PM_BURG]		= B_CASTLE;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_WERKSTATT;

	produktionsmatrix[b][PM_HOLZ]		= 5;
	produktionsmatrix[b][PM_EISEN]		= 5;
	produktionsmatrix[b][PM_STEIN]		= 5;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 350;
	produktionsmatrix[b][PM_TALENT]		= 5;
	produktionsmatrix[b][PM_UNTERHALT]	= 150;
	produktionsmatrix[b][PM_UNTERHALT2]	= 5;
	produktionsmatrix[b][PM_SIZE]		= 0;
	produktionsmatrix[b][PM_BURG]		= B_CASTLE;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_KUECHE;

	produktionsmatrix[b][PM_HOLZ]		= 10;
	produktionsmatrix[b][PM_EISEN]		= 5;
	produktionsmatrix[b][PM_STEIN]		= 7;
	produktionsmatrix[b][PM_GOLD]		= 2;
	produktionsmatrix[b][PM_KOSTEN]		= 400;
	produktionsmatrix[b][PM_TALENT]		= 6;
	produktionsmatrix[b][PM_UNTERHALT]	= 300;
	produktionsmatrix[b][PM_UNTERHALT2]	= 10;
	produktionsmatrix[b][PM_SIZE]		= 10;
	produktionsmatrix[b][PM_BURG]		= B_CASTLE;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_STEG;

	produktionsmatrix[b][PM_HOLZ]		= 2;
	produktionsmatrix[b][PM_EISEN]		= 1;
	produktionsmatrix[b][PM_STEIN]		= 0;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 50;
	produktionsmatrix[b][PM_TALENT]		= 2;
	produktionsmatrix[b][PM_UNTERHALT]	= 30;
	produktionsmatrix[b][PM_UNTERHALT2]	= 0;
	produktionsmatrix[b][PM_SIZE]		= 5;
	produktionsmatrix[b][PM_BURG]		= 0;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_HAFEN;

	produktionsmatrix[b][PM_HOLZ]		= 5;
	produktionsmatrix[b][PM_EISEN]		= 5;
	produktionsmatrix[b][PM_STEIN]		= 7;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 150;
	produktionsmatrix[b][PM_TALENT]		= 3;
	produktionsmatrix[b][PM_UNTERHALT]	= 100;
	produktionsmatrix[b][PM_UNTERHALT2]	= 0;
	produktionsmatrix[b][PM_SIZE]		= 10;
	produktionsmatrix[b][PM_BURG]		= B_TOWER;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_SEEHAFEN;

	produktionsmatrix[b][PM_HOLZ]		= 15;
	produktionsmatrix[b][PM_EISEN]		= 10;
	produktionsmatrix[b][PM_STEIN]		= 10;
	produktionsmatrix[b][PM_GOLD]		= 5;
	produktionsmatrix[b][PM_KOSTEN]		= 500;
	produktionsmatrix[b][PM_TALENT]		= 5;
	produktionsmatrix[b][PM_UNTERHALT]	= 300;
	produktionsmatrix[b][PM_UNTERHALT2]	= 0;
	produktionsmatrix[b][PM_SIZE]		= 20;
	produktionsmatrix[b][PM_BURG]		= B_TOWER;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_WERFT;

	produktionsmatrix[b][PM_HOLZ]		= 10;
	produktionsmatrix[b][PM_EISEN]		= 10;
	produktionsmatrix[b][PM_STEIN]		= 5;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 350;
	produktionsmatrix[b][PM_TALENT]		= 5;
	produktionsmatrix[b][PM_UNTERHALT]	= 100;
	produktionsmatrix[b][PM_UNTERHALT2]	= 5;
	produktionsmatrix[b][PM_SIZE]		= 20;
	produktionsmatrix[b][PM_BURG]		= B_TOWER;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_WEGWEISER;

	produktionsmatrix[b][PM_HOLZ]		= 1;
	produktionsmatrix[b][PM_EISEN]		= 1;
	produktionsmatrix[b][PM_STEIN]		= 1;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 50;
	produktionsmatrix[b][PM_TALENT]		= 1;
	produktionsmatrix[b][PM_UNTERHALT]	= 0;
	produktionsmatrix[b][PM_UNTERHALT2]	= 0;
	produktionsmatrix[b][PM_SIZE]		= 1;
	produktionsmatrix[b][PM_BURG]		= 0;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_MONUMENT;

	produktionsmatrix[b][PM_HOLZ]		= 1;
	produktionsmatrix[b][PM_EISEN]		= 1;
	produktionsmatrix[b][PM_STEIN]		= 1;
	produktionsmatrix[b][PM_GOLD]		= 1;
	produktionsmatrix[b][PM_KOSTEN]		= 150;
	produktionsmatrix[b][PM_TALENT]		= 4;
	produktionsmatrix[b][PM_UNTERHALT]	= 100;
	produktionsmatrix[b][PM_UNTERHALT2]	= 10;
	produktionsmatrix[b][PM_SIZE]		= 0;
	produktionsmatrix[b][PM_BURG]		= 0;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_STEINGRUBE;

	produktionsmatrix[b][PM_HOLZ]		= 5;
	produktionsmatrix[b][PM_EISEN]		= 6;
	produktionsmatrix[b][PM_STEIN]		= 6;
	produktionsmatrix[b][PM_GOLD]		= 0;
	produktionsmatrix[b][PM_KOSTEN]		= 200;
	produktionsmatrix[b][PM_TALENT]		= 5;
	produktionsmatrix[b][PM_UNTERHALT]	= 100;
	produktionsmatrix[b][PM_UNTERHALT2]	= 5;
	produktionsmatrix[b][PM_SIZE]		= 0;
	produktionsmatrix[b][PM_BURG]		= B_TOWER;
	produktionsmatrix[b][PM_BUILDING]	= 0;



	b = BT_KIRCHE;

	produktionsmatrix[b][PM_HOLZ]		= 3;
	produktionsmatrix[b][PM_EISEN]		= 4;
	produktionsmatrix[b][PM_STEIN]		= 5;
	produktionsmatrix[b][PM_GOLD]		= 2;
	produktionsmatrix[b][PM_KOSTEN]		= 200;
	produktionsmatrix[b][PM_TALENT]		= 4;
	produktionsmatrix[b][PM_UNTERHALT]	= 100;
	produktionsmatrix[b][PM_UNTERHALT2]	= 0;
	produktionsmatrix[b][PM_SIZE]		= 25;
	produktionsmatrix[b][PM_BURG]		= B_TOWER;
	produktionsmatrix[b][PM_BUILDING]	= 0;
}

void make_building(region *r, unit *u)
{
	int typ = 0;											// dieses Geb�ude herstellen
	int temp = 0;
	int stm = 0;											// SizeToMake, also Gr��e die erstellt werden kann
	building *b = NULL;										// basteln an diesem Geb�ude
	building *hb = NULL;
	char pbuf[300];
	char *s;

	// Geb�udetyp erkennen
	s = getstr();
	typ = findstr (BuildingNames, s, MAX_BURGTYPE);
	if ((typ < 1) && (!b))
	{
		mistakeu(u,"Geb�udetyp unbekannt");
		return;
	};

	// wenn eine Einheit in einen Geb�ude ist und dieses Geb�ude
	// der gleiche Typ ist, dann wird an diesem Geb�ude gearbeitet
	if (u->building)
	{
		if (u->building->typ == typ) b = u->building;
	}
	if (b) typ = b->typ;		// weiter an _diesem_ Geb�ude bauen



	// jetzt ausrechnen, wieviel wir bauen k�nnten
	temp = (effskill(u,SK_BUILDING) * u->number) / produktionsmatrix[typ][PM_TALENT];
	if (!temp) mistakeu(u, "Hier fehlt das Talent Burgenbau");
	stm = temp;

	if (produktionsmatrix[typ][PM_HOLZ])
	{
		temp = u->items[I_WOOD] / produktionsmatrix[typ][PM_HOLZ];
		if (!temp) mistakeu(u, "Dazu wird Holz ben�tigt");
		stm = min(stm,temp);
	}

	if (produktionsmatrix[typ][PM_EISEN])
	{
		temp = u->items[I_IRON] / produktionsmatrix[typ][PM_EISEN];
		if (!temp) mistakeu(u, "Dazu wird Eisen ben�tigt");
		stm = min(stm,temp);
	}

	if (produktionsmatrix[typ][PM_STEIN])
	{
		temp = u->items[I_STONE] / produktionsmatrix[typ][PM_STEIN];
		if (!temp) mistakeu(u, "Dazu werden Steine ben�tigt");
		stm = min(stm,temp);
	}

	if (produktionsmatrix[typ][PM_KOSTEN])
	{
		temp = u->items[I_SILVER] / produktionsmatrix[typ][PM_KOSTEN];
		if (!temp) mistakeu(u, "Dazu wird Silber ben�tigt");
		stm = min(stm,temp);
	}



	// jetzt suchen einer Burg, wenn Vorraussetzung
	temp = 0;
	if (produktionsmatrix[typ][PM_BURG])
	{
		for(hb=r->buildings; hb; hb=hb->next)	// alle Geb�ude abklappern
		{
			unit *bu = buildingowner(r,hb);

			if (!bu) continue;	// wenn kein Besitzer, dann kann auch nix gebaut werden
			if ((hb->typ == BT_BURG) && (buildingeffsize(hb) >= produktionsmatrix[typ][PM_BURG])) if (isallied(bu, u)) temp = 1;
		}
	}
	if (!produktionsmatrix[typ][PM_BURG]) temp = 1;		// wenn nix ben�tigt wird, muss aber stm weitergereicht werden
	if (!temp)
	{
		sprintf(pbuf,"Wir ben�tigen daf�r ein %s", buildingnames[produktionsmatrix[typ][PM_BURG]]);
		mistakeu(u, pbuf);
	}
	stm *= temp;

	// jetzt suchen eines Geb�udes, wenn Vorraussetzung
	temp = 0;
	if (produktionsmatrix[typ][PM_BUILDING])
	{
		for(hb=r->buildings; hb; hb=hb->next)	// alle Geb�ude abklappern
		{
			if ((hb->typ == produktionsmatrix[typ][PM_BUILDING]) && (isallied(buildingowner(r,hb), u))) temp = 1;
		}
	}
	if (!produktionsmatrix[typ][PM_BUILDING]) temp = 1;		// wenn nix ben�tigt wird, muss aber stm weitergereicht werden
	if (!temp)
	{
		sprintf(pbuf,"Wir ben�tigen daf�r %s", BuildingNames[produktionsmatrix[typ][PM_BUILDING]]);
		mistakeu(u, pbuf);
	}
	stm *= temp;

	if (!stm) return;		// wenn jetzt stm Null ist, dann war schon eine Fehlermeldung, also leise wech

	if (typ == BT_WEGWEISER) stm = 1;	// Wegweiser haben eine max. Gr��e von 1

	if ((produktionsmatrix[typ][PM_SIZE]) && (b))
	{
		temp = produktionsmatrix[typ][PM_SIZE] - b->size;		// soviel noch baubar
		temp = max(temp,0);										// bei temp kann <0 rauskommen !
		stm = min(stm,temp);
		if (!stm)
		{
			mistakeu(u, "Das Geb�ude ist voll ausgebaut");
			return;
		}
	}

	if (!b)
	{
		// machemer 'n neuet Jeb�ude
		b = cmalloc (sizeof (building));
		memset (b, 0, sizeof (building));
  
		do
			b->no++;
		while (findbuilding (b->no));
  
		sprintf (buf, "%s %s", BuildingNames[typ], tobase36(b->no) );
		mnstrcpy (&b->name, buf, NAMESIZE);
		addlist (&r->buildings, b);
  
		// Die Einheit befindet sich automatisch im Inneren des neuen Geb�udes
  
		leave (r, u);
		u->building = b;
		u->owner = 1;
		b->typ = typ;		// Typ angeben
		b->funktion = 0;	// wenn neu erstellt, dann erstmal ohne Funktion
	}

	// jetzt wird am Geb�ude weiter gebaut
	b->size += stm;
	u->items[I_WOOD] -= produktionsmatrix[typ][PM_HOLZ] * stm;
	u->items[I_IRON] -= produktionsmatrix[typ][PM_EISEN] * stm;
	u->items[I_STONE] -= produktionsmatrix[typ][PM_STEIN] * stm;
	u->items[I_GOLD] -= produktionsmatrix[typ][PM_GOLD] * stm;
	u->items[I_SILVER] -= produktionsmatrix[typ][PM_KOSTEN] * stm;

	// Default Befehl setzen
	sprintf(pbuf,"MACHE GEB�UDE %s",BuildingNames[typ]);
	mstrcpy (&u->thisorder, pbuf);

	// Meldung f�r Einheit
	sprintf(pbuf,"%s baut f�r %d Gr��enpunkte an %s weiter (Kosten: %d Holz, %d Steine, %d Eisen, %d Gold, %d Silber)"
				,unitid(u)		// Unitnummer
				,stm			// Gr��enpunkte
				,BuildingNames[typ]
				,produktionsmatrix[typ][PM_HOLZ] * stm
				,produktionsmatrix[typ][PM_STEIN] * stm
				,produktionsmatrix[typ][PM_EISEN] * stm
				,produktionsmatrix[typ][PM_GOLD] * stm
				,produktionsmatrix[typ][PM_KOSTEN] * stm);
	addproduction(u,pbuf);

	if (typ = BT_WEGWEISER) b->wegweiser = varianz(50, 20);	// Wegweiser existieren nur 5o +/- 20 Monde
}

/*
 * Hier jetzt testen ob das Geb�ude diese Runde eine Funktion hat bzw.
 * ob der Unterhalt aufgebracht werden kann, ggf. zerf�llt es
 */
void Gebaeude()
{
	region *r;
	building *b;
	unit *u;
	char pbuf[200];

	for(r=regions; r; r=r->next)	// alle Regionen
	{
		for(b=r->buildings; b; b=b->next)
		{
			// die Funktion ist nur gew�rleistet, wenn b->funktion == 1 ist!
			// Hier wird 1 subtrahiert, damit ist f�r MACHE keine Funktion vorhanden
			// weil b->funktion das erste mal auf 0 f�llt.
			// Ist das aber -1, dann zerf�llt das Geb�ude! Es kann also eine Runde
			// ohne Silber �berleben.
			b->funktion -= 1;

			// Burgen, Wegweiser und Ruinen zerfallen nicht
			if ((b->typ == BT_BURG) || (b->typ == BT_RUINE) || b->typ == BT_WEGWEISER) b->funktion = 1;

			u = buildingowner(r,b);
			if (u) // zur Sicherheit, allerdings sollte das Geb�ude besetzt sein
			{
				// der Besitzer des Geb�udes sollte entsprechend das Silber haben
				if (u->items[I_SILVER] < produktionsmatrix[b->typ][PM_UNTERHALT])
				{
					sprintf(pbuf,"F�r %s fehlt Silber zum Unterhalt",buildingid(b));
					if (u) addwarning(u, pbuf);
				} else
				{
					// ansosnten ist es mehr als genung Silber
					u->items[I_SILVER] -= produktionsmatrix[b->typ][PM_UNTERHALT];
					b->funktion = 1;	// diese Runde mit Funktion
				}
			}

			// Hier wird jetzt getestet ob das Geb�ude zerf�llt
			if (b->funktion<0)
			{
				int halb;	// um soviele Punkte zerf�llt das Geb�ude

				halb = max(b->size/2, 1);	// aber mind. um 1 Gr��enpunkt
				if (b->size>0)
				{
					sprintf(pbuf ,"%s zerf�llt um %d Gr��enpunkte", buildingid(b), halb);
					if (u) addwarning(u,pbuf);
					b->size -= halb;
				}
				if (b->size==0) b->typ = BT_RUINE;
				if (b->typ == BT_MONUMENT && rand()%128 < 32)
				{
					b->typ = BT_RUINE;
				}
			}

			if (b->typ == BT_WEGWEISER)
			{
				b->funktion = 1;
				b->wegweiser--;
				sprintf(buf, "Alter f�r %s: %d", buildingid(b), b->wegweiser);
				// ein verrotteter Wegweiser wird nicht mehr gespeichert
				if (!b->wegweiser) sprintf(buf, "Der Wegweiser %s(%s) ist verrottet", b->name, buildingid(b));
				adddebug(r, buf);
			}
		}
	}
}



/*
 * Funktion testet ob eine Einheit noch in das Geb�ude passt und ob
 * die Einheit damit noch von der Funktion profitieren kann
 */
int imGebaeude(unit *u, region *r)
{
	unit *tu;
	building *tb = NULL;
	int left = -1;

	if (u->building)
	{
		left = u->building->size;
		tb = u->building;
	}

	for(tu = r->units; tu; tu = tu->next)
	{
		if (tu->building == tb)
		{
			left -= tu->number;	// hier alles von left abziehen
			if (tu == u) break;	// Schleife abbrechen, wenn u == tu
		}
	}

	if (left<0)
	{
		return 0;		// zuviele Einheiten im Geb�ude
	} else
	{
		return 1;		// Einheit passte noch in das Geb�ude
	}
}



void DropIllegalBuildingAndShip()
{
	unit *u;
	building *b;
	ship *s;

	puts(" - DropIllegalBuildingAndShip()");

	// wenn die Schiffbauer in einer Werft sind und dann lossegeln oder wie auch immer
	// kann es passieren, das sie in einer Burg UND dem Schiff sind ... allerdings ist
	// die Burg in einer anderen Region ... was nat�rlich Vorlage zum Crash bringt
	// einfach das l�schen was nicht in der Region ist
	for(u = units; u; u = u->next)
	{
		if ((u->building) && (u->ship))
		{
			if (u->building) // Testen ob Burg in der Region ist
			{
				int vorhanden = 0;
				for(b = u->r->buildings; b; b = b->next) if (u->building == b) vorhanden = 1;
				if (!vorhanden) u->building = NULL;
			}
			if (u->ship) // Testen ob Burg in der Region ist
			{
				int vorhanden = 0;
				for(s = u->r->ships; s; s = s->next) if (u->ship == s) vorhanden = 1;
				if (!vorhanden) u->ship = NULL;
			}
		}
	}
}



/*
 *   Einheiten predigen
 */
void predigen()
{
	region *r;
	unit *u;
	strlist *S;
	char pbuf[200];

	puts("- predigen");

	for(r = regions; r; r = r->next)
	{
		r->auswanderung = 0;
		r->wachstum = 0;
	}

	for(r = regions; r; r = r->next)
	{
		for(u = r->units; u; u = u->next)
		{
			building *b = u->building;

			for (S = u->orders; S; S = S->next)
			{
				if (igetkeyword (S->s) == K_PREDIGE)
				{
					double effekt = 0.0;
					if (b) if (imGebaeude(u, r) && b->funktion && (b->typ == BT_KIRCHE)) effekt = 0.2; else effekt = 0.1;
					switch(getparam())
					{
						case P_HEIMAT:	r->auswanderung += effskill(u, SK_RELIGION) * u->number;
										break;
						case P_KINDER:	r->wachstum += effskill(u, SK_RELIGION) * u->number * effekt;
										break;
						default:		sprintf(pbuf,"Parameter ist unbekannt");
										if (u) addwarning(u, pbuf);
										break;
					} // switch
				} // if
			} // alle Befehle
		} // alle Einheiten
	} // alle Regionen
}
