/* German Atlantis PB(E)M host Copyright (C) 1995-1998  Alexander Schroeder

 based on:
 
  Atlantis v1.0  13 September 1993 Copyright 1993 by Russell Wallace
  
   This program may be freely used, modified and distributed.  It may
   not be sold or used commercially without prior written permission
from the author.  */

#include "atlantis.h"
#include <time.h>		/* Added by Bjoern Becker , 20.07.2000 */


int i;

void
getgarbage (void)
{
	faction *f;
	region *r;
	
	/* Get rid of stuff that was only relevant last turn */
	
	puts ("- entferne Texte der letzten Runde und begruesse newbies...");
	
	for (f = factions; f; f = f->next)
    {
		memset (f->showdata, 0, sizeof f->showdata);
		/* f->allies natuerlich nicht loeschen, dort sind die Alliierten
		der letzten Runde gespeichert, die noch gebraucht werden.  */
		freelist (f->mistakes);
		freelist (f->warnings);
		freelist (f->messages);
		freelist (f->battles);
		freelist (f->events);
		freelist (f->income);
		freelist (f->commerce);
		freelist (f->production);
		freelist (f->movement);
		freelist (f->debug);
		freelist (f->alchemie);
		freelist (f->echsennews);
		
		f->mistakes = 0;
		f->warnings = 0;
		f->messages = 0;
		f->battles = 0;
		f->events = 0;
		f->income = 0;
		f->commerce = 0;
		f->production = 0;
		f->movement = 0;
		f->debug = 0;
		f->alchemie = 0;
		f->echsennews = 0;
		
		if (f->newbie)
		/* addmessage (f, "Automatische Begruessung durch den Computer: Willkommen auf Fantasya! "
		"Bitte vergesst Euer Passwort nicht. Es ist in der Vorlage der Befehle am Ende der "
		"Auswertung noch einmal aufgefuehrt, zusammen mit euer Partei-Nr. und eurer Einheit-Nr. "
		"Wenn ihr Fragen habt, koennt ihr euch gerne an den Spielleiter wenden. "
		"Der Spielleiter ist unter 'mogel@x8Bit.de' zu erreichen. "
		"Die Befehle werden an 'fantasya@x8bit.de' geschickt. "
		"Beispiele fuer eure Befehle findet Ihr in der Doku."); */
		printf("\nBegr��ung durch Computer!\n");
    }
	
	for (r = regions; r; r = r->next)
    {
		freelist (r->comments);
		freelist (r->debug);
		
		r->comments = 0;
		r->debug = 0;
    }
}

void
clear_newbie_flags (void)
{
	faction *f;
	
	puts ("- loesche newbies flag...");
	
	for (f = factions; f; f = f->next) f->newbie=0;
}

void
show_all_reports (void)
{
	printf ("Schreibe die Reports der %d. Runde...\n", turn);
	reports ();
	writeaddresses ();
}

/* Hauptschlaufe zur Abwicklung des Spieles! */
void
processorders (void)
{
	//if (!(turn % 4)) SetUnterweltMonster();
	new_units ();
	ConnectUnits();		// Liste der Einheiten neu erstellen, wegen TEMP Einheiten
	plan_monsters ();
	set_passw ();		/* und pruefe auf illegale Befehle */
	setdefaults ();
	instant_orders ();
	mail ();			// Botschaften verschiecken
	website();			// Website eintragen
	cheatit();			// Cheatbefehle ausf�hren
	sortieren();		// sortiert die Einheit vor eine andere
	docontact ();		// Kontaktieren
	combat ();			/* leere Einheiten tauchen auf */
	siege ();
	countPersonen();
	giving ();
	nummer();			// neue Nummer f�r Partei || Einheit
	recruiting ();
	tarnung();			// x8_Rasses.c
	prefix();			// x8_Rasses.c
	benutzen();			// Benutzen von Tr�nken
	trankaktionen();	// diverse Tr�nke wirken jetzt
	quit ();
	ConnectUnits();		// Liste der Einheiten neu erstellen, wegen rausschmiss verschiedener Einheiten
	enter ();
	givecommand ();
	leaving ();
	Gebaeude();			// ob Funktion gew�hrleistet, ggf. Zerfall
	destroy ();
	spionieren();		// hier wird jetzt Spionage durchgef�hrt
	produce ();
	learn ();
	magic ();
	stealing ();
	movement ();
	predigen();
	ConnectUnits();		// Einheiten haben sich bewegt, dadurch zeigt unit->r auf die falsche Region
	DropIllegalBuildingAndShip();
	last_orders ();
	demographics ();
	ConnectUnits();		// Liste der Einheiten neu erstellen, wegen Hunger verschiedener Einheiten
	checkparteitarnung();	// falls einige Parteien ausgel�scht wurden
	countPersonen();

//#ifndef FANTA_LIMITED_EDITION
//	DebugTest();
//#endif
}

void
processturn (void)
{
	/* Der Name der Befehlsdatei liegt in buf.  */
	srand(turn);	// ein Versuch immer die gleichen Ausgangbedinungen pro Runde zu haben
	if (!strcmp (zat, NOZAT)) puts ("Kein ZAT angegeben!");
	getgarbage ();
	if (!readorders ()) return;
	processorders ();
	turn++;
	statistik();
	writesummary ();
	// clear_newbie_flags ();
	savegame();
	saveUnits();
	show_all_reports ();
}

void
show_report (faction *f)
{
	sprintf (buf, "%s.r", tobase36(f->no) );
	if (!cfopen (buf, "wt"))
		return;
	
	report (f);
	
	fclose (F);
}

void
show_computer_report (faction *f)
{
	sprintf (buf, "%s.cr", tobase36(f->no) );
	if (!cfopen (buf, "wt"))
		return;
	
	report_computer (f);
	
	fclose (F);
}

void timecheck()
{
	char b[20];
	struct tm *tt;
	time_t t = time(NULL);

	tt = gmtime(&t);
	if (tt->tm_mon+1 > 6)
	{
		printf("Sorry ... Nutzungszeit ist abgelaufen");
		gets(b);
		exit(100);
	}
}

int main (int argc, char *argv[])
{
	faction *f;
	int i, n, errorlevel, lean=0;

#ifdef FANTA_LIMITED_EDITION
	timecheck();
#endif

	printf ( "\n"
		"\t-+*+-   Fantasya %s   -+*+-\n"
		"\n"
		"\tCopyright (C) 2oo2 - 2oo6 Ronny M. Gorzelitz\n"
		"\tGerman Atlantis (C) Alexander Schroeder\n"
		"\tAtlantis (C) Russel Wallace\n"
		"\n"
		"\n"
		"\tItems: %d\tTalente: %d\tSprueche: %d\n"
		"\tTraenke:%d\tKraeuter: %d\n"
		"\n"
		"\n"
		, fanta_version
		, MAXITEMS,MAXSKILLS,MAXSPELLS
		, MAX_TRAENKE, MAX_KRAEUTER);
	
		/* Es soll auch moeglich sein, eine turn Nummer auf der
		Kommandozeile anzugeben ("-t n"). Dieses turn wird dann in
		initgame () gelesen.  Ein Resultat von 0 muss allerdings
	ueberprueft werden. */
	turn = -1;
	for (i = 1; i+1 < argc; i++)
		if (argv[i][0] == '-'
			|| argv[i][0] == '/')
			switch (argv[i][1])
		{
			
	case 't':
		turn = atoip (argv[i+1]);
		if (!turn && strcmp (argv[i+1], "0"))
			turn = -1;
		break;
		
	case 'v':
		lean = 1;
		break;
		}
		
		/* Falls die automatische Verarbeitung mit -v angegeben ist, dann
		kann das Spiel "lean" geladen werden, das heisst alle Texte der
		letzten Runde werden nicht mehr geladen.  */
		initgame (lean);
		
		/*
		* einge INIT-Routinen
		*/
		SetSkillOffSet();
		SetTraenke();
		SetProduktionsMatrix();
		SetSturmRegion();
		SetEisRegion();
		checkWorlds();
		DropIllegalBuildingAndShip();
		
		/* Bei errorlevel -1 wird das Menu angezeigt. Bei 0 wird etwas
		automatisch prozessiert, und kein Fehler ist passiert. Bei einem
		errorlevel ueber 0 ist ein Fehler passiert. */
		errorlevel = -1;
		for (i = 1; i != argc; i++)
			if (argv[i][0] == '-'
				|| argv[i][0] == '/')
				switch (argv[i][1])
			{
				
	case 't':
		break;   /* muss vor initgames () ausgewertet werden! */
		
	case 'r':
		errorlevel = 0;
		f = 0;
		
		/* Schreibe report fuer eine Partei, falls "-r n" und n eine
		Parteinummer ist. n darf nur 0 sein, falls wirklich "0"
		dort steht. atoip liefert naemlich auch 0, wenn ein Fehler
		passiert ist, und an der Stelle von n keine Zahl steht. */
		
		if (i+1 < argc
			&& argv[i+1][0]
			&& argv[i+1][0] != '-'
			&& argv[i+1][0] != '/')
		{
			n = atoipb36 (argv[i+1]);
			if (n || !strcmp (argv[i+1], "0"))
			{
				f = findfaction (n);
				if (f)
					show_report (f);
				else
					printf ("Partei %s nicht gefunden.\n", tobase36(n) );
			}
			break;
		}
		else
			show_all_reports ();
		break;
		
	case 'c':
		errorlevel = 0;
		f = 0;
		if (i+1 < argc
			&& argv[i+1][0]
			&& argv[i+1][0] != '-'
			&& argv[i+1][0] != '/')
		{
			n = atoipb36 (argv[i+1]);
			if (n || !strcmp (argv[i+1], "0"))
                f = findfaction (n);
			if (f)
			{
				show_computer_report (f);
				break;
			}
		}
		/* falls nicht gelungen: Fehler */
		puts ("Keine gueltige Partei-Nr. angegeben.");
		errorlevel = 1;
		break;
		
	case 's':
		errorlevel = 0;
		writesummary ();
		break;
		
	case 'v':
		if (i+1 < argc
			&& argv[i+1][0]
			&& argv[i+1][0] != '-'
			&& argv[i+1][0] != '/')
		{
			assert (lean);
			strcpy (buf, argv[i+1]);
			printf ("Verwende Befehlsdatei: %s\n", buf);
			processturn ();
			errorlevel = 0;
		}
		else
		{
			puts ("Keine Befehlsdatei angegeben.\n\n");
			errorlevel = 1;
		}
		break;
		
	case 'z':
		if (i+1 < argc
			&& argv[i+1][0]
			&& argv[i+1][0] != '-'
			&& argv[i+1][0] != '/')
		{
			strcpy (zat, argv[i+1]);
		}
		else
		{
			puts ("Kein ZAT angegeben.");
			errorlevel = 1;
		}
		break;
		
	default:
		errorlevel = 1;
		fprintf (stderr, "Usage: %s [options]\n"
			"-r [partei-nr]   : schreibt Reports fuer alle oder nur fuer die\n"
			"                   angegebene Partei\n"
			"-c partei-nr     : schreibt Computer Report fuer die angegebene Partei\n"
			"-t runde         : liest angegeben Runden Daten\n"
			"-s               : schreibt Zusammenfassung neu\n"
			"-z zat           : setzt den ZAT fuer die Reports\n"
			"-v datei         : verarbeitet die angegebene Befehlsdatei\n",
			argv[0]);
		break;
        }
		
		if (errorlevel >= 0) return errorlevel;
		
		puts ("? zeigt das Menue an.");
		
		for (;;)
		{
			printf ("> ");
			gets (buf);
			
			switch (buf[0])
			{
			case 'a':
				showaddresses ();
				break;
				
			case 'A':
				writeaddresses ();
				break;
			case 'c':
				writecrmap ();
				break;
				
			case 'e':
				createcontinent ();
				maxRegions = listlen(regions);
				findcubus();
				connectregions();
				break;
				
			case 'E':
				createworld ();	// erstellt eine neue Welt
				maxRegions = listlen(regions);
				findcubus();
				connectregions();
				break;
				
			case 'i':
			case 'I':
				regioninfo ();
				break;
				
			case 'k':
				showmap (M_TERRAIN);
				break;
				
			case 'K':
				writemap (M_TERRAIN);
				break;
				
			case 'p':
				showmap (M_FACTIONS);
				break;
				
			case 'P':
				writemap (M_FACTIONS);
				break;
				
			case 'u':
				showmap (M_UNARMED);
				break;
				
			case 'U':
				writemap (M_UNARMED);
				break;
				
			case 'n':
				addplayers ();
				break;
				
			case 'N':
				AddPlayerListe();
				break;
				
			case '!':
				newmenu();
				break;
				
			case 'v':
			case 'V':
				printf ("Datei mit den Befehlen? ");
				gets (buf);
				if (buf[0]) processturn ();
				break;
				
			case 'R':
				Rundschreiben();
				break;
				
			case 'r':
				printf ("Partei Nr.? (default: alle) ");
				gets (buf);
				i = atoipb36 (buf);
				if (i || !strcmp (buf, "0"))
				{
					f = findfaction (i);
					if (f)
					{
						show_report (f);
						report_computer (f);
					}
				}
				else
					show_all_reports ();
				break;
				
			case 'S':
			case 's':
			/* Die Zusammenfassung rechnet fuer jede Partei den Wert
			aus, der nachher auch in den Datenfile geschrieben werden
				muss. */
				writesummary ();
				savegame();
				statistik();
				saveUnits();
				break;
				
			case 't':
			case 'T':
				changeterrain ();
				break;
				
			case 'q':
			case 'Q':
				return 0;
				
			case 'L':
				loadgame();
				break;
			case 'l':
				listnames ();
				break;
				
			case 'm':
				addunit (-1);
				break;
			case 'M':
				for(i = 0; i < MAXSPELLS; i++) { printf("%i - %s\n", spelllevel[i], strings[spellnames[i]][0]); }
				break;

			case 'b':
			case 'B':
				addbuilding ();
				break;
				
			case 'y':
			case 'Y':
				writesummary ();
				break;

			case 'd':
				killWorld();
				break;
				
			case '1':
				ConfigOutput();
				break;

			case '2':
				saveUnits();
				break;
				
			case '3':
				statistik();
				break;

			case '9':
				Kolumbus();
				break;

			case '0':
				FineTuning();
				break;
				
			case 'z':
			case 'Z':
				fputs ("Neuer ZAT: ", stdout);
				if (!gets (zat))
				{
					puts ("Kein ZAT angegeben.");
					strcpy (zat, NOZAT);
				}
				break;
				
			default:
				printf ("z - ZAT setzen.  ZAT jetzt: %s\n"
					"v - Befehle verarbeiten -- besser mit der -v Option direkt!\n"
					"\n"
					"e - Erzeuge Regionen. (E - f�r neue Welt)\n"
					"d - loeschen einer Welt (ggf. mit Unterwelt)\n"
					"t - Terraform Regionen.\n"
					"m - Erschaffe Einheiten und Monster.\n"
					"b - Erbaue eine Burg.\n"
					"n - Neue Spieler hinzufuegen. (N - aus 'anmeldungen.dat')\n"
					"\n"
					"a - Adressen anzeigen.\n"
					"i - Info ueber eine Region.\n"
					"k - Karte anzeigen (K - in die Datei 'karte' schreiben).\n"
					"c - Karte als CR schreiben.\n"
					"p - Politische Karte (P - in die Datei 'parteikarte' schreiben).\n"
					"u - Unbewaffneter Regionen (U - in die Datei 'waffenkarte' schreiben)\n"
					"l - Liste aller Laendernamen zeigen.\n"
					"r - Reports schreiben.\tR- Rundschreiben\n"
					"y - Zusammenfassung schreiben.\n"
					"\n"
					"1 - alle Konfigdateien erzeugen\n"
					"2 - Einheitennummern speichern\n"
					"3 - Statistik fuer die Welten erzeugen\n"
					"9 - Kolumbusreport erzeugen\n"
					"0 - Bug-Fixing Routine\n"
					"\n"
					"s - Spielstand speichern. (S - DAT-Spielstand)\n"
					"\n"
					"q - Beenden.\n", zat);
        }
    }
}


