#include "atlantis.h"

void make_trank(region *r, unit *u, int trank, int menge)
{
	int oldmenge = menge;
	int i;
	int gotit;

	u->effekt = 0;
	if (u->building)
	{
		if ((u->building->typ == BT_KUECHE) && (u->building->size == produktionsmatrix[u->building->typ][PM_SIZE])) u->effekt = 1;
	}

	// max. Menge festlegen
	if (!menge)
	{
		menge = (effskill(u,SK_ALCHEMIE) / traenke_LVL[trank]) * u->number;
	} else
	{
		menge = min((effskill(u,SK_ALCHEMIE) / traenke_LVL[trank]) * u->number, menge);
		if (oldmenge != menge)
		{
			sprintf(buf,"nur %dx statt %dx %s m�glich",menge,oldmenge,traenke[trank]);
			addalchemie(u,buf);
		}
	}

	// testen ob alle Kr�uter da sind
	gotit = 0;
	for(i=0; i<MAX_TRAENKE; i++)
	{
		if (u->kraeuter[i]*(u->effekt+1) < zutaten[trank][i])
		{
			sprintf(buf,"f�r %s fehlt noch %s",traenke[trank],kraeuter[i]);
			addalchemie(u,buf);
			gotit = 1;
		}
	}
	if (gotit) return;

	// testen ob Kr�uter reichen ggf. Produktion reduzieren
	for(i=0; i<MAX_KRAEUTER; i++) if (zutaten[trank][i]) menge = min ( (u->kraeuter[i]*(u->effekt+1)) / zutaten[trank][i], menge);
	if (menge<0)
	{
		printf("\nZur produzierende Kr�uter sind auf <0 gesunkten!\n");
		exit(EXIT_FAILURE);
	}

	// Traenke produzieren
	for(i=0; i<MAX_KRAEUTER; i++) u->kraeuter[i] -= zutaten[trank][i] * (menge/(u->effekt+1));
	u->traenke[trank] += menge;
	if (trank == TR_JUNGFERNBLUT) 
	{
		r->peasants -= menge;
		sprintf(buf,"%s verwendet %d Bauern f�r %s",u->name,menge,traenke[trank]);
		addalchemie(u,buf);
	}

	sprintf(buf,"%s produziert %dx %s",u->name,menge,traenke[trank]);
	addalchemie(u,buf);
}

void make_kraeuter(region *r, unit *u, int kraut, int menge)
{
	int n=0;	// Anzahl der produzierten Kr�uter
	char pbuf[400];

	if (!menge) menge = (effskill(u,SK_KRAEUTERKUNDE) * u->number) / 2;
		   else menge = min((effskill(u,SK_KRAEUTERKUNDE) * u->number) / 2,menge);
	n = min(r->kraut[kraut], menge);
	if (n>menge)
	{
		sprintf(pbuf,"%s produziert nur %d statt %d %s",u->name,n,menge,kraeuter[kraut]);
		addevent(u ,pbuf);
	}

	u->kraeuter[kraut] += n;
	r->kraut[kraut] -= n;

	u->skills[SK_KRAEUTERKUNDE] += min (n, u->number) * PRODUCEEXP;

	sprintf(pbuf,"%s produziert %d %s",u->name,n,kraeuter[kraut]);
	addevent(u ,pbuf);

	return;
}

void gib_kraeuter(long menge,unit * u,region * r,unit * u2,int kraut,strlist * S)
{
	char pbuf[MAXSTRING];

	if (menge>u->kraeuter[kraut])
	{
		mistake2 (u,S,"soviel hat die Einheit nicht");
		menge = u->kraeuter[kraut];
	}

	if (u2) u2->kraeuter[kraut] += menge;
	u->kraeuter[kraut] -= menge;

	if (u2)
	{
		sprintf(pbuf,"%s (%s) gib %s (%s) %d %s",u->name,tobase36(u->no),u2->name,tobase36(u2->no),menge,kraeuter[kraut]);
		if (u2->faction != u->faction)
		{
			addcommerce(u2 ,pbuf);
			addcommerce(u ,pbuf);
		} else
		{
			addevent(u ,pbuf);
		}
	} else
	{
		sprintf(pbuf,"%s (%s) wirft %d %s weg", u->name, tobase36(u->no), menge, kraeuter[kraut]);
		addevent(u ,pbuf);
	}

	return;
}

void gib_traenke(long menge,unit * u,region * r,unit * u2,int trank,strlist *S)
{
	char pbuf[MAXSTRING];

	if (menge>u->traenke[trank])
	{
		mistake2 (u,S,"soviel hat die Einheit nicht");
		menge = u->traenke[trank];
	}

	if (u2) u2->traenke[trank] += menge;
	u->traenke[trank] -= menge;

	if (u2)
	{
		sprintf(pbuf,"%s (%s) gib %s (%s) %d %s",u->name,tobase36(u->no),u2->name,tobase36(u2->no),menge,traenke[trank]);
		if (u2->faction != u->faction)
		{
			addcommerce(u2 ,pbuf);
			addcommerce(u ,pbuf);
		} else
		{
			addevent(u ,pbuf);
		}
	} else
	{
		sprintf(pbuf,"%s (%s) wirft %d %s weg", u->name, tobase36(u->no), menge, traenke[trank]);
		addevent(u ,pbuf);
	}

	return;
}

/*
 * Funktion gibt eine neue Richtung zur�ck, wenn O �bergeben wird, wird NO,O oder SO zur�ckgegeben.
 * Wird -1 �bergeben, so wird eine zuf�llige Richtung zur�ckgeben.
 */

int getadirection(int d)
{
	int i = rand()%384;

	if (d<0)
	{
		d = rand() % MAXDIRECTIONS;
	} else
	{
		if (i>256)
		{
			d += 1;
			if (d>MAXDIRECTIONS-1) d = 0;
		} else
		{
			if (i<128)
			{
				d -= 1; 
				if (d<0) d = MAXDIRECTIONS-1;
			} else
			{
				// Richtung bleibt
			}
		}
	}

	return d;
}

/*
 * Diese Funktion l�st Inseln bewegen, eine Insel ist am Anfang komplett von Ozean umgeben
 * sp�ter wird diese Insel an Kontinente "andocken", somit eigentlich keine Insel mehr
 * damit sp�ter noch die Insel als solches erkannt wird, wird r->movement auf 1 gesetzt
 */

void InselnBewegen()
{
	region *r;

	int i;
	int regAnz = 0;

	//return;				// Inseln bewegen sich erstmal nicht mehr

	indicator_reset(maxRegions);

	for(r = regions; r; r=r->next)
	{
		int ozean=0;
		region *r2;

		if (r->terrain != T_OCEAN)
		{
			// Inseln lokaliesieren und als Insel definieren
			for(i=0; i<MAXDIRECTIONS; i++)
			{
				r2 = r->connect[i];
				if (!r2)
				{
					// Chaos z�hlt auch als Ozean
					ozean++;
				} else
				{
					if (r2->terrain == T_OCEAN) ozean++;
				}
			}

			if (r->movement) ozean = 6;
	
			// ist eine Insel und somit Werte setzen und ggf. bewegen
			if (ozean==6)
			{
				int x = r->x,y=r->y,x2,y2;

				r->movement = 1;
				i = getadirection(-1);
				x2 = r->x + delta_x[i];				// neue Position
				y2 = r->y + delta_y[i];
				r2 = findregion(x2,y2,r->z);		// Region an neuer Position

				if (r2)
				{
					if ((r2->terrain == T_OCEAN) && (r2->units == NULL))	// damit Insel nicht Chaos landet und KEINE Einheiten dort
					{
						// Inseln tauschen
						r2->x = x;
						r2->y = y;
						r->x = x2;
						r->y = y2;
					}
				}
			}
		} // r->terrain = T_OCEAN
		
		regAnz++;
		indicator_count_up(regAnz);
	} // for(r=regions

	findcubus();
	connectregions (); // es existieren noch die alten Verbindungen, erzeugt also Fehler im NR
}


void fflush_stdin()
{
	int c;
	while ((c=getchar()) != '\n' && (c != EOF));
}
