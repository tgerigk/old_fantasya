#include "atlantis.h"
#include <io.h>
#include <direct.h>

FILE *out;



void indent(lvl)
{
	int i;
	if (lvl) for(i = 0; i <= lvl * 2; i++) fprintf(out, " ");
}

char *parseData(char *data)
{
	static char buf[1500];
	int i, p;
	p = 0;
	for(i = 0; i <= (signed) strlen(data); i++)
	{
		if (data[i] == '<') { buf[p++] = '&'; buf[p++] = 'l'; buf[p++] = 't'; buf[p++] = ';'; continue; }
		if (data[i] == '>') { buf[p++] = '&'; buf[p++] = 'g'; buf[p++] = 't'; buf[p++] = ';'; continue; }
		if (data[i] == '&') { buf[p++] = '&'; buf[p++] = 'a'; buf[p++] = 'm'; buf[p++] = 'p'; buf[p++] = ';'; continue; }
		buf[p++] = data[i];
	}

	return buf;
}

void writeData(int lvl, char *tag, char *data)
{
	indent(lvl);
	if (!tag) { puts("TAG ist ungueltig"); return; }
	if (!data) { printf("DATA ist ungueltig (TAG: %s)\n", tag); return; }
	fprintf(out, "<%s>%s</%s>\n", tag, parseData(data), tag);
}

void writeVData(int lvl, char *tag, char *data, int value)
{
	indent(lvl);
	if (!tag) { puts("TAG ist ungueltig"); return; }
	if (!data) { printf("DATA ist ungueltig (TAG: %s)\n", tag); return; }
	fprintf(out, "<%s value=\"%d\">%s</%s>\n", tag, value, parseData(data), tag);
}

void writeValue(int lvl, char *tag, int value)
{
	indent(lvl);
	if (!tag) { puts("TAG ist ungueltig"); return; }
	fprintf(out, "<%s>%d</%s>\n", tag, value, tag);
}

void writeTag(int lvl, char *tag)
{
	indent(lvl);
	if (!tag) { puts("TAG ist ungueltig"); return; }
	fprintf(out, "<%s/>\n", tag);
}

void openTag(int lvl, char *tag)
{
	indent(lvl);
	if (!tag) { puts("TAG ist ungueltig"); return; }
	fprintf(out, "<%s>\n", tag);
}

void closeTag(int lvl, char *tag)
{
	indent(lvl);
	if (!tag) { puts("TAG ist ungueltig"); return; }
	fprintf(out, "</%s>\n", tag);
	fflush(out);	// besser ist das
}

void XMLReport(faction *f)
{
	region *r;
	unit *u, *bu;
	ship *sh;
	building *b;
	rfaction *rf;
	faction *hf;
	char buf[20];
	int visible, i, wage, rmoney;

	printf("- XML report (%s) ...\n", tobase36(f->no));
	sprintf(buf, "reports\\%s.xml", tobase36(f->no));
	out = fopen(buf, "w");
	if (out == NULL)
	{
		printf("\t-> konnte '%s' nicht �ffnen\n", buf);
		return;
	}

	fprintf(out, "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");

	openTag(0, "fantasya");
	  openTag(1, "spiel");
	    writeData(2, "name", "fantasya");
		writeValue(2, "runde", turn);
		writeData(2, "date", gamedate(f));
	  closeTag(1, "spiel");
	  openTag(1, "partei");
		writeValue(2, "nummer", f->no);
		writeData(2, "name", f->name);
		writeData(2, "mail", f->addr);
		writeValue(2, "optionen", f->options);
		writeValue(2, "kosten", RecruitCost[f->race]); // Rekurtierungskosten
		writeData(2, "rasse", racenames[f->race][1]);
		for(rf = f->allies; rf; rf = rf->next) { writeValue(3, "allianz", rf->factionno); rf->faction->gotit = 1; }
		openTag(2, "meldungen");
		// TODO Meldungen erzeugen
		closeTag(2, "meldungen");
	  closeTag(1, "partei");

	for(hf = factions; hf; hf = hf->next) hf->gotit = 0;	// alle Parteien sind noch unbekannt
	for(r = regions; r; r = r->next)
	{
		r->gotit = 0;	// Region wird ggf. nicht ben�tigt
		for(u = r->units; u; u = u->next) if (u->faction == f) break;
		if (!u) continue;
		r->gotit = 1;	// eigene Einheiten in der Region ... spart nachher etwas Zeit
		for(u = r->units; u; u = u->next)
		{
			hf = findfaction(u->tarn_no);
			visible = cansee(f, r, u);
			if ((visible == 2) || (hf->gotit))
			{
				if (hf->no != f->no) if (hf->gotit != 2)
				{
					hf->gotit = 2;
					openTag(1, "partei");
					  writeData(2, "name", hf->name);
					  writeValue(2, "nummer", hf->number);
					  writeData(2, "mail", hf->addr);
					closeTag(1, "partei");
				}
			}
		}
	}

	for(i = 0; i < MAXSPELLS; i++)
	{
		if (f->showdata[i] == 1)
		{
			openTag(1, "zauberspruch");
			  writeVData(2, "name", strings[spellnames[i]][f->language], i);
			  writeValue(2, "level", spelllevel[i] * 2);
			  writeData(2, "beschreibung", strings[spelldata[i]][f->language]);
			closeTag(1, "zauberspruch");
		}
	}

	for(r = regions; r; r = r->next)
	{
		if (!r->gotit) continue;
		openTag(1, "region");
		  writeValue(2, "x", r->x - f->xorigin);
		  writeValue(2, "y", r->y - f->yorigin);
		  writeValue(2, "z", r->z);
		  if (r->display) writeData(2, "beschreibung", r->display);
		  writeData(2, "terrain", strings[terrainnames[mainterrain(r)]][0]);
		  writeValue(2, "bauern", r->peasants);
		  writeValue(2, ((r->terrain == T_VULKAN) ? "flugdrachen" : "pferde"), r->horses);
		  writeValue(2, "baeume", r->trees);
		  writeValue(2, "silber", r->money);
		  wage = 0;
		  if (production[r->terrain])
		  {
			  wage = WAGE;
			  b = largestbuilding(r);
			  if (b) wage += buildingeffsize(b) * BONUS;
		  }
		  rmoney = r->money;
		  writeValue(2, "unterhaltung", rmoney + (min (r->peasants, (production[r->terrain] - r->trees) * MAXPEASANTS_PER_AREA) * wage) / ENTERTAINFRACTION );
		  writeValue(2, "rekruten", r->peasants / RECRUITFRACTION);
		  writeValue(2, "lohn", wage);
		  if (r->terrain != T_OCEAN)
		  {
			  writeData(2, "name", r->name);
			  openTag(2, "preise");
			    writeValue(3, "balsam",		(itemprice[0] * r->demand[0] / 100 * (0 == r->produced_good ? -1 : 1)));
			    writeValue(3, "gewuerz",	(itemprice[1] * r->demand[1] / 100 * (1 == r->produced_good ? -1 : 1)));
			    writeValue(3, "juwel",		(itemprice[2] * r->demand[2] / 100 * (2 == r->produced_good ? -1 : 1)));
			    writeValue(3, "myrrhe",		(itemprice[3] * r->demand[3] / 100 * (3 == r->produced_good ? -1 : 1)));
			    writeValue(3, "oel",		(itemprice[4] * r->demand[4] / 100 * (4 == r->produced_good ? -1 : 1)));
			    writeValue(3, "seide",		(itemprice[5] * r->demand[5] / 100 * (5 == r->produced_good ? -1 : 1)));
			    writeValue(3, "weihrauch",	(itemprice[6] * r->demand[6] / 100 * (6 == r->produced_good ? -1 : 1)));
			  closeTag(2, "preise");
		  }

		  for(i = 0; i < 6; i++)
		  {
			  if (r->road[i])
			  {
				  openTag(2, "strasse");
				    writeData(3, "richtung", directions[i]);
					writeValue(3, "gebaut", r->road[i]);
				  closeTag(2, "strasse");
			  }
		  }

		  if (r->hoehle)
		  {
			  hoehle *h = findhoehle(r->hoehle);
			  openTag(2, "hoehle");
			    writeData(3, "name", h->name);
				writeData(3, "beschreibung", h->beschr);
				if (h->offen) writeTag(3, "offen");
			  closeTag(2, "hoehle");
		  }

		  for(b = r->buildings; b; b = b->next)
		  {
			  openTag(2, "gebaeude");
			    writeValue(3, "nummer", b->no);
				writeData(3, "name", b->name);
				if (!b->typ) writeData(3, "typ", buildingnames[buildingeffsize(b)]); else writeData(3, "typ", BuildingNames[b->typ]);
				if (b->display) writeData(3, "beschreibung", b->display);
				writeValue(3, "groesse", b->size);
				u = buildingowner(r, b);
				if (u) writeValue(3, "besitzer", u->no);	// hier ohne Parteizugeh�rigkeit ... kann direkt aus Unit abgelesen werden
				b->voll = b->size;							// "Glasburgen"
			  closeTag(2, "gebaeude");
		  }

		  for(sh = r->ships; sh; sh = sh->next)
		  {
			  openTag(2, "schiff");
			    writeValue(3, "nummer", sh->no);
				writeData(3, "name", sh->name);
				if (sh->display) writeData(3, "beschreibung", sh->display);
				writeData(3, "typ", shiptypes[0][sh->type]);
				u = shipowner(r, sh);
				if (u) writeValue(3, "kapitaen", u->no);		// Partei wie immer direkt aus Unit
				writeValue(3, "groesse", shipcost[sh->type] - sh->left);
			  closeTag(2, "schiff");
		  }

		  for(u = r->units; u; u = u->next)
		  {
			  if ( (visible = cansee(f, r, u)) )
			  {
				  bu = (u->building) ? NULL : u;
				  openTag(2, "einheit");
				    writeData(3, "name", u->name);
					writeValue(3, "nummer", u->no);

					if (u->building)
					{
						unit *hu;
						bu = NULL;
						for(hu = r->units; hu; hu = hu->next) if ((u->building == hu->building) && (hu->faction == f)) bu = hu; else bu = u;
						u->building->voll -= u->number;
						if (u->building->voll < 0) bu = u;
					}
					if (visible == 2)	// volle Sichtbarkeit ... bei Parteitarnung fehlt hier jetzt das Tag!!
					{
						if (f->no == u->faction->no)	// eigene Einheit
						{
							writeValue(3, "partei", f->no);
							if (u->tarn_no != f->no) writeValue(3, "Verkleidet", u->tarn_no);
						} else							// fremde Einheit
						{
							if (u->tarn_no) writeValue(3, "partei", u->tarn_no);
						}
					}
					writeValue(3, "personen", u->number);
					if (u->prefix) writeData(3, "prefix", u->prefix);
					switch (u->type)
					{
						case U_ILLUSION:
						case U_GUARDS:		writeData(3, "rasse", "Mensch");				break;
						case U_MAN:			writeData(3, "rasse", racenames[u->raceTT][1]);	break;
						case U_UNDEAD:		writeData(3, "rasse", "Untoter");				break;
						case U_FIREDRAGON:	writeData(3, "rasse", "Feuerdrache");			break;
						case U_DRAGON:		writeData(3, "rasse", "Drache");				break;
						case U_WYRM:		writeData(3, "rasse", "Wyrm");					break;
						default:			writeData(3, "rasse", "unbekannte Rasse");		break;
					}
					if (u->display) writeData(3, "beschreibung", u->display);
					if (u->guard)	writeTag(3, "bewacht");
					if (u->building) writeValue(3, "gebaeude", u->building->no);
					if (u->ship)	writeValue(3, "schiff", u->ship->no);

					if (u->faction == f)	// geh�rt zur eigenen partei ... also mehr Infos
					{
						switch(u->status)
						{
							case ST_AVOID:	writeData(3, "kampfstatus", "nicht");			break;
							case ST_BEHIND:	writeData(3, "kampfstatus", "hinten");			break;
							default:		writeData(3, "kampfstatus", "vorne");
						}
						if (u->tarnung && effskill(u, SK_STEALTH)) writeTag(3, "getarnt");
						if (u->faction->race == R_DEMON) writeTag(3, "echse");
						writeData(3, "zustand", getLebenszustand(u));
						if (u->lastorder) writeData(3, "command", u->lastorder);
						if ((u->thisorder) && (!u->lastorder)) writeData(3, "command", u->thisorder);
						if (u->thisorder2) writeData(3, "command", u->thisorder2);
						if (u->comments)
						{
							strlist *tempstr = u->comments;
							while(tempstr)
							{
								writeData(3, "commend", tempstr->s);
								tempstr = tempstr->next;
							}
						}
						for(i = 0; i < MAXSKILLS; i++) if (u->skills[i]) writeVData(3, "talent", skillnames[i], u->skills[i]);
						for(i = 0; i < MAXSPELLS; i++) if (u->spells[i]) writeData(3, "zauberspruch", strings[spellnames[i]][f->language]);
					} // u->faction == f

					if (visible == 2) // TODO sollte das nicht einfach nur (visible) sein ??? ... sonst werden Gegenst�nde nicht von Partei getarnen Einheiten gezeigt
					{
						for(i = 0; i < MAXITEMS; i++)
						{
							if (u->items[i])
							{
								if (i == I_SILVER)
								{
									if (u->faction = f)
									{
										writeVData(3, "gegenstand", strings[itemnames[0][i]][0], u->items[i]);
									} else
									{
										if (u->items[i] < 500 * u->number)
										{
											writeVData(3, "gegenstand", "Silberbeutel", 1);
										} else
										{
											writeVData(3, "gegenstand", "Schatzkiste", 1);
										}
									}
								} else
								{
									if ((u->building) && (!bu) && (u->faction != f))
									{
										writeVData(3, "gegenstand", strings[itemnames[0][i]][0], 1);
									} else
									{
										writeVData(3, "gegenstand", strings[itemnames[0][i]][0], u->items[i]);
									}
								}
							}
						}
						if (u->faction == f)
						{
							for(i = 0; i < MAX_KRAEUTER; i++) if (u->kraeuter[i]) writeVData(3, "gegenstand", kraeuter[i], u->kraeuter[i]);
							for(i = 0; i < MAX_TRAENKE; i++) if (u->traenke[i]) writeVData(3, "gegenstand", traenke[i], u->traenke[i]);
						} else
						{
							for(i = 0; i < MAX_KRAEUTER; i++) if (u->kraeuter[i]) { writeVData(3, "gegenstand", "Kr�uterbeutel", 1); break; }
							for(i = 0; i < MAX_TRAENKE; i++) if(u->traenke[i]) { writeVData(3, "gegenstand", "Tr�nke und Mixturen", 1); break; }
						}
					} // visible == 2
				  closeTag(2, "einheit");
			  }
		  }
		closeTag(1, "region");
	}


	fprintf(out, "</fantasya>\n");
	fclose(out);
}
