/* German Atlantis PB(E)M host Copyright (C) 1995-1999  Alexander Schroeder

 based on:
 
  Atlantis v1.0 13 September 1993 Copyright 1993 by Russell Wallace
  
   This program may be freely used, modified and distributed.  It may
   not be sold or used commercially without prior written permission
from the author.  */

#include "atlantis.h"

/* ------------------------------------------------------------- */

int
finddirection (char *s)
{
	return findstr (directions, s, MAXDIRECTIONS_S);
}

int
getdirection (void)
{
	int i = finddirection (getstr ());
	return i>5 ? i-6 : i;
}

/* ------------------------------------------------------------- */

double
weight (unit * u)
{
	int i;
	double n;
	
	n = 0;
	
	for (i = 0; i != LASTLUXURY; i++) n += ((double)u->items[i] * itemweight[i]);
	
	n += u->number * PERSONWEIGHT;
	
	return n;
}

int
ridingcapacity (unit * u)
{
	int w, n;
	int Wagoncapacity = WAGONCAPACITY;
	
	if (u->race == R_TROLL) Wagoncapacity += 20;

	n = 0;
	
	/* Man traegt sein eigenes Gewicht plus seine Kapazitaet! Die Menschen
	tragen nichts (siehe walkingcapacity). ein wagen zaehlt nur, wenn er von
	zwei pferden gezogen wird */
	
	w = min ((u->items[I_HORSE]+u->items[I_PEGASUS]+u->items[I_FLUGDRACHEN]) / HORSESNEEDED, u->items[I_WAGON]);
	n += w * Wagoncapacity;
	
	/*
	* 2 Trolle k�nnen einen Wagen ziehen
	*/
	
	if ((u->items[I_WAGON] != w) && (u->race == R_TROLL))
	{
		int p = u->number;
		/*
		* wenn noch Wagen �brig sind, also w aus den Pferden berechnet wurde
		* k�nnen Trolle diese ziehen. Zuerst m�ssen aber die bereits verwendeten
		* Wagen bzw. Trolle abgezogen werden
		*/
		
		p -= w;								// das sind die �brigen Trolle, da ein Troll f�r 2 Pferde ben�tigt wird
		w = u->items[I_WAGON] - w;			// das sind die �brigen Wagen
		
		n += min(w,(p/2)) * Wagoncapacity;	// zus�tzlich zum Transportieren, da 2 Trolle einen Wagen ziehen
	}
	
	n += (u->items[I_HORSE]+u->items[I_PEGASUS]) * HORSECAPACITY;
	n += u->items[I_FLUGDRACHEN] * DRAGONCAPACITY;
	
	return n;
}

int walkingcapacity (unit * u)
{
	int n;
	
	/* Das Gewicht, welches die Pferde tragen, plus das Gewicht, welches die
	Leute tragen */
	
	n = ridingcapacity (u) + u->number * (u->type == U_MAN ? personcapacity[u->race] : carryingcapacity[u->type]);
	
	return n;
}

/* Changed by Bjoern Becker (code by Matthias Strunk) , 26.07.2000 */

int canwalk (unit * u)
{
	// I_HORSE und I_PEGASUS werden als eins gez�hlt  -----v		-----v
	return weight (u) - walkingcapacity (u) <= 0 && (u->items[I_HORSE]+u->items[I_PEGASUS]+u->items[I_FLUGDRACHEN]) <= max(1,2*effskill (u,SK_RIDING))*u->number;
}

int canride (region *r, unit *u)
{
	return effskill (u, SK_RIDING)
		&& weight (u) - ridingcapacity (u) <= 0
		&& r->terrain != T_OCEAN
		&& (u->items[I_HORSE]+u->items[I_PEGASUS]+u->items[I_FLUGDRACHEN]) <= 2*effskill (u, SK_RIDING)*u->number;
}

/* End of changes */

int
capacity (region * r, ship * sh)
{
	double n;
	unit *u;
	
	n = 0;
	
	for (u = r->units; u; u = u->next) if (u->ship == sh) n += weight (u);
	
	return (int) (shipcapacity[sh->type] - n);
}

int
cansail (region * r, ship * sh)
{
	return capacity (r,sh) >= 0;
}

int
enoughsailors (region * r, ship * sh)
{
	int n;
	unit *u;
	
	n = 0;
	
	for (u = r->units; u; u = u->next)
		if (u->ship == sh)
			n += effskill (u, SK_SAILING) * u->number;
		
		return n >= sailors[sh->type];
}

/* ------------------------------------------------------------- */

region *
drift_to (region *r, ship *sh)
{
	
/* Bestimmt ob ein Schiff treibt, und wohin es treibt. Kann das Schiff
	nicht treiben, wird 0 zurueck geliefert. */
	
	region *goal;
	unit *u;
	int i;
	
	/* Kapitaen bestimmen.  Falls kein Kapitaen da ist, driftet das Schiff so oder so.  */
	for (u = r->units; u; u = u->next) if (u->ship == sh && u->owner) break;
		if (u)
		{
		/* Falls der Kapitaen kein u->thisorder hat, dann muss er sich
		schon bewegt haben. Auch in driftenden Schiffen haben alle
		Einheiten thisorder[0]=0 gesetzt bekommen, weil die Einheiten
		in der neuen Region sonst ihre thisorder nocheinmal
			durchfuehren koennten.  */
			if (!u->thisorder[0])
				return 0;
			
				/* Kapitaen mit genuegend Talent da? Schiff nicht Beschaedigt?
				Genuegend Matrosen? Genuegend leicht? Dann sind die
			Bedingungen von ship_ready () erfuellt.  */
			if (effskill (u, SK_SAILING) >= sh->type
				&& !sh->left
				&& enoughsailors (r, sh)
				&& cansail (r, sh))
				return 0;
			
				/* Verzauberte Schiffe driften nicht, ausser sie wurden mit
			einem Sturmwind verzaubert.  */
			if (sh->enchanted && sh->enchanted != SP_STORM_WINDS)
				return 0;
		}
		
		/* Auswahl einer Richtung: Zuerst auf Land, dann zufaellig.  Ich
		denke, dass es nicht moeglich ist, dass hier keine Richtung mehr
		moeglich ist.  */
		for (i = 0; i != MAXDIRECTIONS; i++)
			if (r->connect[i] && r->connect[i]->terrain != T_OCEAN)
				break;
			if (i == MAXDIRECTIONS)
				do
				{
					i = rand () % MAXDIRECTIONS;
				}
				while (!r->connect[i]);
				
				goal = r->connect[i];
				
				/* Wenn das Schiff aber driftet, und wenn ein Sturmwind blaest, dann
				soll das Schiff 2 Regionen weit segeln. */
				if (goal && goal->terrain == T_OCEAN && goal->connect[i]
					&& sh->enchanted == SP_STORM_WINDS)
					goal = goal->connect[i];
				
				return goal;
				
}

void
drowning_men (region *r)
{
	unit *u;
	
	if ((r->terrain != T_OCEAN) && (r->terrain != T_LAVASTROM)) return;

	if (mainterrain(r) == T_OCEAN)
	{
		for (u = r->units; u; u = u->next)
			if (!u->ship && u->enchanted != SP_WATER_WALKING)
			{
				sprintf (buf, "%s ertrinkt in %s.", unitid (u), regionid (r, u->faction->xorigin , u->faction->yorigin ));
				addwarning (u , buf);
				u->number = 0;
			}
	}

	if (mainterrain(r) == T_LAVASTROM)
	{
		for (u = r->units; u; u = u->next)
		{
			sprintf (buf, "%s verbrennt in %s.", unitid (u), regionid (r, u->faction->xorigin , u->faction->yorigin ));
			addwarning (u , buf);
			u->number = 0;
		}
	}
};

void
drifting_ships (region *r)
{
	region *goal;
	faction *f;
	ship *sh, *sh2;
	unit *u, *u2;
	
	/* Schiffe auf hoher See koennen driften. */
	
	if (r->terrain != T_OCEAN)
		return;
	
	for (sh = r->ships; sh;)
    {
		/* Da Schiffe sich bewegen koennen, kann sich sh->next aendern. Deswegen vorher abspeichern. */
		sh2 = sh->next;
		
		goal = drift_to (r, sh);
		if (goal)
		{
			for (f = factions; f; f = f->next)
				f->dh = 0;
			
			translist (&r->ships, &goal->ships, sh);
			for (u = r->units; u;)
			{
				u2 = u->next;
				
				if (u->ship == sh)
				{
					translist (&r->units, &goal->units, u);
					u->thisorder[0] = 0;
					u->faction->dh = 1;
				}
				
				u = u2;
			}
			
			for (f = factions; f; f = f->next)
				if (f->dh)
				{
					sprintf (buf, "Das Schiff %s wurde von Winden nach %s getrieben.", shipid (sh), regionid (goal, f->xorigin , f->yorigin ));
					addwarning (shipowner(r, sh), buf);
				};
		}
		
		/* naechstes Schiff */
		sh = sh2;
    }
}

/* ------------------------------------------------------------- */

region * movewhere (region * r, unit *u)
{
	region *r2;
	int i;

	i = getdirection ();
	if (i == -1) return 0;

	// Verlaufen im Eis bzw. Sandstrom
	if ((r->terrain == T_SANDSTROM) && !(effskill(u,SK_RIDING) && (u->items[I_PEGASUS] == u->number) && (u->items[I_WAGON] == 0) && (u->items[I_CATAPULT] == 0)))
	{
		if (rand() % 100 < 66)
		{
			if (rand() % 100 < 50) i--; else i++;
			if (i < 0) i = MAXDIRECTIONS-1;
			if (i > MAXDIRECTIONS-1) i = 0;
			sprintf(buf, "%s verl�uft sich im Sandstrom von %s", unitid(u), r->name);
			addmovement(u, buf);
		}
	}

	if (mainterrain(r) == T_EIS)
	{
		if (rand() % 100 < 66)
		{
			if (rand() % 100 < 50) i--; else i++;
			if (i < 0) i = MAXDIRECTIONS-1;
			if (i > MAXDIRECTIONS-1) i = 0;
			sprintf(buf, "%s verl�uft sich im Eis (%d,%d)", unitid(u), r->x - u->faction->xorigin, r->y - u->faction->yorigin);
			addmovement(u, buf);
		}
	}

	r2 = r->connect[i];
	
	/* Falls man neue Parteien genau an der Grenze zum Chaos ausgesetzt
	hat und diese Einheiten in der ersten Runde in Chaos schicken,
	dann ist r2 hier noch nicht gesetzt.  i ist die Richtung, in der
	eine neue Insel angefuegt werden soll.  makeblock() ruft
	connectregions() auf, so dass r->connect[i] nun definiert sein
	sollte!  */
	
	if (!r2)
    {
		printf ("   Neue Insel neben %s.\n", regionid (r , 0 ,0 ));
		makeblock (r->x + delta_x[i], r->y + delta_y[i], r->z);		/* Changed by Bjoern Becker , 26.07.2000 */
		seed_monsters (r->x + delta_x[i], r->y + delta_y[i], r->z);	/* Changed by Bjoern Becker , 26.07.2000 */
		connectregions();	// ohne FindCubus neu aufzubauen!!
		r2 = r->connect[i];
    }
	
	assert (r2);
	
	/* r2 enthaelt nun die existierende Zielregion - ihre Nachbaren
	sollen auch schon alle existieren.  Dies erleichtert das
	Umherschauen bei den Reports!  Bei automatisch generierten Inseln
	(hier!) werden Drachen ausgesetzt.  
	
     i zeigt nun in alle Richtungen, und fuer jede wird geprueft ob
     dort Chaos (Regionen existieren noch nicht) herrscht.  Die
     Herkunftsregion wird hier zwar mitgeprueft, aber das ist mir
	egal.  */
	
	for (i = 0; i != MAXDIRECTIONS; i++)
	{
		if (!r2->connect[i])
		{
			printf ("   Neue Insel neben %s.\n", regionid (r, 0 ,0 ));
			makeblock (r2->x + delta_x[i], r2->y + delta_y[i], r2->z);		/* Changed by Bjoern Becker , 26.07.2000 */
			seed_monsters (r2->x + delta_x[i], r2->y + delta_y[i], r2->z);	/* Changed by Bjoern Becker , 26.07.2000 */
			connectregions();	// ohne FindCubus neu aufzubauen!!
		}
	}

	return r2;
}

/* Diese Funktion uebertraegt die Rolle des Burgherren oder Kapitaens
auf die naechste Einheit, welche Anrecht darauf hat, falls der
Burgherren oder der Kapitaen seinen Besitz verlaesst.  */

void leave (region * r, unit * u)
{
	unit *u2;
	building *b;
	ship *sh;
	int leaveship = 0;

	// Werft: da eine Einheit in einer Werft und einem Schiff sitzen k�nnen, m�ssen sie aus einem zuerst raus
	// da es mit Schiffen effektiver ist als mit Geb�uden, kommt bei einem VERLASSE Befehl zuerst das Schiff
	// dran
	if (u->ship)
    {
		sh = u->ship;
		u->ship = 0;
		
		if (u->building) if (u->building->typ == BT_WERFT) leaveship = 1;

		if (u->owner)
        {
			u->owner = 0;
			
			for (u2 = r->units; u2; u2 = u2->next)
				if (u2->faction == u->faction && u2->ship == sh)
				{
					if (u != u2) u2->owner = 1;
					return;
				}
				
			for (u2 = r->units; u2; u2 = u2->next)
				if (u2->ship == sh)
				{
					if (u != u2) u2->owner = 1;
					return;
				}
        }
    }

	if (u->building)
    {
		if (leaveship) return;	// Einheit hat schon mal das Schiff verlassen

		b = u->building;
		u->building = 0;
		
		if (u->owner)
        {
			u->owner = 0;
			
			for (u2 = r->units; u2; u2 = u2->next)
				if (u2->faction == u->faction && u2->building == b)
				{
					u2->owner = 1;
					return;
				}
				
				for (u2 = r->units; u2; u2 = u2->next)
					if (u2->building == b)
					{
						u2->owner = 1;
						return;
					}
        }
    }
}

/* ------------------------------------------------------------- */

unit *
present (region * r, unit * u)
{
	unit *u2;
	
	for (u2 = r->units; u2; u2 = u2->next)
		if (u2 == u)
			break;
		
		return u2;
}

void
caught_target (region * r, unit * u)
{
	
	/* Verfolgungen melden */
	
	if (u->target)
    {
		
	/* Misserfolgsmeldung, oder bei erfolgreichem Verfolgen unter
		Umstaenden eine Warnung. */
		
		if (!present (r, u->target))
        {
			sprintf (buf, "%s hat uns in %s abgehaengt.",
				unitid (u->target), regionid (r, u->faction->xorigin , u->faction->yorigin ));
			addwarning (u , buf);
        }
		else if (!isallied (u->target, u)
			&& cansee (u->target->faction, r, u))
        {
			sprintf (buf, "%s ist uns bis nach %s gefolgt.",
				unitid (u), regionid (r, u->target->faction->xorigin , u->target->faction->yorigin ));
			addwarning (u->target, buf);		// u->target->faction
        }
    }
}

/* ------------------------------------------------------------- */

void
travel (region * r, unit * u, region * r2)
{
	/* Tech: Zu fuss reist man 1 Region, zu Rferde 2 Regionen. Mit
	Strassen reist man Doppelt so weit. Berechnet wird das mit BP
	(Bewegungspunkten). Zu Fuss hat man 2 BPs, zu Pferde 4, ie. die
	BP entsprechen der max. Anzahl Regionen, die man reisen kann.
	Normalerweise verliert man 2 BP pro Region, bei Strassen nur 1
	BP. Strassen verbinden Regionen, wenn Start- und Zielregion 100%
	Strassen haben. */

	region *r3;
	int k, m, i, dh;
	region *rv[MAXSPEED];
	int fliegen = 0;
	
	/* Auf hoher See ohne Wasserwandeln wird man spaehter noch ertrinken. */
	if ((mainterrain(r) == T_OCEAN) && (u->enchanted != SP_WATER_WALKING)) return;
	
	if (!canwalk (u))
    {
		mistakeu (u, "Die Einheit traegt zuviel Gewicht, um sich bewegen zu koennen");
		return;
    }

	// Test ob Einheit Pegasus hat und damit fliegen k�nnte,
	// fliegen kann eine Einheit, wenn sie Reiten hat und keine Wagen und keine Katapulte,
	if (effskill(u,SK_RIDING) && 
		(u->items[I_PEGASUS] + u->items[I_FLUGDRACHEN] > 0) && 
		(u->items[I_WAGON] + u->items[I_CATAPULT] == 0)) fliegen = 1;

	// Einheiten in Geroellebenen kommen nicht weiter *grins*
	if ((r->terrain == T_GEROELLEBENE) && (!fliegen))			// Pegasus kann aber fliegen
	{
		if (rand() % 100 < 50)
		{
			sprintf(buf, "%s kann in der Geroellebene von %s nicht weiter reisen. Grosse Steine versperrten den Weg.", unitid(u), r->name);
			addmovement(u, buf);
			return;
		}
	}

	/* wir suchen so lange nach neuen Richtungen, wie es geht. Diese werden
	dann nacheinander ausgefuehrt. */
	
	/* im array rv[] speichern wir die regionen ab, durch die wir wandern. */
	
	/* BPs! */
	
	k = 2;
	if (canride (r, u))
	{
		if ((!u->items[I_HORSE]) && (u->items[I_PEGASUS] + u->items[I_FLUGDRACHEN]) && (fliegen))	// nur wenn ein Pegasus fliegen kann, dann 3 Regionen
		{
			if(u->items[I_PEGASUS])
			{
				// wenn man nur Pegasus hat, kann man 3 Regionen weit Reisen
				k = 6;
			} else
			{
				// mit Flugdrachen sogar 6 Regionen
				k = 12;
			}
		} else
		{
			// sonst nur 2
			k = 4;
		}
	}
	
	m = 0;
	r3 = r;
	
	/* die naechste Region, in die man wandert, wird durch movewhere aus der
	letzten Region bestimmt.
	
     Anfangen tun wir bei r. r2 ist beim ersten Durchlauf schon gesetzt
     (Parameter!), das ziel des schrittes. m zaehlt die schritte, k sind die
	noch moeglichen schritte, r3 die letzte gueltige, befahrene Region. */
	
	while (r2 != 0)
    {
		if (roadto (r3, r2)) k -= 1; else k -= 2;
		
		if (k < 0) break;
		
		if (present (r3, u->target)) break;
		
		if (r3->blocked)
        {
			sprintf (buf, "%s konnte aus %s nicht ausreisen.", unitid (u), regionid (r3, u->faction->xorigin , u->faction->yorigin ));
			addwarning (u , buf);
			break;
			
        }
				
		// jetzt testen ob Einheit �ber Ozean kann
		if ((mainterrain(r2) == T_OCEAN) && (u->enchanted != SP_WATER_WALKING) && (!fliegen))
        {
			sprintf (buf, "%s entdeckt, dass (%d,%d) Ozean ist.", unitid (u), r2->x - u->faction->xorigin, r2->y - u->faction->yorigin);
			addwarning (u , buf);
			break;
        }

		// Einheiten die den Lavastrom betreten, verbrennen
		if ((r2->terrain == T_LAVASTROM) && (!fliegen))		// Pegasus darf dar�ber
		{
			sprintf(buf, "%s bemerkt die Hitze des Lavastromes (%d,%d)", unitid(u), r->x-u->faction->xorigin, r->y-u->faction->yorigin);
			addwarning(u,buf);
			break;
		}

		// Einheit hat Moor betreten
		if (u->faction->no != 0)	// Monster sterben nicht im Moor
		{
			if ((r2->terrain == T_MOOR) && (!fliegen))			// mit Pegasus kann man ja fliegen
			{
				if (!r2->road)	// eine Strasse kann Leben retten
				{
					int i, dead;
					// Tote ausrechnen
					for(i = 0; i != u->number; i++) if (rand() % 100 < 66) dead++;
					dead = max(0,dead);
					sprintf(buf, "%s verliert im Moor von %s %d Personen.", unitid(u), regionid(r2, u->faction->xorigin, u->faction->yorigin), dead);
					u->number -= dead;
					assert(u->number >= 0);
					addwarning(u, buf);
					// Items k�rzen ... ebenfalls 66%
					for(i = 0; i <= MAXITEMS; i++)
					{
						int j, kill = 0;
						for(j = 0; j <= u->items[i]; j++) if (rand() % 100 < 66) kill++;
						u->items[i] = max(0, u->items[i] - kill);
					}
				}
			}
		}
		
		rv[m] = r2;
		m++;
		
		r3 = r2;
		r2 = movewhere (r2, u);
    }
	
	/* Nun enthaelt r3 die letzte Region, in die man gewandert ist. Wir
	generieren hier ein Ereignis fuer den Spieler, das ihm sagt, bis wohin
	er gewandert ist, falls er ueberhaupt vom Fleck gekommen ist. Das ist
	nicht der Fall, wenn er in den ozean zu wandern versuchte */
	
	if (r3 != r)
    {
		sprintf (buf, "%s ", unitid (u));
		
		if (canride (r, u))
        {
			scat ("reitet");
			u->skills[SK_RIDING] += u->number * PRODUCEEXP;
        }
		else
			scat ("wandert");
		scat (" von ");
		scat (regionid (r, u->faction->xorigin , u->faction->yorigin ));
		scat (" nach ");
		scat (regionid (r3, u->faction->xorigin , u->faction->yorigin ));
		scat (".");
		
		/* Ueber die letzte region braucht es keinen Bericht */
		
		m--;
		if (m > 0)
        {
			scat (" Dabei ");
			if (m > 1)
				scat ("wurden ");
			else
				scat ("wurde ");
			
			dh = 0;
			for (i = 0; i != m; i++)
            {
				if (dh)
                {
					if (i == m - 1)
						scat (" und ");
					else
						scat (", ");
                }
				dh = 1;
				
				scat (trailinto[rv[i]->terrain]);
				scat (" ");
				scat (regionid (rv[i], u->faction->xorigin , u->faction->yorigin ));
            }
			scat (" durchquert.");
        }
		
		addmovement (u , buf);
		u->guard = 0;
		leave (r, u);
		translist (&r->units, &r3->units, u);
		
		/* Verfolgungen melden */
		caught_target (r3, u);		
    }
}

int
ship_ready (region * r, unit * u)
{
	if (!u->owner)
    {
		mistakeu (u, "Wir sind nicht der Kapitaen des Schiffes");
		return 0;
    }
	
	if (effskill (u, SK_SAILING) < u->ship->type)
    {
		sprintf (buf, "Der Kapitaen muss mindestens Segeln %d haben, um %s zu befehligen ", u->ship->type, shiptypes[1][u->ship->type]);
		mistakeu (u, buf);
		return 0;
    }
	
	if (u->ship->left)
    {
		mistakeu (u, "Das Schiff ist noch nicht fertig gebaut");
		return 0;
    }
	
	if (!enoughsailors (r, u->ship))
    {
		mistakeu (u, "Auf dem Schiff befinden sich zuwenig erfahrene Seeleute");
		return 0;
    }
	
	if (!cansail (r, u->ship))
    {
		mistakeu (u, "Das Schiff ist zu schwer beladen, um in See zu stechen");
		return 0;
    }
	
	return 1;
}

void
sail (region * starting_point, unit * u, region * next_point)
{
	region *current_point;
	faction *f;
	unit *u2, *u3;
	int k, m, l, i;
	region *rv[MAXSPEED];

	/* u ist der Kapitaen.  */
	if (!ship_ready (starting_point, u)) return;
	
	/* wir suchen so lange nach neuen Richtungen, wie es geht. Diese werden
	dann nacheinander ausgefuehrt. */
	
	/* im array rv[] speichern wir die kuestenregionen ab, durch die wir segeln
	(geht nur bei Halbinseln). */
	
	k = shipspeed[u->ship->type];
	if (u->race == R_MARINER) k++;						// Aquaner sind etwas schneller
	if (u->ship->enchanted == SP_STORM_WINDS) k *= 2;
	if (u->ship->type == SH_BOAT && u->items[I_WINGED_HELMET]) k *= 4;

	assert (k < MAXSPEED); /* zur Sicherheit */
	
	l = 0;
	m = 0;
	current_point = starting_point;
	
	/* Die naechste Region, in die man segelt, wird durch movewhere ()
	aus der letzten Region bestimmt.
	
     Anfangen tun wir bei starting_point.  next_point ist beim ersten
     Durchlauf schon gesetzt (Parameter!).  l zaehlt gesichtete
     Kuestenstreifen, rv[] speichert die gesichteten Kuestenstreifen,
     m zaehlt befahrene Felder, current_point ist die letzte gueltige,
	befahrene Region. */
	
	do
    {
		
		/* Man kann nur vom Ozean in den Hafen, oder von Hafen zum
		Ozean, oder von Ozean zu Ozean fahren - aber nicht von Hafen
		zu Hafen.  */
		if (mainterrain(current_point) != T_OCEAN && mainterrain(next_point) != T_OCEAN)
        {
			sprintf (buf, "Das Schiff %s entdeckt, dass (%d,%d) Festland ist.",shipid (u->ship), next_point->x, next_point->y);
			addwarning (u , buf);
			break;
        }
		
		if (mainterrain(next_point) == T_LAVASTROM)
		{
			sprintf (buf, "Das Schiff %s entdeckt, dass (%d,%d) ein Lavastrom ist.",shipid (u->ship), next_point->x, next_point->y);
			addwarning (u , buf);
			break;
		}

		/* Falls Blockade, endet die Seglerei hier.  */
		if (current_point->blocked)
        {
			sprintf (buf, "Das Schiff %s konnte (%d,%d) nicht verlassen.",shipid (u->ship), current_point->x, current_point->y);
			addwarning (u , buf);
			break;
        }

		if (current_point->terrain == T_LAVASTROM)
		{
			sprintf (buf, "Der Kapitaen des Schiffes %s will den Lavastrom (%d,%d) nicht besegeln.",shipid(u->ship), current_point->x-u->faction->xorigin, current_point->y-u->faction->yorigin);
			addwarning (u,buf);
			break;
		}
		
		/* Verfolgung erfolgreich? */
		if (present (current_point, u->target)) break;
		
		/* aus Taktischen Gr�nden d�rfen nur Ebenen und W�lder mit einem Boot angefahren werden */
		if (u->ship->type != SH_BOAT)
		{
			if ((mainterrain(current_point) == T_OCEAN) && ((next_point->terrain != T_PLAIN) && (mainterrain(next_point) != T_OCEAN)))
			{
				int gotit = 0;	// wenn 1, dann kann angelegt werden
				unit * hu;

				for(hu = next_point->units ; hu; hu = hu->next) // alle Units abklappern und schauen welche in einem Geb�ude sitzt
				{
					if (hu->building && isallied(u, hu))	// ist die Einheit in einem Geb�ude? Wenn ja, welches?
					{
						if ((hu->building->typ == BT_STEG)     && (imGebaeude(hu,next_point) && (hu->building->funktion)) && (u->ship->type == SH_LONGBOAT)) gotit = 1;
						if ((hu->building->typ == BT_HAFEN)    && (imGebaeude(hu,next_point) && (hu->building->funktion)) && ((u->ship->type == SH_LONGBOAT) || (u->ship->type == SH_DRAGONSHIP))) gotit = 1;
						if ((hu->building->typ == BT_SEEHAFEN) && (imGebaeude(hu,next_point) && (hu->building->funktion)) /* hier k�nnen alle Schiffe anlegen */ ) gotit = 1;
					}
				}

				if (!gotit)
				{
					sprintf(buf,"%s kann nur in Ebenen und W�lder anlegen.",u->ship->name);
					addwarning(u ,buf);
					next_point = current_point;
					break;
				}
			}
		}
			
		// alle Regionen aufnehmen, auch dem Startpunkt
		rv[l] = current_point;
		l++;

		/* Falls kein Problem, eines weiter ziehen */
		current_point = next_point;
		next_point = movewhere (current_point, u);
		m++;
    }
	while (next_point != 0 && m < k);
	
	/* Nun enthaelt current_point die Region, in der das Schiff seine
	Runde beendet hat.  Wir generieren hier ein Ereignis fuer den
	Spieler, das ihm sagt, bis wohin er gesegelt ist, falls er
	ueberhaupt vom Fleck gekommen ist.  Das ist nicht der Fall, wenn
	er von der Kueste ins Inland zu segeln versuchte.  */
	if (starting_point != current_point)
    {
		sprintf (buf, "Das Schiff %s segelt", shipid (u->ship));
		scat (" von ");
		scat (regionid (starting_point, u->faction->xorigin , u->faction->yorigin ));
		scat (" nach ");
		scat (regionid (current_point, u->faction->xorigin , u->faction->yorigin ));
		u->skills[SK_SAILING] += u->number * PRODUCEEXP;
		
		l--; // die letzte Region kann ignoriert werden, sie wird eh aufgelistet
		
		if (l > 0)
        {
			for (i = 0; i<MAXSPEED; i++) u->ship->rv[i] = NULL;
			for (i = 0; i != l; i++) u->ship->rv[i] = rv[i];
        }
		
		for (f = factions; f; f = f->next) f->dh = 0;
		
			/* Das Schiff und alle Einheiten darin werden nun von
		starting_point nach current_point verschoben.  */
		translist (&starting_point->ships, &current_point->ships, u->ship);
		for (u2 = starting_point->units; u2;)
        {
			u3 = u2->next;
			if (u2->ship == u->ship)
            {
				translist (&starting_point->units, &current_point->units, u2);
				u2->thisorder[0] = 0;
				u2->faction->dh = 1;
				u2->guard = 0;
            }
			u2 = u3;
        }
		
		/* Meldung ueber Bewegungen.  */
		for (f = factions; f; f = f->next)
			if (f->dh)
				// addmovement (f, buf);
				addmovement(u,buf);
			
			/* Verfolgungen melden.  */
			caught_target (current_point, u);

	}
}

/* Segeln, Wandern, Reiten */

void
move (region * r, unit * u)
{
	region *r2;
	unit *tu;

	// da Schiffbauer in einer Werft und einem Schiff sitzen k�nnen m�ssen sie hier das
	// Schiff verlassen, es sei denn sie sind Owner, dann haben sie das Kommando gegeben
	// und k�nnen das Schiff auch nicht verlassen, hier m�ssen sie den Befehl selber geben
	for(tu = r->units; tu; tu = tu->next)
	{
		if ((tu->owner) && (tu->ship)) break;			// Kapit�ne verlassen keine Schiffe, aber daf�r Geb�ude
		if ((tu->ship) && (tu->building)) leave(r, tu);	// Einheit verl�sst Schiff
	}


	r2 = movewhere (r, u);
	
	if (!r2)
		mistakeu (u, "Die Richtung wurde nicht erkannt");
	else if (u->ship)
		sail (r, u, r2);
	else
		travel (r, u, r2);
	
		/* nachdem alle Richtungen abgearbeitet wurden, und alle Einheiten
		transferiert wurden, kann der aktuelle Befehl geloescht werden -
		sonst bewegt sich die Einheit in der neuen Region nochmals, wenn
	die neue Region erst nachher abgearbeitet wird. */
	
	u->thisorder[0] = 0;
}

/* Bewegung, Verfolgung */

void
movement (void)
{
	
	region *r;
	unit *u, *u2;
	
	puts ("- Bewegungen zu Land und zur See...");
	
	for (r = regions; r; r = r->next)
    {
	/* Ziele von Verfolgungen setzen. Dies muss geschehen, bevor sich die
		Ziele aus der Region absetzen. */
		for (u = r->units; u; u = u->next)
			if (igetkeyword (u->thisorder) == K_FOLLOW)
			{
				u2 = getunit (r, u);
				if (!u2)
				{
					mistakeu (u, "Die Einheit wurde nicht gefunden");
					continue;
				}
				if (igetkeyword (u2->thisorder) != K_MOVE)
				{
					mistakeu (u, "Die Einheit bewegte sich nicht");
					continue;
				}
				u->target = u2;
				mstrcpy (&u->thisorder, u2->thisorder);
			}
			
			/* Bewegungen: Zuerst muessen sich alle Einheiten ohne u->target
			bewegen (NACH), dann starten die Verfolger mit u->target
			(FOLGE) und hoehren erst auf, wenn sie ihr Ziel erreicht
			haben.  Wir verwenden hier *nicht* die Konstruktion:
			
			 u2 = u->next;
			 if (!u->target && igetkeyword (u->thisorder) == K_MOVE)
			 ...
			 u = u2;
			 
			  Dies bringt Probleme mit sich, wenn u ein Schiff
			  kommandiert, und u2 auch auf diesem Schiff reist.  Dann wird
			  naemlich u2 auf 0 oder auf sonst eine seltsame Einheit
			  gesetzt!  Deswegen nehmen wir lieber ein wenig Muehe auf uns
			  und fangen den ganzen Loop wieder von vorne an.  Diejenigen,
			  die sich bewegt haben, werden u->thisorder[0]=0 bekommen
			  haben, so dass diese Schlaufe keine Endlosschlaufe sein
			wird!  */
			for (u = r->units; u;)
			{
				if (!u->target && igetkeyword (u->thisorder) == K_MOVE)
				{
					move (r, u);
					u = r->units;
				}
				else 
					u = u->next;
			}
			
			/* Verfolger starten */
			for (u = r->units; u;)
			{
				if (u->target && igetkeyword (u->thisorder) == K_MOVE)
				{
					move (r, u);
					u = r->units;
				}
				else 
					u = u->next;
			}
			
			/* Herrenlose Schiffe */
			drifting_ships (r);
			/* Einheiten ohne Schiff und Magie auf hoher See. */
			drowning_men (r);
    }
}

/* ------------------------------------------------------------- */
