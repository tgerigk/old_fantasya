/* German Atlantis PB(E)M host Copyright (C) 1995-1998   Alexander Schroeder

   based on:

   Atlantis v1.0  13 September 1993 Copyright 1993 by Russell Wallace

   This program may be freely used, modified and distributed.  It may
   not be sold or used commercially without prior written permission
   from the author.  */

/* Definitions for all strings that players have contact with for the
   atlantis host. Since a lot of data types are thus defined here,
   associated information is also defined here. */

#include "atlantis.h"

char base36[36] =
{
  '0',	//  0
  '1',	//  1
  '2',	//  2
  '3',	//  3
  '4',	//  4
  '5',	//  5
  '6',	//  6
  '7',	//  7
  '8',	//  8
  '9',	//  9
  'a',	// 10
  'b',	// 11
  'c',	// 12
  'd',	// 13
  'e',	// 14
  'f',	// 15
  'g',	// 16
  'h',	// 17
  'i',	// 18
  'j',	// 19
  'k',	// 20
  'L',	// 21
  'm',	// 22
  'n',	// 23
  'o',	// 24
  'p',	// 25
  'q',	// 26
  'r',	// 27
  's',	// 28
  't',	// 29
  'u',	// 30
  'v',	// 31
  'w',	// 32
  'x',	// 33
  'y',	// 34
  'z',	// 35
};

/* Added by Bjoern Becker , 23.07.2000 */
/* Gibt denn waffenschaden an. 1. Anzahl der W�rfel , 2. Augenzahl des W�rfels , 3. Bonus */

int weapondamage [MAXWEAPONS] [3] =
{
  { 1 , 4 , 0 },
  { 2 , 4 , 0 },
  { 1 , 6 , 0 },
  { 3 , 4 , 0 },
  { 2 , 4 , 0 },
  { 5 , 10 , 10 },
  { 2 , 4 , 4 },
};

int racehp [MAXRACES] =
{
  0,		// Illusionen
  0,		// Menschen
  0,		// Zwerge
  0,		// Elfen
  0,		// Orks
  0,		// Trolle
  0,		// Halblinge
  0,		// Meermenschen
  0,		// Echsenmenschen
  0,		// Zentauren
};

/* End of changes */

/* Added by Bjoern Becker , 27.07.2000 */

char *racenames [MAXRACES] [2] =
{
  {"Monster","Monster" },
  {"Mensch","Menschen" },
  {"Zwerg","Zwerge" },
  {"Elf","Elfen" },
  {"Ork","Orks" },
  {"Troll","Trolle" },
  {"Halbling","Halblinge" },
  {"Aquaner","Aquaner" },
  {"Echsenmensch","Echsenmenschen" },
  {"Zentaur", "Zentauren" },
};

/*
 * Hilfeoptionen
 */

char *hilfe_optionen[HILFE_MAX] =
{
	"Silber",
	"Kaempfen",
	"Wahrnehmung",			// evt.
	"Gib"
	"Bewachen",
	"Parteitarnung",
	"Alles",
};

/*
 * skilloffset muss sp�ter noch ge�ndert werden, diese Tabelle
 * ist SEHR un�bersichtlich
 */

int skilloffset [MAXRACES] [MAXSKILLS] =
{
	{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
	{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
	{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
	{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
	{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
	{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
	{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
	{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
	{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
	{ 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, },
};

int produktionsmatrix[MAX_BURGTYPE][MAX_PM] =
{
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
};

/* End of changes */

char *keywords[MAXKEYWORDS] =
{
  "ADRESSE",
  "FAULENZEN",
  "ATTACKIEREN",
  "BENENNEN",
  "BEKLAUEN",
  "BELAGEREN",
  "BESCHREIBEN",
  "BETRETEN",
  "BEWACHEN",
  "BOTSCHAFT",
  "ENDE",
  "FINDEN",
  "FOLGEN",
  "FORSCHEN",
  "GIB",
  "HELFEN",
  "KAEMPFEN",
  "KAMPFZAUBER",
  "KAUFEN",
  "KONTAKTIEREN",
  "LEHREN",
  "LERNEN",
  "LIEFEREN",
  "MACHEN",
  "NACH",
  "PASSWORT",
  "REKRUTIEREN",
  "SAMMELN",
  "OPTION",				// war mal SENDE
  "STIRB",
  "TREIBEN",
  "UNTERHALTEN",
  "VERKAUFEN",
  "VERLASSEN",
  "ZAUBEREN",
  "ZEIGEN",
  "ZERSTOEREN",
  "//",					/* Added by Bjoern Becker , 20.07.2000 */
  "URSPRUNG",
  "REGION",
  "NAECHSTER",
  "TARNE",				// Fantasya
  "PRAEFIX",
  "BENUTZEN",
  "SPIONIEREN",
  "NUMMER",
  "WEBSITE",
  "PREDIGEN",
  "SORTIEREN",
  "SET",				// Cheatkommando f�r freigeschaltete Spieler
};

char *parameters[MAXPARAMS] =
{
  "ALLES",
  "BAUERN",
  "BEUTE",
  "BURG",
  "EINHEIT",
  "HINTEN",
  "KOMMANDO",
  "MANN",
  "NICHT",
  "NAECHSTER",
  "PARTEI",
  "PERSONEN",
  "REGION",
  "SCHIFF",
  "SILBER",
  "STRASSEN",
  "TEMP",
  "UND",
  "ZAUBERBUCH",
  "LOCALE",
  "DE",
  "RASSE",
  "NUMMER",
  "ERESSEA",
  "GEBAEUDE",
  "HOEHLE",
  "N�CHSTER",
  "HEIMAT",
  "KINDER",
  "ITEM",
  "SKILL",
  "RACE",
  "BEAST",
  "FANTASYA",
};

char *options[MAXOPTIONS] =
{
  "AUSWERTUNG",		// 0
  "COMPUTER",		// 1
  "ZEITUNG",		// 2
  "KOMMENTAR",		// 3
  "STATISTIK",		// 4
  "xDEBUG",			// 5 - Sch�tzt vor Missbrauch, aber nicht wirklich
  "ZIP",			// 6
  "RAR",			// 7
  "BZ2",			// 8
  "TGZ",			// 9
  "TXT",			// 10
  "VERSCHIEDENES",	// 11
  "EINKOMMEN",		// 12
  "HANDEL",			// 13
  "PRODUKTION",		// 14
  "BEWEGUNGEN",		// 15
  "KOLUMBUS",		// 16 ... nur so pauschal mal ... weil mir halt danach ist
};

char *skillnames[MAXSKILLS] =
{
  "Armbrustschiessen",
  "Bogenschiessen",
  "Katapultbedienung",
  "Hiebwaffen",
  "Speerkampf",
  "Reiten",
  "Taktik",
  "Bergbau",
  "Burgenbau",
  "Handeln",
  "Holzfaellen",
  "Magie",
  "Pferdedressur",
  "Ruestungsbau",
  "Schiffbau",
  "Segeln",
  "Steinbau",
  "Strassenbau",
  "Tarnung",
  "Unterhaltung",
  "Waffenbau",
  "Wagenbau",
  "Wahrnehmung",
  "Steuereintreiben",
  "Bogenbau",
  "Spionage",
  "Alchemie",
  "Kraeuterkunde",
  "Ausdauer",
  "Religion",
};

int typenames[2][MAXTYPES] =
{
  {
    ST_PERSON,
    ST_UNDEAD,
    ST_ILLUSION,
    ST_FIREDRAKE,
    ST_DRAGON,
    ST_WYRM,
    ST_PERSON,  /* Wachen sehen immer aus wie Personen.  */
  },
  {
    ST_PERSONS,
    ST_UNDEADS,
    ST_ILLUSIONS,
    ST_FIREDRAKES,
    ST_DRAGONS,
    ST_WYRMS,
    ST_PERSONS,  /* Wachen sehen immer aus wie Personen.  */
  }
};

int income[MAXTYPES] =		// Wovon ??
{
  20,			// Personen
  20,			// Untote
  0,			// Illusionen
  150,			// Feuerdrachen
  1000,			// Drachen
  5000,			// Wyrme
  15,			// Personen
};

/* Changed by Bjoern Becker , 26.07.2000 */
/* Mogel, irchen'd wann Winter 2oo2/2oo3 */

char *directions[MAXDIRECTIONS_S] = /* enth�lt jetzt 6 weitere Richtungen, also die Abk�rzungen */
{
  "Nordosten",
  "Nordwesten",
  "Osten",
  "Suedosten",
  "Suedwesten",
  "Westen",
  "NO",
  "NW",
  "O",
  "SO",
  "SW",
  "W",
};

char back[MAXDIRECTIONS] =
{
  D_SOUTHWEST,
  D_SOUTHEAST,
  D_WEST,
  D_NORTHWEST,
  D_NORTHEAST,
  D_EAST,
};

char delta_x[MAXDIRECTIONS] =
{
 0,
 -1,
 1,
 1,
 0,
 -1,
};

char delta_y[MAXDIRECTIONS] =
{
  1,
  1,
  0,
  -1,
  -1,
  0,
};

/* End of changes */

int terrainnames[MAXTERRAINS_NEW] =
{
  ST_OCEAN,
  ST_PLAIN,
  ST_FOREST,
  ST_SWAMP,
  ST_DESERT,
  ST_HIGHLAND,
  ST_MOUNTAIN,
  ST_GLACIER,
  ST_LAVASTROM,				// hier beginnt die Unterwelt
  ST_OEDLAND,
  ST_TROCKENWALD,
  ST_MOOR,
  ST_SANDSTROM,
  ST_GEROELLEBENE,
  ST_VULKAN,
  ST_AKTIVER_VULKAN,
  ST_EIS,
};

char terrainsymbols[MAXTERRAINS_NEW] = ".+WSTHBG,OTMSGVAE";

char *roadinto[MAXTERRAINS_NEW] =
{
  "in den",
  "in die Ebene von",
  "in den Wald von",
  "in den Sumpf von",
  "durch die W�ste von",
  "auf das Hochland von",
  "in die Berge von",
  "auf den Gletscher von",
  "in den Lavastrom",
  "in das �dland von",
  "in den Trockenwald von",
  "in das Moor von",
  "durch den Sandstrom von",
  "auf die Ger�llebene von",
  "in den Vulkan von",
  "auf den aktiven Vulkan von",
  "Eis",
};

char *trailinto[MAXTERRAINS_NEW] =
{
  "Ozean",
  "die Ebene von",
  "der Wald von",
  "der Sumpf von",
  "die W�ste von",
  "das Hochland von",
  "die Berge von",
  "der Gletscher von",
  "Lavastrom",
  "das �dland von",
  "der Trockenwald von",
  "das Moor von",
  "der Sandstrom von",
  "die Ger�llebene von",
  "den Vulkan von",
  "den aktiven Vulkan von",
  "Eis",
};

int production[MAXTERRAINS_NEW] =		// vermutlich max. Anzahl von B�umen in den Regionen
{
  0,
  1000,
  1000,
  200,
  50,
  400,
  100,
  10,
  0,		// Lava						// eq. Ozean, kann aber nicht �berquert werden
  500,		// �dland					// eq. Ebene mit Bauern (werden hier aber nicht vertrieben)
  200,		// Trockenwald				// sind zwar B�ume drauf, wachsen aber nicht nach (evt. spezielle B�ume mit Wachstum!)
  0,		// Moor						// 66% Chanche generell zu sterben, aber Personenweise
  10,		// Sandstrom				// 66% Chanche nach Rechts bzw. Links abzudriften (je 33%)
  100,		// Ger�llebene				// 50% Chanche nicht weiter zu kommen
  5,		// Vulkan					// like Berg, aber hier nur Gold
  0,		// aktiver Vulkan
  0,		// Eis
};

int roadreq[MAXTERRAINS_NEW] =			// ben�tigte Steine pro Region
{
  0,
  50,		// Ebene
  100,		// Wald
  200,		// Sumpf
  150,		// W�ste
  100,		// Hochland
  250,		// Berge
  350,		// Gletscher
  0,		// Lava
  100,		// �dland
  100,		// Trockenwald
  400,		// Moor
  300,		// Sandstrom
  150,		// Ger�llebene
  250,		// Vulkan
  0,		// aktiver Vulkan
  0,		// Eis
};

char *shiptypes[2][MAXSHIPS] =
{
  {
    "Boot",
    "Langboot",
    "Drachenschiff",
    "Karavelle",
    "Trireme",
  },
  {"ein Boot",
   "ein Langboot",
   "ein Drachenschiff",
   "eine Karavelle",
   "eine Trireme",
  }
};

int sailors[MAXSHIPS] =
{
  2,
  10,
  50,
  30,
  120,
};

int shipcapacity[MAXSHIPS] =
{
  50,
  500,
  1000,
  3000,
  2000,
};

int shipcost[MAXSHIPS] =			// Holz pro Schiff
{
  5,
  50,
  100,
  250,
  200,
};

char shipspeed[MAXSHIPS] =
{
  2,
  4,
  5,
  6,
  8,
};

char *buildingnames[MAXBUILDINGS] =
{
  "Baustelle",
  "Befestigung",
  "Turm",
  "Schloss",
  "Festung",
  "Zitadelle",
};

/* Die Groesse gibt auch an, welches Talent man mindestens haben muss,
   um an der Burg weiterzuarbeiten (bei einer Burg zB. 3), sowie den
   Bonus-multiplikator fuer K_WORK! */

int buildingcapacity[MAXBUILDINGS] =
{
  0,
  2,
  10,
  50,
  250,
  1250,
};

int itemnames[2][MAXITEMS] =	// wird hier was eingef�gt, so muss es weiter unten auch ge�ndert werden!
{
  {
    ST_SILVER,
    ST_IRON,
    ST_WOOD,
    ST_STONE,
	ST_GOLD,
    ST_HORSE,
	ST_PEGASUS,
	ST_EINHORN,
	ST_FLUGDRACHE,
    ST_WAGON,
    ST_CATAPULT,
    ST_SWORD,
    ST_SPEAR,
    ST_CROSSBOW,
    ST_LONGBOW,
    ST_CHAIN_MAIL,
    ST_PLATE_ARMOR,
    ST_BALM,
    ST_SPICE,
    ST_JEWELRY,
    ST_MYRRH,
    ST_OIL,
    ST_SILK,
    ST_INCENSE,
    ST_I1,
    ST_I2,
    ST_AMULET_OF_HEALING,
    ST_AMULET_OF_TRUE_SEEING,
    ST_CLOAK_OF_INVULNERABILITY,
    ST_RING_OF_INVISIBILITY,
    ST_RING_OF_POWER,
    ST_RUNESWORD,
    ST_SHIELDSTONE,
    ST_WINGED_HELMET,
    ST_DRAGON_PLATE,
    ST_I4,
  },
  {
    ST_SILVERS,
    ST_IRONS,
    ST_WOODS,
    ST_STONES,
	ST_GOLD,
    ST_HORSES,
	ST_PEGASUS,
	ST_EINHORN,
	ST_FLUGDRACHEN,
    ST_WAGONS,
    ST_CATAPULTS,
    ST_SWORDS,
    ST_SPEARS,
    ST_CROSSBOWS,
    ST_LONGBOWS,
    ST_CHAIN_MAILS,
    ST_PLATE_ARMORS,
    ST_BALMS,
    ST_SPICES,
    ST_JEWELRIES,
    ST_MYRRHS,
    ST_OILS,
    ST_SILKS,
    ST_INCENSES,
    ST_I1S,
    ST_I2S,
    ST_AMULETS_OF_HEALING,
    ST_AMULETS_OF_TRUE_SEEING,
    ST_CLOAKS_OF_INVULNERABILITY,
    ST_RINGS_OF_INVISIBILITY,
    ST_RINGS_OF_POWER,
    ST_RUNESWORDS,
    ST_SHIELDSTONES,
    ST_WINGED_HELMETS,
    ST_DRAGON_PLATES,
    ST_I4S,
  }
};

int itemskill[LASTPRODUCT] =
{
  -1,				// Silber
  SK_MINING,		// Eisen
  SK_LUMBERJACK,	// Holz
  SK_QUARRYING,		// Stein
  SK_MINING,		// Gold
  SK_HORSE_TRAINING,// Pferd
  -1,				// Pegasus
  -1,				// Einhorn
  SK_HORSE_TRAINING,// Flugdrachen
  SK_CARTMAKER,		// Wagen
  SK_CARTMAKER,		// Katapult
  SK_WEAPONSMITH,	// Schwert
  SK_WEAPONSMITH,	// Speer
  SK_FLETCHER,		// Armbrust
  SK_FLETCHER,		// Bogen
  SK_ARMORER,		// Kettenhemd
  SK_ARMORER,		// Plattenpanzer
};

double itemweight[LASTLUXURY] =			// Gewicht der Produkte
{
  0.01,				// Silber
  5,				// Eisen
  5,				// Holz
  60,				// Stein
  15,				// Gold
  50,				// Pferd
  50,				// Pegasus
  50,				// Einhorn
  90,				// Flugdrachen
  40,				// Wagen
  60,				// Katapult
  1,				// Schwert
  1,				// Speer
  1,				// Armbrust
  1,				// Bogen
  2,				// Kettenhemd
  4,				// Plattenpanzer
  1,				// Balsam
  1,				// Gew�rz
  1,				// Juwelen
  1,				// Mhyrre
  1,				// �l
  1,				// Silks
  1,				// Incenses
};

char itemquality[LASTPRODUCT] =			// Mindesttalent zur Produktion ?
{
  0,				// Silber
  1,				// Eisen
  1,				// Holz
  1,				// Stein
  1,				// Gold
  1,				// Pferd
  0,				// Pegasus
  0,				// Einhorn
  4,				// Flugdrachen
  1,				// Wagen
  3,				// Katapult
  1,				// Schwert
  2,				// Speer
  2,				// Armbrust
  3,				// Bogen
  1,				// Kettenhemd
  3,				// Plattenpanzer
};

int rawmaterial[LASTPRODUCT] =			// Ausgangsmaterial ?
{
  0,				// Silber
  0,				// Eisen
  0,				// Holz
  0,				// Stein
  0,				// Gold
  0,				// Pferd
  0,				// Pegasus
  0,				// Einhorn
  0,				// Flugdrachen
  I_WOOD,			// Wagen
  I_WOOD,			// Katapult
  I_IRON,			// Schwert
  I_WOOD,			// Speer
  I_WOOD,			// Armbrust
  I_WOOD,			// Bogen
  I_IRON,			// Kettenhemd
  I_IRON,			// Plattenpanzer
};

char rawquantity[LASTPRODUCT] =
{
  0,				// Silber
  0,				// Eisen
  0,				// Holz
  0,				// Stein
  0,				// Gold
  0,				// Pferd
  0,				// Pegasus
  0,				// Einhorn
  0,				// Flugdrachen
  5,				// Wagen
  10,				// Katapult
  1,				// Schwert
  1,				// Speer
  1,				// Armbrust
  1,				// Bogen
  3,				// Kettenhemd
  5,				// Plattenpanzer
};

int itemprice[MAXLUXURIES] =
{
  4,
  5,
  7,
  5,
  3,
  6,
  4,
};

int spellnames[MAXSPELLS] =
{
    ST_BLACK_WIND,	//4
    ST_CAUSE_FEAR,	// 2
    ST_RUST,		// 1
    ST_DAZZLING_LIGHT,	// 1
    ST_FIREBALL,	// 2
    ST_HAND_OF_DEATH, // 3
    ST_HEAL, // 2
    ST_INSPIRE_COURAGE, // 2
    ST_LIGHTNING_BOLT, // 1
    ST_GOLEM_SERVICE, // 1
    ST_CLAWS_OF_THE_DEEP, // 1
    ST_MAKE_AMULET_OF_HEALING, // 3
    ST_MAKE_AMULET_OF_TRUE_SEEING, // 3
    ST_MAKE_CLOAK_OF_INVULNERABILITY, // 3
    ST_MAKE_RING_OF_INVISIBILITY, // 3
    ST_MAKE_RING_OF_POWER, // 4
    ST_MAKE_RUNESWORD, // 3
    ST_MAKE_SHIELDSTONE, // 4
    ST_REMOTE_MAP, // 3
    ST_PLAGUE, // 2
    ST_DREAM_MAP, // 2
    ST_SHIELD, // 3
    ST_SUNFIRE, // 5
    ST_TELEPORT, // 3
    ST_MAP, // 1
    ST_INFERNO, // 5
    ST_HOLY_WOOD, // 1
    ST_TREMMOR, // 2
    ST_SUMMON_UNDEAD, // 2
    ST_CONJURE_KNIGHTS, // 1
    ST_STORM_WINDS, // 1
    ST_FOG_WEBS, // 2
    ST_NIGHT_EYES, // 1
    ST_WATER_WALKING, // 2
    ST_MAKE_WINGED_HELMET, // 2
    ST_HAMMER, // 4
    ST_PROVOCATION, // 4
    ST_BLESSING, // 5
    ST_11,
    ST_12,
    ST_13,
    ST_14,
    ST_15,
    ST_16,
    ST_17,
};

int spellitem[MAXSPELLS] = 
{
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  I_AMULET_OF_HEALING,
  I_AMULET_OF_TRUE_SEEING,
  I_CLOAK_OF_INVULNERABILITY,
  I_RING_OF_INVISIBILITY,
  I_RING_OF_POWER,
  I_RUNESWORD,
  I_SHIELDSTONE,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  I_WINGED_HELMET,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
  -1,
};

char spelllevel[MAXSPELLS] =
{
  4,
  2,
  1,
  1,
  2,
  3,
  2,
  2,
  1,
  1,
  1,
  3,
  3,
  3,
  3,
  4,
  3,
  4,
  3,
  2,
  2,
  3,
  5,
  3,
  1,
  5,
  1,
  2,
  2,
  1,
  1,
  2,
  1,
  2,
  2,
  4,
  4,
  5,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
};

char iscombatspell[MAXSPELLS] =
{
  1,
  1,
  0,
  1,
  1,
  1,
  0,
  1,
  1,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  1,
  1,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
};

/* Ich habe Soeren Schwartz garantiert, dass keine Beschreibung laenger als 1500 chars ist.  */

int spelldata[MAXSPELLS] =
{
    ST_BLACK_WIND_DATA,
    ST_CAUSE_FEAR_DATA,
    ST_RUST_DATA,
    ST_DAZZLING_LIGHT_DATA,
    ST_FIREBALL_DATA,
    ST_HAND_OF_DEATH_DATA,
    ST_HEAL_DATA,
    ST_INSPIRE_COURAGE_DATA,
    ST_LIGHTNING_BOLT_DATA,
    ST_GOLEM_SERVICE_DATA,
    ST_CLAWS_OF_THE_DEEP_DATA,
    ST_MAKE_AMULET_OF_HEALING_DATA,
    ST_MAKE_AMULET_OF_TRUE_SEEING_DATA,
    ST_MAKE_CLOAK_OF_INVULNERABILITY_DATA,
    ST_MAKE_RING_OF_INVISIBILITY_DATA,
    ST_MAKE_RING_OF_POWER_DATA,
    ST_MAKE_RUNESWORD_DATA,
    ST_MAKE_SHIELDSTONE_DATA,
    ST_REMOTE_MAP_DATA,
    ST_PLAGUE_DATA,
    ST_DREAM_MAP_DATA,
    ST_SHIELD_DATA,
    ST_SUNFIRE_DATA,
    ST_TELEPORT_DATA,
    ST_MAP_DATA,
    ST_INFERNO_DATA,
    ST_HOLY_WOOD_DATA,
    ST_TREMMOR_DATA,
    ST_SUMMON_UNDEAD_DATA,
    ST_CONJURE_KNIGHTS_DATA,
    ST_STORM_WINDS_DATA,
    ST_FOG_WEBS_DATA,
    ST_NIGHT_EYES_DATA,
    ST_WATER_WALKING_DATA,
    ST_MAKE_WINGED_HELMET_DATA,
    ST_HAMMER_DATA,
    ST_PROVOCATION_DATA,
    ST_BLESSING_DATA,
    ST_11_DATA,
    ST_12_DATA,
    ST_13_DATA,
    ST_14_DATA,
    ST_15_DATA,
    ST_16_DATA,
    ST_17_DATA,
};

char woodsize[MAXWOODS] =				// h�?
{
  0,
  10,
  20,
  60,
  80,
};

int carryingcapacity[MAXTYPES] =		// Transportkapazit�ten der Einheiten (?)
{
  17,			
  22,
  50,
  150,
  450,
  15000,
  17,
};
