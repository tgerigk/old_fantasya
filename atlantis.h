/* German Atlantis PB(E)M host Copyright (C) 1995-1999  Alexander Schroeder

   based on:

   Atlantis v1.0  13 September 1993 Copyright 1993 by Russell Wallace

   This program may be freely used, modified and distributed. It may
   not be sold or used commercially without prior written permission
   from the author.  */

//#define FANTA_LIMITED_EDITION

#ifndef ATLANTIS_H
#define ATLANTIS_H

/* Fuer FILE braucht man hier eigentlich nur stdio.h; da aber andere
   Funktionen auch in fast allen .o files gebraucht werden, werden
   hier die meisten libraries schon eingebunden.  assert.h fuer assert
   (), stdlib.h fuer rand (), string.h fuer strcat (), etc.  */

#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <io.h>

/* Indicator soll einigermassen unabhaengig sein. */
#include "indicato.h"

/* Translation soll unabhaengig sein.  */
#include "translate.h"

/* Zuerst defines, welche gebraucht werden. */

/* Folgende Konstanten werden fuer alte Versionen gebraucht (Vxx).
   Sie werden nur readgame () verwendet werden, um Datenfiles alter
   Versionen richtig lesen zu koennen.  Aendert sich der source code,
   aber nicht der Datenfile (wie so oft), muss nur RELEASE_VERSION
   erhoeht werden.  Alte Versionen koennen periodisch geloescht werden
   (zusammen mit dem code, der diese Konstanten verwendet).  */

#define V20                     20
#define V24                     24
#define V32                     32
#define V40                     40
#define V50                     50
#define V60                     60
/* Added & Modified by Bjoern Becker , 20.07.2000 */
#define V62			62
#define V621			621
#define V63			63
#define V64			64
#define V65			65
#define V66			66
#define V67			67
#define V68			68
#define RELEASE_VERSION         68
/* end of changes */

/* Default fuer die Auswertung, falls kein ZAT angegeben wird. */

#define NOZAT  "Sonntag, 21 Uhr"

/* Fuer das Lesen der Befehle.  */

#define MAXSTRING               1500
#define MAXLINE                 5000
#define SPACE_REPLACEMENT       ("\240"[0])
#define SPACE                   ' '
#define ESCAPE_CHAR             '\\'

/* Laengenbeschraenkungen fuer einzelne Datenfelder.  Oft ist diese
   Beschraenkung nicht wirklich noetig, weil die Text dynamisch
   cmalloced ("checked" malloc) werden.  Es dient sozusagen zur
   Reduktion des Datenfiles...  */

#define DISPLAYSIZE             1000
#define NAMESIZE                80

// Angaben �ber das Startgeld und was sp�teinsteiger pro zug dazu bekommen.

#define STARTMONEY              10000
#define STARTMONEY_BONUS	20

/* Beim wievielten NMR wird die Partei geloescht. */

#define ORDERGAP                4

/* Sprache von Debug-Meldungen ist Deutsch.  Falls dort mehrsprachige
   Dinge wie zB. itemnames vorkommen, benoetigt man den Index der
   Default-Sprache.  Dies ist die erste Sprache im [LANGUAGES]
   Abschnitt in language.def -- Deutsch sollte 0 sein.  Da alle Debug
   Texte sowieso sprachunabhaengig sind, wurde dort auch 0
   hineingeschrieben (aus Faulheit).  */

/* Wieviele Tage lernt man in einem Monat bei der Anwendung eines
   Talentes.  Wieviel Silber braucht ein Wesen pro Monat, um sich zu
   ernaehren.  Wieviele Bauern leben in einer "Parzelle" -- eine
   Region hat 1000 Parzellen, in denen je ein Baum oder ein Pferd oder
   (i.A.) 10 Bauern leben koennen.  Wieviel Grundlohn hat man beim
   ARBEITE Befehl.  Wieviel Bonus gibt es bei hoeheren Burgengroessen.  */

#define PRODUCEEXP              ((rand() % 10) + 5)
#define MAINTENANCE             10
#define MAXPEASANTS_PER_AREA    10
#define WAGE                    11
#define BONUS                   1

/* Maximale Geschwindigkeiten -- die doppelte Geschwindigkeit des
   schnellsten Schiffes, wegen dem Sturmwind-Zauber.  Wird benoetigt
   fuer einen array von Regionen, durch den Schiffe segeln.  Im Moment
   ist das die Bootsgeschwindigkeit mit Sturmwind und Helm der Sieben
   Winde: 3 * 2 * 3 = 18; auch sehr hoch: Trireme mit Sturmwind: 8 * 2
   = 16.  */

#define MAXSPEED                30				/* sehr Gro�z�gig !! */

// import von "x8_fantasya.h"
enum
{ 
	HILFE_SILBER, 
	HILFE_KAEMPFE, 
	HILFE_WAHRNEHMUNG, 
	HILFE_GIB, 
	HILFE_BEWACHE, 
	HILFE_PARTEITARNUNG, 
	HILFE_MAX
};

enum 
{ 
	KR_SUMPFKRAUT,		// Ebene
	KR_BLUETENGRUMPF,	// Wald
	KR_WIRZBLATT,		// Sumpf
	KR_FLACHTUPF,		// W�ste
	KR_TROCKENWURZ,		// Hochland
	KR_GROTTENOLM,		// Berge
	KR_SCHNUPFNIES,		// Gletscher
	MAX_KRAEUTER,
};

enum 
{ 
	TR_WUNDPUDER,
	TR_JUNGFERNBLUT,
	TR_GEHIRNTOT,
	TR_WISSENSSALBE,
	TR_NACHTFIEBER,
	MAX_TRAENKE, 
};

enum
{
	x8_TEXT,			// Textauswertung
	x8_CR,				// Computerreport
	x8_STATISTIK,		// Statistik
	x8_ZUGVORLAGE,		// Zugvorlage
	x8_ZEITUNG,			// Zeitung abonieren
	x8_EMAILS,			// e-Mails an Textauswertung h�ngen
	x8_ZIP,				// wahrscheinlich immer als ZIP verschicken
	x8_VERSCHIEDENES,
	x8_EINKOMMEN,
	x8_HANDEL,
	x8_PRODUKTION,
	x8_BEWEGUNG,
	x8_KOLUMBUS,
	x8_GULRAK,
	x8_CRTRENNUNG,		// erzeugt f�r alle Welten einen CR
	MAX_OPTIONS,
};

extern char *x8_Options[MAX_OPTIONS];

enum	// Geb�udetypen
{
	BT_BURG,
	BT_HOLZFAELLERHUETTE,
	BT_MINE,
	BT_STEINBRUCH,
	BT_SAEGEWERK,
	BT_BERGWERK,
	BT_SCHMIEDE,
	BT_WERKSTATT,
	BT_KUECHE,
	BT_STEG,
	BT_HAFEN,
	BT_SEEHAFEN,
	BT_WERFT,
	BT_MONUMENT,
	BT_RUINE,
	BT_WEGWEISER,
	BT_STEINGRUBE,
	BT_KIRCHE,
	MAX_BURGTYPE,
};

extern char * BuildingNames[MAX_BURGTYPE];

enum	// enth�lt die Spalten der Produktionsmatrix f�r Geb�ude, inkl. Unterhalt pro Runde
{
	PM_HOLZ,
	PM_EISEN,
	PM_STEIN,
	PM_GOLD,
	PM_KOSTEN,		// Baukosten
	PM_TALENT,		// Talent ist immer Burgenbau
	PM_UNTERHALT,	// Unterhalt pro Runde, gilt ab Baubeginn !
	PM_UNTERHALT2,	// Unterhalt zu den Einheiten die drinnen sind
	PM_SIZE,		// Gr��e des Geb�udes, 0 -> unendlich
	PM_BURG,		// welcher Burgtyp wird vorrausgesetzt, hier nur als effsize
	PM_BUILDING,	// welches Geb�ude wird noch ben�tigt !! b->size EQ PM_SIZE !!
	MAX_PM,
};

extern int produktionsmatrix[MAX_BURGTYPE][MAX_PM];

enum
{
	PIR_BERGBAU,		// Bergbauer
	PIR_HOLZFAELLER,	// Holzf�ller
	PIR_TARNUNG,		// Verstecker
	PIR_WAHRNEMUNG,		// Aufpasser
	PIR_TAKTIK,			// Taktiker
	PIR_SEGELN,			// Matrosen
	PIR_PIRAT,			// Krieger auf dem Schiff
	PIR_SCHMIEDE,		// machen Schwerter
	PIR_RUESTUNG,		// machen R�stungen
	PIR_STEUERN,		// Steuereintreiben
	PIR_SCHIFFBAUER,	// Schiffbauer
	PIR_TRANSPORT,		// Transporteure
};

// weiter mit GA
enum
  {
    R_NORACE,	// bzw. Monster
    R_HUMAN,
    R_DWARF,
    R_ELF,
    R_ORK,
    R_TROLL,
    R_HALFLING,
    R_MARINER,
    R_DEMON,
	R_ZENTAUREN,
    MAXRACES,
  };

extern char *racenames [MAXRACES] [2];

enum	// Kartenanzeige im Editor
  {
    M_TERRAIN,
    M_FACTIONS,
    M_UNARMED,
	M_HOEHLE,			// H�hlen
	M_STURM,			// Ansicht der Sturmwahrscheinlichkeiten auf See
	M_EIS,				// Vereisung im Winter
	M_PARTEI,			// anzeige einzelner Parteien
    MAXMODES,
  };

enum
  {
    ST_FIGHT,
    ST_BEHIND,
    ST_AVOID,
  };

enum
  {
    K_ADDRESS,
    K_WORK,
    K_ATTACK,
    K_NAME,
    K_STEAL,
    K_BESIEGE,
    K_DISPLAY,
    K_ENTER,
    K_GUARD,
    K_MAIL,
    K_END,
    K_FIND,
    K_FOLLOW,
    K_RESEARCH,
    K_GIVE,
    K_ALLY,
    K_STATUS,
    K_COMBAT,
    K_BUY,
    K_CONTACT,
    K_TEACH,
    K_STUDY,
    K_DELIVER,
    K_MAKE,
    K_MOVE,
    K_PASSWORD,
    K_RECRUIT,
    K_COLLECT,
    K_SEND,
    K_QUIT,
    K_TAX,
    K_ENTERTAIN,
    K_SELL,
    K_LEAVE,
    K_CAST,
    K_RESHOW,
    K_DESTROY,
    K_COMMENT,				/* Added by Bjoern Becker , 20.07.2000 */
    K_ORIGIN,				/* Added by Bjoern Becker , 31.07.2000 */
    K_REGION,
	K_NAECHSTER,
	K_TARNUNG,
	K_PRAEFIX,
	K_BENUTZEN,
	K_SPIONIEREN,
	K_NUMMER,
	K_WEBSITE,
	K_PREDIGE,
	K_SORTIEREN,
	K_CHEAT,
    MAXKEYWORDS,
  };

extern char *keywords[MAXKEYWORDS];

enum
  {
    P_ALL,
    P_PEASANT,
    P_LOOT,
    P_BUILDING,
    P_UNIT,
    P_BEHIND,
    P_CONTROL,
    P_MAN,
    P_NOT,
    P_NEXT,
    P_FACTION,
    P_PERSON,
    P_REGION,
    P_SHIP,
    P_SILVER,
    P_ROAD,
    P_TEMP,
    P_AND,
    P_SPELLBOOK,
	P_LOCALE,
	P_DE,
	P_RASSE,
	P_NUMMER,
	P_ERESSEA,
	P_GEBAEUDE,
	P_HOEHLE,
	P_NEXT2,				// wird ben�tigt zum einlesen von Befehlsdateien
	P_HEIMAT,				// Auswanderung entgegen
	P_KINDER,				// Vermehrung der Bauern
	P_ITEM,					// Cheater
	P_SKILL,				// Cheater
	P_RACE,					// Cheater
	P_BEAST,				// Cheater
	P_FANTASYA,
    MAXPARAMS,
  };

extern char *parameters[MAXPARAMS];

enum
  {
    O_REPORT,
    O_COMPUTER,
    O_ZINE,
    O_COMMENTS,
    O_STATISTICS,
    O_DEBUG,
    O_ZIP,
    O_RAR,
    O_BZ2,
    O_TGZ,
    O_TXT,
    O_WMISC,
    O_WINCOME,
    O_WCOMMERCE,
    O_WPRODUCTION,
    O_WMOVEMENT,
	O_KOLUMBUS,
    MAXOPTIONS,
  };

extern char *options[MAXOPTIONS];

enum
  {
    SK_CROSSBOW,
    SK_LONGBOW,
    SK_CATAPULT,
    SK_SWORD,
    SK_SPEAR,
    SK_RIDING,
    SK_TACTICS,
    SK_MINING,
    SK_BUILDING,
    SK_TRADE,
    SK_LUMBERJACK,
    SK_MAGIC,
    SK_HORSE_TRAINING,
    SK_ARMORER,
    SK_SHIPBUILDING,
    SK_SAILING,
    SK_QUARRYING,
    SK_ROAD_BUILDING,
    SK_STEALTH,
    SK_ENTERTAINMENT,
    SK_WEAPONSMITH,
    SK_CARTMAKER,
    SK_OBSERVATION,
    SK_TAXING,
    SK_FLETCHER,			// Bogenbau
	SK_SPIONAGE,
	SK_ALCHEMIE,
	SK_KRAEUTERKUNDE,
	SK_AUSDAUER,
	SK_RELIGION,
    MAXSKILLS
    /* Changes here will affect the syntax checker acheck.c! */
  };

extern char *skillnames[MAXSKILLS];
extern int skilloffset [MAXRACES] [MAXSKILLS];  // Added by Bjoern Becker , 28.07.2000

enum
  {
    U_MAN,
    U_UNDEAD,
    U_ILLUSION,
    U_FIREDRAGON,
    U_DRAGON,
    U_WYRM,
    U_GUARDS,
    MAXTYPES,
  };

extern int typenames[2][MAXTYPES];
extern int income[MAXTYPES];

/* Changed by Bjoern Becker , 25.07.2000 */

enum
  {
    D_NORTHEAST,
    D_NORTHWEST,
    D_EAST,
    D_SOUTHEAST,
    D_SOUTHWEST,
    D_WEST,
	MAXDIRECTIONS,
    D_NORTHEAST_S = MAXDIRECTIONS,	// Abk�rzungen
    D_NORTHWEST_S,
    D_EAST_S,
    D_SOUTHEAST_S,
    D_SOUTHWEST_S,
    D_WEST_S,
    MAXDIRECTIONS_S,
  };

/* End of changes */

/* Changed by Bjoern Becker , 26.07.2000 */

extern char back[MAXDIRECTIONS];
extern char delta_x[MAXDIRECTIONS];
extern char delta_y[MAXDIRECTIONS];
extern char *directions[MAXDIRECTIONS_S];

/* End of changes */

/* Sollten sich diese Terrains aendern, muessen gewisse Zaubersprueche auch angepasst werden!  */

enum
{
    T_OCEAN,
    T_PLAIN,
    T_FOREST,                   /* wird zu T_PLAIN konvertiert */
    T_SWAMP,
    T_DESERT,                   /* kann aus T_PLAIN entstehen */
    T_HIGHLAND,
    T_MOUNTAIN,
    T_GLACIER,                  /* kann aus T_MOUNTAIN entstehen */
    MAXTERRAINS,
	T_LAVASTROM = MAXTERRAINS,	// Unterweltregionen
	T_OEDLAND,
	T_TROCKENWALD,
	T_MOOR,
	T_SANDSTROM,
	T_GEROELLEBENE,
	T_VULKAN,
	T_AKTIVER_VULKAN,
	T_EIS,
	MAXTERRAINS_NEW,
};

extern int terrainnames[MAXTERRAINS_NEW];
extern char terrainsymbols[MAXTERRAINS_NEW];
extern int production[MAXTERRAINS_NEW];
extern int roadreq[MAXTERRAINS_NEW];
extern char *roadinto[MAXTERRAINS_NEW];
extern char *trailinto[MAXTERRAINS_NEW];

enum
  {
    SH_BOAT,
    SH_LONGBOAT,
    SH_DRAGONSHIP,
    SH_CARAVELL,
    SH_TRIREME,
    MAXSHIPS,
  };

extern char *shiptypes[2][MAXSHIPS];
extern int sailors[MAXSHIPS];
extern int shipcapacity[MAXSHIPS];
extern int shipcost[MAXSHIPS];
extern char shipspeed[MAXSHIPS];

enum
  {
    B_SITE,
    B_FORTIFICATION,
    B_TOWER,
    B_CASTLE,
    B_FORTRESS,
    B_CITADEL,
    MAXBUILDINGS,
  };

#define STONERECYCLE                    50

extern int buildingcapacity[MAXBUILDINGS];
extern char *buildingnames[MAXBUILDINGS];

/* Added by Bjoern Becker 23.08.2000 */

enum
  {
    W_UNARMED,
    W_SWORD,
    W_SPEAR,
    W_CROSSBOW,
    W_LONGBOW,
    W_CATAPULT,
    W_RUNESWORD,
    MAXWEAPONS,
  };

extern int weapondamage [MAXWEAPONS] [3];
extern int racehp [MAXRACES];
extern unsigned char MaxAlchemie[MAXRACES];
extern int personweight[MAXRACES];
extern int personcapacity[MAXRACES];

/* End of changes */

enum
  {
    I_SILVER,
    I_IRON,			/* start of ressources and products */
    I_WOOD,
    I_STONE,
	I_GOLD,
    I_HORSE,		/* end of ressources */
	I_PEGASUS,
	I_EINHORN,
	I_FLUGDRACHEN,

    I_WAGON,
    I_CATAPULT,
    I_SWORD,
    I_SPEAR,
    I_CROSSBOW,
    I_LONGBOW,
    I_CHAIN_MAIL,
    I_PLATE_ARMOR,	/* end of products */

    I_BALM,		/* start of luxuries */
    I_SPICE,
    I_JEWELRY,
    I_MYRRH,
    I_OIL,
    I_SILK,
    I_INCENSE,		/* end of luxuries */

    I_1,		/* start of magic items */
    I_2,
    I_AMULET_OF_HEALING,
    I_AMULET_OF_TRUE_SEEING,
    I_CLOAK_OF_INVULNERABILITY,
    I_RING_OF_INVISIBILITY,
    I_RING_OF_POWER,
    I_RUNESWORD,
    I_SHIELDSTONE,
    I_WINGED_HELMET,
    I_DRAGON_PLATE,
    I_4,				/* end of magic items */
    MAXITEMS
    /* Changes here will affect the syntax checker acheck.c! */
  };

extern int itemnames[2][MAXITEMS];											// ?? [Einzahl || Mehrzahl][MAXITEMS] ??

#define LASTRESSOURCE   (I_HORSE +1)
#define isressource(i)  (0 <= i && i < LASTRESSOURCE)

#define LASTPRODUCT     (I_PLATE_ARMOR +1)
#define isproduct(i)    (0 <= i && i < LASTPRODUCT)

#define FIRSTLUXURY     (I_BALM)
#define LASTLUXURY      (I_INCENSE +1)
#define MAXLUXURIES     (LASTLUXURY - FIRSTLUXURY)
#define isluxury(i)     (FIRSTLUXURY <= i && i < LASTLUXURY)

#define FIRST_MAGIC_ITEM (I_1)
#define LAST_MAGIC_ITEM (I_4)

extern int itemskill[LASTPRODUCT];
extern double itemweight[LASTLUXURY];
extern char itemquality[LASTPRODUCT];
extern int rawmaterial[LASTPRODUCT];
extern char rawquantity[LASTPRODUCT];
extern int itemprice[MAXLUXURIES];

#define TEACHNUMBER              10
#define STUDYCOST               (100 + max(0, 150 * effskill(u, SK_MAGIC)))
#define RESEARCHCOST			(100 + max(0, 150 * i))

#define MAXMAGICIANS            MaxMagicans[u->faction->race]
#define MAXALCHEMIE             MaxAlchemie[u->faction->race]
#define ITEMCOST                200

#define RELIGIONDIVIDER			5

/* Werden die folgenden defines geaendert, muessen die
   Spruchbeschreibungen auch geaendert werden. */

#define ZAP_SURVIVAL           80

#define NIGHT_EYE_TALENT        5
#define NIGHT_EYES_MAX          3

#define PLAGUE_SURVIVAL        40

#define MAP_RANGE               3
#define DIRECTED_MAP_DISTANCE  40

#define WATERWALK_MAX           4

#define TELEPORT_MAX_WEIGHT    1000

enum
  {
    SP_BLACK_WIND,
    SP_CAUSE_FEAR,
    SP_RUST,
    SP_DAZZLING_LIGHT,
    SP_FIREBALL,
    SP_HAND_OF_DEATH,
    SP_HEAL,
    SP_INSPIRE_COURAGE,
    SP_LIGHTNING_BOLT,
    SP_GOLEM_SERVICE,
    SP_CLAWS_OF_THE_DEEP,
    SP_MAKE_AMULET_OF_HEALING,
    SP_MAKE_AMULET_OF_TRUE_SEEING,
    SP_MAKE_CLOAK_OF_INVULNERABILITY,
    SP_MAKE_RING_OF_INVISIBILITY,
    SP_MAKE_RING_OF_POWER,
    SP_MAKE_RUNESWORD,
    SP_MAKE_SHIELDSTONE,
    SP_REMOTE_MAP,
    SP_PLAGUE,
    SP_DREAM_MAP,
    SP_SHIELD,
    SP_SUNFIRE,
    SP_TELEPORT,
    SP_MAP,
    SP_INFERNO,
    SP_HOLY_WOOD,
    SP_TREMMOR,
    SP_SUMMON_UNDEAD,
    SP_CONJURE_KNIGHTS,
    SP_STORM_WINDS,
    SP_FOG_WEBS,
    SP_NIGHT_EYES,
    SP_WATER_WALKING,
    SP_MAKE_WINGED_HELMET,
    SP_HAMMER,
    SP_PROVOCATION,
    SP_BLESSING,
    SP_11, /* Locate Plane */
    SP_12, /* Map other Plane */
    SP_13, /* Plane-shift */
    SP_14,
    SP_15,
    SP_16,
    SP_17,
    MAXSPELLS,
    /* Changes here will affect the syntax checker acheck.c! */
  };

extern int spellitem[MAXSPELLS];
extern int spellnames[MAXSPELLS];
extern char spelllevel[MAXSPELLS];
extern char iscombatspell[MAXSPELLS];
extern int spelldata[MAXSPELLS];

enum
  {
    W_NONE,
    W_BARELY,
    W_COPSE,
    W_FOREST,
    W_JUNGLE,
    MAXWOODS,
  };

extern char woodsize[MAXWOODS];

#define DEMANDRISE          (25)
#define DEMANDFALL          (150)
#define DEMANDFACTOR        (2500)
#define MAXDEMAND           (10000)
#define MINDEMAND           (100)

//#define PEASANTGROWTH       (0.1)	/* war mal (3) !! Wachstum der Bauern ? */
#define PEASANTMOVE         (5)   /* Normales Wandern in Nachbarregionen */
#define PEASANTGREED        (5)   /* Zusaetzl. zur reichsten Nachbarregion. */

#define STARVATION_SURVIVAL       (3)
#define OVERPOPULATION_FRACTION   (10)

#define HORSEGROWTH         horsegrowth[(turn - 1) % 12]	/* Wachstum der Pferde */
#define HORSEMOVE           (5)

#define FORESTGROWTH        forestgrowth[(turn - 1) % 12]	/* neues Wachstum des Waldes */
#define FORESTSPREAD        (1)

/* Defines for items and movement.  */

/* Die Tragekapaz. ist hardcodiert mit defines, da es bis jetzt
   sowieso nur 2 Objecte gibt, die etwas tragen. Ein Mensch wiegt 10,
   traegt also 5, ein Pferd wiegt 50, traegt also 20.  ein Wagen wird
   von zwei Pferden gezogen und traegt total 140, davon 40 die Pferde,
   macht nur noch 100, aber samt eigenem Gewicht (40) macht also
   140. */

#define PERSONWEIGHT    personweight[u->race]

#define HORSECAPACITY   70
#define WAGONCAPACITY  140
#define DRAGONCAPACITY 100

#define HORSESNEEDED    2

extern int carryingcapacity[MAXTYPES];

/* Verschiedene kleine Funktionen. */

// da MS Visual C++ entsprechnde Funktionen anbietet
#undef		min
#undef		max

#define min(a,b)                ((a) < (b) ? (a) : (b))
#define max(a,b)                ((a) > (b) ? (a) : (b))

#define addptr(p,i)             ((void *)(((char *)p) + i))

#define addlist2(l,p)           (*l = p, l = &p->next)

/* Structs, welche fuer die Datenstruktur wichtig sind. */

typedef struct list
  {
    struct list *next;
  }
list;

typedef struct list2
  {
    struct list2 *next;
    struct list2 *kette;
  }
list2;

struct unit;
typedef struct unit unit;

typedef struct strlist
  {
    struct strlist *next;
	int uno;				// Faulheit, h�ngt mit der Zeile dar�ber und x8_save.c zusammen ... UnitNumber
	int regx,regy,regz;		// seit o.oo.8 an die Region ketten
    char s[1];		// der Text   -   muss immer hinten sein !!!!! hier wird C-Feature/C-Bug ausgenutzt
	//int guardien;			// sollte immer 0 sein
  }
strlist;

typedef struct building
  {
    struct building *next;
    int no;
    char *name;
    char *display;
	char *monument;			// wenn es ein Monument ist, kann man hier den Typ (Pyramide, Tempel) angeben
    char besieged;			// belagert?
    int size;
    int sizeleft;
	int voll;				// ist Person > size
	int typ;				// Burg, Ruine, etc. (default: BT_BURG)
	int funktion;			// wurde Unterhalt bezahlt bzw. ist Funktion m�glich?
	int schiff;				// Nummer des Schiffes an dem gebaut wird, da pro Werft nur 1 Schiff m�glich
	int wegweiser;			// bei Null zerf�llt der Wegweiser
  }
building;

typedef struct ship
  {
    struct ship *next;
    int no;
    char *name;
    char *display;
    char enchanted;
    int type;
    int left;
	struct region *rv[MAXSPEED];		// durch diese Regionen ist das Schiff gesegelt
	int burn;							// Schiff verbrennt auf dem Lavastrom
} ship;

typedef struct region
  {
    struct region *next;				// Standartverkettung der Regionen

	struct region *NextX;				// Verkettungen f�r FindCubus
	struct region *NextY;
	struct region *LastX;
	struct region *LastY;

    int x, y, z;
    char *name;
    char *display;
    struct region *connect[MAXDIRECTIONS];
    int terrain;
    int trees;
    int horses;
    int peasants;
    int money;
    int road[MAXDIRECTIONS];
    int demand[MAXLUXURIES];
    int produced_good;
    char blocked;
    strlist *comments;
    strlist *debug;
    building *buildings;
    ship *ships;
    unit *units;
    int newpeasants;
    int newhorses;
    int newtrees;
	int hoehle;				// Nummer der genauen H�hle
	int gotit;				// muss die Region noch gezeigt werden (0 - ja; 1 - nein; 2 - angrenzende Region)
	int alter;				// Alter der Region (damit Newbies nicht so weit auseinander landen)
	int movement;			// Insel (bzw. Region), die sich bewegt
	int kraut[MAXTERRAINS];
	int mainKraut;			// dieses Kraut w�chst in dieser Region bevorzugt
	int eisen;				// 7o .. 13o Eisen max. pro ZAT
	int steine;				// 7o .. 13o Steine max. pro ZAT
	int gold;				// 2o .. 4o Gold max. pro ZAT
	int sturm;				// Sturmwahrscheinlichkeit
	int eis;				// Vereisung im Winter
	int eisbackup;			// siehe Funktion !!
	double wachstum;			// zus�tzliches Wachstum der Bauern ... abh�ngig von PREDIGEN
	int auswanderung;		// negative Auswanderung der Bauern ... abh�ngig von PREDIGEN
  }
region;

struct faction;

typedef struct rfaction
  {
    struct rfaction *next;
    struct faction *faction;
    int factionno;
	int hilfe_modus[HILFE_MAX];				// welche HELFE sind gesetzt ?
  }
rfaction;

typedef struct runit
  {
    struct runit *next;
    struct unit *unit;
  }
runit;

typedef struct faction
  {
    struct faction *next;
    int no;
    char *name;
    char *addr;
    char *passw;
	char *website;
    int lastorders;
    char newbie;
    int options;
    char seendata[MAXSPELLS];			//   - ? -
    char showdata[MAXSPELLS];			//   - ? -
    rfaction *allies;
    strlist *mistakes;
    strlist *warnings;
    strlist *messages;
    strlist *battles;
    strlist *events;
    strlist *income;
    strlist *commerce;
    strlist *production;
    strlist *movement;
    strlist *debug;
    strlist *status;
	strlist *alchemie;		// Nachrichten f�r Alchemie und Kr�uterkunde
	strlist *echsennews;	// Nachrichten f�r Echsenmenschen Ereignisse
    char alive;
    char attacking;
    char seesbattle;
    char dh;
    int nregions;
    int nunits;
    int number;
    int money;
    int old_value;
    int language;
    int race;		/* Added by Bjoern Becker , 27.07.2000 */
    int xorigin;	// Added by Bjoern Becker , 28.07.2000
    int yorigin;	// Added by Bjoern Becker , 28.07.2000

	int cheatenabled; // o.o1.6 / 3 --- o2.o1.2oo6

	int gotit;		// zum Zwischenspeichern f�r irgendwas
	int alter;		// Newbieschutz f�r K�mpfe, ggf. f�r Sonstiges
	int anzahl;		// Anzahl aller Personen dieser Partei
	int gaeste;		// Anzahl der Personen die eine andere Rasse haben

	int items[MAXITEMS];	// alles was Partei hat
	int einkommen;

  }
faction;

struct unit
  {
    struct unit *next;

	struct unit *kette;		// n�chste Einheit in der Kette
	struct region *r;		// Einheit steht in dieser Region

    int no;
    char *name;
    char *display;
    int number;				// Personen pro Einheit
    int type;
    int effect;
    int enchanted;			// Zauberspr�che f�r mehrere Runden
    faction *faction;
    runit *contacts;
    unit *target;
    building *building;
    ship *ship;
    char owner;
    char status;
    char guard;
    char attacking;
    building *besieging;
    /* Unterschied von thisorder und lastoder macht es moeglich, beim NACH Befehl den alten Default Befehl zu verwenden.  */
    char *thisorder;
    char *lastorder;
    char *thisorder2;        /* Fuer den LIEFERE Befehl.  */
    int combatspell;
    int skills[MAXSKILLS];
    int items[MAXITEMS];
    int spells[MAXSPELLS];
    strlist *orders;			// should be the Vorlage-Kommentare
    int alias;
    int dead;					// Gefallene Soldaten
    int learning;
    int n;						// irgend eine Z�hlung / je nach aktueller Funktion
    int wants;               /* Bei der Produktion: Anzahl der gewuenschten Produkte.  */
    int wants2;              /* Beim Handel:  Anzahl Produkte, die zu kaufen sind.  */
    int wants_recruits;      /* Beim Rekrutieren: Anzahl gewuenschter Rekruten -- da REKRUTIERE ein kurzer Befehl ist.  */
    int *litems;
    int side;
    struct unit *collector;  /* Falls gesetzt erhaelt diese Einheit alle Beute nach dem Kampf.  */
    strlist *comments;			/* Added by Bjoern Becker , 20.07.2000 */
    strlist *commentsTab;		/* Added by Matthias Strunk , 9.10.2000 */
    int race;				
	int raceTT;					// Tarntypus
	char *prefix;				// ein Pr�fix
	int tarn_no;				// als welche Parteinummer (!) tarnen, Name der Partei aus findfaction() [sofern sie existiert!]
								// wenn dies Null ist, ist die Parteitarnung aktiv
	int tarnung;				// ist Einheit getarnt?
	int traenke[MAX_TRAENKE];	// alle Tr�nke & Tinkturen
	int kraeuter[MAX_KRAEUTER];	// Kr�uterbeutel
	int lebenspunkte;			// Anzahl der Trefferpunkte die bereits _verbraucht_ sind, ist leichter mit der Ausdauer zu berechnen
	int gotit;
	int hoehle;					// bereits eine Hoehle betreten

	// ................ Wirkungen von Tr�nken �ber mehrere Runden
	int	jungfernblut;			// Angabe erfolgt in 1oo Bauern, davon verwendet jeder Echsenmensch 1 (!) Bauern, dadurch wirkt
								// der Trank f�r einen einzelnen �ber 1oo Runden
	int wissenssalbe;			// wirkt f�r 1o Personen bei 3o Lerntagen pro Person, macht das 3oo Lernpunkte, werden einzeln abgezogen
	int gehirntot;				// gilt das selbe wie f�r Wissenssalbe
	// Produktion von Rohstoffen, n�tig wegen Bergwerk und so, da es sich mit alter Technik nich vertr�gt
	int produkt;				// das soll gemacht werden, jede Einheit kann eh nur ein Produkt machen
	int effekt;					// Effekte vom Bergwerk bzw. S�gewerk oder so

	// 3 Debug Variablen
	int action;		// Einheit hat Funktion durchlaufen
	int bevor;		// Wert vor der Funktion
	int after;		// Wert nach der Funktion
	int einkommen;	// das hat die Einheit diese Runde durch Unterhalten oder Steuern verdient
  };

typedef struct order
  {
    struct order *next;
    unit *unit;
    int qty;
    int type;
  }
order;



/* globale Variablen */

extern int turn;
extern int highest_unit_no;
extern char buf[MAXLINE];
extern char zat[NAMESIZE];
extern FILE *F;
extern int memorytotal;

extern region *regions;
extern faction *factions;
extern unit *units;

/* Funktionen fuer Speicher und Listen-Management */

void *cmalloc (int n);

void addlist (void *l1, void *p1);
void choplist (list **l, list *p);
void translist (void *l1, void *l2, void *p);
void removelist (void *l, void *p);
void promotelist (void *l, void *p);
void freelist (void *p1);
int listlen (void *l);
int listlen2 (void *l);

strlist *makestrlist (char *s);
void addstrlist (strlist **SP, char *s);
void catstrlist (strlist **SP, strlist *S); /* Added by Bjoern Becker , 27.07.2000 */

/* Fehler, Warnungen, Meldungen */

void mistake2 (unit *u, strlist *S, char *comment);
void mistakeu (unit *u, char *comment);

void addwarning (unit *u, char *s);
void addevent (unit *u, char *s);
void addmessage (unit *u, char *s);
void addcomment (region *r, char *s);
void addbattle (unit *u, char *s);
void adddebug (region *r, char *s);
void addincome (unit *u, char *s);
void addcommerce (unit *u, char *s);
void addproduction (unit *u, char *s);
void addstatus (unit *u, char *s);
void addmovement (unit *u, char *s);
void addalchemie (unit *u, char *s);
void addechsennews (unit *u, char *s);


/* Manipulation von buf */

void scat (char *s);
void icat (int n);

/* String Manipulation allgemein */

int findstr (char **v, char *s, unsigned int n);
int _findstr (int v[], char *s, int n); /* Testversion als Ersatz */

void nstrcpy (char *to, char *from, int n);
void mnstrcpy (char **to, char *from, int n);
void mstrcpy (char **to, char *from);

int geti (void);
int atoip (char *s);

/* Verteilung von vielen Zufallszahlen, meist die Anzahl Opfer von Zauberspruechen (zB. werden 2-50 getoetet). */

int lovar (int n);

/* Funktionen fuer Talente */

int distribute (int old, int new, int n);

int cansee (faction *f, region *r, unit *u);
int effskill (unit *u, int i);

/* Befehle lesen */

char *getstr (void);
char *agetstr (void);
char *igetstr (char *init);
char *aigetstr (char *init);

int finditem (char *s);
int getitem (void);
int findparam (char *s);
int igetparam (char *s);
int getparam (void);
int getspell (void);
int findkeyword (char *s);
int igetkeyword (char *s);

region *findregion (int x, int y, int z);

extern int getunit0;
extern int getunitpeasants;
extern char getunit_text[20];
unit *getunit (region *r, unit *u);
unit *getunitglobal (region *r, unit *u);
unit *findunitglobal (int n);
faction *findfaction (int n);
faction *getfaction (void);

region *findregion (int x, int y, int z);
region * findregion_old (int x, int y, int z);

int isallied (unit *u, unit *u2);

/* Namen von Objekten */

char *factionid (faction *f);
char *regionid (region *r , int xo , int yo);
char *unitid (unit *u);
char *unitid2 (unit *u);
char *buildingid (building *b);
char *buildingtype (building *b);
char *shipid (ship *sh);

building *largestbuilding (region *r);

/* Burgen und Schiffe */

int armedmen (unit * u);

void siege (void);

int buildingeffsize (building * b);
void build_road (region * r, unit * u);

void build_building (region * r, unit * u);
building *create_building (region *r, unit *u);

void create_ship (region * r, unit * u, int newtype);
void continue_ship (region * r, unit * u);

building *getbuilding (region * r);
ship *getship (region * r);

building *findbuilding (int n);
ship *findship (int n);

unit *buildingowner (region * r, building * b);
unit *shipowner (region * r, ship * sh);

void enter (void);
void givecommand (void);
void leaving (void);
void destroy (void);

/* Kampf */

void combat (void);

/* Kontakte */

int besieged (unit * u);
int slipthru (region * r, unit * u, building * b);
int can_contact (region * r, unit * u, unit * u2);
int contacts (region * r, unit * u, unit * u2);
void docontact (void);

/* Erschaffen von Monstern und Inseln */

void createmonsters (void);
void addunit (int type);
void addplayers (void);
void addaplayerat (region * r);
void connectregions (void);
void makeblock (int x1, int y1, int z);
void seed_monsters (int x1, int y1, int z);
void createcontinent (void);
void createworld(void);
void listnames (void);
void changeterrain (void);
void addbuilding (void);
void regioninfo (void);
void showmap (int mode);
void writemap (int mode);
void writecrmap (void);
int sphericalx (int coord, int welt);
int sphericaly (int coord, int welt);
void generate_password (faction *f);

/* Wirtschaft */

/* Wieviel ein Rekrut kostet, und welcher Teil (1/4) der Bauern rekrutiert
   werden kann. */

#define RECRUITFRACTION         20		// 1/2o-tel zum Rekrutieren

/* Welchen Teil des Silbers die Bauern fuer Unterhaltung ausgeben (1/20), und
   wieviel Silber ein Unterhalter pro Talentpunkt bekommt. */

#define ENTERTAININCOME         20
#define ENTERTAINFRACTION       20

/* Wieviele Silbermuenzen jeweils auf einmal "getaxed" werden. */

#define	TAXINCOME				20
#define TAXFRACTION             20

/* Wieviel Silber pro Talentpunkt geklaut wird. */

#define STEALINCOME             (40 + rand() % 20)

extern int income[MAXTYPES];
extern int itemprice[MAXLUXURIES];

int findshiptype (char *s);

void scramble (void *v1, int n, int width);
void recruiting (void);
void giving (void);
void produce (void);
void stealing (void);
int collectmoney (region * r, unit * collector, int n);

/* Diverse */

void quit (void);
int getoption (void);
int wanderoff (region * r, double p);
void demographics (void);
void instant_orders (void);
void last_orders (void);
void set_passw (void);
void mail (void);

void setdefaults (void);

/* Magie */

int magicians (faction *f);
int findspell (char *s);
int cancast (unit *u, int i);

void magic (void);
void research (unit *u);

/* Monster */

unit *make_undead_unit (region * r, faction * f, int n);
unit *make_illsionary_unit (region * r, faction * f, int n);
unit *make_guards_unit (region * r, faction * f, int n);
unit *make_guarded_tower_unit (region *r, faction *f, int n, int m);
unit *make_firedragon_unit (region * r, faction * f, int n);
unit *make_dragon_unit (region * r, faction * f, int n);
unit *make_wyrm_unit (region * r, faction * f, int n);

unit *createunit (region *r1);
void new_units (void);

int richest_neighbour (region * r);
void plan_monsters (void);
void age_unit (region * r, unit * u);

/* Bewegung */

int getdirection (void);

double weight (unit *u);
int walkingcapacity (unit *u);
int capacity (region *r, ship *sh);
void leave (region *r, unit *u);
void movement (void);
void drowning_men (region *r);
int cansail(region* r, ship *sh);

/* Reports */

extern char *directions[MAXDIRECTIONS_S];
extern char *roadinto[MAXTERRAINS_NEW];
extern char *trailinto[MAXTERRAINS_NEW];

void rparagraph (char *s, int indent, int mark);
char *spunit (faction *f, region *r, unit *u, int battle);
void rpunit (faction *f, region *r, unit *u, int indent);

void describe (region *r, faction *f);
int roadto (region * r, region * r2);
char *gamedate (faction *f);

void reports (void);
void report (faction * f);
void report_computer (faction * f);

/* Speichern und Laden */

int cfopen (char *filename, char *mode);
void getbuf (void);
int readorders (void);
void initgame (int lean);
void showaddresses (void);
void writeaddresses (void);
void writesummary (void);
int netaddress (char *s);

/* Lernen und Lehren */

void learn (void);
void teach (region * r, unit * u);

/* Terrain */

void makeblock (int x1, int y1, int z);
int mainterrain (region * r);

/* Base 36 */

extern char base36 [36];

int todec ( char* b36 );			// Base36 Zahl in Dezimale Zahl umwandeln
char * tobase36 ( int dezimal );		// Dezimale Zahl in Base36 Zahl umwandeln
int getib36 ( void );				// Das Selbe wie GETI , nur das die Zahl als Base36 interpretiert wird
int atoipb36 (char *s);
int gaeste(int anz);				// gibt die Anzahl der m�glichen anderen Rassenpersonen zur�ck



/*
 * eigene Funktionen die im orginal Quellcode stehen
 */
building * bestbuilding(region * r);	// liefert die gr��te Burg einer Region



// eigene Sachen einbinden
#include "x8_fantasya.h"



/*
 * diese Funktionen werden aus save.c exportiert um sie in x8_save.c verwenden zu k�nnen
 */

void ws(char *s);		// alles zum speichern
void wspace();
void wnl();
void wi(int n);
void wstrlist (strlist * S);

long value (faction *f);	// keine Ahnung wo die jetzt herkommt ist aber wichtig
int listlen (void *l);	// zum z�hlen der L�nge und f�r 1oo%-Counter beim laden

void rs (char *s);		// alles zum laden
int ri (void);
void mstrcpy (char **to, char *from);
void mrs (char **s);

void terraform (region * r, int terrain);
void addbuilding (void);

#endif
