/* German Atlantis PB(E)M host Copyright (C) 1995-1998   Alexander Schroeder

 based on:
 
  Atlantis v1.0  13 September 1993 Copyright 1993 by Russell Wallace
  
   This program may be freely used, modified and distributed.  It may
   not be sold or used commercially without prior written permission
from the author.  */

#include "atlantis.h"
#include	<io.h>
#include	<direct.h>

// Changed by Bjoern Becker , 28.07.2000

#define  DEBUG

#define  OLD_C_REPORT_VERSION        6
#define  C_REPORT_VERSION   64

#define  REPORTWIDTH            72
#define  MAILDELAY              20
#define  MAILCHECK_INTERVAL     10

/* set to "" if unwanted */

#define MAILCHECK "\nif waithost; then echo Mailing more...; else exit 1; fi\n\n"



faction * akt_f;	// enth�lt den aktuellen Spieler, f�r den der 'NR' erzeugt wird

int netaddress (char *s); // zum gefunden werden
void generatePHPListe();  // dito

/* ------------------------------------------------------------- */

char *
buildingtype (building * b)
{
	if (b->typ == BT_BURG) return buildingnames[buildingeffsize (b)];
	return BuildingNames[b->typ];
}

/* ------------------------------------------------------------- */

char * gamedate (faction *f)
{
	static char buf[40];
	static int monthnames[] =
	{
		ST_JANUARY,
			ST_FEBRUARY,
			ST_MARCH,
			ST_APRIL,
			ST_MAY,
			ST_JUNE,
			ST_JULY,
			ST_AUGUST,
			ST_SEPTEMBER,
			ST_OCTOBER,
			ST_NOVEMBER,
			ST_DECEMBER,
	};
	
	if (turn) strcpy (buf, translate (ST_DATE, f->language, strings[monthnames[(turn - 1) % 12]][f->language], ((turn - 1) / 12) + 1));
	   else strcpy (buf, strings[ST_NO_TIME][f->language]);
	   
	   return buf;
}

/* ------------------------------------------------------------- */

void
sparagraph (strlist ** SP, char *s, int indent, int mark)
{
	
/* Die Liste SP wird mit dem String s aufgefuellt, mit indent und
einer mark, falls angegeben. SP wurde also auf 0 gesetzt vor dem
	Aufruf. */
	
	int i, j, width;
	int firstline;
	static char buf[128];
	
	width = REPORTWIDTH - indent;
	firstline = 1;
	
	for (;;)
    {
		i = 0;
		
		/* i zeigt auf das Ende der aktuellen Zeile.  j wird auf i gesetzt und um ein Wort verlaengert.  Falls das
		gelingt, wird i auf j gesetzt.  Ist j breiter als der Report, wird abgebrochen.  Wird abgebrochen, obwohl
		i immer noch 0 ist (dh. das erste Wort der Zeile ist laenger als die Zeile selber), wird i einfach auf die
		maximale Breite gesetzt (und dieses erste Wort wird zwangs-getrennt).  */
		do
        {
			j = i;
			while (s[j] && s[j] != ' ')
				j++;
			if (j > width)
            {
				if (i == 0)
					i = width - 1;
				break;
            }
			i = j + 1;
        }
		while (s[j]);
		
		/* Einrueckung, markierung innerhalb der Einrueckung.  */
		for (j = 0; j != indent; j++)
			buf[j] = ' ';
		if (firstline && mark)
			buf[indent - 2] = mark;
		
		for (j = 0; j != i - 1; j++)
			buf[indent + j] = s[j];
		buf[indent + j] = 0;
		
		addstrlist (SP, buf);
		
		if (s[i - 1] == 0)
			break;
		
		s += i;
		firstline = 0;
    }
}

void
spskill (unit * u, int i, int *dh, int days)
{
	if (!u->skills[i])
		return;
	
	scat (", ");
	
	if (!*dh)
    {
		scat ("Talente: ");
		*dh = 1;
    }
	
	scat (skillnames[i]);
	scat (" ");
	icat (max(0,effskill (u, i)));
	
	if (days) // Lerntage anzeigen
	{
		assert (u->number);
		scat (" [");
		icat (u->skills[i] / u->number);
		scat ("]");
	}
	
}

char *
spunit (faction * f, region * r, unit * u, int battle)
{
	int i;
	int dh,gotit;
	int kapa;
	int kapaf;
	
	unit *bu;	// fremde Einheit in eigener Burg?
	unit *hu;
	
	char pbuf[20];	// ein Privater Buffer
	
	strcpy (buf, u->faction == f ? "*" : "-");  /* Wird nachher gesondert verwendet als buf[0]!  */
	scat (unitid (u));                          /* Die wirkliche Beschreibung beginnt bei buf[1].  */
	
	/* Parteizugehoerigkeit nur bei cansee() und keine Parteitarnung */
	if (cansee (f, r, u) == 2)
    {
		faction *hf;
		if (u->faction->no != 0)	// wenn keine Monster, dann dieser Zweig
		{
			if (u->tarn_no != 0)	// Partei getarnte Einheiten haben die Monsternummer
			{
				scat (", ");
				if ((buf[0] == '*') && (u->tarn_no != u->faction->no)) scat(" getarnt als ");
				hf = findfaction(u->tarn_no);					// hier ist ggf. die richtige Parteinummer nochmal drinnen
				scat (factionid (hf));
				hf->gotit = 1;									// merken f�r Adressenliste
			} else
			{
				if (buf[0] == '*') scat(", parteigetarnt");	// sonst nix
			}
		} else // jetzt Monster abarbeiten
		{
			scat(", Legionen des Chaos (0)");
		}
    }
	
	// fremde Einheit in eigener Burg, wenn nicht werden in der Burg nur je 1 Person angezeigt
	bu = NULL;
	if (u->building)
	{
		for(hu=r->units; hu; hu=hu->next)
		{
			if ((u->building) && (u->building == hu->building) && (hu->faction == f)) bu = hu;
		}
	} else
	{
		bu = u;
	}
	
	// wenn die Burg voll ist, als im Kampf keine Einheiten mehr sch�tzt, kann sie auch
	// keine Einheiten verstecken
	if (u->building)
	{
		u->building->voll -= u->number;
		if (u->building->voll < 0) bu = u;
	}
	
	/*
	* Rasse ausgeben, bei Echsenmenschen wird der Tarntypus angegeben und falls es die
	* eigene Einheit ist in Klammern noch 'Echsenmensch[en]'
	*/
	i = u->number; u->number = (!bu) ? 1 : u->number; // Backup der Personenanzahl, wird hier etwas einfacher
	scat(", ");
	sprintf(pbuf,"%d ",u->number); scat(pbuf);
	if (u->faction->no != 0)
	{
		switch(u->type)
		{
			case U_MAN:			sprintf(pbuf,"%s",u->number!=1?racenames[u->raceTT][1]:racenames[u->raceTT][0]);
								if (u->prefix)
								{
									if (strlen(u->prefix))
									{
										scat(u->prefix);
										if (isalpha(u->prefix[strlen(u->prefix)-1]))
										{
											pbuf[0] = tolower(pbuf[0]);
										}
									}
								}
								scat(pbuf);
								break;
			case U_ILLUSION:
			case U_GUARDS:		if (u->number != 1) scat("Menschen"); else scat("Mensch");				break;
			case U_UNDEAD:		if (u->number != 1) scat("Untote"); else scat("Untoter");				break;
			case U_FIREDRAGON:	if (u->number != 1) scat("Feuerdrachen"); else scat("Feuerdrachen");	break;
			case U_DRAGON:		if (u->number != 1) scat("Drachen"); else scat("Drachen");				break;
			case U_WYRM:		if (u->number != 1) scat("Wyrme"); else scat("Wyrm");					break;
			default:			scat("Fehler: unbekanntes Monster");									break;
		}
	} else // Monster
	{
		switch (u->type)
		{
			// Aussehen
			case U_ILLUSION:
			case U_GUARDS:
			case U_MAN:			if (u->number != 1) scat("Menschen"); else scat("Mensch");				break;
			case U_UNDEAD:		if (u->number != 1) scat("Untote"); else scat("Untoter");				break;
			case U_FIREDRAGON:	if (u->number != 1) scat("Feuerdrachen"); else scat("Feuerdrachen");	break;
			case U_DRAGON:		if (u->number != 1) scat("Drachen"); else scat("Drachen");				break;
			case U_WYRM:		if (u->number != 1) scat("Wyrme"); else scat("Wyrm");					break;
			default:			scat("Fehler: unbekanntes Monster");									break;
		}
	}
	u->number = i; // Personenanzahl wieder herstellen
	
	// falls eigene Einheit ggf. Echsenmenschen ausgeben
	if ((buf[0]=='*') && (u->race == R_DEMON))
	{
		scat(" (");
		scat(u->number!=1?racenames[u->race][1]:racenames[u->race][0]);
		scat(") ");
	}
	
	if (f == u->faction) { scat(", ");	scat(getLebenszustand(u)); }

	/* status */
	if (u->faction == f || battle)
		switch (u->status)
	{
      case ST_FIGHT:
		  scat (", kampfbereit");
		  break;
		  
      case ST_BEHIND:
		  scat (", k�mpft hinten");
		  break;
		  
      case ST_AVOID:
		  scat (", k�mpft nicht");
		  break;
	}
	
	if (f == u->faction && effskill(u,SK_STEALTH)) if (u->tarnung) scat(", getarnt"); else scat(", ungetarnt");
	if (u->guard) scat (", bewacht die Region");
	
	if (u->besieging)
    {
		scat (", belagert ");
		scat (buildingid (u->besieging));
    }
	
	dh = 0;
	if (u->faction == f)
    {
		for (i = 0; i != MAXSKILLS; i++) spskill (u, i, &dh, 1);
		switch (u->enchanted)
		{
		case SP_NIGHT_EYES:
			scat (", ");
			if (!dh) scat ("hat leuchtende Augen");
			break;
		case SP_WATER_WALKING:
			scat (", ");
			if (!dh) scat ("schwebt �ber dem Boden");
			break;
		}
    }
	
	dh = 0;
	
	/*
	* Hier jetzt Kapazit�t ausblenden, wenn es keine eigene Einheit ist
	*/
	if (u->faction == f)
	{
		kapa=0;
		kapa=walkingcapacity(u); // - u->number*personweight[u->faction->race];
		kapaf=kapa- (int)(weight(u)+0.999);
		scat (", ");
		if (kapaf >= 0)
		{
			scat (translate (ST_CAPACITY, f->language, kapaf, (kapa - u->number*personweight[u->faction->race]) ));
		} else
		{
			scat("Kapazit�t: �berladen mit ");
			icat(0 - kapaf);
			scat(" Gewichtseinheiten");
		}
	}
	
	for (i = 0; i != MAXITEMS; i++)
		if (u->items[i])
		{
			if (!dh)
			{
				scat (strings[ST_HAS][f->language]);
				if (i != I_SILVER)
				{
					scat (translate (ST_QUANTITY_IN_LIST, f->language, (bu) ? u->items[i] : 1, strings[itemnames[u->items[i] != 1][i]][f->language]));
				} else
				{
					if (u->faction != f)
					{
						if (u->items[i] < 500 * u->number) scat ("Silberbeutel"); else scat("Schatzkiste");
					} else
					{
						scat (" ");
						icat(u->items[i]);
						scat (" Silber");
					}
				}
				dh = 1;
			} else scat (translate (ST_QUANTITY_IN_LIST, f->language, (bu) ? u->items[i] : 1, strings[itemnames[u->items[i] != 1][i]][f->language]));
		}
		
		gotit = 0;
		if (u->faction !=f)
		{
			for(i=0; i<MAX_KRAEUTER; i++) if (u->kraeuter[i]) gotit = 1;
			if (gotit) scat(", Kr�uterbeutel");
		} else
		{
			for(i=0; i<MAX_KRAEUTER; i++)
			{
				if (u->kraeuter[i])
				{
					scat(", ");
					icat(u->kraeuter[i]);
					scat(" ");
					scat(kraeuter[i]);
				}
			}
		}
		
		gotit = 0;
		if (u->faction !=f)
		{
			for(i=0; i<MAX_TRAENKE; i++) if (u->traenke[i]) gotit = 1;
			if (gotit) scat(", Tr�nke & Tinkturen");
		} else
		{
			for(i=0; i<MAX_TRAENKE; i++)
			{
				if (u->traenke[i])
				{
					scat(", ");
					icat(u->traenke[i]);
					scat(" ");
					scat(traenke[i]);
				}
			}
		}
		
		if (u->faction == f)
		{
			dh = 0;
			for (i = 0; i != MAXSPELLS; i++)
				if (!iscombatspell[i] && u->spells[i])
				{
					scat (", ");
					if (!dh)
					{
						scat (strings[ST_SPELLS][f->language]);
						dh = 1;
					}
					scat (strings[spellnames[i]][f->language]);
				}
				
				dh = 0;
				for (i = 0; i != MAXSPELLS; i++)
					if (iscombatspell[i] && u->spells[i])
					{
						scat (", ");
						if (!dh)
						{
							scat (strings[ST_COMBAT_SPELLS][f->language]);
							dh = 1;
						}
						scat (strings[spellnames[i]][f->language]);
					}
					
					if (u->combatspell >= 0)
					{
						assert (dh);
						scat (", ");
						scat (strings[ST_SET][f->language]);
						scat (strings[spellnames[u->combatspell]][f->language]);
					}
					
					if (!battle)
					{
						if (u->lastorder || u->thisorder2)
							scat (", Default: ");
						if (u->lastorder)
						{
							scat ("\"");
							scat (u->lastorder);
							scat ("\"");
						}
						if (u->lastorder && u->thisorder2)
							scat (" und ");
						if (u->thisorder2)
						{
							scat ("\"");
							scat (u->thisorder2);
							scat ("\"");
						}
					}
		}
		
		i = 0;
		
		if (u->display)
		{
			scat ("; ");
			scat (u->display);
			
			i = u->display[strlen (u->display) - 1];
		}
		
		if (i != '!' && i != '?' && i != '.')
			scat (".");
		
		return buf;
}

/* ------------------------------------------------------------- */

int outi;
unsigned char outbuf[1500];

void
rpc (int c)
{
	outbuf[outi++] = c;
	assert (outi < (int) sizeof outbuf);
}

void
rnl (void)
{
	int i;
	int rc, vc;
	
	i = outi;
	while (i && isspace (outbuf[i - 1]))
		i--;
	outbuf[i] = 0;
	
	i = 0;
	rc = 0;
	vc = 0;
	
	while (outbuf[i])
    {
		switch (outbuf[i])
        {
        case ' ':
			vc++;
			break;
			
        case '\t':
			vc = (vc & ~7) + 8;
			break;
			
        default:
			while (rc / 8 != vc / 8)
            {
				if ((rc & 7) == 7)
					fputc (' ', F);
				else
					fputc ('\t', F);
				rc = (rc & ~7) + 8;
            }
			
			while (rc != vc)
            {
				fputc (' ', F);
				rc++;
            }
			
			fputc (outbuf[i], F);
			rc++;
			vc++;
        }
		
		i++;
    }
	
	fputc ('\n', F);
	outi = 0;
}

void
rps (char *s)
{
	rpc (' ');                    /* neuer rand */
	while (*s)
		rpc (*s++);
}

void
rpstrlist (strlist * S)
{
	while (S)
    {
		rps (S->s);
		rnl ();
		S = S->next;
    }
}

int
crpstrlist (char *title, int n , strlist * S)
{
	while (S)
    {
		fprintf (F, "MESSAGE %d\n" , n );
		
		if (strcmp(title,"FEHLER") == 0)		fprintf(F,"2001;type\n");
		if (strcmp(title,"WARNUNGEN") == 0)		fprintf(F,"2002;type\n");
		if (strcmp(title,"MELDUNGEN") == 0)		fprintf(F,"2003;type\n");
		if (strcmp(title,"KAEMPFE") == 0)		fprintf(F,"2004;type\n");
		if (strcmp(title,"EREIGNISSE") == 0)	fprintf(F,"2005;type\n");
		if (strcmp(title,"EINKOMMEN") == 0)		fprintf(F,"2006;type\n");
		if (strcmp(title,"HANDEL") == 0)		fprintf(F,"2007;type\n");
		if (strcmp(title,"PRODUKTION") == 0)	fprintf(F,"2008;type\n");
		if (strcmp(title,"BEWEGUNGEN") == 0)	fprintf(F,"2009;type\n");
		if (strcmp(title,"ALCHEMIE") == 0)		fprintf(F,"2010;type\n");
		if (strcmp(title,"ECHSENNEWS") == 0)	fprintf(F,"2011;type\n");
		
		fprintf (F, "\"%s\";rendered\n", S->s);
		if (S->uno)
		{
			unit *u = findunitglobal(S->uno);
			// nur ausgeben wenn Einheit noch Personen hat, sonst wird es in Magellan nicht angezeigt
			fprintf (F, "%d;unit\n",S->uno);	
			if (u) fprintf (F, "%d %d %d;region\n"
							 ,u->r->x - u->faction->xorigin
							 ,u->r->y - u->faction->yorigin
							 ,u->r->z);
		}

		S = S->next;
		n++;
    }
	return n;
}

void
old_crpstrlist (char *title, strlist * S)
{
	fprintf (F, "%s\n", title);
	while (S)
    {
		fprintf (F, "\"%s\"\n", S->s);
		S = S->next;
    }
}

void
rparagraph (char *s, int indent, int mark)
{
	strlist *S;
	
	S = 0;
	sparagraph (&S, s, indent, mark);
	rpstrlist (S);
	freelist (S);
}

void
rpunit (faction * f, region * r, unit * u, int indent)
{
	strlist *S;
	
	rnl ();                       /* neue leerzeile */
	
	S = 0;
	spunit (f, r, u, 0);
	sparagraph (&S, buf + 1, indent, buf[0]);
	rpstrlist (S);
	freelist (S);
}

void
center (char *s)
{
	int i;
	
	/* Bei Namen die genau 80 Zeichen lang sind, kann es hier Probleme geben.
	Seltsamerweise wird i dann auf MAXINT oder aehnlich initialisiert.
	Deswegen keine Strings die laenger als REPORTWIDTH sind! */
	
	// assert (REPORTWIDTH >= strlen (s));
	
	if (REPORTWIDTH > strlen (s))
	{
		for (i = (REPORTWIDTH - strlen (s)) / 2; i; i--) rpc (' ');
		rps (s);
	} else
	{
		puts(s);
	}
	rnl ();
}

void
untitled_strlist (strlist * S)
{
	strlist *T;
	
	while (S)
    {
		T = 0;
		sparagraph (&T, S->s, 0, 0);
		rpstrlist (T);
		freelist (T);
		S = S->next;
    }
}

void
titled_strlist (char *s, strlist * S)
{
	if (S)
    {
		rnl ();
		rps (s);
		rnl ();
		rnl ();
		untitled_strlist (S);
    }
}

void
centred_title_strlist (char *s, strlist * S)
{
	strlist *T;
	
	if (S)
    {
		rnl ();
		center (s);
		rnl ();
		
		while (S)
        {
			T = 0;
			if (S->s[0] == '*' || S->s[0] == '-')
				/* Eine Einheit in f->battles, muss noch formatiert werden. */
				sparagraph (&T, S->s + 1, 4, S->s[0]);
			else
				sparagraph (&T, S->s, 0, 0);
			rpstrlist (T);
			freelist (T);
			S = S->next;
        }
    }
}

void
centred_paragraph (char *s)
{
	strlist *S;
	
	S = 0;
	sparagraph (&S, s, 0, 0);
	while (S)
    {
		center (S->s);
		S = S->next;
    }
	freelist (S);
}

/* ------------------------------------------------------------- */

void
prices (region *r, faction *f)
{
	int i;
	
	/* Beginne Paragraphen */
	if (r->buildings)
		strcpy (buf, strings[ST_CURRENT_PRICES][f->language]);
	else
		strcpy (buf, strings[ST_PRICES_WITHOUT_BUILDINGS][f->language]);
	for (i = 0; i != MAXLUXURIES; i++)
    {
		if (i==0)
			scat (translate (ST_FIRST_PRICE, f->language, strings[itemnames[1][FIRSTLUXURY + i]][f->language],
			itemprice[i] * r->demand[i] / 100));
		else if (i == MAXLUXURIES - 1)
			scat (translate (ST_LAST_PRICE, f->language, strings[itemnames[1][FIRSTLUXURY + i]][f->language],
			itemprice[i] * r->demand[i] / 100));
		else
			scat (translate (ST_PRICE, f->language, strings[itemnames[1][FIRSTLUXURY + i]][f->language],
			itemprice[i] * r->demand[i] / 100));
    }
	scat (translate (ST_PEASANTS_MAKE, f->language,
		strings[itemnames[1][FIRSTLUXURY + r->produced_good]][f->language]));
	
	/* Schreibe Paragraphen */
	rparagraph (buf, 0, 0);
}

/* ------------------------------------------------------------- */

int
roads (region * r, int richtung)
{
	return (r->road[richtung] && r->road[richtung] >= roadreq[r->terrain]);
}

int roadto (region * r, region * r2)
{
/* wenn es hier genug strassen gibt, und verbunden ist, und es dort genug
	strassen gibt, dann existiert eine strasse in diese richtung */
	
	int i, richtung;

	for(i = 0; i < MAXDIRECTIONS; i++) if (r->connect[i] == r2) richtung = i;
	if (!r || !r2 || !roads (r, richtung) || !roads (r2, back[richtung])) return 0;	
	for (i = 0; i != MAXDIRECTIONS; i++) if (r->connect[i] == r2) return 1;
		
	return 0;
}

/* ------------------------------------------------------------- */

void
describe (region *r, faction *f)
{
	int dh, n, d, i;
	
	/* Name */
	
	strcpy (buf, regionid (r, f->xorigin , f->yorigin ));
	
	/* Terrain */
	
	if (r->terrain != T_OCEAN)
	{
		scat (", ");
		scat (strings[terrainnames[mainterrain (r)]][f->language]);
	}
	
	/* Baeume */
	
	if (r->trees)
    {
		scat (", ");
		scat (translate (r->trees != 1 ? ST_TREES : ST_TREE, f->language, r->trees));
    }
	
	/* Bauern & Geld */
	
	if (r->peasants)
    {
		scat (", ");
		scat (translate (r->peasants != 1 ? ST_PEASANTS : ST_PEASANT, f->language, r->peasants));
		
		if (r->money)
        {
			scat (", $");
			icat (r->money);
        }
    }
	
	/* Pferde */
	if (r->horses)
		scat (translate (ST_QUANTITY_IN_LIST, f->language, r->horses,
		strings[itemnames[r->horses > 1][I_HORSE]][f->language]));
	
	scat (".");
	
	/*
	 *	hier wird als erstes wird die Anzahl der vorhanden Strassenst�cke gez�hlt,
	 *	fertige Strassen m�ssen �bersehen werden
	 */
	n = 0;
	for(i = 0; i < MAXDIRECTIONS; i++) if (r->road[i] && !roadto(r, r->connect[i])) n++;
	if (n)
	{
		scat(" Ein Strassenstueck fuehrt ");
		for(i = 0; i < MAXDIRECTIONS; i++)
		{
			if (r->road[i] && !roadto(r, r->connect[i]))
			{
				scat ("nach ");
				scat (directions[i]);
				scat (" (");
				icat (100 * r->road[i] / roadreq[r->terrain]);
				scat ("% fertig) ");
				scat (roadinto[mainterrain (r->connect[i])]);
				scat (" ");
				scat (regionid (r->connect[i], f->xorigin , f->yorigin ));
				switch(--n)
				{
					 case 0: scat(".");		break;
					 case 1: scat(" und ");	break;
					default: scat(", ");	break;
				}
			}
		}
	}

	/*
	 *	dann folgen die kompletten Strassen
	 */
	n = 0;
	for(i = 0; i < MAXDIRECTIONS; i++) if (roadto(r, r->connect[i])) n++;
	if (n)
	{
		scat(" Die Strasse ");
		for(i = 0; i < MAXDIRECTIONS; i++)
		{
			if (roadto(r, r->connect[i]))
			{
				scat ("nach ");
				scat (directions[i]);
				scat (" ");
				scat (roadinto[mainterrain (r->connect[i])]);
				scat (" ");
				scat (regionid (r->connect[i], f->xorigin , f->yorigin ));
				switch(--n)
				{
					 case 0: scat(" ist fertig ausgebaut.");		break;
					 case 1: scat(" und ");	break;
					default: scat(", ");	break;
				}
			}
		}
	}
	
	/* Richtungen ohne Strassen zaehlen */
	
	n = 0;
	for (d = 0; d != MAXDIRECTIONS; d++)
		if (!roadto (r, r->connect[d]) && !r->road[d])
			n++;
		
		if (n)
		{
			scat (" Im");
			
			dh = 0;
			i = 0;
			for (d = 0; d != MAXDIRECTIONS; d++)
				if (!roadto (r, r->connect[d]) && !r->road[d])
				{
					/* "und" vor dem letzten, aber nicht beim ersten */
					
					i++;
					if (dh)
					{
						if (i == n)
							scat (" und im");
						else
							scat (", im");
					}
					
					scat (" ");
					scat (directions[d]);
					scat (" ");
					if (!dh)
					{
						/* Fallunterscheidung bei "Berge", falls die erste Region "Berge" heisst. */
						if (mainterrain (r->connect[d]) == T_MOUNTAIN)
							scat ("liegen ");
						else
							scat ("liegt ");
					}
					scat (trailinto[mainterrain (r->connect[d])]);
					if (r->connect[d])
					{
						if (r->connect[d]->terrain != T_OCEAN)
						{
							scat (" ");
							scat (regionid (r->connect[d], f->xorigin , f->yorigin ));
						}
					} else
					{
						scat(" Chaos");
					}
					
					dh = 1;
				}
				scat (".");
		}
		
		/* Beschreibung */
		
		if (r->display)
		{
			scat (" ");
			scat (r->display);
			
			n = r->display[strlen (r->display) - 1];
			if (n != '!' && n != '?' && n != '.')
				scat (".");
		}
		
		/* Schreibe Paragraphen */
		
		rparagraph (buf, 0, 0);
		
		/* Kommentare, zB. Effekte von Zauberspruechen wie "Nebelnetze" */
		
		if (r->comments)
			untitled_strlist (r->comments);
		
}

void
guards (region *r)
{
	faction *f;
	unit *u;
	int i, n;
	
	/* Bewachung */
	
	for (u = r->units; u; u = u->next)
		if (u->guard)
			break;
		if (!u)
			return;
		
		for (f = factions; f; f = f->next)
			f->dh = 0;
		
		n = 0;
		for (u = r->units; u; u = u->next)
			if (u->guard)
			{
				u->faction->dh = 1;
				n++;
			}
			
			strcpy (buf, "Die Region wird von ");
			
			i = 0;
			n = 0;
			for (f = factions; f; f = f->next)
				if (f->dh)
				{
					i++;
					if (n)
					{
						if (i == n)
							scat (" und ");
						else
							scat (", ");
					}
					n = 1;
					scat (factionid (f));
				}
				scat (" bewacht.");
				rnl ();
				rparagraph (buf, 0, 0);
}

void
statistics (region * r, faction * f)
{
	unit *u;
	building *b;
	strlist *S;
	int i, number, money, maxwork, rmoney, items[MAXITEMS], wage;
	
	S = 0;
	
	/* Arbeiten.  */
	if (production[r->terrain])
    {
		wage = WAGE;
		b = largestbuilding (r);
		if (b) wage += buildingeffsize (b) * BONUS;
		
		maxwork = (production[r->terrain] - r->trees) * MAXPEASANTS_PER_AREA;
		rmoney = min (r->peasants, (production[r->terrain] - r->trees) * MAXPEASANTS_PER_AREA) * wage;
		
		rmoney += r->money;
		sprintf (buf, "Die Bauern erhalten %d Silber Lohn und geben davon %d Silber f�r Unterhaltung aus."
			, wage
			, (r->money + (min (r->peasants, (production[r->terrain] - r->trees) * MAXPEASANTS_PER_AREA) * wage)) / ENTERTAINFRACTION);
		addstrlist (&S, buf);
		
		sprintf (buf, "%d Luxusgueter zum kaufen und %d zum verkaufen", DEMANDFALL * r->peasants / DEMANDFACTOR,DEMANDRISE * r->peasants / DEMANDFACTOR);
		/* 6x mehr als beim Verkauf */
		addstrlist (&S, buf);
		
		sprintf (buf, "%d Bauern wollen in Euren Dienst treten.",r->peasants / RECRUITFRACTION);
		addstrlist (&S, buf);
    }
	
	sprintf(buf," "); addstrlist(&S,buf);
	
	number = 0;
	money = 0;
	memset (items, 0, sizeof items);
	
	for (u = r->units; u; u = u->next)
		if (u->faction == f)
		{
			number += u->number;
			money += u->items[I_SILVER];
			for (i = 0; i != MAXITEMS; i++)
				items[i] += u->items[i];
		}
		sprintf (buf, "Personen: %d", number);
		addstrlist (&S, buf);
		
		for (i = 0; i != MAXITEMS; i++)
			if (items[i])
			{
				sprintf (buf, "%s: %d", strings[itemnames[1][i]][f->language], items[i]);
				addstrlist (&S, buf);
			}
			
			/* Ausgabe */
			titled_strlist ("Statistik", S);
			freelist (S);
}

void
template_unit (strlist *S, unit *u)
{
/* In order_template () verwendet man *S, und damit das dortige *S
	manipuliert werden kann, verwenden wir hier also **S.  */
	strlist *ms;
	
	/* Einheit mit Name.  */
	sprintf (buf, "%s %s;\t\t%s [%d, $%d]", parameters[P_UNIT],tobase36(u->no) , u->name, u->number, u->items[I_SILVER]);
	addstrlist (&S, buf);
	
	/* Default Befehl.  */
	if (u->lastorder)
    {
		sprintf (buf, "   %s", u->lastorder);
		addstrlist (&S, buf);
    }
	if (u->thisorder2)	// LIEFERE
    {
		sprintf (buf, "   %s", u->thisorder2);
		addstrlist (&S, buf);
    }
	
	if (u->comments)
	{
		for(ms=u->comments; ms; ms=ms->next)
		{
			sprintf(buf,"   %s", ms->s);
			addstrlist(&S,buf);
		}
		
	}
	
	/* end of changes */
	
}

void
order_template (faction *f)
{
	strlist *S;
	region *r;
	building *b;
	ship *sh;
	unit *u;
	
	S = 0;
	addstrlist (&S, "; Vorlage fuer den n�chsten Zug:");
	addstrlist (&S, "");
	
	printf("- Zugvorlage f�r %s (%s)\n",f->name,tobase36(f->no));
	
	sprintf (buf, "%s %s \"%s\"", parameters[P_FACTION], tobase36(f->no), f->passw ? f->passw : "");
	addstrlist (&S, buf);
	
	for (r = regions; r; r = r->next)
    {
		/* Nur falls es units gibt.  */
		
		for (u = r->units; u; u = u->next) if (u->faction == f) break;
		if (!u) continue;
		
		/* Region */
		addstrlist (&S, "");
		addstrlist (&S, "");
		sprintf (buf, "; %s", regionid (r, f->xorigin , f->yorigin ));
		addstrlist (&S, buf);
		
		/* Einheiten in Burgen */
		for (b = r->buildings; b; b = b->next)
        {
			addstrlist (&S, "");
			sprintf (buf, "; BURG %s\t\t%s", tobase36(b->no), b->name);
			addstrlist (&S, buf);
			for (u = r->units; u; u = u->next) if (u->faction == f && u->building == b && u->owner) template_unit (S, u);
			for (u = r->units; u; u = u->next) if (u->faction == f && u->building == b && !u->owner) template_unit (S, u);
        }
		
		/* Einheiten in Schiffen */
		for (sh = r->ships; sh; sh = sh->next)
        {
			addstrlist (&S, "");
			sprintf (buf, "; SCHIFF %s\t\t%s", tobase36(sh->no), sh->name);
			addstrlist (&S, buf);
			for (u = r->units; u; u = u->next) if (u->faction == f && u->ship == sh && u->owner) template_unit (S, u);
			for (u = r->units; u; u = u->next) if (u->faction == f && u->ship == sh && !u->owner) template_unit (S, u);
        }
		
		/* Restliche Einheiten */
		addstrlist (&S, "");
		for (u = r->units; u; u = u->next) if (u->faction == f && !u->building && !u->ship && cansee (f, r, u)) template_unit (S, u);
    }
	
	/* Zum Schluss: NAECHSTER.  */
	addstrlist (&S, "");
	//sprintf (buf, parameters[P_NEXT]);
	addstrlist (&S, "N�CHSTER");
	
	rpstrlist (S);
	freelist (S);
	
	fprintf(F,"\n");
}

void
allies (faction * f)
{
	int dh, m, i;
	rfaction *rf;
	
	if (f->allies)
    {
		m = 0;
		for (rf = f->allies; rf; rf = rf->next)
			m++;
		
		dh = 0;
		strcpy (buf, "Wir helfen folgenden Parteien: ");
		
		i = 0;
		for (rf = f->allies; rf; rf = rf->next)
        {
			i++;
			if (dh)
            {
				if (i == m)
					scat (" und ");
				else
					scat (", ");
            }
			dh = 1;
			scat (factionid (rf->faction));
        }
		
		scat (".");
		rnl ();
		rparagraph (buf, 0, 0);
    }
}

int crtrennung;
int crwelt;

void report_computer (faction * f)
{
	int visible, d, wage, rmoney ,n;
	region *r;
	rfaction *rf;
	building *b;
	ship *sh;
	unit *u;
	unit *bu;				// Einheit die sich in einer feindlichen Burg befindet, dadurch werden wieder alle Personen angezeigt
	int unique = 1;
	int head = 0;
	int welt = 0;
	int i;
	strlist *tempstr;
	
	int cr[5000][3];		// Koordinaten der Chaos-Regionen
	int cr_count = 0;		// Anzahl der erkannten Chaos-Regionen
	
	faction *hf;
	
	printf ("- Computer Report fuer %s...\n", factionid (f));
	fprintf (F, "VERSION %d\n", C_REPORT_VERSION);
	fprintf (F, "\"de\";locale\n");
	fprintf (F, "\"Fantasya\";Spiel\n");
	fprintf (F, "\"Standard\";Konfiguration\n");
	fprintf (F, "\"Hex\";Koordinaten\n");
	fprintf (F, "36;Basis\n");
	fprintf (F, "1;Umlaute\n");
	fprintf (F, "%d;Runde\n" , (turn == 0 ? 1 : turn));
	fprintf (F, "1;Zeitalter\n");
	
	fprintf (F, "PARTEI %d\n" , f->no );
	fprintf (F, "\"de\";locale\n");
	fprintf (F, "%d;Runde\n" , (turn == 0 ? 1 : turn));
	// fprintf (F, "\"%s\";Passwort\n" , f->passw);
	fprintf (F, "%d;Optionen\n" , f->options);
	fprintf (F, "\"%s\";Typ\n" , racenames[f->race][1]);
	fprintf (F, "%d;Rekrutierungskosten\n" , RecruitCost[f->race]);
	fprintf (F, "%d;Anzahl Personen\n" , f->number);
	fprintf (F, "\"Fantasya\";Magiegebiet\n");
	fprintf (F, "\"%s\";Parteiname\n" , f->name);
	fprintf (F, "\"%s\";email\n" , f->addr);
	for (rf = f->allies; rf; rf = rf->next)
    {
		fprintf (F, "ALLIANZ %d\n" , rf->faction->no );
		fprintf (F, "\"%s\";Parteiname\n", rf->faction->name );
		fprintf (F, "%d;Status\n" , 27);
    };
	
	
	n = 1;
	n = crpstrlist ("FEHLER", 0 , f->mistakes);
	n = crpstrlist ("WARNUNGEN", n , f->warnings);
	n = crpstrlist ("MELDUNGEN", n , f->messages);
	n = crpstrlist ("KAEMPFE", n , f->battles);
	n = crpstrlist ("EREIGNISSE", n , f->events);
	n = crpstrlist ("EINKOMMEN", n , f->income);
	n = crpstrlist ("HANDEL", n , f->commerce);
	n = crpstrlist ("PRODUKTION", n , f->production);
	n = crpstrlist ("BEWEGUNGEN", n , f->movement);
	n = crpstrlist ("ALCHEMIE", n , f->alchemie);
	n = crpstrlist ("ECHSENNEWS", n , f->echsennews);
	
	for(hf = factions; hf; hf = hf->next) hf->gotit = 0;		// Partei noch unbekannt
	for(cr_count = 0; cr_count<5000; cr_count++) { cr[cr_count][0] = 0; cr[cr_count][1] = 0; cr[cr_count][2] = 0;}
	
	for (r = regions; r; r = r->next)
    {
		r->gotit = 0;		// Region noch nicht "gezeichnet"
		for (u = r->units; u; u = u->next) if (u->faction == f) break;
		if (!u) continue;
		for (u = r->units; u; u = u->next)
		{
			visible = cansee (f, r, u);
			if (visible == 2)
			{
				hf = findfaction(u->tarn_no);	// Ausgabe dieser Partei !!
				if (hf->no != f->no)
				{
					if (hf->gotit == 0)
					{
						hf->gotit = 1;
						fprintf (F, "PARTEI %d\n" , hf->no );
						fprintf (F, "\"%s\";Parteiname\n" , hf->name);
						fprintf (F, "\"%s\";email\n" , hf->addr);
					}
				}
			}
		}
    }
	
	for(d = 0; d < MAXSPELLS; d++)
	{
		// Zauberspr�che werden immer f�r jeden(!) Magier gezeigt ... auch wenn
		// nur einer den Spruch sehen wollte
		if (f->showdata[d] == 1)
		{
			fprintf(F, "ZAUBER %d\n", d);
			fprintf(F, "\"%s\";name\n", strings[spellnames[d]][f->language]);
			fprintf(F, "%d;level\n", spelllevel[d] * 2);
			fprintf(F, "\"%s\";info\n", strings[spelldata[d]][f->language]);
			fprintf(F, "1;rank\n");
		}
	}

	for (r = regions; r; r = r->next)
    {
		for (u = r->units; u; u = u->next) if (u->faction == f) break;		// eigene Einheiten suchen
		if (!u) continue;	// Region ist komplett leer

		// Trennung der Welten f�r EHMV User
		if (crtrennung == 1) if (r->z != crwelt) continue;
		
		r->gotit = 1;
		welt = r->z;
		
		fprintf (F, "REGION %d %d %d\n", (r->x) - f->xorigin , (r->y) - f->yorigin, r->z);
		if (r->terrain != T_OCEAN)
		{
			fprintf (F, "\"%s\"%s", r->name , ";Name\n");
		} else
		{
			//fprintf (F, "0;maxworkers\n");
			fprintf (F, "\"true\";isOcean\n");
		}
		if (r->display) fprintf(F, "\"%s\";Beschr\n", r->display);
		fprintf (F, "\"%s\";Terrain\n" , strings[terrainnames[mainterrain (r)]][0]);
		fprintf (F, "%d;Bauern\n" , r->peasants );
		fprintf (F, ((r->terrain == T_VULKAN) ? "%d;Flugdrachen\n" : "%d;Pferde\n") , r->horses );
		fprintf (F, "%d;Baeume\n" , r->trees );
		fprintf (F, "%d;Silber\n" , r->money );
  		/* if (r->terrain == T_MOUNTAIN)
		{
			fprintf(F, "100;eisen\n");
			fprintf(F, "100;stein\n");
			fprintf(F, "RESOURCE 1992739472\n");
			fprintf(F, "\"Stein\";type\n");
			fprintf(F, "100;number\n");
			fprintf(F, "1,skill\n");
			fprintf(F, "RESOURCE 235898859\n");
			fprintf(F, "\"Eisen\";type\n");
			fprintf(F, "100;number\n");
			fprintf(F, "1,skill\n");
		} */
		wage = 0;
		if (production[r->terrain])
        {
			wage = WAGE;
			b = largestbuilding (r);
			if (b) wage += buildingeffsize (b) * BONUS;
        }

		rmoney = r->money;
		fprintf (F, "%d;Unterh\n" , (rmoney + (min (r->peasants, (production[r->terrain] - r->trees) * MAXPEASANTS_PER_AREA) * wage)) / ENTERTAINFRACTION );
		fprintf (F, "%d;Rekruten\n" , r->peasants / RECRUITFRACTION );
		fprintf (F, "%d;Lohn\n" , wage );
		if (r->terrain != T_OCEAN)
		{
			fprintf (F, "PREISE\n");
			fprintf (F, "%d;Balsam\n",		(itemprice[0] * r->demand[0] / 100 * (0 == r->produced_good ? -1 : 1)));
			fprintf (F, "%d;Gew�rz\n",		(itemprice[1] * r->demand[1] / 100 * (1 == r->produced_good ? -1 : 1)));
			fprintf (F, "%d;Juwel\n",		(itemprice[2] * r->demand[2] / 100 * (2 == r->produced_good ? -1 : 1)));
			fprintf (F, "%d;Myrrhe\n",		(itemprice[3] * r->demand[3] / 100 * (3 == r->produced_good ? -1 : 1)));
			fprintf (F, "%d;�l\n",			(itemprice[4] * r->demand[4] / 100 * (4 == r->produced_good ? -1 : 1)));
			fprintf (F, "%d;Seide\n",		(itemprice[5] * r->demand[5] / 100 * (5 == r->produced_good ? -1 : 1)));
			fprintf (F, "%d;Weihrauch\n",	(itemprice[6] * r->demand[6] / 100 * (6 == r->produced_good ? -1 : 1)));
		};

		for(i = 0; i<6; i++)
		{
			// Konvertierung der Richtungen von Fantasya nach Magellan
			int roadConvert[MAXDIRECTIONS] = { D_NORTHWEST, D_NORTHEAST, D_EAST, D_SOUTHEAST, D_SOUTHWEST, D_WEST };
			if (r->road[roadConvert[i]])
			{
				fprintf(F,"GRENZE %d\n",i);
				fprintf(F,"\"Strasse\";typ\n");
				fprintf(F,"%d;richtung\n", i);
				fprintf(F,"%d;prozent\n",100 * r->road[roadConvert[i]] / roadreq[r->terrain]);
			}
		}
		
		if (r->hoehle)
		{
			hoehle *h = findhoehle(r->hoehle);

			fprintf (F, "BURG %d\n", h->nummer * (r->z > 0 ? 1 : -1));
			fprintf (F, "\"%s\";Name\n", h->name);
			fprintf (F, "\"Hoehle\";Typ\n");
			fprintf (F, "\"%s\";Beschr\n", h->beschr);
			fprintf (F, "0;Groesse\n");
		}

		for (b = r->buildings; b; b = b->next)
        {
			fprintf (F, "BURG %d\n", b->no );
			fprintf (F, "\"%s\";Name\n", b->name ? b->name : "");
			if (!b->typ)
			{
				fprintf (F, "\"%s\";Typ\n",buildingnames[buildingeffsize(b)]);
			} else
			{
				fprintf (F, "\"%s\";Typ\n", BuildingNames[b->typ]);
			};
			if (b->display) fprintf (F, "\"%s\";Beschr\n", b->display);
			fprintf (F, "%d;Groesse\n", b->size);
			u = buildingowner (r, b);
			if (u) fprintf (F, "%d;Besitzer\n", u->no );
			if (u && cansee (f, r, u) == 2) fprintf (F, "%d;Partei\n", u->tarn_no );
			b->voll = b->size;
        }
		
		for (sh = r->ships; sh; sh = sh->next)
        {
			fprintf (F, "SCHIFF %d\n", sh->no );
			fprintf (F, "\"%s\";Name\n", sh->name ? sh->name : "");
			if (sh->display) fprintf (F, "\"%s\";Beschr\n", sh->display);
			fprintf (F, "\"%s\";Typ\n", shiptypes[0][sh->type]);
			u = shipowner (r, sh);
			if (u) fprintf (F, "%d;Kapitaen\n", u->no );
			if (u && cansee (f , r , u) == 2) fprintf (F, "%d;Partei\n", u->tarn_no );
			fprintf (F,"%d;Groesse\n",shipcost[sh->type] - sh->left);
			if (!(cansail(r, sh)) && (u->faction == f)) fprintf(F, "\"ueberladen\";Kapazitaet\n");
        }
		
		for (u = r->units; u; u = u->next)
			if ((visible = cansee (f, r, u)))
			{
				bu = (u->building) ? NULL : u;
				fprintf (F, "EINHEIT %d\n", u->no );
				fprintf (F, "\"%s\";Name\n", u->name ? u->name : "");
				
				if (u->building)
				{
					unit *hu;
					
					// fremde Einheit in eigener Burg, wenn nicht werden in der Burg nur je 1 Person angezeigt
					bu = NULL;
					if (u->building)
					{
						for(hu=r->units; hu; hu=hu->next)
						{
							if ((u->building) && (u->building == hu->building) && (hu->faction == f)) bu = hu;
						}
					} else
					{
						bu = u;
					}
					// wenn die Burg voll ist, also im Kampf keine Einheiten mehr sch�tzt, kann sie auch
					// keine Einheiten verstecken
					if (u->building)
					{
						u->building->voll -= u->number;
						if (u->building->voll < 0) bu = u;
					}
				}
				
				if (visible == 2)
				{
					if (f->no == u->faction->no)
					{
						fprintf (F, "%d;Partei\n", u->faction->no);
						if (u->tarn_no != f->no)
						{
							fprintf(F, "%d;Verkleidet\n", u->tarn_no);
						}
					} else
					{
						if (u->tarn_no)
						{
							fprintf (F, "%d;Partei\n", u->tarn_no );
						} else
						{
							fprintf (F, "-1;Partei\n");
						}
					}
				} else
				{
					fprintf (F, "-1;Partei\n");	// sonst Partei getarnt
				}

				if ((u->tarn_no != u->faction->no) && (u->faction == f)) fprintf (F,"%d;Verkleidung\n",u->tarn_no);
				fprintf (F, "%d;Anzahl\n", (!bu) ? 1 : u->number);
				if (u->prefix) fprintf (F, "\"%s\";typprefix\n",u->prefix);
				
				if (u->faction->no != 0)
				{
					switch (u->type)
					{
						// Aussehen
						case U_ILLUSION:
						case U_GUARDS:		if (u->number != 1) fprintf(F,"\"Menschen\";Typ\n"); else fprintf(F,"\"Mensch\";Typ\n");			break;
						case U_MAN:			fprintf (F, "\"%s\";Typ\n" , racenames[u->raceTT][1]);												break;
						case U_UNDEAD:		if (u->number != 1) fprintf(F,"\"Untote\";Typ\n"); else fprintf(F,"\"Untoter\";Typ\n");				break;
						case U_FIREDRAGON:	if (u->number != 1) fprintf(F,"\"Feuerdrachen\";Typ\n"); else fprintf(F,"\"Feuerdrachen\";Typ\n");	break;
						case U_DRAGON:		if (u->number != 1) fprintf(F,"\"Drachen\";Typ\n"); else fprintf(F,"\"Drachen\";Typ\n");			break;
						case U_WYRM:		if (u->number != 1) fprintf(F,"\"Wyrme\";Typ\n"); else fprintf(F,"\"Wyrm\";Typ\n");					break;
						default:			fprintf(F,"\"Fehler: unbekanntes Monster\";Typ\n");													break;
					} // switch u->type
				} else // Monster
				{
					switch (u->type)
					{
						// Aussehen
					case U_ILLUSION:
					case U_GUARDS:
					case U_MAN:			if (u->number != 1) fprintf(F,"\"Menschen\";Typ\n"); else fprintf(F,"\"Mensch\";Typ\n");	break;
					case U_UNDEAD:		if (u->number != 1) fprintf(F,"\"Untote\";Typ\n"); else fprintf(F,"\"Untoter\";Typ\n");		break;
					case U_FIREDRAGON:	if (u->number != 1) fprintf(F,"\"Feuerdrachen\";Typ\n"); else fprintf(F,"\"Feuerdrachen\";Typ\n");	break;
					case U_DRAGON:		if (u->number != 1) fprintf(F,"\"Drachen\";Typ\n"); else fprintf(F,"\"Drachen\";Typ\n");	break;
					case U_WYRM:		if (u->number != 1) fprintf(F,"\"Wyrme\";Typ\n"); else fprintf(F,"\"Wyrm\";Typ\n");		break;
					default:			fprintf(F,"\"Fehler: unbekanntes Monster\";Typ\n");									break;
					} // switch u->type
				} // u->faction->no != 0

				if (u->display) fprintf (F, "\"%s\";Beschr\n", u->display);
				if (u->guard) fprintf (F, "%d;bewacht\n", 57);
				if (u->building) fprintf (F ,"%d;Burg\n" , u->building->no );
				if (u->ship) fprintf (F , "%d;Schiff\n" , u->ship->no );
								
				if (u->faction == f)
				{
					if (u->status == ST_AVOID)
					{
						fprintf (F , "4;Kampfstatus\n");
						fprintf (F , "\"k�mpft nicht\";Kampfposition\n");
					} else 
					{
						if (u->status == ST_BEHIND)
						{
							fprintf (F , "2;Kampfstatus\n");
							fprintf (F , "\"k�mpft hinten\";Kampfposition\n");
						} else
						{
							fprintf (F , "\"k�mpft vorne\";Kampfposition\n");
						}
					} // Kampfstatus
					if (u->tarnung && effskill(u,SK_STEALTH)) fprintf(F,"%d;Tarnung\n",effskill(u,SK_STEALTH));
					if (u->faction->race == R_DEMON) fprintf (F, "\"%s\";wahrerTyp\n" , racenames[u->faction->race][1]);
					fprintf(F, "\"%s\";hp\n", getLebenszustand(u));
					fputs ("COMMANDS\n", F);
					if (u->lastorder) fprintf (F , "\"%s\"\n" , u->lastorder);
					if ((u->thisorder) && (!u->lastorder)) fprintf (F , "\"%s\"\n" , u->thisorder);
					if (u->thisorder2) fprintf (F , "\"%s\"\n" , u->thisorder2);
					if (u->comments)
					{
						tempstr = u->comments;
						while (tempstr)
						{
							fprintf (F , "\"%s\"\n" , tempstr->s);
							tempstr = tempstr->next;
						};
					};
					
					for (d = 0; d != MAXSKILLS; d++)
						if (u->skills[d] != 0)
						{
							if (head == 0)
							{
								fputs ("TALENTE\n", F);
								head = 1;
							};
							
							fprintf (F, "%d %d;%s\n", u->skills[d], effskill (u, d), skillnames[d]);
						};
						head = 0;
						
						for (d = 0; d != MAXSPELLS; d++)
						{
							if (u->spells[d] != 0)
							{
								if (head == 0)
								{
									fputs ("SPRUECHE\n", F);
									head = 1;
								};
								fprintf (F, "\"%s\"\n" , strings[spellnames[d]] [f->language]);
							};
						}
						head = 0;

				} // f->faction == f
				
				if (visible)
				{
					// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					// diese Zeile erzeugt immer einen GEGENSTAENDE-Block !!!!
					// dieser ist ggf. leer
					fputs ("GEGENSTAENDE\n" , F);
					// ist aber im Moment einfacher, da brauch ich mir jetzt nicht die Pladde machen
					// wie ich das mit den Kr�utern und Tr�nken einbaue
					// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					
					for (d = 0; d != MAXITEMS; d++)
						if ( u->items[d] != 0 )
						{
							// wurde verlegt, siehe oben
							// if (head == 0)
							//  {
							//    fputs ("GEGENSTAENDE\n" , F);
							//    head = 1;
							//  };
							if (d != I_SILVER)
							{
								int i = u->items[d];
								
								if ((u->building) && (!bu) && (u->faction != f)) i = 1;
								
								fprintf (F, "%d;%s\n", i, strings[itemnames[0][d]][0]);
							} else
							{
								if (f != u->faction)
								{
									if (u->items[d] < 500 * u->number) fprintf(F,"1;Silberbeutel\n"); else fprintf(F,"1;Schatzkiste\n");
								} else
								{
									fprintf(F,"%d;Silber\n",u->items[d]);
								}
							}
							
						};
						
						// Kr�uter
						head = 0;
						if (u->faction != f)
						{
							for(d=0; d<MAX_KRAEUTER; d++) if (!head && u->kraeuter[d]) { head=1; fprintf(F,"1;Kr�uterbeutel\n"); }
						} else
						{
							for(d=0; d<MAX_KRAEUTER; d++)
							{
								if (u->kraeuter[d]) fprintf(F,"%d;%s\n",u->kraeuter[d],kraeuter[d]);
							}
						}
						
						// Traenke & Tinkturen
						head = 0;
						if (u->faction != f)
						{
							for(d=0; d<MAX_TRAENKE; d++) if (!head && u->traenke[d]) { head=1; fprintf(F,"1;Tinkturen\n"); }
						} else
						{
							for(d=0; d<MAX_TRAENKE; d++)
							{
								if (u->traenke[d]) fprintf(F,"%d;%s\n",u->traenke[d],traenke[d]);
							}
						}
						
				};
				head = 0;
        }		  
    }

	// Regionen zeichnen durch die Schiffe gereist sind
	for(r=regions; r; r=r->next)
	{
		for(u = r->units; u; u = u->next)
		{
			int i;

			if ((u->ship) && (u->faction == f))
			{
				sh = u->ship;
				for(i=0; i<MAXSPEED; i++)
				{
					if (sh->rv[i] != NULL) 
					{
						if (!sh->rv[i]->gotit)	// wenn sie nicht bisher gezeichnet wurde
						{
							region *hr = sh->rv[i];

							hr->gotit = 1;				// Region wird/wurde gezeichnet
							fprintf (F, "REGION %d %d %d\n" , hr->x-f->xorigin , hr->y-f->yorigin, hr->z);
							if (hr->terrain != T_OCEAN) fprintf (F, "\"%s\"%s", hr->name , ";Name\n");
							fprintf (F, "\"%s\";Terrain\n" , strings[terrainnames[mainterrain (hr)]][0]);
						}
					}
				}
			} // Einheiten
		} // Schiffe in den Regionen
	} // alle Regionen
	
	// jetzt noch alle angrenzenden Region "zeichen"
	cr_count = 0;
    for (r = regions; r; r = r->next)
    {
		if (r->gotit == 1)
		{
			for (d = 0; d != MAXDIRECTIONS; d++)
			{
				int rx,ry;		// zu suchende Region
				region* hr;		// zu suchende Region
				
				rx = r->x + delta_x[d];		// beginnend mit NW, dann im Uhrzeigersinn
				ry = r->y + delta_y[d];
				welt = r->z;
				hr = findregion(rx,ry,welt);
				
				if (hr != NULL) // dann ist es keine Chaos Region
				{
					if (hr->gotit == 0)	// wurde noch nicht "gezeichnet"
					{
						// Region muss noch gezeichnet werden
						fprintf (F, "REGION %d %d %d\n" , hr->x-f->xorigin , hr->y-f->yorigin, welt);
						if (hr->terrain != T_OCEAN) fprintf (F, "\"%s\"%s", hr->name , ";Name\n");
						fprintf (F, "\"%s\";Terrain\n" , strings[terrainnames[mainterrain (hr)]][0]);
						hr->gotit = 2;	// Region wurde gezeichnet, damit aber an angrenzenden Regionen
										// keine weiteren angrenzenden Regionen gezeichnet werden, wird
										// diese Region mit 2 gekennzeichnet !!
						// Strassen einzeichnen
						for(i = 0; i<6; i++)
						{
							// Konvertierung der Richtungen von Fantasya nach Magellan
							int roadConvert[MAXDIRECTIONS] = { D_NORTHWEST, D_NORTHEAST, D_EAST, D_SOUTHEAST, D_SOUTHWEST, D_WEST };
							if (hr->road[roadConvert[i]])
							{
								fprintf(F,"GRENZE %d\n",i);
								fprintf(F,"\"Strasse\";typ\n");
								fprintf(F,"%d;richtung\n", i);
								fprintf(F,"%d;prozent\n",100 * hr->road[roadConvert[i]] / roadreq[hr->terrain]);
							}
						}
					} else
					{
						// Region wurde bereits gezeichnet
					}
				} else
				{
				/*
				* diese Region ist Chaos! Deshalb erstmal zwischen speichern, da eine Chaos-Region
				* noch keine eigene Struktur hat, Programm w�rde also abst�rzen
					*/
					cr[cr_count][0] = rx;
					cr[cr_count][1] = ry;
					cr[cr_count][2] = welt;
					cr_count++;
					if (cr_count == 5000) 
					{
						printf("\n -+*+-   zuviele Chaos-Regionen   -+*+-\n\n");
						exit(EXIT_FAILURE);
					}
				} // hf != NULL
			}	// MAXDIRECTIONS
		}	// r->gotit = 1
	} // alle Region
	
	// nur noch Chaos-Regionen zeichnen
	for(d = 0;d < cr_count; d++)
	{
		fprintf (F, "REGION %d %d %d\n" , cr[d][0]-f->xorigin , cr[d][1]-f->yorigin, cr[d][2]);
		fprintf (F, "\"Chaos\";Terrain\n");
	}
	
    fprintf (F, "MESSAGETYPE 1\n");
    fprintf (F, "\"\\\"$rendered\\\"\";text\n");
	
	fprintf(F,"MESSAGETYPE 2001\n");
	fprintf(F,"\"\\\"$rendered\\\"\";text\n");
	fprintf(F,"\"Fehler\";section\n");
	
	fprintf(F,"MESSAGETYPE 2002\n");
	fprintf(F,"\"\\\"$rendered\\\"\";text\n");
	fprintf(F,"\"Warnungen und Hinweise\";section\n");
	
	fprintf(F,"MESSAGETYPE 2003\n");
	fprintf(F,"\"\\\"$rendered\\\"\";text\n");
	fprintf(F,"\"Allgemeine Meldungen\";section\n");
	
	fprintf(F,"MESSAGETYPE 2004\n");
	fprintf(F,"\"\\\"$rendered\\\"\";text\n");
	fprintf(F,"\"K�mpfe\";section\n");
	
	fprintf(F,"MESSAGETYPE 2005\n");
	fprintf(F,"\"\\\"$rendered\\\"\";text\n");
	fprintf(F,"\"Ereignisse\";section\n");
	
	fprintf(F,"MESSAGETYPE 2006\n");
	fprintf(F,"\"\\\"$rendered\\\"\";text\n");
	fprintf(F,"\"Unterhaltung und Steuern\";section\n");
	
	fprintf(F,"MESSAGETYPE 2007\n");
	fprintf(F,"\"\\\"$rendered\\\"\";text\n");
	fprintf(F,"\"Handel\";section\n");
	
	fprintf(F,"MESSAGETYPE 2008\n");
	fprintf(F,"\"\\\"$rendered\\\"\";text\n");
	fprintf(F,"\"Produktion\";section\n");
	
	fprintf(F,"MESSAGETYPE 2009\n");
	fprintf(F,"\"\\\"$rendered\\\"\";text\n");
	fprintf(F,"\"Bewegungen\";section\n");
	
	fprintf(F,"MESSAGETYPE 2010\n");
	fprintf(F,"\"\\\"$rendered\\\"\";text\n");
	fprintf(F,"\"Alchemie und Kr�uterkunde\";section\n");
	
	fprintf(F,"MESSAGETYPE 2011\n");
	fprintf(F,"\"\\\"$rendered\\\"\";text\n");
	fprintf(F,"\"Echsenmenschen Infos\";section\n");
}


void writecrmap (void)
{
/*	int d, wage, rmoney ,n;
	faction *f = findfaction(0);
	region *r;
	building *b;
	ship *sh;
	unit *u;
	int facinf [maxXYC];
	int xy [maxXYC] [2]; // Muss eventuell vergr�ssert oder dynamisch geregelt werden.
	int xyc = 0;
	int head = 0;
*/
	return;
}

void
report (faction * f)
{
	int i;
	int anyunits;
	region *r;
	building *b;
	ship *sh;
	unit *u;
	
	int titelzeile = 0;	// wenn 1 dann 'Adressenliste:' geschrieben
	
	faction *hf;	// Hilfsfaction um damit alle gotit zu l�schen (wird ben�tigt f�r die Adressliste)
	
	int wants_text;
	int wants_cr;
	int wants_statistik;
	int wants_zugvorlage;
	int wants_zeitung;
	int wants_emails;
	int wants_zip;
	int wants_bewegung;
	int wants_verschiedenes;
	int wants_einkommen;
	int wants_handel;
	int wants_produktion;
	int wants_gulrak;
	int wants_unterweltcr;

	i = (int) pow (2, x8_TEXT);				wants_text			= (f->options & i);
	i = (int) pow (2, x8_CR);				wants_cr			= (f->options & i);
	i = (int) pow (2, x8_STATISTIK);		wants_statistik		= (f->options & i);
	i = (int) pow (2, x8_ZUGVORLAGE);		wants_zugvorlage	= (f->options & i);
	i = (int) pow (2, x8_ZEITUNG);			wants_zeitung		= (f->options & i);
	i = (int) pow (2, x8_EMAILS);			wants_emails		= (f->options & i);
	i = (int) pow (2, x8_ZIP);				wants_zip			= (f->options & i);
	i = (int) pow (2, x8_VERSCHIEDENES);	wants_verschiedenes = (f->options & i);
	i = (int) pow (2, x8_EINKOMMEN);		wants_einkommen		= (f->options & i);
	i = (int) pow (2, x8_HANDEL);			wants_handel		= (f->options & i);
	i = (int) pow (2, x8_PRODUKTION);		wants_produktion	= (f->options & i);
	i = (int) pow (2, x8_BEWEGUNG);			wants_bewegung		= (f->options & i);
	i = (int) pow (2, x8_GULRAK);			wants_gulrak		= (f->options & i);
	i = (int) pow (2, x8_CRTRENNUNG);		wants_unterweltcr	= (f->options & i);
	
	
	
	akt_f = f;	// aktuellen Spieler sichern f�r 'spunit()'
	
	for(hf = factions;hf;hf=hf->next) hf->gotit = 0; // sicher ist sicherer
	
	printf ("- Report fuer %s...\n", factionid (f));
	
	center (strings[ST_ATLANTIS_REPORT][f->language]);
	centred_paragraph (factionid (f));
	center (gamedate (f));
	strcpy (buf, translate (ST_ZAT, f->language, zat));
	center (buf);
	rnl();

	sprintf(buf,"Optionen:");
	if (wants_text)				scat(" Text");
	if (wants_cr)				scat(" Computer");
	if (wants_statistik)		scat(" Statistik");
	if (wants_zugvorlage)		scat(" Zugvorlage");
	if (wants_zip)				scat(" ZIP");
	if (wants_gulrak)			scat(" Gulrak");
	if (wants_unterweltcr)		scat(" CRTrennung");
	center(buf);

	sprintf(buf, "Report:");
	if (wants_verschiedenes)	scat(" Verschiedenes");
	if (wants_einkommen)		scat(" Einkommen");
	if (wants_handel)			scat(" Handel");
	if (wants_produktion)		scat(" Produktion");
	if (wants_bewegung)			scat(" Bewegung");
	center(buf);

	rnl();

	if (f->race == R_HUMAN)
	{
		sprintf(buf, "Dein Volk hat %d Personen und %d Immigranten.", f->anzahl, f->gaeste);
		center(buf);
		sprintf(buf, "Du kannst noch %d aufnehmen.", gaeste(f->anzahl));
		center(buf);
		rnl();
	}

	centred_title_strlist (strings[ST_MISTAKES][f->language], f->mistakes);
	centred_title_strlist (strings[ST_WARNINGS][f->language], f->warnings);
	
	centred_title_strlist (strings[ST_MESSAGES][f->language], f->messages);
	centred_title_strlist (strings[ST_BATTLES][f->language], f->battles);
	
	//if (wants_debug) /* Debug ist nicht sprachunabhaengig! */
	//  {
	//    centred_title_strlist ("Debug", f->debug);
	//    printf ("  %s hat DEBUG gesetzt!\n", factionid (f));
	//  }
	
	if (wants_verschiedenes)centred_title_strlist (strings[ST_MISCELLANEOUS][f->language], f->events);	// Ereignisse
	if (wants_einkommen)	centred_title_strlist (strings[ST_INCOME][f->language], f->income);			// Einkommen
	if (wants_handel)		centred_title_strlist (strings[ST_COMMERCE][f->language], f->commerce);		// Handel
	if (wants_produktion)	centred_title_strlist (strings[ST_PRODUCTION][f->language], f->production);	// Produktion
	if (wants_bewegung)		centred_title_strlist (strings[ST_MOVEMENTS][f->language], f->movement);	// Bewegungen
	centred_title_strlist ("Alchemie und Kraeuterkunde", f->alchemie);
	centred_title_strlist ("Echsennews", f->echsennews);
	
	for (i = 0; i != MAXSPELLS; i++)
		if (f->showdata[i])
			break;
		
		if (i != MAXSPELLS)
		{
			rnl ();
			center (strings[ST_NEW_SPELLS][f->language]);
			
			for (i = 0; i != MAXSPELLS; i++)
				if (f->showdata[i])
				{
					rnl ();
					center (strings[spellnames[i]][f->language]);
					strcpy (buf, translate (ST_LEVEL, f->language, spelllevel[i]));
					center (buf);
					rnl ();
					
					rparagraph (strings[spelldata[i]][f->language], 0, 0);
				}
		}
		
		rnl ();
		center (strings[ST_STATE_OF_AFFAIRS][f->language]);
		
		centred_title_strlist (strings[ST_STATUS][f->language], f->status);
		
		allies (f);
		
		anyunits = 0;
		
		for (r = regions; r; r = r->next)
		{
			/* nur report, falls es units gibt */
			
			for (u = r->units; u; u = u->next) if (u->faction == f) break;
			if (!u) continue;
			
			anyunits = 1;
			
			/* Debug */
			
			//      if (wants_debug && r->debug)
			//	{
			//	  rnl ();                   /* leerzeile */
			//	  center ("------------------------------------------------------------------------");
			//	  titled_strlist ("Debug", r->debug);
			//	}
			
			/* Beschreibung */
			
			rnl ();                   /* leerzeile */
			center ("-----------------------------------------------------------------------");
			rnl ();                   /* leerzeile */
			describe (r, f);
			
			/* Preise nur auf dem Festland */
			
			if (r->terrain != T_OCEAN)
			{
				rnl ();               /* leerzeile */
				prices (r, f);
			}
			
			/* Bewachen */
			
			guards (r);
			
			/* Statistik */
			
			if (wants_statistik) statistics (r, f);
			
			// H�hle

			if (r->hoehle)
			{
				hoehle *h;
				
				h = findhoehle(r->hoehle);

				sprintf(buf," "); rparagraph (buf, 4, 0);	// irgendwie habe ich hier einen Bug ausgebessert

				sprintf(buf,"%s (%s), Hoehle, %s", h->name, tobase36(h->nummer), h->beschr);
				rparagraph (buf, 4, 0);
			}

			/* Burgen und ihre Einheiten */
			
			for (b = r->buildings; b; b = b->next)
			{
				rnl ();               /* neue leerzeile */
				
				sprintf (buf, "%s, %s, %s",buildingid (b), translate (ST_SIZE, f->language, b->size), buildingtype (b));
				b->voll = b->size;
				
				if (b->besieged) scat (strings[ST_BESIEGED][f->language]);
				
				i = 0;
				if (b->display)
				{
					scat ("; ");
					scat (b->display);
					
					i = b->display[strlen (b->display) - 1];
				}
				if (i != '!' && i != '?' && i != '.') scat (".");
				
				rparagraph (buf, 4, 0);
				
				/* Ich vertraue nicht darauf, dass die jeweiligen Kommando
				Inhaber wirklich weiter vorne in der Liste stehen.
				Deswegen werden sie hier explizit wieder gesucht und an
				die erste Stelle gesetzt.  Es waere interessant, woher
				diese Fehler herruehren.  */
				
				for (u = r->units; u; u = u->next)
					if (u->building == b && u->owner)
					{
						rpunit (f, r, u, 8);
						break;
					}
					
					for (u = r->units; u; u = u->next)
						if (u->building == b && !u->owner)
							rpunit (f, r, u, 8);
			}
			
			/* Schiffe und ihre Einheiten */
			
			for (sh = r->ships; sh; sh = sh->next)
			{
				rnl ();               /* neue leerzeile */
				
				sprintf (buf, "%s, %s", shipid (sh), shiptypes[0][sh->type]);
				if (sh->left)
				{
					if (r->terrain == T_OCEAN)
					{
						scat (", ");
						scat (translate (ST_DAMAGED, f->language, 100 * sh->left / shipcost[sh->type]));
					}
					else
					{
						scat (", ");
						scat (translate (ST_INCOMPLETE, f->language, 100 * (shipcost[sh->type] - sh->left) / shipcost[sh->type]));
					}
				}
				else
				{
					scat (", ");
					scat (translate (ST_CAPACITY, f->language, capacity (r, sh), shipcapacity[sh->type]));
				}
				
				i = 0;
				if (sh->display)
				{
					scat ("; ");
					scat (sh->display);
					
					i = sh->display[strlen (sh->display) - 1];
				}
				if (i != '!' && i != '?' && i != '.')
					scat (".");
				
				rparagraph (buf, 4, 0);
				
				/* Hier gilt der gleiche Kommentar wie oben bei den Burgen.  */
				
				for (u = r->units; u; u = u->next)
					if (u->ship == sh && u->owner)
					{
						rpunit (f, r, u, 8);
						break;
					}
					
					for (u = r->units; u; u = u->next)
						if (u->ship == sh && !u->owner)
							rpunit (f, r, u, 8);
			}
			
			/* Restliche Einheiten */
			for (u = r->units; u; u = u->next)
				if (!u->building && !u->ship && cansee (f, r, u))
					rpunit (f, r, u, 4);
    }
	
	/*
	   * Ausgabe einer Adressenliste
	   */
	   for(hf=factions;hf;hf=hf->next)
	   {
		   if (hf->gotit == 1)
		   {
			   // hf->gotit = 0;
			   if (titelzeile != 1)
			   {
				   titelzeile = 1;
				   // rnl ();                   /* leerzeile */
				   fprintf(F,"\n------------------------------------------------------------------------\n");
				   fprintf(F,"\n Adressenliste:\n\n");
				   //rnl ();                   /* leerzeile */
			   } // titelzeile
			   if (netaddress(hf->addr) == 1)
			   {
				   fprintf(F," [%4s] '%s' %s\n",tobase36(hf->no),hf->name,buf);
				   // rparagraph (buf, 0, 0);
			   } else
			   {
				   fprintf(F," [%4s] '%s' keine e-Mail\n",tobase36(hf->no),hf->name);
				   // rparagraph (buf, 0, 0);
			   }
		   }
	   }
	   fprintf(F,"\n");
	   
	   if (f->no)
	   {
		   if (!anyunits)
		   {
			   rnl ();
			   rparagraph (strings[ST_NO_MORE_UNITS][f->language], 0, 0);
		   }
		   fflush(F);
		   fclose (F);
		   sprintf (buf, "reports/%s.zr", tobase36(f->no) );
		   if (cfopen (buf, "wt"))
		   {
		   /*
		   * als erstes die alte Datei schlie�en, da sie vor Aufruf ge�ffnet wird
		   * dann wird die Datei f�r Zugvorlage ge�ffnet, diese wird wiederum in
		   * der Funktion geschlossen, die den Normalreport �ffnet !!
			   */
			   order_template (f);
			   fflush(F);
		   }
	   }
}

/* ------------------------------------------------------------- */

FILE *BAT;

/* ------------------------------------------------------------- */

int
netaddress (char *s)
{
/* Die erste email Adresse wird verwendet.  Simulierte regexp:
[-._@0-9a-zA-Z]*@[-._@0-9a-zA-Z]* -- es ist moeglich nach der
adresse noch info - zB. den vollen Namen oder die Tel Nr
anzufuegen: "alex@zool.unizh.ch - Alexander Schroeder, Tel 01 /
	313 13 72" ist OK.  */
	int i, j;
	char *c;
	
	if (!s)
		return 0;
	
	c = strchr (s, '@');
	if (!c)
		return 0;
	
	/* Setzte buf auf die Netadresse!  */
	i = c - s;
	j = 0;
	
	/* Finde Start der Adresse.  Am Ende zeigt i auf den ersten Char,
	der *nicht* mehr zur Adresse geh�rt.  */
	while (i >= 0 && s[i] &&
		(isalnum (s[i]) ||
		s[i] == '@' ||
		s[i] == '-' ||
		s[i] == '.' ||
		s[i] == '_'))
		i--;
	
		/* Kopiere Adresse -- zuerst i auf Start der Adresse zeigen lassen.
		Falls s mit der email Adresse began, war i -1, nun wird i++
	gemacht, so dass ab s[0] kopiert wird -- was richtig ist.  */
	i++;
	while (s[i] &&
		(isalnum (s[i]) ||
		s[i] == '@' ||
		s[i] == '-' ||
		s[i] == '.' ||
		s[i] == '_'))
		buf[j++] = s[i++];
	
	/* beende adresse */
	buf[j] = 0;
	
	/* test der adresse, weitere koennen folgen: */
	if (!strchr (buf, '@'))
		return 0;
	
	return 1;
}

/* ------------------------------------------------------------- */

int net_report_count;

/* ------------------------------------------------------------- */

void
openbatch (char *dir)
{
	faction *f;
	int i;
/* Falls mind. ein Spieler mit email Adresse gefunden wird, schreibe
den header des batch files. "if (BAT)" kann verwendet werden, um
zu pruefen ob netspieler vorhanden sind und ins mailit batch
geschrieben werden darf. Mit dem batch werden auch alle Zeitungen
	und Kommentare verschickt. */
	sprintf (buf, "mailit.ini");
	if (!(BAT = fopen (buf, "w")))
		puts ("Die Datei 'mailit.ini' konnte nicht erzeugt werden!");
	else
	{
		fprintf(BAT, "[Fantasya]\n");
		fprintf(BAT, "Parteien=%d\n", listlen(factions) - 1);	// Monster werden nicht mit gez�hlt
		fprintf(BAT, "Runde=%d\n", turn);
		for(f = factions, i = 0; f; f = f->next, i++) fprintf(BAT, "ParteiNr_%d=%s\n", i, tobase36(f->no));
		fprintf(BAT,"\n");
	}
		
	net_report_count = 0;
}

void
closebatch (void)
{
	if (BAT)
    {
		fclose (BAT);
    }
}

/* ------------------------------------------------------------- */

void
repdir (char *dir)
{
	
/* macht REPORT subdirectory (falls nicht vorhanden) und loescht alle
	files darin (falls vorhanden) */
	
	int						dp;
	long					handle;
	struct _finddata_t		data_t;
	
	dp=_chdir(dir);
	
	if (dp != 0)
	{
		handle=_findfirst("*.*", &data_t);
		while (_findnext(handle, &data_t)==0)
			unlink(data_t.name);
		_findclose(handle);
	}
	else
		_mkdir(dir);
}

/* ------------------------------------------------------------- */


/* ------------------------------------------------------------- */

void
reports (void)
{
	faction *f;

	char pbuf[1000];
	char buf2[100];
	
	int gotit;
	int i;
	
	int wants_text;
	int wants_cr;
	int wants_statistik;
	int wants_zugvorlage;
	int wants_zeitung;
	int wants_emails;
	int wants_zip;
	int wants_gulrak;
	int wants_unterweltcr;
	
	/* macht REPORT subdirectory (falls nicht vorhanden) und loescht alle
	files darin (falls vorhanden) */
	
	/* oeffnet file BAT (mailit batch file) */
	
	openbatch ("reports");
	
	for (f = factions; f; f = f->next)
	{
		if (f->no == 0) continue;		// CR & NR & ZR f�r Monster �berspringen

		i = (int) pow (2, x8_TEXT);				wants_text			= (f->options & i);
		i = (int) pow (2, x8_CR);				wants_cr			= (f->options & i);
		i = (int) pow (2, x8_STATISTIK);		wants_statistik		= (f->options & i);
		i = (int) pow (2, x8_ZUGVORLAGE);		wants_zugvorlage	= (f->options & i);
		i = (int) pow (2, x8_ZEITUNG);			wants_zeitung		= (f->options & i);
		i = (int) pow (2, x8_EMAILS);			wants_emails		= (f->options & i);
		i = (int) pow (2, x8_ZIP);				wants_zip			= 1; // seit Version o.o1.6 / 1 immer via ZIP(f->options & i);
		i = (int) pow (2, x8_GULRAK);			wants_gulrak		= (f->options & i);
		i = (int) pow (2, x8_CRTRENNUNG);		wants_unterweltcr	= (f->options & i);
		
		gotit = 0;
		
		/* Schreiben der Reports ind /REPORTS: *.nr sind normale Reports, *.cr sind Computer Reports.
		Netreports werden im mailit file verschickt. Der mailit file
		wird mit BAT gebraucht. */
		
		// erzeugt werden immer ALLE Reporte
		sprintf (buf2, "reports/%s.nr", tobase36(f->no) );
		if (cfopen (buf2, "wt"))
		{
			report (f); // hier wird irgendwann die Zugvorlage noch erzeugt
			fclose (F);
			gotit = 1;
		}
		
		// jetzt wird der CR erzeugt ... ggf. muss er getrennt werden
		//if (wants_unterweltcr)
		//{
		//	unit *u;
		//	region *r;
		//	welt *w;

		//	// Init
		//	crtrennung = 1;
		//	for(w = welten; w; w = w->next) w->gotit = 0;

		//	// jetzt alle Welten raussuchen
		//	for(r = regions; r; r = r->next)
		//	{
		//		for(u = r->units; u; u = u->next)
		//		{
		//			if (u->faction == f)
		//			{
		//				w = GetWorld(r->z);
		//				w->gotit = 1;
		//			}
		//		}
		//	}

		//	// jetzt wird der CR der Welt erzeugt
		//	for(w = welten; w; w = w->next)
		//	{
		//		crwelt = w->ebene;
		//		sprintf (buf2, "reports/%s_%d.cr", tobase36(f->no), crwelt );
		//		if (cfopen (buf2, "wt"))
		//		{
		//			report_computer (f);
		//			fclose (F);
		//			gotit = 1;
		//		}
		//	}
		//} else
		//{
			crtrennung = 0;
			sprintf (buf2, "reports/%s.cr", tobase36(f->no) );
			if (cfopen (buf2, "wt"))
			{
				report_computer (f);
				fclose (F);
				gotit = 1;
			}
		//}

		//XMLReport(f);	// neuer XML Report

		/*
		 *	hier wird jetzt Gulrak's Vorlage gestartet ... dabei werden der CR und der ZR neu bearbeitet und einfach
		 *  in die alten Dateien gespeichert (wohl eher kopiert)
		 */
		//if (wants_gulrak)
		//{
		//	char sys[200];
		//	char options[] = ""; // "-b -g -hb -kl -l -m -n -si -sb -sk -st -t -up -us -uv";
		//	sprintf(sys, "vorlage\\vorlage %s -wall reports\\%s.cr > reports\\%s.zr", options, tobase36(f->no), tobase36(f->no));
		//	system(sys);
		//	sprintf(sys, "vorlage\\vorlage -cr -f -o reports\\alternativ.cr reports\\%s.cr", tobase36(f->no), tobase36(f->no));
		//	system(sys);
		//	sprintf(sys, "del reports\\%s.cr", tobase36(f->no));
		//	system(sys);
		//	sprintf(sys, "copy reports\\alternativ.cr reports\\%s.cr", tobase36(f->no));
		//	system(sys);
		//}

		/* Auch Kommentar und Zeitung muss per mail verschickt werden. buf enthaelt die adresse nach Aufruf von
		netaddress.  BAT sollte gesetzt sein, sonst gab es schon eine Fehlermeldung. */
		if (netaddress (f->addr) && BAT && (f->no != 0))
		{
			fprintf(BAT, "[Partei_%s]\n", tobase36(f->no));
			fprintf(BAT, "Name=%s\n", f->name);
			fprintf(BAT, "Mail=%s\n", buf);

			// l�schen des alten ZIP's
			sprintf(pbuf ,"del reports\\zip\\%s.zip", tobase36(f->no));
			puts(pbuf);
			system(pbuf);

			// *.nr
			if (wants_text)
			{
				if (wants_zip)
				{
					sprintf(pbuf, "zip reports\\zip\\%s.zip -j reports\\%s.nr", tobase36(f->no), tobase36(f->no));
					system(pbuf);
					fprintf(BAT, "Text=0\n");
				} else
				{
					fprintf(BAT, "Text=1\n");
				}
			};

			// *.zr
			if (wants_zugvorlage)
			{
				if (wants_zip)
				{
					sprintf(pbuf, "zip reports\\zip\\%s.zip -j reports\\%s.zr", tobase36(f->no), tobase36(f->no));
					system(pbuf);
					fprintf(BAT, "Zugvorlage=0\n");
				} else
				{
					fprintf(BAT, "Zugvorlage=1\n");
				}
			};

			// *.cr
			if (wants_cr)
			{
				if (wants_zip)
				{
					if (wants_unterweltcr)
					{
						welt *w;
						for(w = welten; w; w = w->next)
						{
							if (w->gotit)
							{
								sprintf(pbuf, "zip reports\\zip\\%s.zip -j reports\\%s_%d.cr", tobase36(f->no), tobase36(f->no), w->ebene);
								system(pbuf);
							}
						}
					} else
					{
						sprintf(pbuf, "zip reports\\zip\\%s.zip -j reports\\%s.cr", tobase36(f->no), tobase36(f->no));
						system(pbuf);
					}
					fprintf(BAT, "Computer=0\n");
				} else
				{
					fprintf(BAT, "Computer=1\n");
				}
			};

			if (wants_zip)
			{
				fprintf(BAT, "ZIP=1\n");
			} else
			{
				fprintf(BAT, "ZIP=0\n");
			}
			
		} // MailIt.ini
		
	} // alle Parteien
	
	/* schliesst BAT und verschickt Zeitungen und Kommentare */
	
	closebatch ();
	writecrmap ();

	generatePHPListe();
}

void generatePHPListe()
{
	// hier wird die Liste der emails und passw�rter f�r PHP erzeugt
	faction *f;
	FILE *out;

	puts("- generate PHP Liste");

	// Daten f�r MD5 "Verschl�sselung"
	out = fopen("playerliste.dat", "w");
	for(f = factions; f; f = f->next)
	{
		netaddress (f->addr);
		fprintf(out, "%s\n%s\n%s\n", tobase36(f->no), f->passw, buf);
	}
	fclose(out);
}
